﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace xsolla.Exception
{
    public class InvalidSignatureException : XsollaWebhookException
    {
        public InvalidSignatureException(string message, System.Exception innerException = null)
            : base(message, innerException)
        {

        }
        public override string ErrorCode => "INVALID_SIGNATURE";
        public override HttpStatusCode HttpStatusCode => HttpStatusCode.Unauthorized;
    }
}
