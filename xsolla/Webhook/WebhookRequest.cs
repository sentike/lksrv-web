﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;

namespace xsolla.Webhook
{

    public class Symphony
    {
        public Symphony()
        {
            Headers = (HttpRequestHeaders) FormatterServices.GetUninitializedObject(typeof(HttpRequestHeaders));
        }

        public string Content { get; set; }
        public string IpAddress { get; set; }
        public HttpRequestHeaders Headers { get; set; }
    }

    public class WebhookRequest
    {
        public string body { get; private set; }
        public HttpRequestHeaders headers { get; private set; }
        public string clientIp { get; private set; }

        internal static WebhookRequest fromGlobals()
        {
            var symphony = new Symphony();
            var request = HttpContext.Current.Request;

            var headers = request.Headers;
            foreach (var key in request.Headers.Keys.Cast<string>())
            {
                symphony.Headers.Add(key, headers.Get(key));
            }

            symphony.IpAddress = request.UserHostAddress;
            symphony.Content = new StreamReader(request.InputStream).ReadToEnd();
            return fromGlobals(symphony);
        }

        public static WebhookRequest fromGlobals(Symphony symphony)
        {
            return new WebhookRequest(symphony.Headers, symphony.Content, symphony.IpAddress);
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new System.Exception("Local IP Address Not Found!");
        }


        public WebhookRequest(HttpRequestHeaders headers, string body, string clientIp = null)
        {
            this.headers = headers;
            this.clientIp = clientIp;
            this.body = body;
        }

        public dynamic toArray()
        {
            return Json.Decode(this.body);
        }

    }
}
