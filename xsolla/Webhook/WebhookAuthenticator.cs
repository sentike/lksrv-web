﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using xsolla.Exception;

namespace xsolla.Webhook
{
    public class WebhookAuthenticator
    {
        private readonly static SHA1 Sha1 = SHA1.Create();
        private readonly static string[] xsollaSubnets = new[]
        {
            "159.255.220.240/28",
            "185.30.20.16/29",
            "185.30.21.0/24",
            "185.30.21.16/29"
        };

        protected string projectSecretKey;

        public WebhookAuthenticator(string projectSecretKey)
        {
            this.projectSecretKey = projectSecretKey;
        }

        public void authenticate(WebhookRequest webhookRequest, bool checkClientIp = true)
        {
            if (checkClientIp)
            {
                authenticateClientIp(webhookRequest.clientIp);
            }
            authenticateSignature(webhookRequest);
        }

        public void authenticateClientIp(string clientIp)
        {
            return;
            throw new NotImplementedException();
        }

        public void authenticateSignature(WebhookRequest webhookRequest)
        {
            var headers = webhookRequest.headers;
            if(headers.Contains("authorization") == false)
                throw new InvalidSignatureException("Authorization header not found in Xsolla webhook request. Please check troubleshooting section in README.md https://github.com/xsolla/xsolla-sdk-php#troubleshooting");

            var match = Regex.Match(headers.GetValues("authorization").First(), @"^Signature ([0-9a-f]{40})$");
            if (match.Groups.Count == 0 || match.Success == false)
            {
                throw new InvalidSignatureException($"Signature not found in \"Authorization\" header from Xsolla webhook request: {headers.GetValues("authorization").First()}");
            }
            var clientSignature = match.Groups[1].ToString();


            var serverSignature = SHA1Util.SHA1HashStringForUTF8String(string.Concat(webhookRequest.body, projectSecretKey));
            if (serverSignature != clientSignature)
            {
                throw new InvalidSignatureException($"Invalid Signature. Signature provided in \"Authorization\" header {clientSignature} | server: {serverSignature} does not match with expected ");
            }
        }

        public static class SHA1Util
        {
            /// <summary>
            /// Compute hash for string encoded as UTF8
            /// </summary>
            /// <param name="s">String to be hashed</param>
            /// <returns>40-character hex string</returns>
            public static string SHA1HashStringForUTF8String(string s)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(s);

                var sha1 = SHA1.Create();
                byte[] hashBytes = sha1.ComputeHash(bytes);

                return HexStringFromBytes(hashBytes);
            }

            /// <summary>
            /// Convert an array of bytes to a string of hex digits
            /// </summary>
            /// <param name="bytes">array of bytes</param>
            /// <returns>String of hex digits</returns>
            public static string HexStringFromBytes(byte[] bytes)
            {
                var sb = new StringBuilder();
                foreach (byte b in bytes)
                {
                    var hex = b.ToString("x2");
                    sb.Append(hex);
                }
                return sb.ToString();
            }
        }

    }
}
