﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace xsolla.Webhook
{
    public class CancelSubscriptionMessage : Message
    {
        public CancelSubscriptionMessage(object request) : base(request) { }
        public dynamic Subscription => request.subscription;
    }
}
