﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace xsolla.Webhook
{
    public abstract class Message
    {
        protected const string USER_VALIDATION = "user_validation";
        protected const string PAYMENT = "payment";
        protected const string REFUND = "refund";
        protected const string CREATE_SUBSCRIPTION = "create_subscription";
        protected const string CANCEL_SUBSCRIPTION = "cancel_subscription";
        protected const string USER_BALANCE = "user_balance_operation";
        protected readonly dynamic request;

        private static Dictionary<string, Type> classMap = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase)
        {
            {"USER_VALIDATION", typeof (UserValidationMessage)},
            {"USER_BALANCE", typeof (UserBalanceMessage)},
            {"CREATE_SUBSCRIPTION", typeof (CreateSubscriptionMessage)},
            {"CANCEL_SUBSCRIPTION", typeof (CancelSubscriptionMessage)},
            {"PAYMENT", typeof (PaymentMessage)},
            {"REFUND", typeof (RefundMessage)}
        };

        protected Message(dynamic request)
        {
            this.request = request;
        }

        public static Message fromArray(dynamic request)
        {
            if(request.notification_type == null)
                throw new ArgumentNullException(nameof(request.notification_type), "notification_type key not found in Xsolla webhook request");

            string notificationType = request.notification_type.ToString();
            if(classMap.ContainsKey(notificationType) == false)
                throw new KeyNotFoundException($"Unknown notification_type in Xsolla webhook request: {notificationType}");

            return Activator.CreateInstance(classMap[notificationType], request);
        }

        public string NotificationType => request.notification_type;

        public bool IsUserValidation => NotificationType == USER_VALIDATION;

        public bool IsPayment => NotificationType == PAYMENT;

        public bool IsRefund => NotificationType == REFUND;

        public dynamic User => request.user;

        public string UserId => User.id;

    }
}
