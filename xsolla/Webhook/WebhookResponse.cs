﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using xsolla.Exception;

namespace xsolla.Webhook
{
    class WebhookResponse
    {
        public HttpResponseMessage symfonyResponse { get; private set; }
        public string Body { get; private set; }

        public WebhookResponse(HttpStatusCode httpStatus = HttpStatusCode.OK, string body = "")
        {
            string contentType = string.IsNullOrEmpty(body) ? "text/plain" :"application/json";

            Body = body;

            symfonyResponse = new HttpResponseMessage(httpStatus)
            {
                Content = new StringContent(body, Encoding.ASCII, contentType),
                StatusCode = httpStatus               
            };

            symfonyResponse.Headers.Add("x-xsolla-sdk", Settings.Version);
        }

        public static WebhookResponse fromException(System.Exception exception)
        {
            if (exception is XsollaWebhookException)
            {
                var webhookException = (XsollaWebhookException) exception;
                return fromErrorCode(webhookException.ErrorCode, webhookException.Message, webhookException.HttpStatusCode);
            }
            return fromErrorCode("FATAL_ERROR", exception.Message);
        }

        public static WebhookResponse fromErrorCode(string xsollaErrorCode, string message = "",
            HttpStatusCode httpStatus = HttpStatusCode.InternalServerError)
        {
            dynamic body = new JObject();
            body.error = new JObject();
            body.error.code = xsollaErrorCode;
            body.error.message = message;
            return new WebhookResponse(httpStatus, body.ToString());
        }

    }
}
