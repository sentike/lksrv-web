﻿using System;
using System.Data.Entity.ModelConfiguration;
using Microsoft.AspNet.Identity.EntityFramework;

namespace VRS.Models.User
{
    public class ApplicationUserClaim : IdentityUserClaim<Guid>
    {
        //public new virtual Guid UserId
        //{
        //    get { return Guid.Parse(base.UserId); }
        //    set { base.UserId = value.ToString(); }
        //}

        public class Configuration : EntityTypeConfiguration<ApplicationUserClaim>
        {
            public Configuration()
            {
                HasKey(u => u.UserId);
                ToTable("vrs.UserClaims");
            }
        }
    }
}
