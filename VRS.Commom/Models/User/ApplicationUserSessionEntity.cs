﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace VRS.Models.User
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum ApplicationUserSessionSource
    {
        VRS,

        //=============================
        Loka,
        LokaWeb,
        //=============================
        End,
    }

    public class ApplicationUserSessionEntity
    {
        public Guid SessionId { get; set; }
        public Guid AccountId { get; set; }
        public virtual ApplicationUser AccountEntity { get; set; }

        public DateTime LogInDate { get; set; }
        public DateTime LogOutDate { get; set; }
        public string Address { get; set; }

        public IPAddress IpAddress
        {
            get { return IPAddress.Parse(Address); }
            set { Address = value.ToString(); }
        }

        public ApplicationUserSessionSource Source { get; set; }


        public ApplicationUserSessionEntity() { }

        public ApplicationUserSessionEntity(Guid sessionId, Guid accountId, string address, ApplicationUserSessionSource source)
        {
            SessionId = sessionId;
            AccountId = accountId;
            LogInDate = DateTime.UtcNow;
            LogOutDate= DateTime.UtcNow;
            Source = source;
            Address = address;
        }

        public class Configuration : EntityTypeConfiguration<ApplicationUserSessionEntity>
        {
            public Configuration()
            {
                HasKey(p => p.SessionId);
                ToTable("vrs.ApplicationUserSessionEntity");
                Property(p => p.Address).HasMaxLength(128);
                Ignore(p => p.IpAddress);
            }
        }
    }
}
