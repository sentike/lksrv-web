﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using VRS.Models.User;

namespace VRS.Infrastructure.Identity
{
    public static class ApplicationUserManagerHelper
    {
        public static async Task<IdentityResult> ConfirmEmaiCode(this ApplicationUserManager manager, Guid userId, string code)
        {
            if (manager.UserTokenProvider == null)
            {
                manager.UserTokenProvider = new EmailTokenProvider<ApplicationUser, Guid>();
            }

            var result = await manager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                var user = manager.Users.First(e => e.Id == userId);
                if (string.IsNullOrWhiteSpace(user.PasswordHash))
                {
                    var password = Guid.NewGuid().ToString("d").Substring(1, 8) + Guid.NewGuid().ToString("d").Substring(1, 8);
                    var result2 = await manager.ChangePasswordAsync(userId, String.Empty, password);
                    if (result2.Succeeded)
                    {
                        var sb = new StringBuilder(1024);
                        sb.AppendLine("Your account was successfully confirmed!");
                        sb.AppendLine("");
                        sb.AppendLine("Information to log in");
                        sb.AppendLine($"Email: {user.Email}");
                        sb.AppendLine($"Password: {password}");
                        sb.AppendLine("==============================");
                        sb.AppendLine("Our website: https://lokagame.com");
                        sb.AppendLine("Facebook: https://www.facebook.com/LOKAgame");
                        sb.AppendLine("Twitter: https://twitter.com/VRSProduction");
                        sb.AppendLine("VK: https://vk.com/lokagamevr");
                        sb.AppendLine("==============================");
                        sb.AppendLine("");
                        sb.AppendLine("The site is in development, and now it has no meaning.");
                        sb.AppendLine("If you have any problems or have any suggestions, you can leave them <a href=\"http://steamcommunity.com/app/488720/discussions/0/152393186486167122\">here</a>.");
                        await manager.SafeSendEmailAsync(userId, "Confirm account", sb.ToString());
                    }
                    else
                    {
                        var sb = new StringBuilder(1024);
                        sb.AppendLine("Your account was successfully confirmed!");
                        sb.AppendLine("==============================");
                        sb.AppendLine("Our website: https://lokagame.com");
                        sb.AppendLine("Facebook: https://www.facebook.com/LOKAgame");
                        sb.AppendLine("Twitter: https://twitter.com/VRSProduction");
                        sb.AppendLine("VK: https://vk.com/lokagamevr");
                        sb.AppendLine("==============================");
                        sb.AppendLine("");
                        sb.AppendLine("The site is in development, and now it has no meaning.");
                        sb.AppendLine("If you have any problems or have any suggestions, you can leave them <a href=\"http://steamcommunity.com/app/488720/discussions/0/152393186486167122\">here</a>.");
                        await manager.SafeSendEmailAsync(userId, "Confirm account", sb.ToString());
                    }
                }
                else
                {
                    var sb = new StringBuilder(1024);
                    sb.AppendLine("Your account was successfully confirmed!");
                    sb.AppendLine("==============================");
                    sb.AppendLine("Our website: https://lokagame.com");
                    sb.AppendLine("Facebook: https://www.facebook.com/LOKAgame");
                    sb.AppendLine("Twitter: https://twitter.com/VRSProduction");
                    sb.AppendLine("VK: https://vk.com/lokagamevr");
                    sb.AppendLine("==============================");
                    sb.AppendLine("");
                    sb.AppendLine("The site is in development, and now it has no meaning.");
                    sb.AppendLine("If you have any problems or have any suggestions, you can leave them <a href=\"http://steamcommunity.com/app/488720/discussions/0/152393186486167122\">here</a>.");
                    await manager.SafeSendEmailAsync(userId, "Confirm account", sb.ToString());
                }
            }
            else
            {
                await manager.SafeSendEmailAsync(userId, "Confirm account", $"Unable to verify account, errors: { string.Join(",", result.Errors)}");
            }
            return result;
        }

        public const string HtmlNewLine = "<br>";
        public static void SendEmailCode(this ApplicationUserManager manager, Guid userId)
        {
            if (manager.UserTokenProvider == null)
            {
                manager.UserTokenProvider = new EmailTokenProvider<ApplicationUser, Guid>();
            }
            Task.Run(async () =>
            {
                await manager.UpdateSecurityStampAsync(userId);
                string code = await manager.GenerateEmailConfirmationTokenAsync(userId);
                var callbackUrl = new Uri($"https://lokagame.com/Account/ConfirmEmail?UserId={userId}&code={code}");

                var sb = new StringBuilder(1024);
                sb.AppendLine($"{HtmlNewLine}Confirm your account by click <a href=\"{callbackUrl}\">here</a> or enter the code in the game: <b>{code}</b>");
                sb.AppendLine($"{HtmlNewLine}==============================");
                sb.AppendLine($"{HtmlNewLine}Our website: https://lokagame.com");
                sb.AppendLine($"{HtmlNewLine}Facebook: https://www.facebook.com/LOKAgame");
                sb.AppendLine($"{HtmlNewLine}Twitter: https://twitter.com/VRSProduction");
                sb.AppendLine($"{HtmlNewLine}VK: https://vk.com/lokagamevr");
                sb.AppendLine($"{HtmlNewLine}==============================");
                sb.AppendLine($"{HtmlNewLine}");
                sb.AppendLine($"{HtmlNewLine}The site is in development, and now it has no meaning.");
                sb.AppendLine($"{HtmlNewLine}If you have any problems or have any suggestions, you can leave them <a href=\"http://steamcommunity.com/app/488720/discussions/0/152393186486167122\">here</a>.");
                await manager.SafeSendEmailAsync(userId, "Confirm account", sb.ToString());
            }).Wait();
        }

        public static Task SafeSendEmailAsync(this ApplicationUserManager manager, Guid userId, string subject, string body)
        {
            if (manager.EmailService == null)
            {
                manager.EmailService = new EmailService();
            }

            return manager.SendEmailAsync(userId, subject, body);
        }

    }

    // Настройка диспетчера пользователей приложения. UserManager определяется в ASP.NET Identity и используется приложением.
    public class ApplicationUserManager : UserManager<ApplicationUser, Guid>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser, Guid> store)
            : base(store)
        {
        }

        public static ApplicationUserManager CreateForSteam(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new ApplicationUserStore(context.Get<ApplicationDbContext>()));
            return manager;
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new ApplicationUserStore(context.Get<ApplicationDbContext>()));
            // Настройка логики проверки имен пользователей
            manager.UserValidator = new UserValidator<ApplicationUser, Guid>(manager)
            {
                AllowOnlyAlphanumericUserNames = false, RequireUniqueEmail = true
            };

            // Настройка логики проверки паролей
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 3,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Настройка параметров блокировки по умолчанию
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Регистрация поставщиков двухфакторной проверки подлинности. Для получения кода проверки пользователя в данном приложении используется телефон и сообщения электронной почты
            // Здесь можно указать собственный поставщик и подключить его.
            manager.RegisterTwoFactorProvider("Код, полученный по телефону", new PhoneNumberTokenProvider<ApplicationUser, Guid>
            {
                MessageFormat = "Ваш код безопасности: {0}"
            });
            manager.RegisterTwoFactorProvider("Код из сообщения", new EmailTokenProvider<ApplicationUser, Guid>
            {
                Subject = "Код безопасности",
                BodyFormat = "Ваш код безопасности: {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();

            manager.UserTokenProvider = new EmailTokenProvider<ApplicationUser, Guid>();
            return manager;
        }
    }
}
