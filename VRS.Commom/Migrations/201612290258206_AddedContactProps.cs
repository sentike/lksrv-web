namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedContactProps : DbMigration
    {
        public override void Up()
        {
            AddColumn("vrs.Users", "SecretAnswer", c => c.String(maxLength: 16));
            AddColumn("vrs.Users", "EmailReserveValue", c => c.String(maxLength: 64));
            AddColumn("vrs.Users", "EmailSafeValue", c => c.String(maxLength: 64));
            AddColumn("vrs.Users", "EmailChangeDate", c => c.DateTime(nullable: false));
            AddColumn("vrs.Users", "PhoneChangeDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("vrs.Users", "PhoneChangeDate");
            DropColumn("vrs.Users", "EmailChangeDate");
            DropColumn("vrs.Users", "EmailSafeValue");
            DropColumn("vrs.Users", "EmailReserveValue");
            DropColumn("vrs.Users", "SecretAnswer");
        }
    }
}
