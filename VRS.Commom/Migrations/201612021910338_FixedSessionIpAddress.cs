namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedSessionIpAddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("vrs.ApplicationUserSessionEntity", "Address", c => c.String(maxLength: 16, storeType: "\"cidr\""));
            DropColumn("vrs.ApplicationUserSessionEntity", "Address_Address");
            DropColumn("vrs.ApplicationUserSessionEntity", "Address_ScopeId");
        }
        
        public override void Down()
        {
            AddColumn("vrs.ApplicationUserSessionEntity", "Address_ScopeId", c => c.Long(nullable: false));
            AddColumn("vrs.ApplicationUserSessionEntity", "Address_Address", c => c.Long(nullable: false));
            DropColumn("vrs.ApplicationUserSessionEntity", "Address");
        }
    }
}
