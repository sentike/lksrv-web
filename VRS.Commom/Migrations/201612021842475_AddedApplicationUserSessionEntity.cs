namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedApplicationUserSessionEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "vrs.ApplicationUserSessionEntity",
                c => new
                    {
                        SessionId = c.Guid(nullable: false),
                        AccountId = c.Guid(nullable: false),
                        LogInDate = c.DateTime(nullable: false),
                        LogOutDate = c.DateTime(nullable: false),
                        Address_Address = c.Long(nullable: false),
                        Address_ScopeId = c.Long(nullable: false),
                        Source = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SessionId)
                .ForeignKey("vrs.Users", t => t.AccountId, cascadeDelete: true)
                .Index(t => t.AccountId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("vrs.ApplicationUserSessionEntity", "AccountId", "vrs.Users");
            DropIndex("vrs.ApplicationUserSessionEntity", new[] { "AccountId" });
            DropTable("vrs.ApplicationUserSessionEntity");
        }
    }
}
