namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedInvoiceId : DbMigration
    {
        public override void Up()
        {
            AddColumn("payment.UserPaymentEntity", "InvoiceId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("payment.UserPaymentEntity", "InvoiceId");
        }
    }
}
