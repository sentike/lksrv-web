namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFirstInit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "vrs.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "vrs.UserRoles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("vrs.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("vrs.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "vrs.UserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("vrs.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "vrs.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Balance = c.Long(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "vrs.UserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("vrs.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("vrs.UserRoles", "UserId", "vrs.Users");
            DropForeignKey("vrs.UserLogins", "UserId", "vrs.Users");
            DropForeignKey("vrs.UserClaims", "UserId", "vrs.Users");
            DropForeignKey("vrs.UserRoles", "RoleId", "vrs.Roles");
            DropIndex("vrs.UserClaims", new[] { "UserId" });
            DropIndex("vrs.Users", "UserNameIndex");
            DropIndex("vrs.UserLogins", new[] { "UserId" });
            DropIndex("vrs.UserRoles", new[] { "RoleId" });
            DropIndex("vrs.UserRoles", new[] { "UserId" });
            DropIndex("vrs.Roles", "RoleNameIndex");
            DropTable("vrs.UserClaims");
            DropTable("vrs.Users");
            DropTable("vrs.UserLogins");
            DropTable("vrs.UserRoles");
            DropTable("vrs.Roles");
        }
    }
}
