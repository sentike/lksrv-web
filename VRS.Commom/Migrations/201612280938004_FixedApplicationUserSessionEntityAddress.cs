namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedApplicationUserSessionEntityAddress : DbMigration
    {
        public override void Up()
        {
            AlterColumn("vrs.ApplicationUserSessionEntity", "Address", c => c.String(maxLength: 128, storeType: "\"cidr\""));
        }
        
        public override void Down()
        {
            AlterColumn("vrs.ApplicationUserSessionEntity", "Address", c => c.String(maxLength: 16, storeType: "\"cidr\""));
        }
    }
}
