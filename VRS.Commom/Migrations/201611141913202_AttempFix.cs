namespace VRS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AttempFix : DbMigration
    {
        public override void Up()
        {
            DropIndex("vrs.Users", new[] { "PhoneNumber" });
            AlterColumn("vrs.Users", "PhoneNumber", c => c.String(maxLength: 20));
            CreateIndex("vrs.Users", "PhoneNumber", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("vrs.Users", new[] { "PhoneNumber" });
            AlterColumn("vrs.Users", "PhoneNumber", c => c.String());
            CreateIndex("vrs.Users", "PhoneNumber", unique: true);
        }
    }
}
