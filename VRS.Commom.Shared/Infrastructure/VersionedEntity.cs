﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRS.Infrastructure
{
    public interface IVersionedEntity
    {
        Guid RowVersion { get; set; }
    }
}
