﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace VRS.Infrastructure
{
    public static class DisposeHelper
    {
        public static void SafeDispose<T>(this T o) where T : IDisposable
        {
            try
            {
                if (o != null)
                {
                    using (o)
                    {
                        o.Dispose();
                    }
                }
            }
            catch (Exception e)
            {
                LoggerContainer.GlobalLogger.WriteExceptionMessage("SafeDispose", e);
            }
        }
    }

    public interface IAbstractDataRepository
    {
        int SaveChanges();
    }

    public abstract class AbstractDataRepository<TContext> : IDisposable, IAbstractDataRepository
    where TContext : DbContext, new()
    {
        public readonly TContext Context;
        public readonly bool AllowDispose;

        [SuppressMessage("ReSharper", "StaticMemberInGenericType")]
        public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        [SuppressMessage("ReSharper", "StaticMemberInGenericType")]
        public static readonly Logger DataBaseLogger = LogManager.GetLogger("DataBaseLogger");

        protected AbstractDataRepository(TContext context)
        {
            AllowDispose = false;
            Context = context;
        }

        protected AbstractDataRepository()
            : this(new TContext())
        {
            AllowDispose = true;
        }

        public void Dispose()
        {
            if (AllowDispose)
            {
                using (Context)
                {
                    SaveChanges();
                }
            }
        }

        public int SaveChanges()
        {
            try
            {
                return Context?.SaveChanges() ?? 0;
            }
            catch (Exception e)
            {
               LoggerContainer.DataBaseLogger.WriteExceptionMessage($"Failed SaveChanges in {typeof(TContext).FullName}", e);
            }
            return 0;
        }
    }

    public abstract class AbstractDataRepository<TContext,
    TEntity, TEntityKey> : AbstractDataRepository<TContext>
    where TContext : DbContext, new()
    where TEntity : class
    where TEntityKey : IComparable<TEntityKey>
    {
        //===================================================================================
        public abstract TEntity TakeEntity(TEntityKey key);
        public abstract bool ExistEntity(TEntityKey itemId);
        public abstract IQueryable<TEntity> TakeEntities(byte? category);

        //===================================================================================
        protected AbstractDataRepository(TContext context) : base(context) { }
        protected AbstractDataRepository() : base() { }
    }

    public abstract class AbstractDataRepository<TContext, 
        TEntity, TEntityKey,
        TProperty, TPropertyKey> : AbstractDataRepository<TContext, TEntity, TEntityKey>
        where TContext : DbContext, new()
        where TEntity : class
        where TProperty : class
        where TEntityKey : IComparable<TEntityKey>
        where TPropertyKey : IComparable<TPropertyKey>
    {
        public abstract TProperty TakeProperty(TEntityKey key, TPropertyKey sub);

        //===================================================================================
        protected AbstractDataRepository(TContext context): base(context){}
        protected AbstractDataRepository(): base(){}
    }
}
