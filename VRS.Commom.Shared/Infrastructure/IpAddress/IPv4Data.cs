﻿using System;
using System.Diagnostics.CodeAnalysis;


namespace VRS.Infrastructure.IpAddress
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    internal class IPv4Data
    {
        public long IpFrom { get; set; }
        public long IpTo { get; set; }
        public string Registry { get; set; }
        public long Assigned { get; set; }
        public string Iso3166TwoLetterCode { get; set; }
        public string Iso3166ThreeLetterCode { get; set; }
        public string Country { get; set; }
    }
}