﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Exceptions
{
    public class ObjectNotFoundException : OperationException
    {
        public ObjectNotFoundException(int responseCode, object responseData, string exception) : base(responseCode, responseData, exception)
        {
        }

        public ObjectNotFoundException(int responseCode, object responseData, string message, System.Exception exception) : base(responseCode, responseData, message, exception)
        {
        }

        public ObjectNotFoundException(int responseCode) : base(responseCode)
        {
        }

        public ObjectNotFoundException(int responseCode, string exception) : base(responseCode, exception)
        {
        }

        public ObjectNotFoundException(int responseCode, string message, System.Exception exception) : base(responseCode, message, exception)
        {
        }
    }
}
