﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;


namespace Loka.Common.Company.Task
{
    public class ScenarioTaskView
    {
        public int TaskId { get; set; }
        public int TargetAmount { get; set; }
        public byte EquipmentLevel { get; set; }
        public int PeriodOfExecutionInSeconds { get; set; }
    }

    public static class ViewHelper
    {
        private static readonly IMapper Mapper;
        static ViewHelper()
        {
            Mapper = new MapperConfiguration(c =>
            {
                c.CreateMap<ScenarioTaskEntity, ScenarioTaskView>();
            }).CreateMapper();
        }

        public static List<ScenarioTaskView> AsView(this List<ScenarioTaskEntity> entity)
        {
            return entity.Select(m => m.AsView()).ToList();
        }

        public static ScenarioTaskView AsView(this ScenarioTaskEntity entity)
        {
            return Mapper.Map<ScenarioTaskView>(entity);
        }
    }
}
