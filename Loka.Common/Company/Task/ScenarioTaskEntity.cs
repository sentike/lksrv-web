﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Achievements;
using Loka.Common.Company.Mission;

namespace Loka.Common.Company.Task
{
    public class ScenarioTaskEntity
    {
        public long TaskId { get; set; }
        public ScenarioTaskType TaskType { get; set; }
        
        //------------------------------------------------
        public short MissionId { get; set; }
        public virtual ScenarioMissionEntity MissionEntity { get; set; }

        //------------------------------------------------
        public AchievementTypeId TargetAction { get; set; }
        public int TargetAmount { get; set; }

        //------------------------------------------------
        public byte EquipmentLevel { get; set; }

        //------------------------------------------------
        public int PeriodOfExecutionInSeconds { get; set; }
        public TimeSpan PeriodOfExecution => TimeSpan.FromSeconds(PeriodOfExecutionInSeconds);

        public class Configuration : EntityTypeConfiguration<ScenarioTaskEntity>
        {
            public Configuration()
            {
                HasKey(p => p.TaskId).Property(p => p.TaskId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                ToTable("company.ScenarioTaskEntity");
            }
        }
    }
}
