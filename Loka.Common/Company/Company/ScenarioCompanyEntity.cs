﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Company.Mission;
using Loka.Common.Company.Task;

namespace Loka.Common.Company.Company
{
    public class ScenarioCompanyEntity
    {
        public ScenarioCompanyId CompanyId { get; set; }
        
        //------------------------------------------------
        public virtual List<ScenarioMissionEntity> MissionEntities { get; set; } = new List<ScenarioMissionEntity>(10);

        public class Configuration : EntityTypeConfiguration<ScenarioCompanyEntity>
        {
            public Configuration()
            {
                ToTable("company.ScenarioCompanyEntity");
                HasKey(p => p.CompanyId).Property(p => p.CompanyId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                HasMany(p => p.MissionEntities).WithRequired(p => p.CompanyEntity).HasForeignKey(p => p.CompanyId).WillCascadeOnDelete(true);
            }
        }

    }
}
