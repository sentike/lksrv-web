﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Company.Company
{
    public enum ScenarioCompanyId : long
    { 
        Main,
        Keep,
        End,
    }
}
