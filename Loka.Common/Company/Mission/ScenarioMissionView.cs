﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Loka.Common.Company.Company;
using Loka.Common.Company.Task;

namespace Loka.Common.Company.Mission
{
    public class ScenarioMissionView
    {
        public short MissionId { get; set; }
        public byte AbstractMissionId { get; set; }

        //------------------------------------------------
        public short NextMissionId { get; set; }
        public ScenarioCompanyId CompanyId { get; set; }
        
        //------------------------------------------------
        public List<ScenarioTaskView> TaskViews { get; set; } = new List<ScenarioTaskView>();
    }

    public static partial class ViewHelper
    {
        private static readonly IMapper Mapper;
        static ViewHelper()
        {
            Mapper = new MapperConfiguration(c =>
            {
                c.CreateMap<ScenarioMissionEntity, ScenarioMissionView>()
                    .ForMember(p => p.NextMissionId, m => m.MapFrom(r => r.NextMissionId ?? -1));
            }).CreateMapper();
        }

        public static List<ScenarioMissionView> AsView(this List<ScenarioMissionEntity> entity)
        {
            return entity.Select(m => m.AsView()).ToList();
        }

        public static ScenarioMissionView AsView(this ScenarioMissionEntity entity)
        {
            return Mapper.Map<ScenarioMissionView>(entity);
        }
    }

}
