﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Company.Company;
using Loka.Common.Company.Task;

namespace Loka.Common.Company.Mission
{
    public class ScenarioMissionEntity
    {
        public short MissionId { get; set; }
        public byte AbstractMissionId { get; set; }

        //------------------------------------------------
        public short? NextMissionId { get; set; }
        public virtual ScenarioMissionEntity NextMissionEntity { get; set; }
        
        //------------------------------------------------
        public ScenarioCompanyId CompanyId { get; set; }
        public virtual ScenarioCompanyEntity CompanyEntity { get; set; }

        //------------------------------------------------
        public virtual List<ScenarioTaskEntity> TaskEntities { get; set; } = new List<ScenarioTaskEntity>(8);

        public class Configuration : EntityTypeConfiguration<ScenarioMissionEntity>
        {
            public Configuration()
            {
                ToTable("company.ScenarioMissionEntity");
                HasKey(p => p.MissionId).Property(p => p.MissionId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                HasMany(p => p.TaskEntities).WithRequired(p => p.MissionEntity).HasForeignKey(p => p.MissionId).WillCascadeOnDelete(true);
                HasOptional(l => l.NextMissionEntity).WithMany().HasForeignKey(p => p.NextMissionId).WillCascadeOnDelete(true);

                Property(p => p.CompanyId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("CompanyId_AbstractMissionId", 0) { IsUnique = true }));
                Property(p => p.AbstractMissionId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("CompanyId_AbstractMissionId", 1) { IsUnique = true }));
            }
        }

    }
}
