﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Exceptions;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Store.Fraction;
using Loka.Common.Store.Item.Addon;
using Loka.Common.Store.Item.Armour;
using Loka.Common.Store.Item.Character;
using Loka.Common.Store.Item.Skin;

using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;
using Loka.Common.Store.Item;
using Loka.Common.Store.Item.Ammo;
using Loka.Common.Store.Item.Kit;
using Loka.Common.Store.Item.Weapon;
using Loka.Common.Store.Property;
using Loka.Common.Store.Service;
using Loka.Server.Infrastructure.DataRepository;
using VRS.Infrastructure;
using Loka.Common.Transaction;

namespace Loka.Common.Store.Abstract
{
    public interface IAbstractItemModel<TModelIdType> where TModelIdType : struct, IConvertible
    {
        TModelIdType ModelId { get; set; }
    }

    public interface IAbstractItemInterface
    {
	    short ModelId { get; set; }
        CategoryTypeId CategoryTypeId { get; set; }
        CharacterModelFlagId CharacterModelsFlag { get; set; }
        int Level { get; set; }
        FractionTypeId FractionId { get; set; }
        StoreItemCost Cost { get; set; }
    }

    public enum ItemMigrationAction
    {
        None,
        RemoveIfExist,
        CreateIfNotExist
    }

    //[Flags]
    [Flags]
    public enum ItemInstanceFlags
    {
        None,
        Hidded = 1,
        Subscribe = 2,
        Miner = 4,
        Storage = 8,
        IgnoreRequirements = 16,
        AllowSelling = 32,
    }

    public class ItemInstanceEntity : IAbstractItemInterface
	{
        protected static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public virtual PlayerInventoryItemEntity Buy(PlayerEntity buyer, PlayerEntity recipient, long amount = 1, bool ignorePrice = false, bool ignoreLevel = false)
        {
            return Buy(buyer, recipient, amount, out _, ignorePrice, ignoreLevel);
        }

        public virtual List<CharacterProfileTemplate> CharacterProfileTemplates { get; set; } = new List<CharacterProfileTemplate>(16);
        public virtual PlayerInventoryItemEntity Buy(PlayerEntity buyer, PlayerEntity recipient, long amount, out long bought, bool ignoreprice = false, bool ignoreLevel = false)
        {
            bought = 0;
            amount = Math.Max(amount, 1);
            if (ignoreLevel || IsMatchRequirements(buyer))
            {
                if (IsExistItem(recipient))
                {
                    if (IsSubscribe)
                    {
                        if (ignoreprice == false && SafeSubscribeCost.Amount <= 0)
                        {
                            throw new ArgumentNullException(nameof(SafeSubscribeCost), $"Невалидная стоимость {SafeSubscribeCost} продления подписки {this}");
                        }

                        bought = buyer.Count(SafeSubscribeCost);
                        bought = Math.Min(bought, amount);
                        var cost = SafeSubscribeCost * bought;
                        if (ignoreprice)
                        {
                            bought = amount;
                            cost.Amount = 0;
                        }

                        if (ignoreprice || bought > 0 && buyer.Pay(cost))
                        {
                            var item = ProcessOfSubscribe(recipient, bought);
                            if (item != null)
                            {
                                item.PlayerInstance = recipient;
                                item.ItemModelEntity = this;
                            }
                            return item;
                        }
                        else
                        {
                            throw new NotEnoughMoneyException(402, $"Not enough money for Subscribe {ModelIdName} | Cost: {cost} [SafeSubscribeCost: {SafeSubscribeCost}] [Amount: {bought} of {amount}] | {buyer.TakeCash(cost)}", buyer.TakeCash(cost), cost);
                        }
                    }
                    else
                    {
                        throw new NotMatchRequirementsException(702, "The subject does not support renewals");
                    }
                }
                else
                {
                    if (ignoreprice == false && Cost.Amount <= 0)
                    {
                        throw new ArgumentNullException(nameof(Cost), $"Невалидная стоимость {SafeSubscribeCost} покупки {this}");
                    }

                    bought = IsSubscribe ? buyer.Count(Cost) : 1;
                    bought = Math.Min(bought, amount);
                    var cost = Cost * bought;

                    if (ignoreprice)
                    {
                        bought = amount;
                        cost.Amount = 0;
                    }

                    if (ignoreprice || bought > 0 && buyer.Pay(cost))
                    {
                        var item = ProcessOfBuy(buyer, recipient, bought);
                        if (item != null)
                        {
                            item.PlayerInstance = recipient;
                            item.ItemModelEntity = this;
                        }
                        return item;
                    }
                    else
                    {
                        throw new NotEnoughMoneyException(402, $"Not enough money for Buy {ModelIdName} | Cost: {cost} [Amount: {bought}] | {buyer.TakeCash(cost)}", buyer.TakeCash(cost), cost);
                    }
                }
            }
            else
            {
                throw new NotMatchRequirementsException(701, $"Not matched minimum requirements for {ModelIdName} [{ModelId} - {CategoryTypeId}] Cost: {Cost}| " +
                                                             $"{FractionId} | {buyer?.ReputationList.SingleOrDefault(p => p.FractionId == FractionId)?.CurrentRank} of {Level}");
            }
        }


        protected virtual PlayerInventoryItemEntity ProcessOfBuy(PlayerEntity buyer, PlayerEntity recipient, long amount)
        {
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(amount), amount, $"Invalid amount: {amount}");
            }

            PlayerInventoryItemEntity inventoryItem = Factory(recipient);
            if (IsSubscribe)
            {
                inventoryItem.ValidityDate = DateTime.UtcNow.AddHours(Duration * amount);
            }

            inventoryItem.Amount = amount;
            //=============================================================================
            //buyer.TransactionEntities.Add(PaymentTransactionEntity.Factory(this));
            //recipient.TransactionEntities.Add(PaymentTransactionEntity.Factory(this, TransactionPaymentType.Gift));

            //=============================================================================
            recipient.InventoryItemList.Add(inventoryItem);

            //=============================================================================
            return inventoryItem;
        }

        protected virtual PlayerInventoryItemEntity ProcessOfSubscribe(PlayerEntity player, long amount)
        {
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(amount), amount, $"Invalid amount: {amount}");
            }

            if (IsCountable)
            {
                var item = GetExistItemWithValidate(player);
                if (item.Amount <= 0)
                {
                    item.Amount = amount;
                }
                else
                {
                    item.Amount = item.Amount + amount;
                }
                return item;
            }
            else
            {
                var item = GetExistItemWithValidate(player);
                if (item.ValidityDate == null || item.ValidityDate < DateTime.UtcNow)
                {
                    item.ValidityDate = DateTime.UtcNow.AddHours(Duration * amount);
                }
                else
                {
                    item.ValidityDate = item.ValidityDate.Value.AddHours(Duration * amount);
                }
                return item;
            }
        }

        public bool OnDelivery(DataRepository db, PlayerEntity player, long amount, out ICountable item)
        {
            if (amount <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(amount), amount,
                    $"[OnDelivery][{ModelId} -- {ModelIdName} | {CategoryTypeId}] Invalid amount: {amount}");
            }

            if (CategoryTypeId == CategoryTypeId.Currency)
            {
                LoggerContainer.StoreLogger.Info($"[OrderDelivery][OnDelivery][{player.PlayerName}] - CashEntities: {player.CashEntities.Count} | amount: {amount}");
                var cash = player.TakeCash(Cost.Currency);
                cash.Give(amount);
                item = cash; ;
                return true;
            }
            else
            {
                PlayerInventoryItemEntity i;
                if (IsExistItem(player))
                {
                    if (IsSubscribe)
                    {
                        item = i = ProcessOfSubscribe(player, 1);
                    }
                    else
                    {
                        throw new NotMatchRequirementsException(702, "The subject does not support renewals");
                    }
                }
                else
                {
                    item = i = ProcessOfBuy(player, player, 1);
                }
                return OnDeliveryProcess(player, i);
            }
        }

        protected virtual bool OnDeliveryProcess(PlayerEntity player, PlayerInventoryItemEntity item)
        {
            if (CategoryTypeId == CategoryTypeId.Service)
            {
                Logger.Info($"OnDeliveryProcess: {player.PlayerName} | {player.SubscribeEndDate}");
                //=============================================================================
                //  Renew their subscription, if not bought the game
                if (player.SubscribeEndDate != null)
                {
                    // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
                    if (player.SubscribeEndDate < DateTime.UtcNow)
                    {
                        player.SubscribeEndDate = DateTime.UtcNow.AddHours(Duration);

                    }
                    else
                    {
                        player.SubscribeEndDate = player.SubscribeEndDate.Value.AddHours(Duration);
                    }
                }

                // If the player has the full version of the game - we do not do anything, 
                // but we put a premium account in inventory
                //=============================================================================
                return true;
            }
            throw new NotImplementedException("OnDeliveryProcess: Called abstract method");
        }

        public virtual PlayerInventoryItemEntity Factory(PlayerEntity player)
        {
            return PlayerInventoryItemEntity.Factory(player.PlayerId, this); ;
        }

        protected bool IsCountable => CategoryTypeId == CategoryTypeId.Building ||
                                      CategoryTypeId == CategoryTypeId.Service ||
                                      CategoryTypeId == CategoryTypeId.Currency;

        protected virtual bool IsMatchRequirements(PlayerEntity player)
        {
            return Flags.HasFlag(ItemInstanceFlags.IgnoreRequirements)
                    || IsCountable
                    || player.ReputationList.Any(f => f.FractionId == FractionId && f.CurrentRank >= Level);
        }

        protected virtual PlayerInventoryItemEntity GetExistItemWithValidate(PlayerEntity player)
        {
            var item = GetExistItem(player);
            if (item == null)
            {
                throw new ObjectNotFoundException
                (
                    700, 
                    $"Not found exist item in inventory: ModelId: {ModelId} [{ModelIdName}] | CategoryTypeId: {CategoryTypeId}",
                    new KeyNotFoundException
                    (
                        string.Join($" | {Environment.NewLine}", player.InventoryItemList.OrderBy(i => i.CategoryTypeId).Select(i => $"ModelId: {i.ModelId} / {i.CategoryTypeId}"))
                    )
                );
            }
            return item;
        }

        protected virtual PlayerInventoryItemEntity GetExistItem(PlayerEntity player)
        {
            return player.InventoryItemList.FirstOrDefault(i => i.ModelId == ModelId && i.CategoryTypeId == CategoryTypeId);;
        }

        protected virtual bool IsExistItem(PlayerEntity player)
        {
            return GetExistItem(player) != null;
        }

        
        public short ModelId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }
        public CharacterModelFlagId CharacterModelsFlag { get; set; }
        public string AdminComment { get; set; }

        public ItemInstanceFlags Flags { get; set; }

        public ItemInstanceFlags[] FlagsArray()
        {
            List<ItemInstanceFlags> flags = new List<ItemInstanceFlags>(10);
            foreach (var f in Enum.GetValues(typeof(ItemInstanceFlags)).Cast<ItemInstanceFlags>().ToArray())
            {
                if (Flags.HasFlag(f))
                {
                    flags.Add(f);
                }
            }
            return flags.ToArray();
        }

        public bool IsSubscribe =>
            Flags.HasFlag(ItemInstanceFlags.Subscribe)
            || IsCountable;


        public int Level { get; set; }
        public ItemSkinTypeId DefaultSkinId { get; set; }
        //public ItemSkinInstance DefaultSkinInstance { get; set; }

        public FractionTypeId FractionId { get; set; }
        public virtual FractionEntity FractionEntity { get; set; }

        public virtual List<ItemAddonContainer> Addons { get; protected set; } = new List<ItemAddonContainer>(16);

        public short DefaultAmount { get; set; }
        public StoreItemCost Cost { get; set; }
        public StoreItemCost SubscribeCost { get; set; }
        public StoreItemCost SafeSubscribeCost => SubscribeCost.Amount == 0 ? Cost : SubscribeCost;

        public string ModelIdName
        {
            get
            {
                switch (CategoryTypeId)
                {
                    case CategoryTypeId.Weapon:     return ((WeaponModelId)ModelId).ToString();
                    case CategoryTypeId.Armour:     return ((ArmourModelId)ModelId).ToString();                
                    case CategoryTypeId.Ammo:       return ((AmmoModelId)ModelId).ToString();              
                    case CategoryTypeId.Service:    return ((ServiceModelId)ModelId).ToString();                    
                    case CategoryTypeId.Addon:      return ((ItemAddonTypeId)ModelId).ToString();                 
                    case CategoryTypeId.Character:  return ((CharacterModelId)ModelId).ToString();                      
                    case CategoryTypeId.Profile:    return ((PlayerProfileTypeId)ModelId).ToString();                    
                    case CategoryTypeId.Material:   return ((ItemSkinTypeId)ModelId).ToString();                     
                    case CategoryTypeId.Kit:        return ((KitModelId)ModelId).ToString();                
                    case CategoryTypeId.Grenade:    return ((GrenadeModelId)ModelId).ToString();                    
                    case CategoryTypeId.Building:   return ((BuildingModelId)ModelId).ToString();
                    case CategoryTypeId.Currency: return ((DonateModelId)ModelId).ToString();
                    default:
                        return ModelId.ToString();
                }
            }
        }

        public PlayerProfileTypeId TargetProfileId { get; set; }
        public ItemMigrationAction MigrationAction { get; set; }

        /// <summary>
        /// Duration in Hours
        /// </summary>
        public long Duration { get; set; }

        public int DisplayPosition { get; set; }

        public bool IsAllowCharacter(byte characterModelId)
        {
            return IsAllowCharacter((CharacterModelId)characterModelId);
        }

        public bool IsAllowCharacter(CharacterModelId characterModelId)
        {
            return CharacterModelsFlag == CharacterModelFlagId.All || CharacterModelsFlag.HasFlag(characterModelId.ToFlag());
        }


        public ItemInstanceEntity() { }

        public ItemInstanceEntity(ItemInstanceEntity instance)
        {
            ModelId = instance.ModelId;
            CategoryTypeId = instance.CategoryTypeId;
            Flags = instance.Flags;
            Level = instance.Level;
            DefaultSkinId = instance.DefaultSkinId;
            FractionId = instance.FractionId;
            Cost = instance.Cost;
            SubscribeCost = instance.Cost;
        }

        public override string ToString()
        {
            return $"{ModelIdName} {CategoryTypeId} * {Flags} * [ {Level} level ] {FractionId} {Cost}";
        }

        public class Configuration : EntityTypeConfiguration<ItemInstanceEntity>
        {
            public Configuration()
            {
                ToTable("store.AbstractItemInstance");
                Property(p => p.ModelId).IsRequired();
                Property(p => p.CategoryTypeId).IsRequired();
                HasKey(i => new { i.ModelId, i.CategoryTypeId});
                HasRequired(f => f.FractionEntity).WithMany(f => f.ItemList).HasForeignKey(f => f.FractionId);
                Property(p => p.AdminComment).IsOptional().HasMaxLength(128);
				//HasRequired(s => s.DefaultSkinInstance).WithMany().HasForeignKey(s => s.DefaultSkinId);

				HasMany(p => p.PropertyEntities).WithRequired(p => p.ModelEntity).HasForeignKey(p => new { p.ModelId, p.CategoryTypeId }).WillCascadeOnDelete(true);
			}
		}

		public virtual ICollection<ItemInstancePropertyEntity> PropertyEntities { get; } = new List<ItemInstancePropertyEntity>();
	}
}
