﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loka.Common.Store.Fraction;
using Loka.Common.Store.Item.Character;
using Loka.Common.Store.Property;
using Newtonsoft.Json;


namespace Loka.Common.Store.Abstract
{
    public class ItemBaseContainer : IAbstractItemInterface
    {
        public ItemBaseContainer(ItemInstanceEntity entity)
        {
            ModelId = entity.ModelId;
            CategoryTypeId = entity.CategoryTypeId;
            CharacterModelsFlag = entity.CharacterModelsFlag;
	        Properties = entity.PropertyEntities.ToDictionary(p => p.Key, p => p.Value);
			Level = entity.Level;
            FractionId = entity.FractionId;
            Cost = entity.Cost;

            if (entity.DisplayPosition == 0)
            {
                DisplayPosition = null;
            }
            else
            {
                DisplayPosition = entity.DisplayPosition;
            }
        }

        public short ModelId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }
        public CharacterModelFlagId CharacterModelsFlag { get; set; }
        public int Level { get; set; }
        public FractionTypeId FractionId { get; set; }
        public StoreItemCost Cost { get; set; }

		public Dictionary<ItemInstancePropertyName, string> Properties { get; set; }


        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? DisplayPosition { get; set; }
    }
}
