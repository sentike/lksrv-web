﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Store.Abstract;

namespace Loka.Common.Store
{
    public class CharacterProfileTemplate
    {
        public int TemplateId { get; set; }

        //======================================================
        public int CharacterId { get; set; }
        public short CharacterModelId { get; set; }
        public CategoryTypeId CharacterCategoryId { get; set; }
        public virtual ItemInstanceEntity CharacterModelEntity { get; set; }


        //======================================================
        

        public short ItemModelId { get; set; }
        public CategoryTypeId ItemCategoryId { get; set; }
        public virtual ItemInstanceEntity ItemModelEntity { get; set; }

        //======================================================
        public PlayerProfileInstanceSlotId ProfileSlot { get; set; }

        //======================================================
        public override string ToString()
        {
            return $"[{TemplateId}] | {CharacterModelEntity?.ModelIdName}[{CharacterCategoryId} - {CharacterModelId}] | {ProfileSlot}: {ItemModelEntity?.ModelIdName}[{ItemCategoryId} - {ItemModelId}]";
        }

        //======================================================
        public class Configuration : EntityTypeConfiguration<CharacterProfileTemplate>
        {
            public Configuration()
            {
                HasKey(p => p.TemplateId);
                ToTable("store.CharacterProfileTemplate");

                HasRequired(p => p.CharacterModelEntity).WithMany(p => p.CharacterProfileTemplates).HasForeignKey(p => new {p.CharacterModelId, p.CharacterCategoryId}).WillCascadeOnDelete(true);
                HasRequired(p => p.ItemModelEntity).WithMany().HasForeignKey(p => new { p.ItemModelId, p.ItemCategoryId }).WillCascadeOnDelete(true);

                //Property(p => p.CharacterModelId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ProfileItemSlot", 0) { IsUnique = true }));
                //Property(p => p.ProfileSlot).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ProfileItemSlot", 1) { IsUnique = true }));
            }
        }
    }
}
