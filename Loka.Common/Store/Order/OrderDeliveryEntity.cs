﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store.Order
{
    public enum OrderDeliveryStatus
    {
        None,

        PlayerNotFound,
        ItemsWasEmpty,
        DeliveredSuccessfully,
        DeliveredConflicted,

        ApplicationError,
        DataBaseError,

        ParserError,
        DeliveredEarlier,
    }

    public class OrderDeliveryEntity
    {
        //===========================================
        public Guid EntityId { get; set; }
        public Guid CreatorId { get; set; }
        public Guid? InvoiceId { get; set; }

        //===========================================
        public DateTime CreatedDate { get; set; }
        public string Exception { get; set; }

        //===========================================
        public OrderDeliveryStatus Status { get; set; }
        public OrderDeliveryStatus LastStatus { get; set; }

        public void SwitchStatus(OrderDeliveryStatus status)
        {
            LastStatus = Status;
            Status = status;
        }

        public virtual ICollection<OrderProductDeliveryEntity> ProductEntities { get; set; } = new List<OrderProductDeliveryEntity>(8);

        public class Configuration : EntityTypeConfiguration<OrderDeliveryEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("store.OrderDeliveryEntity");
                HasMany(p => p.ProductEntities).WithRequired(p => p.OrderEntity).HasForeignKey(p => p.OrderId).WillCascadeOnDelete(true);
            }
        }
    }
}
