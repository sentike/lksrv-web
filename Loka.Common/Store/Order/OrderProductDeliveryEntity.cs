﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store.Order
{
    public enum OrderItemDeliveryStatus
    {
        None,
        Delivered,
        DeliveryFailed,

        EntityNotFound,
        DeliveryException,

    }

    public class OrderProductDeliveryEntity
    {
        public OrderProductDeliveryEntity()
        {
            
        }
        public OrderProductDeliveryEntity(Guid entityId, long amount, int model, CategoryTypeId category)
        {
            EntityId = Guid.NewGuid();
            ProductId = entityId;

            Status = OrderItemDeliveryStatus.None;

            ItemCategory = category;
            CategoryTypeId = category;
            ItemModel = (short)model;
            Amount = amount;
        }

        public Guid EntityId { get; set; }
        public Guid ProductId { get; set; }
        public string Exception { get; set; }

        //======================================
        public Guid OrderId { get; set; }
        public virtual OrderDeliveryEntity OrderEntity { get; set; }


        //======================================
        public OrderItemDeliveryStatus Status { get; set; }

        public void SwitchStatus(OrderItemDeliveryStatus status)
        {
            Status = status;
        }

        public CategoryTypeId ItemCategory { get; set; }
        public short ItemModel { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }

        public long Amount { get; set; }

        public class Configuration : EntityTypeConfiguration<OrderProductDeliveryEntity>
        {
            public Configuration()
            {
                HasKey(i => i.EntityId);
                ToTable("store.OrderProductDeliveryEntity");
            }
        }
    }
}
