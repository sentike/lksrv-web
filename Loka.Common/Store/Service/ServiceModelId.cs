﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Loka.Common.Store.Service
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum ServiceModelId
    {
        PremiumAccount_0001Hour,            //  Premium Account for 1 hour
        PremiumAccount_0006Hour,            //  Premium Account for 6 hour
        PremiumAccount_0024Hour,            //  Premium Account for 1 day
        PremiumAccount_0072Hour,            //  Premium Account for 3 day
        PremiumAccount_0168Hour,            //  Premium Account for 7 day
        PremiumAccount_0744Hour,            //  Premium Account for 1 month
        PremiumAccount_2232Hour,            //  Premium Account for 3 month
        PremiumAccount_4464Hour,            //  Premium Account for 6 month

        BoosterOfReputation_0001Hour,       //  Premium Account for 1 hour
        BoosterOfReputation_0006Hour,       //  Premium Account for 6 hour
        BoosterOfReputation_0024Hour,       //  Premium Account for 1 day
        BoosterOfReputation_0072Hour,       //  Premium Account for 3 day
        BoosterOfReputation_0168Hour,       //  Premium Account for 7 day
        BoosterOfReputation_0744Hour,       //  Premium Account for 1 month
        BoosterOfReputation_2232Hour,       //  Premium Account for 3 month
        BoosterOfReputation_4464Hour,       //  Premium Account for 6 month

        BoosterOfExperience_0001Hour,       //  Premium Account for 1 hour
        BoosterOfExperience_0006Hour,       //  Premium Account for 6 hour
        BoosterOfExperience_0024Hour,       //  Premium Account for 1 day
        BoosterOfExperience_0072Hour,       //  Premium Account for 3 day
        BoosterOfExperience_0168Hour,       //  Premium Account for 7 day
        BoosterOfExperience_0744Hour,       //  Premium Account for 1 month
        BoosterOfExperience_2232Hour,       //  Premium Account for 3 month
        BoosterOfExperience_4464Hour,       //  Premium Account for 6 month

        BoosterOfMoney_0001Hour,            //  Premium Account for 1 hour
        BoosterOfMoney_0006Hour,            //  Premium Account for 6 hour
        BoosterOfMoney_0024Hour,            //  Premium Account for 1 day
        BoosterOfMoney_0072Hour,            //  Premium Account for 3 day
        BoosterOfMoney_0168Hour,            //  Premium Account for 7 day
        BoosterOfMoney_0744Hour,            //  Premium Account for 1 month
        BoosterOfMoney_2232Hour,            //  Premium Account for 3 month
        BoosterOfMoney_4464Hour,            //  Premium Account for 6 month

        End
    };


    public static class ServiceTypeHelper
    {
        public static bool IsType(this ServiceModelId modelId, ServiceTypeId typeId)
        {
            return modelId.AsType() == typeId;
        }

        public static ServiceTypeId AsType(this ServiceModelId modelId)
        {
            if(modelId >= ServiceModelId.PremiumAccount_0001Hour && modelId<= ServiceModelId.PremiumAccount_4464Hour) return ServiceTypeId.PremiumAccount;
            if(modelId >= ServiceModelId.BoosterOfReputation_0001Hour && modelId <= ServiceModelId.BoosterOfReputation_4464Hour) return ServiceTypeId.BoosterOfReputation;
            if(modelId >= ServiceModelId.BoosterOfExperience_0001Hour && modelId <= ServiceModelId.BoosterOfExperience_4464Hour) return ServiceTypeId.BoosterOfExperience;
            if(modelId >= ServiceModelId.BoosterOfMoney_0001Hour && modelId <= ServiceModelId.BoosterOfMoney_4464Hour) return ServiceTypeId.BoosterOfMoney;
            return ServiceTypeId.None;
        }
    }

}

