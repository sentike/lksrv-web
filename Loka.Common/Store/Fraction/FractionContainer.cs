﻿using System;
using Loka;

namespace Loka.Common.Store.Fraction
{
    public sealed class FractionContainer
    {
        public FractionContainer(FractionEntity entity)
        {
            ModelId = entity.FractionId;
        }

        public FractionTypeId ModelId;
    }
}
