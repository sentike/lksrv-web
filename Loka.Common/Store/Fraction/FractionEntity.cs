﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Achievements;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Item.Ammo;
using Loka.Common.Store.Item.Armour;
using Loka.Common.Store.Item.Weapon;


namespace Loka.Common.Store.Fraction
{
    public class FractionEntity
    {
        public FractionEntity() { }

        public FractionTypeId FractionId { get; set; }

        //public StoreItemDiscount Discount { get; set; }

        public virtual List<ItemInstanceEntity> ItemList { get; set; } = new List<ItemInstanceEntity>(32);

        public AchievementTypeId FractionAchievementId { get; set; }

        public class Configuration : EntityTypeConfiguration<FractionEntity>
        {
            public Configuration()
            {
                HasKey(a => a.FractionId);
                ToTable("store.FractionEntity");
                HasMany(f => f.ItemList).WithRequired(f => f.FractionEntity).HasForeignKey(f => f.FractionId);
            }
        }
    }



}
