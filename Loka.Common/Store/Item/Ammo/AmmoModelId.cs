﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store.Item.Ammo
{
    public enum AmmoModelId
    {
        PeopleMan,
        PeopleWoman,
        RobotMan,
        RobotWoman,
        RobotXb33,
        CyborgLady,
        CyborgTrooper,
        CyborgDroidEpic,
        End
    };
}
