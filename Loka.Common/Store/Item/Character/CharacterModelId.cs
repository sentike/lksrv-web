﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store.Item.Character
{
    public enum CharacterModelId
    {
        PeopleMan,
        PeopleWoman,
        RobotMan,
        RobotWoman,
        RobotXb33,
        CyborgLady,
        RobotSprinter,
        PeopleSoldier,
        End
    };

    [Flags]
    public enum CharacterModelFlagId
    {
        All,
        PeopleMan = 1,
        PeopleWoman = 2,
        RobotMan = 4,
        RobotWoman = 8,
        RobotXb33 = 16,
        CyborgLady = 32,
        CyborgTrooper = 64,
        CyborgDroidEpic = 128,
        End = -1
    }

    public static class CharacterModelHelper
    {
        public static CharacterModelFlagId ToFlag(this CharacterModelId characterModelId)
        {
            switch (characterModelId)
            {
                case CharacterModelId.PeopleMan: return CharacterModelFlagId.PeopleMan;
                case CharacterModelId.PeopleWoman: return CharacterModelFlagId.PeopleWoman;
                case CharacterModelId.RobotMan: return CharacterModelFlagId.RobotMan;
                case CharacterModelId.RobotWoman: return CharacterModelFlagId.RobotWoman;
                case CharacterModelId.RobotXb33: return CharacterModelFlagId.RobotXb33;
                case CharacterModelId.CyborgLady: return CharacterModelFlagId.CyborgLady;
               // case CharacterModelId.CyborgTrooper: return CharacterModelFlagId.CyborgTrooper;
               // case CharacterModelId.CyborgDroidEpic: return CharacterModelFlagId.CyborgDroidEpic;
                case CharacterModelId.End:              return CharacterModelFlagId.All;
                default: return CharacterModelFlagId.All;
            }
        }
    }
}
