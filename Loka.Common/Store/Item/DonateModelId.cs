﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store.Item
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum DonateModelId
    {
        None,

        Money = 7,
        Crystal = 8,
        Bitcoin = 9
    }
}
