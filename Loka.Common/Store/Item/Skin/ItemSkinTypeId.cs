﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store.Item.Skin
{
    public enum ItemSkinTypeId
    {
        Default,
        White,
        Black,
        SandCamo,
        GreenCamo,
        GreenPixelCamo,
        Silver,
        Gold,
        End
    }
}
