﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Loka.Common.Store.Item.Kit
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum GrenadeModelId
    {
        Explosive,
        Precision,
        End
    };
}
