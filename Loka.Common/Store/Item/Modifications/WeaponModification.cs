﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store.Item.Modifications
{
    public static class WeaponModification
    {
        public static readonly IReadOnlyDictionary<WeaponModificationTypeId, ModificationRange> List = new Dictionary
            <WeaponModificationTypeId, ModificationRange>()
        {
            {WeaponModificationTypeId.Damage, new ModificationRange(1, 10)},
            {WeaponModificationTypeId.Piercing, new ModificationRange(1, 10)},
            {WeaponModificationTypeId.Distance, new ModificationRange(1, 10)},
            {WeaponModificationTypeId.Dispersion, new ModificationRange(1, 10)},
            {WeaponModificationTypeId.Recoil, new ModificationRange(-10, -1)},
            {WeaponModificationTypeId.ScatterFromHip, new ModificationRange(-10, -1)},
            {WeaponModificationTypeId.Reloadspeed, new ModificationRange(-10, -1)},
            {WeaponModificationTypeId.Aimingtime, new ModificationRange(-10, -1)},
            {WeaponModificationTypeId.Firing, new ModificationRange(1, 10)},
            {WeaponModificationTypeId.Mass, new ModificationRange(-10, -1)},
            {WeaponModificationTypeId.Reliability, new ModificationRange(1, 10)},
        };

    }
}
