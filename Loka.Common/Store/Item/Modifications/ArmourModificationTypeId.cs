﻿namespace Loka.Common.Store.Item.Modifications
{
    public enum ArmourModificationTypeId
    {
        None,

        /// <summary>
        /// Вес
        /// </summary>
        Mass,

        /// <summary>
        /// Надёжность
        /// </summary>
        Reliability,

        /// <summary>
        /// Защита от кровотечения	
        /// </summary>
        Antibleeding,

        /// <summary>
        /// Броня
        /// </summary>
        Armor,

        /// <summary>
        /// Скорость передвижения	
        /// </summary>
        Movementspeed,

        /// <summary>
        /// Затраты энергии	
        /// </summary>
        Energycosts,

        /// <summary>
        /// Восстановление энергии	
        /// </summary>
        Energyregeneration,

        /// <summary>
        /// Регенерация
        /// </summary>
        HealthRegeneration,

        /// <summary>
        /// Переносимый вес	
        /// </summary>
        Carriedweight,

        /// <summary>
        /// Запас кислорода	
        /// </summary>
        Amountofoxygen,

        End
    }
}