﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store.Abstract;

namespace Loka.Common.Store.Item.Modifications
{
    public static class ArmourModification
    {
        public static readonly IReadOnlyDictionary<ArmourModificationTypeId, ModificationRange> List = new Dictionary
            <ArmourModificationTypeId, ModificationRange>()
        {
            {ArmourModificationTypeId.Amountofoxygen, new ModificationRange(1, 10)},
            {ArmourModificationTypeId.Antibleeding, new ModificationRange(1, 10)},
            {ArmourModificationTypeId.Armor, new ModificationRange(1, 10)},
            {ArmourModificationTypeId.Carriedweight, new ModificationRange(1, 10)},
            {ArmourModificationTypeId.Energycosts, new ModificationRange(1, 10)},
            {ArmourModificationTypeId.Energyregeneration, new ModificationRange(1, 10)},
            {ArmourModificationTypeId.HealthRegeneration, new ModificationRange(1, 10)},
            {ArmourModificationTypeId.Movementspeed, new ModificationRange(1, 10)},
            {ArmourModificationTypeId.Mass, new ModificationRange(-10, -1)},
            {ArmourModificationTypeId.Reliability, new ModificationRange(1, 10)},
        };
    }
}
