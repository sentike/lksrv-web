﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace Loka.Common.Store.Item.Modifications
{
    public struct ModificationRange
    {
        public ModificationRange(float min, float max)
        {
            Min = min;
            Max = max;
        }

        public readonly float Min;
        public readonly float Max;
    }
}