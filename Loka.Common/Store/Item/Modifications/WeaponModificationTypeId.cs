﻿namespace Loka.Common.Store.Item.Modifications
{
    public enum WeaponModificationTypeId
    {
        None,

        /// <summary>
        /// Урон
        /// </summary>
        Damage,

        /// <summary>
        /// Бронебойность
        /// </summary>
        Piercing,

        /// <summary>
        /// Дальность
        /// </summary>
        Distance,

        /// <summary>
        /// Дисперсия
        /// </summary>
        Dispersion,

        /// <summary>
        /// Отдача
        /// </summary>
        Recoil,

        /// <summary>
        /// Разброс от бедра	
        /// </summary>
        ScatterFromHip,

        /// <summary>
        /// Скорость перезарядки	
        /// </summary>
        Reloadspeed,

        /// <summary>
        /// Время прицеливания	
        /// </summary>
        Aimingtime,

        /// <summary>
        /// Скорострельность	
        /// </summary>
        Firing,

        /// <summary>
        /// Вес
        /// </summary>
        Mass,

        /// <summary>
        /// Надёжность
        /// </summary>
        Reliability,

        End
    }
}