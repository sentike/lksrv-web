﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Loka.Common.Store.Item.Kit
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum KitModelId
    {
        AidKit_1,
        AidKit_2,
        AidKit_3,
        AidKit_4,

        Scaner_1,
        Scaner_2,
        Scaner_3,
        Scaner_4,

        End
    };
}
