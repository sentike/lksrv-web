﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store.Abstract;

namespace Loka.Common.Store.Item.Addon
{
    public class AbstractItemAddonInstance : ItemInstanceEntity
    {
        public List<ItemAddonContainer> Containers { get; protected set; } = new List<ItemAddonContainer>(16);

        public AbstractItemAddonInstance() { }
        public AbstractItemAddonInstance(ItemInstanceEntity instance) : base(instance) { }

        public new ItemAddonTypeId ModelId
        {
            get { return (ItemAddonTypeId)base.ModelId; }
            set { base.ModelId = (short)value; }
        }

        public new class Configuration : EntityTypeConfiguration<AbstractItemAddonInstance>
        {
            public Configuration()
            {
                ToTable("store.AddonItemInstance");
            }
        }
    }
}
