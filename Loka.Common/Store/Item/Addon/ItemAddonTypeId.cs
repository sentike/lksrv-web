﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store.Abstract;

namespace Loka.Common.Store.Item.Addon
{
    public enum ItemAddonTypeId
    {
        None,
        Scopex3,
        Scopex4,
        Scopex48,
        Scopex8,
        Scopex16,
    }
}
