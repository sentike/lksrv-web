﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Loka.Common.Store.Abstract;

namespace Loka.Common.Store.Item.Addon
{
    public class ItemAddonContainer //: IAbstractItemModel<ItemAddonTypeId>
    {
        public int ContainerId { get; set; }
        

        public short ModelId { get; set; }
        public CategoryTypeId ModelCategoryTypeId { get; set; }
        public virtual AbstractItemAddonInstance ModelInstance { get; set; }


        public int TargetProductId { get; set; }
        public short ItemId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }
        public virtual ItemInstanceEntity ItemInstance { get; set; }
    
        public class Configuration : EntityTypeConfiguration<ItemAddonContainer>
        {
            public Configuration()
            {
                ToTable("store.AddonItemContainer");
                HasKey(k => k.ContainerId).Property(k => k.ContainerId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                HasRequired(i => i.ItemInstance).WithMany(i => i.Addons).HasForeignKey(i => new { i.ItemId, i.CategoryTypeId});
                HasRequired(i => i.ModelInstance).WithMany(i => i.Containers).HasForeignKey(i => new {i.ModelId, i.ModelCategoryTypeId });
            }
        }
    
    }
}