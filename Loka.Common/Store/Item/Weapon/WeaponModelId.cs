﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Loka.Common.Store.Item.Weapon
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum WeaponModelId
    {
        MilitaryDark_AssaultRifle,
        MilitaryDark_SniperRifle,
        MilitaryDark_Shotgun,
        MilitaryDark_Pistol,
        MilitaryDark_RocketLauncher,
        MilitaryDark_GrenadeLauncher,
        MilitarySilver_AssaultRifle,
        MilitarySilver_SniperRifle,
        MilitarySilver_Shotgun,
        MilitarySilver_Pistol,
        MilitarySilver_RocketLauncher,
        MilitarySilver_GrenadeLauncher,
        ScifiDark_AssaultRifle,
        ScifiDark_SniperRifle,
        ScifiDark_Shotgun,
        ScifiDark_Pistol,
        ScifiDark_RocketLauncher,
        ScifiDark_GrenadeLauncher,
        ScifiWhite_AssaultRifle,
        ScifiWhite_SniperRifle,
        ScifiWhite_Shotgun,
        ScifiWhite_Pistol,
        ScifiWhite_RocketLauncher,
        ScifiWhite_GrenadeLauncher,
        LOKA_MasterTrust,
        LOKA_DTASRS,
        LOKA_BenelliM4,
        LOKA_SunGazer,
        LOKA_Stoner96,
        LOKA_SafdarAli,
        LOKA_SunGazerM687,

        // Need add to db
        IRB_RifleP90,
        IRB_ShotgunStriker,

        IRB_Assault_AK47,
        IRB_Assault_GE36,
        IRB_Pistol_Beretta,
        IRB_Pistol_SK_Sock17,
        IRB_Shotgun_BenelliSuperNova,
        IRB_Shotgun_Rem870,
        IRB_Assault_AP5,
        IRB_Assault_AP5SD,
        IRB_Assault_AR4,
        IRB_Assault_GE3,
        IRB_Pistol_Battlehawk,
        IRB_Shotgun_Moss500,
        IRB_44_Magnum,
        IRB_SVD_Sniper,

        End
    };
}
