﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store
{
    public enum GameCurrency : short
    {
        Money,
        Donate,
        Coin
    }

    [ComplexType]
    public class StoreItemCost
    {
        public int Amount { get; set; }
        public GameCurrency Currency { get; set; }

        public StoreItemCost() { }

        public StoreItemCost(double amount, GameCurrency currency)
        {
            Amount = (int)amount;
            Currency = currency;
        }

        public StoreItemCost(long amount, GameCurrency currency)
        {
            Amount = (int) amount;
            Currency = currency;
        }

        public StoreItemCost(int amount, GameCurrency currency)
        {
            Amount = amount;
            Currency = currency;
        }

        public static StoreItemCost operator *(StoreItemCost a, float b)
        {
            return new StoreItemCost
            {
                Amount = (int)(a.Amount * b),
                Currency = a.Currency
            };
        }

        public static StoreItemCost operator *(StoreItemCost a, long b)
        {
            return new StoreItemCost
            {
                Amount = (int) (a.Amount * b),
                Currency = a.Currency
            };
        }

        public StoreItemCost Invert() => new StoreItemCost
        {
            Amount = - this.Amount,
            Currency = this.Currency
        };

        public override string ToString()
        {
            return $"{Amount} {Currency}";
        }
    }
}
