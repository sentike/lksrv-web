﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Building.Player;
using Loka.Common.Infrastructure;
using Loka.Common.Store.Abstract;

namespace Loka.Common.Store.Property
{
	public static class ItemInstancePropertyHelper
	{
        public static ItemInstancePropertyName[] Values { get; } = Enum.GetValues(typeof(ItemInstancePropertyName)).Cast<ItemInstancePropertyName>().ToArray();

	}
}
