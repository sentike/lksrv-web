﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store.Abstract;

namespace Loka.Common.Store.Property
{
	public class ItemInstancePropertyEntity
	{
		public Guid EntityId { get; set; }

		public short ModelId { get; set; }
		public CategoryTypeId CategoryTypeId { get; set; }
		public virtual ItemInstanceEntity ModelEntity { get; set; }


		public ItemInstancePropertyName Key { get; set; }
		public DateTime EditDate { get; set; }
		public long Hash { get; set; }
		public string Last { get; set; }
		public string Value { get; set; }
        public string Default { get; set; }

        public class Configuration : EntityTypeConfiguration<ItemInstancePropertyEntity>
		{
			public Configuration()
			{
				HasKey(p => p.EntityId);
				ToTable("store.ItemInstancePropertyEntity");

				Property(x => x.Value).IsRequired().HasMaxLength(64);
                Property(x => x.Last).HasMaxLength(64);
                Property(x => x.Default).HasMaxLength(64);


                Property(x => x.ModelId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_model_cat_key", 1) { IsUnique = true }));
				Property(x => x.CategoryTypeId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_model_cat_key", 2) { IsUnique = true }));
				Property(x => x.Key).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_model_cat_key", 3) { IsUnique = true }));
			}
		}


	}
}
