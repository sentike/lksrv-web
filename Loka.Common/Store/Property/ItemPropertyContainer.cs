﻿using Loka.Common.Building.Player;
using Loka.Common.Infrastructure;
using Loka.Common.Store.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store.Property
{
    public class InstancePropertyContainer<TValue>
    {
        //===================================================
        private InstancePropertyContainer()
        {
            throw new NotImplementedException("Default constructor not supported!");
        }

        public InstancePropertyContainer(ItemInstancePropertyEntity entity)
        {
            Entity = entity;
        }

        //===================================================
        public Guid Id => Entity.EntityId;
        public ItemInstancePropertyEntity Entity { get; }

        //===================================================
        public ItemInstancePropertyName Key => Entity.Key;
        public string RawDefault => Entity.Value;
        public string RawValue => Entity.Value;

        //===================================================
        public TValue Last => RawLast.GetAs<TValue>();
        public TValue Default => RawDefault.GetAs<TValue>();

        public TValue Value
        {
            get => RawValue.GetAs<TValue>();
            set
            {
                Entity.Last = Entity.Value;
                Entity.Value = value.ToString();
                Entity.EditDate = DateTime.UtcNow;
                Entity.Hash = Entity.Value.Crc64Hash();
            }
        }

        //===================================================
        public DateTime EditDate => Entity.EditDate;
        public long Hash => Entity.Hash;
        public string RawLast => Entity.Last;
    }

    public class BuildingPropertyContainer<TValue>
    {
        //===================================================
        private BuildingPropertyContainer()
        {
            throw new NotImplementedException("Default constructor not supported!");
        }

        public BuildingPropertyContainer(WorldBuildingPropertyEntity entity)
        {
            Entity = entity;
        }

        //===================================================
        public Guid Id => Entity.EntityId;
        public WorldBuildingPropertyEntity Entity { get; }

        //===================================================
        public ItemInstancePropertyName Key => Entity.Key;
        public string RawValue => Entity.Value;

        //===================================================
        public TValue Last => RawLast.GetAs<TValue>();

        public TValue Value
        {
            get => RawValue.GetAs<TValue>();
            set
            {
                Entity.Last = Entity.Value;
                Entity.Value = value.ToString();
                Entity.EditDate = DateTime.UtcNow;
                Entity.Hash = Entity.Value.Crc64Hash();
            }
        }

        //===================================================
        public DateTime EditDate
        {
            get { return Entity.EditDate; }
            set { Entity.EditDate = value; }
        }
        public long Hash => Entity.Hash;
        public string RawLast => Entity.Last;
    }

    public class DynamicPropertyContainer<TValue>
    {
        public InstancePropertyContainer<TValue> Instance { get; set; }
        public BuildingPropertyContainer<TValue> Property { get; set; }

        //===================================================
        private DynamicPropertyContainer()
        {
            throw new NotImplementedException("Default constructor not supported!");
        }

        public DynamicPropertyContainer(InstancePropertyContainer<TValue> instance, BuildingPropertyContainer<TValue> property)
        {
            Instance = instance;
            Property = property;
        }
    }

    public static class DynamicPropertyHelper
    {
        //==========================================================================================================
        public static bool Any(this ItemInstanceEntity entity, ItemInstancePropertyName key)
        {
            return entity.PropertyEntities.Any(p => p.Key == key);
        }

        public static InstancePropertyContainer<TValue> Get<TValue>(this ItemInstanceEntity entity, ItemInstancePropertyName key)
        {
            var property = entity.PropertyEntities.SingleOrDefault(p => p.Key == key);
            if(property == null)
            {
                return null;
            }
            return new InstancePropertyContainer<TValue>(property);
        }

        //==========================================================================================================
        public static DynamicPropertyContainer<TValue> Get<TValue>(this WorldBuildingItemEntity entity, ItemInstancePropertyName key)
        {
            //--------------------------------------
            if (entity.ModelEntity == null)
            {
                return null;
                throw new ArgumentNullException(nameof(entity.ModelEntity), $"ModelEntity[{entity.ModelId} - {entity.CategoryTypeId}] not loaded for {entity.ItemId}");
            }

            //--------------------------------------
            var instance = entity.ModelEntity.PropertyEntities.SingleOrDefault(p => p.Key == key);
            if (instance == null)
            {
                return null;
            }

            //--------------------------------------
            var property = entity.PropertyEntities.SingleOrDefault(p => p.Key == key);
            if (property == null)
            {
                entity.PropertyEntities.Add(property = WorldBuildingPropertyEntity.Factory(entity, instance));
            }

            //--------------------------------------
            return new DynamicPropertyContainer<TValue>(new InstancePropertyContainer<TValue>(instance), new BuildingPropertyContainer<TValue>(property));
        }

        //==========================================================================================================
        public static T GetAs<T>(this InstancePropertyContainer<T> property)
        {
            if (string.IsNullOrWhiteSpace(property.RawValue))
            {
                return default(T);
            }

            try
            {
                return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(property.RawValue);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(T);
            }
        }

        //==========================================================================================================
        public static T GetAs<T>(this string property)
        {
            if (string.IsNullOrWhiteSpace(property))
            {
                return default(T);
            }

            try
            {
                return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(property);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(T);
            }
        }
    }

}
