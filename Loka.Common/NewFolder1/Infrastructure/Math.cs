﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Loka.Infrastructure
{
    public static class CMath
    {
        public static readonly Random random = new Random();
        public static readonly Regex EmailRegex = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }

        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0) return min;
            if (val.CompareTo(max) > 0) return max;
            return val;
        }

        public static bool IsPowerOfTwo(this uint val)
        {
            return val != 0 && (val & (val - 1)) == 0;
        }

        public static double GetRandomNumber(double minimum, double maximum)
        {
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        public static Int32 UnixTime() => (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

        public static Int32 ToUnixTime(this DateTime time) => (int)(time.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

        public static Int64 ToSecondsTime(this DateTime time) => Convert.ToInt64((time - DateTime.UtcNow).TotalSeconds);

        public static Int64 ToElapsedSeconds(this DateTime? time) => Math.Abs(Convert.ToInt64(((time ?? DateTime.UtcNow) - DateTime.UtcNow).TotalSeconds));

        public static Int64 ToElapsedSeconds(this DateTime time) => Math.Abs(Convert.ToInt64((time - DateTime.UtcNow).TotalSeconds));
        public static Int64 ToLocalElapsedSeconds(this DateTime time) => Math.Abs(Convert.ToInt64((time - DateTime.Now).TotalSeconds));
        public static bool ElapsedMore(this DateTime time, TimeSpan elapsed) => time - DateTime.UtcNow >= elapsed;

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).AddSeconds(unixTimeStamp);
        }


        public static string GetHash(this HashAlgorithm hash, string input)
        {
            // Convert the input string to a byte array and compute the hash.
            byte[] data = hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

    }
}