﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Infrastructure
{
    public interface IReqestValidation
    {
        bool IsValidModel { get; }
    }

    public class FReqestOneParam<T>
    {
        public T Value;

        public FReqestOneParam()
        {
            
        }

        public FReqestOneParam(T v)
        {
            this.Value = v;
        }


        public static implicit operator FReqestOneParam<T>(T val)
        {
            return new FReqestOneParam<T>(val);
        }

        public static implicit operator T(FReqestOneParam<T> val)
        {
            return val.Value;
        }

    }
}
