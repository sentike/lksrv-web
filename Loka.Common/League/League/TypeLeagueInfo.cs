﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Loka.Server.League
{
    [ComplexType]
    public class TypeLeagueInfo
    {
        public string Name { get; set; }

        public string Abbr { get; set; }

        public DateTime FoundedDate { get; set; }

        public LeagueAccessType AccessType { get; set; }

        public long JoinPrice { get; set; }
    }
}
