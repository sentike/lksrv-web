﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Loka.Server.League
{
    public interface ILeagueRepository<TLeagueInstance>
    {
        [NotNull]
        IReadOnlyList<TLeagueInstance> LeagueList { get; }

        [NotNull]
        Dictionary<Guid, TLeagueInstance> LeagueDictionary { get; }

        bool IsValidName(string name, LeagueNameCompare compare);

        bool IsExist(string name, LeagueNameCompare compare);

        bool IsExist(Guid leagueId);

        [CanBeNull]
        TLeagueInstance FindBy(string name, LeagueNameCompare compare);

        [CanBeNull]
        TLeagueInstance FindBy(Guid leagueId);

        [NotNull]
        TLeagueInstance Create(object corporation, Guid playerId);

        bool Insert(TLeagueInstance leagueInstance);

        bool Join(TLeagueInstance leagueInstance, Guid playerId);

        bool Join(Guid leagueId, Guid playerId);

        bool Edit(TLeagueInstance leagueInstance, object request);

        bool Edit(Guid leagueId, object request);

    }
}
