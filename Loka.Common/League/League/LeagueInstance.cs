﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Instance;

namespace Loka.Server.League
{
    public abstract class LeagueInstance<TLeagueMemberInstance>
    {
        protected LeagueInstance() { }

        protected LeagueInstance(string name, string abbr, LeagueAccessType accessType)
        {
            Info = new TypeLeagueInfo()
            {
                Name = name,
                Abbr = abbr,
                AccessType = accessType,
                FoundedDate = DateTime.UtcNow,
                JoinPrice = 0
            };
        }

        public virtual List<TLeagueMemberInstance> MemberList { get; set; } = new List<TLeagueMemberInstance>(8);

        public Guid LeagueId { get; set; }

        public TypeLeagueInfo Info { get; set; }

       // public PlayerCashBalance Cash { get; set; }
    }
}
