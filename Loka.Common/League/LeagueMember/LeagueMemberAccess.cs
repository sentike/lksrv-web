﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Server.LeagueMember
{
    public enum LeagueMemberAccess
    {
        Member,
        Commander,
        Moderator,
        Leader,
    }
}
