﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.League;

namespace Loka.Server.LeagueMember
{
    public abstract class LeagueMemeberInstance<TLeagueInstance, TPlayerInstance>
    {
        //public Guid MemberId { get; set; }

        public Guid LeagueId { get; set; }

        public TLeagueInstance LeagueEnity { get; set; }

        public Guid PlayerId { get; set; }

        public virtual TPlayerInstance PlayerEntity { get; set; }

        public DateTime JoinDate { get; set; }

        public LeagueMemberAccess Access { get; set; }
        public bool IsMember => Access >= LeagueMemberAccess.Member;
        public bool IsCommander => Access >= LeagueMemberAccess.Commander;
        public bool IsModerator => Access >= LeagueMemberAccess.Moderator;
        public bool IsLeader => Access >= LeagueMemberAccess.Leader;
    }
}
