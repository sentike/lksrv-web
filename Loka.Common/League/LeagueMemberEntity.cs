﻿using System.Data.Entity.ModelConfiguration;
using Loka.Server.LeagueMember;
using Loka.Server.Player.Models;

namespace Loka.Server.League.Models
{
    public class LeagueMemberEntity : LeagueMemeberInstance<LeagueEntity, PlayerEntity>
    {
        public class Configuration : EntityTypeConfiguration<LeagueMemberEntity>
        {
            public Configuration()
            {
                HasKey(p => p.PlayerId);
                ToTable("league.LeagueMemberEntity");
                HasRequired(l => l.LeagueEnity).WithMany(l => l.MemberList).HasForeignKey(l => l.LeagueId).WillCascadeOnDelete(true);
            }
        }
    }
}
