﻿using System.Data.Entity.ModelConfiguration;

namespace Loka.Server.League.Models
{
    public class LeagueEntity : LeagueInstance<LeagueMemberEntity>
    {
        protected LeagueEntity() : base()
        {
            
        }

        protected LeagueEntity(string name, string abbr, LeagueAccessType accessType)
            : base(name, abbr, accessType)
        {

        }

        public class Configuration : EntityTypeConfiguration<LeagueEntity>
        {
            public Configuration()
            {
                HasKey(p => p.LeagueId);
                ToTable("league.LeagueEntity");

            }
        }
    }
}
