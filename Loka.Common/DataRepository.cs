﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Reflection;
using System.Web;
using InteractivePreGeneratedViews;
using Loka.Common.Achievements.Container;
using Loka.Common.Achievements.Instance;
using Loka.Common.Building.Player;
using Loka.Common.Building.Template;
using Loka.Common.Chat.Chanel;
using Loka.Common.Cluster;
using Loka.Common.Company.Company;
using Loka.Common.Company.Mission;
using Loka.Common.Company.Task;
using Loka.Common.Improvements.Category;
using Loka.Common.Improvements.Instance;
using Loka.Common.Improvements.Sentence;
using Loka.Common.Improvements.Vote;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.GameMode.Duel;
using Loka.Common.Match.GameMode.OpenWorld;
using Loka.Common.Match.GameMode.PvE;
using Loka.Common.Match.GameMode.PvP;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Common.Player.Achievements;
using Loka.Common.Player.Company;
using Loka.Common.Player.Fraction;
using Loka.Common.Player.Instance;
using Loka.Common.Player.Profile.Item;
using Loka.Common.Player.Profile.Profile;
using Loka.Common.Player.Statistic;
using Loka.Common.Player.Statistic.PvE;
using Loka.Server.League.Models;
using Loka.Server.Models.Account.Promo;
using Loka.Server.Models.Partner;
using Loka.Server.Models.Promo;
using Loka.Server.Models.Queue;
using Loka.Server.Partner;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;
using Loka.Common.Player.Inventory.Service;
using Loka.Common.Player.Worlds;
using Loka.Common.Store;
using Loka.Common.Store.Item.Ammo;
using Loka.Common.Store.Item.Armour;
using Loka.Common.Store.Item.Character;
using Loka.Common.Store.Fraction;
using Loka.Common.Store.Item.Weapon;

using Loka.Common.Store.Item.Addon;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Item.Kit;
using Loka.Common.Store.Item.Modifications;
using Loka.Common.Store.Item.Skin;
using Loka.Common.Store.Order;
using Loka.Common.Store.Property;
using Loka.Common.Store.Service;
using Loka.Common.Transaction;
using Loka.Common.Tutorial;

using VRS.Infrastructure;

namespace Loka.Server.Infrastructure.DataRepository
{

    public class DataRepository : DbContext
    {
        static DataRepository()
        {
            try
            {
                using (var db = new DataRepository())
                {
                    var file = new FileViewCacheFactory($"{HttpRuntime.AppDomainAppPath}/Content/Cache.xml");
                    InteractiveViews.SetViewCacheFactory(db, file);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public DataRepository()
            : base("name=Loka.Server"/*, throwIfV1Schema: false*/)
        {
           //Configuration.AutoDetectChangesEnabled = true;
           //Configuration.ProxyCreationEnabled = true;
           //Configuration.LazyLoadingEnabled = true;
           // Configuration.ValidateOnSaveEnabled = false;
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                SaveChanges();
            }
            catch (Exception e)
            {
                LoggerContainer.DataBaseLogger.WriteExceptionMessage($"[DataRepository][Dispose][StackTrace: {Environment.StackTrace}]", e);
            }

            base.Dispose(disposing);
        }

        public DbSet<ChatChanelEntity> ChatChanelEntities { get; set; }
        public DbSet<ChatChanelMemberEntity> ChatChanelMemberEntities { get; set; }
        public DbSet<ChatChanelMessageEntity> ChatChanelMessageEntities { get; set; }


        //==============================================================================================================================================================================
        //  Improvements
        //==============================================================================================================================================================================
        public DbSet<MatchTransactionEntity> MatchTransactionEntities { get; set; }
        public DbSet<OrderTransactionEntity> OrderTransactionEntities { get; set; }
        public DbSet<StoreTransactionEntity> StoreTransactionEntities { get; set; }



        public DbSet<ImprovementLocalizationEntity> ImprovementLocalizationEntities { get; set; }
        public DbSet<ImprovementVoteEntity> ImprovementVoteEntities { get; set; }
        public DbSet<ImprovementEntity> ImprovementEntities { get; set; }


        public DbSet<SentenceVoteEntity> SentenceVoteEntities { get; set; }
        public DbSet<SentenceEntity> SentenceEntities { get; set; }
        public DbSet<ScenarioCompanyEntity> ScenarioCompanyEntities { get; set; }
        public DbSet<ScenarioMissionEntity> ScenarioMissionEntities { get; set; }
        public DbSet<ScenarioTaskEntity> ScenarioTaskEntities { get; set; }
        public DbSet<PlayerMissionEntity> PlayerMissionEntities { get; set; }
        public DbSet<PlayerWorlEntity> PlayerWorlEntities { get; set; }



        public readonly object SyncRoot = new object();
        public DbSet<MatchEntity> MatchEntity { get; set; }
        public DbSet<MatchMemberEntity> MatchMember { get; set; }
        public DbSet<MatchTeamEntity> MatchTeam { get; set; }

        public DbSet<PlayerGameSessionEntity> GameSessionEntity { get; set; }

        public DbSet<ClusterInstance> ClusterInstance { get; set; }


        public DbSet<QueueEntity> QueueEntity { get; set; }
        public DbSet<SquadInviteEntity> SquadInviteEntities { get; set; }

        //public DbSet<QueueMember> QueueMember { get; set; }


        public DbSet<GameMapEntity> GameMapEntity { get; set; }
        public DbSet<GameModeEntity> GameModeEntity { get; set; }
        public DbSet<PlayerPvEMatchStatisticEntity> PvEMatchStatisticEntities { get; set; }

        public DbSet<PartnerAccountEntity> PartnerAccount { get; set; }
        public DbSet<PartnerCodeEntity> PartnerCode { get; set; }


        public DbSet<PromoItemEntity> PromoItem { get; set; }


        //==============================================================================================================================================================================
        //  Store
        //==============================================================================================================================================================================

        public DbSet<ItemInstanceEntity> StoreItemInstance { get; set; }
	    public DbSet<ItemInstancePropertyEntity> StoreItemProperties { get; set; }
		public DbSet<FractionEntity> StoreFractionEntities { get; set; }

        //==============================================================================================================================================================================
        //                                  [ Player ]
        //==============================================================================================================================================================================
        public DbSet<WorldBuildingItemEntity> BuildingItemEntities { get; set; }
        public DbSet<WorldBuildingPropertyEntity> BuildingPropertyEntities { get; set; }


        public DbSet<BaseTemplateEntity> BaseTemplateEntities { get; set; }
        public DbSet<PlayerTemplateEntity> PlayerTemplateEntities { get; set; }
        public DbSet<TemplateBuildingEntity> TemplateBuildingEntities { get; set; }
        public DbSet<TemplateBuildingAttachEntity> TemplateBuildingAttachEntities { get; set; }

        public DbSet<TutorialInstanceEntity> TutorialInstanceEntities { get; set; }
        public DbSet<TutorialStepEntity> TutorialStepEntities { get; set; }
        public DbSet<CharacterProfileTemplate> CharacterProfileTemplates { get; set; }

        public DbSet<PlayerCashEntity> PlayerCashEntities { get; set; }

        public DbSet<PlayerEntity> AccountPlayerEntity { get; set; }
        public DbSet<PlayerFriendEntity> PlayerFriendEntity { get; set; }
        public DbSet<PlayerPromoCodeEntity> PlayerPromoCode { get; set; }
        public DbSet<PlayerProfileItemEntity> PlayerProfileItem { get; set; }

        public DbSet<SocialGroupEntity> SocialGroupEntity { get; set; }
        public DbSet<SocialTaskEntity> SocialTaskEntity { get; set; }

        public DbSet<PlayerSocialTaskEntity> PlayerSocialTaskEntity { get; set; }



        public DbSet<PlayerProfileEntity> AccountPlayerPreSetEntity { get; set; }

        public DbSet<PlayerInventoryItemEntity> PlayerInventortItem { get; set; }

        public DbSet<AbstractTargetPlayerItem> PlayerTargetItem { get; set; }
        public DbSet<PlayerAddonAttachedEntity> PlayerInstalledAddon { get; set; }
        public DbSet<PlayerTutorialEntity> PlayerTutorialEntities { get; set; }


        public DbSet<OrderDeliveryEntity> OrderDeliveryEntities { get; set; }


        //==============================================================================================================================================================================
        //                                  [ League ]
        //==============================================================================================================================================================================
        public DbSet<LeagueEntity> LeagueEntity { get; set; }
        public DbSet<LeagueMemberEntity> LeagueMemeberEntity { get; set; }


        public DbSet<AchievementContainerEntity> Achievements { get; set; }
        public DbSet<AchievementInstanceEntity> AchievementInstances { get; set; }
        public DbSet<PlayerAchievementContainerEntity> PlayerAchievement { get; set; }
        public static DataRepository Create()
        {
            return new DataRepository();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(DataRepository)));
        }

        //public override int SaveChanges()
        //{
        //    foreach (var entry in ChangeTracker.Entries<IVersionedEntity>().ToList())
        //    {
        //        if (entry.State == EntityState.Unchanged)
        //        {
        //            continue;
        //        }
        //        entry.Entity.RowVersion = Guid.NewGuid();
        //    }
        //
        //    return base.SaveChanges();
        //}
    }

}
