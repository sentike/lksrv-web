﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Models.Promo;
using Loka.Server.Player.Models;

namespace Loka.Server.Models.Account.Promo
{
    public enum SocialRepostState
    {
        None,
        Done,
        NotDone,
        Deleted,
        Confirmed,
    }

    public class PlayerSocialTaskEntity
    {
        public Guid PlayerTaskId { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public Guid SocialTaskId { get; set; }
        public virtual SocialTaskEntity SocialTaskEntity { get; set; }

        public DateTime RepostDate { get; set; }
        public SocialRepostState State { get; set; }

        public DateTime? ConfirmDate { get; set; }

        public PlayerSocialTaskEntity() { }

        public PlayerSocialTaskEntity(Guid playerId, Guid socialTaskId)
        {
            PlayerTaskId = Guid.NewGuid();
            PlayerId = playerId;
            SocialTaskId = socialTaskId;
            RepostDate = DateTime.UtcNow;
            ConfirmDate = DateTime.UtcNow.AddMinutes(9);
        }


        public class Configuration : EntityTypeConfiguration<PlayerSocialTaskEntity>
        {
            public Configuration()
            {
                HasKey(p => p.PlayerTaskId);
                ToTable("players.PlayerSocialTaskEntity");

                HasRequired(f => f.SocialTaskEntity).WithMany().HasForeignKey(f => f.SocialTaskId).WillCascadeOnDelete(true);
            }
        }
    }
}
