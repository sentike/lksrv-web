﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Server.Player.Models
{
    public class PlayerPromoCodeEntity
    {
        public Guid PlayerCodeId { get; set; }
        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public Guid CodeId { get; set; }
        public DateTime ActivationDate { get; set; }

        public PlayerPromoCodeEntity()
        {
            
        }

        public PlayerPromoCodeEntity(Guid codeId)
        {
            PlayerCodeId = Guid.NewGuid();
            ActivationDate = DateTime.UtcNow;
            CodeId = codeId;
        }

        public class Configuration : EntityTypeConfiguration<PlayerPromoCodeEntity>
        {
            public Configuration()
            {
                HasKey(p => p.PlayerCodeId);
                ToTable("players.PlayerPromoCode");

            }
        }
    }
}
