﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Player.Instance
{
    [ComplexType]
    public class PlayerWeekBonus
    {
        public const int BonusAmount = 100;
        public static readonly TimeSpan NotifyPeriod = TimeSpan.FromHours(6);
        public DateTime JoinDate { get; set; }
        public DateTime NotifyDate { get; set; }
        public byte JoinCount { get; set; }

        public bool Calculate()
        {
            if ((DateTime.UtcNow.Date - JoinDate) > TimeSpan.FromDays(0.7))
            {
                JoinCount = 0;
            }
            else if(DateTime.UtcNow > JoinDate && JoinDate.Date != DateTime.UtcNow.Date)
            {
                JoinCount++;
                JoinDate = DateTime.UtcNow;
                return true;
            }
            JoinDate = DateTime.UtcNow;
            return false;
        }
    }
}
