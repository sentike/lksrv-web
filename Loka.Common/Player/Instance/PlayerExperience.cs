﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Player.Instance
{
    [ComplexType]
    public class PlayerExperience
    {
        /// <summary>
        /// Player Account Level 
        /// </summary>
        public int Level { get; set; }

        public long Experience { get; set; }

        public long NextExperience => Convert.ToInt64(1500L + 9500L * Level);

        /// <summary>
        /// The experience gained during the tournament
        /// </summary>
        public long WeekExperience { get; set; }

        /// <summary>
        /// The time spent in the game during the tournament
        /// </summary>
        public long WeekActivityTime { get; set; }

        public TimeSpan WeekActivity => TimeSpan.FromSeconds(WeekActivityTime);


        public void Calculate()
        {
            while (Experience >= NextExperience)
            {
                Experience -= NextExperience;
                Level++;
            }
        }

        public bool IsAccess(int level) => Level >= level;
    };


}
