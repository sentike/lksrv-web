﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Loka.Common.Building.Player;
using Loka.Common.Building.Template;
using Loka.Common.Cluster;
using Loka.Common.Improvements.Sentence;
using Loka.Common.Improvements.Vote;
using Loka.Common.Infrastructure;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Player.Achievements;
using Loka.Common.Player.Company;
using Loka.Common.Player.Fraction;
using Loka.Common.Player.Instance;
using Loka.Common.Player.Profile.Profile;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Player.Statistic;
using Loka.Common.Player.Statistic.Global;
using Loka.Common.Player.Statistic.Match;
using Loka.Common.Player.Statistic.PvE;
using Loka.Common.Store;
using Loka.Common.Store.Fraction;

using Loka.Player;
using Loka.Server.League.Models;
using Loka.Server.Models.Account;
using Loka.Server.Models.Account.Promo;
using Loka.Server.Models.Queue;
using Loka.Common.Player.Inventory;
using Loka.Common.Player.Inventory.Service;
using Loka.Common.Player.Profile.Item;
using Loka.Common.Player.Worlds;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Item.Weapon;
using Loka.Common.Store.Property;
using Loka.Common.Tutorial;
using Loka.Infrastructure;

using Loka.Server.Infrastructure.Migration;

using VRS.Infrastructure;
using VRS.Infrastructure.Environment;

namespace Loka.Server.Player.Models
{
    public enum AccountMigrationAction
    {
        None,
        AddNotExistProfilesAndCharacters,
        AddBonus500Money,
        CreateCashEntities
    }

    [Flags]
    public enum PlayerRole : short
    {
        None = 0,
        Partner = 1,
        Moderator = 2,
        Administrator = 4,
        Developer = 8,
        BetaTester = 16,
    }

    [SuppressMessage("ReSharper", "ClassWithVirtualMembersNeverInherited.Global")]
    public class PlayerEntity : IVersionedEntity
    {
        public Guid PlayerId { get; set; }
        public string PlayerName { get; set; }
        public DateTime? PremiumEndDate { get; set; }
        public PlayerExperience Experience { get; set; }
        public PlayerWeekBonus WeekBonus { get; set; }
        public PlayerRole Role { get; set; }

        public void HarvestCash(long amount)
        {
            ForcePay(GameCurrency.Money, -amount);
        }

        public DateTime RegistrationDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public LanguageTypeId Language { get; set; }


        public AccountMigrationAction MigrationAction { get; set; }
        public AccountMigrationAction NextMigrationAction => MigrationAction + 1;
        public Guid LastSessionId { get; set; }

        //public virtual ApplicationUser User { get; set; }
        public bool IsOnline => (DateTime.UtcNow - LastActivityDate).TotalSeconds < 10;

        #region Inventory
        public virtual ICollection<PlayerInventoryItemEntity> InventoryItemList { get; } = new List<PlayerInventoryItemEntity>(64);
        public IReadOnlyList<PlayerInventoryItemEntity> InventoryWeaponList => InventoryItemList.Where(w => w.CategoryTypeId == CategoryTypeId.Weapon).ToList();
        public IReadOnlyList<PlayerInventoryItemEntity> InventoryArmourList => InventoryItemList.Where(w => w.CategoryTypeId == CategoryTypeId.Armour).ToList();
        public IReadOnlyList<PlayerInventoryItemEntity> CharacterList => InventoryItemList.Where(w => w.CategoryTypeId == CategoryTypeId.Character).ToList();
        public IReadOnlyList<PlayerInventoryItemEntity> KitList => InventoryItemList.Where(w => w.CategoryTypeId == CategoryTypeId.Kit).ToList();
        public IReadOnlyList<PlayerInventoryItemEntity> GrenadeList => InventoryItemList.Where(w => w.CategoryTypeId == CategoryTypeId.Grenade).ToList();
        public IReadOnlyList<PlayerInventoryItemEntity> ServiceList => InventoryItemList.Where(w => w.CategoryTypeId == CategoryTypeId.Service).ToList();
        public IReadOnlyList<PlayerInventoryItemEntity> BuildingList => InventoryItemList.Where(w => w.CategoryTypeId == CategoryTypeId.Building).ToList();

        public virtual ICollection<PlayerPvEMatchStatisticEntity> PvEMatchStatisticEntities { get; set; } = new List<PlayerPvEMatchStatisticEntity>(10); 
        public virtual ICollection<PlayerUsingServiceEntity> UsingServiceEntities { get; set; } = new List<PlayerUsingServiceEntity>(3);
        public virtual ICollection<ImprovementVoteEntity> VoteEntities { get; set; } = new List<ImprovementVoteEntity>(32);
        public virtual ICollection<SentenceVoteEntity> SentenceVoteEntities { get; set; } = new List<SentenceVoteEntity>(32);
        public virtual ICollection<SentenceEntity> SentenceEntities { get; set; } = new List<SentenceEntity>(1);
        public bool IsAllowVoteImprovement => VoteEntities.Any(v => v.VoteDate >= DateTimeHelper.StartOfWeek(DateTime.UtcNow)) == false;
        public bool IsDonatePushSentence => SentenceEntities.Any(v => v.CreatedDate >= DateTimeHelper.StartOfWeek(DateTime.UtcNow));

        public bool IsAllowPushProposal => DateOfNextAvailableProposal() == 0;

        public long DateOfNextAvailableProposal()
        {
            var sentence = SentenceEntities.OrderByDescending(s => s.CreatedDate).FirstOrDefault();
            if (sentence == null) return 0;

            var deltaTime = DateTime.UtcNow - sentence.CreatedDate;
            if (deltaTime >= TimeSpan.FromHours(1)) return 0;
            return DateTime.UtcNow.AddSeconds(60 * 60 - deltaTime.TotalSeconds).ToUnixTime();
        }
        
        public void FillEmptyProfilesByWeapons()
        {
            foreach (var profile in InventoryProfileList)
            {
                if (profile.CharacterItemEntity == null)
                {
                    profile.CharacterItemEntity = CharacterList.First();
                }

                var character = profile.CharacterItemEntity;
                var templates = character.ItemModelEntity.CharacterProfileTemplates;
                foreach (var template in templates)
                {
                    if (profile.ItemSlotDictionary.ContainsKey(template.ProfileSlot) == false)
                    {
                        var item = InventoryItemList.SingleOrDefault(p => p.CategoryTypeId == template.ItemCategoryId 
                            && p.ModelId == template.ItemModelId);

                        if (item != null)
                        {
                            profile.Items.Add(PlayerProfileItemEntity.Factory(item, template.ProfileSlot));
                        }
                    }
                    else
                    {
                        var slot = profile.ItemSlotDictionary[template.ProfileSlot];
                        if (slot.InventoryItemEntity.ItemModelEntity == null)
                        {
                            var item = InventoryItemList.SingleOrDefault(p => p.CategoryTypeId == template.ItemCategoryId
                                                                              && p.ModelId == template.ItemModelId);
                            if (item != null)
                            {
                                slot.ItemId = item.ItemId;
                                slot.InventoryItemEntity = item;
                            }
                        }
                    }
                }

            }

            //foreach (var profile in InventoryProfileList)
            //{
            //    if (profile.CharacterItemEntity == null)
            //    {
            //        profile.CharacterItemEntity = CharacterList.FirstOrDefault();
            //    }
            //
            //
            //    Debug.Assert(profile.CharacterItemEntity != null, "profile.CharacterEntity != null");
            //
            //    var avalibleWeapons = InventoryWeaponList.Where(i => i.ItemModelEntity != null && i.ItemModelEntity.IsAllowCharacter(profile.CharacterItemEntity.ModelId) && i.ItemModelEntity.Level <= profile.Level + 1).ToArray();
            //    if (profile.ItemSlotDictionary.ContainsKey(PlayerProfileInstanceSlotId.PrimaryWeapon) == false)
            //    {
            //        var primaryWeapon = avalibleWeapons.OrderByDescending(i => i.ItemModelEntity.Level).FirstOrDefault();
            //        if (primaryWeapon != null)
            //        {
            //            profile.Equip(primaryWeapon, PlayerProfileInstanceSlotId.PrimaryWeapon);
            //        }
            //
            //        if (profile.ItemSlotDictionary.ContainsKey(PlayerProfileInstanceSlotId.SecondaryWeapon) == false)
            //        {
            //            var primaryWeaponId = primaryWeapon?.ModelId;
            //            var weapon = avalibleWeapons.LastOrDefault(i => i.ModelId != primaryWeaponId);
            //            if (weapon != null)
            //            {
            //                profile.Equip(weapon, PlayerProfileInstanceSlotId.SecondaryWeapon);
            //            }
            //        }
            //    }
            //    
            //}
        }

        public virtual ICollection<PlayerProfileEntity> InventoryProfileList { get; } =  new List<PlayerProfileEntity>(5);
        public virtual ICollection<PlayerAchievementContainerEntity> AchievementList { get; } = new List<PlayerAchievementContainerEntity>(32);
        #endregion

        //==================================================================================================================
        //  Cash

        public const long DefaultMoneyAmount = 10000;
        public const long DefaultMoneyLimit = 10000;
        public long MoneyLimit { get; set; } = DefaultMoneyLimit;

        public virtual ICollection<PlayerCashEntity> CashEntities { get; private set; } = new List<PlayerCashEntity>(8);

        public PlayerCashEntity TakeCash(GameCurrency currency)
        {
            var cash = CashEntities.FirstOrDefault(p => p.Currency == currency);
            if (cash == null)
            {
                CashEntities.Add(cash = new PlayerCashEntity(currency, 0));
            }
            return cash;
        } 

        public PlayerCashEntity TakeCash(StoreItemCost cost) => TakeCash(cost.Currency);
        public long AvalibleCash() => MoneyLimit - TakeCash(GameCurrency.Money).Amount;

        public PlayerCashEntity CashMoney => TakeCash(GameCurrency.Money);
        public PlayerCashEntity CashDonate => TakeCash(GameCurrency.Donate);
        public PlayerCashEntity CashCoin => TakeCash(GameCurrency.Coin);

        public bool Pay(StoreItemCost cost)
        {
            return TakeCash(cost).Pay(cost);
        }

	    public bool Pay(GameCurrency currency, long amount)
	    {
		    return TakeCash(currency).Pay(amount);
	    }

		public long Count(StoreItemCost cost)
        {
            return TakeCash(cost).AvalibleCount(cost);
        }

        public void ForcePay(StoreItemCost cost)
        {
            TakeCash(cost).ForcePay(cost);
        }

        public void ForcePay(GameCurrency currency, long amount)
        {
            TakeCash(currency).Give(amount);
        }

        public void Give(GameCurrency currency, long amount)
        {
            TakeCash(currency).Give(amount);
        }

        public void Give(StoreItemCost amount)
        {
            TakeCash(amount).Give(amount);
        }

        public bool IsAllowPay(StoreItemCost cost)
        {
            return TakeCash(cost).IsAllowPay(cost);
        }

        public bool IsAllowPay(GameCurrency currency, long cost)
        {
            return TakeCash(currency).IsAllowPay(cost);
        }

        //==================================================================================================================
        public virtual ICollection<PlayerFriendEntity> FriendList { get; } = new List<PlayerFriendEntity>(5);

        public virtual ICollection<PlayerPromoCodeEntity> PromoCodes { get; } = new List<PlayerPromoCodeEntity>(8); 

        public virtual ICollection<PlayerSocialTaskEntity> SocialTaskList { get; } = new List<PlayerSocialTaskEntity>(16);
        public virtual ICollection<MatchMemberEntity> MatchMembers { get; } = new List<MatchMemberEntity>(1000);

        public virtual ICollection<PlayerMissionEntity> MissionEntities { get; } = new List<PlayerMissionEntity>(16);

        #region SquadInvites

        /// <summary>
        /// Incoming squad or duel invite list
        /// </summary>
        public virtual List<SquadInviteEntity> IncomingSquadInvites { get; } = new List<SquadInviteEntity>(1);

        /// <summary>
        /// Outgoing squad or duel invite list
        /// </summary>
        public virtual List<SquadInviteEntity> OutgoingSquadInvites { get; } = new List<SquadInviteEntity>(1);
        #endregion

        public Guid? CurrentTutorialId { get; set; }
        public virtual PlayerTutorialEntity CurrentTutorialEntity { get; set; }
        public virtual ICollection<PlayerTutorialEntity> PlayerTutorialEntities { get; } = new List<PlayerTutorialEntity>(16);


        //-------------------------------------------------------------------------------------------------------------------------------

        #region Fraction & Reputation
        /// <summary>
        /// List of fraction reputation & progress
        /// </summary>
        public virtual ICollection<PlayerFractionReputationEntity> ReputationList { get; set; } = new HashSet<PlayerFractionReputationEntity>();

        public Guid? CurrentFractionId { get; set; }
        public virtual PlayerFractionReputationEntity CurrentFractionEntity { get; set; }

        public void GiveReputaion(FractionTypeId fractionTypeId, long reputation)
        {
            var fraction = ReputationList.SingleOrDefault(f => f.FractionId == fractionTypeId);
            if (fraction == null)
            {
                ReputationList.Add(fraction = new PlayerFractionReputationEntity()
                {
                    FractionId = fractionTypeId,
                    Reputation = reputation,
                    ReputationId = Guid.NewGuid()
                });
            }
            else
            {
                fraction.Reputation += reputation;
            }
            fraction.Update();
        }

        #endregion

        public virtual LeagueMemberEntity LeagueMemberEntity { get; set; }

        public Guid? FirstPartnerId { get; set; }

        public PlayerStatisticEntity Statistic { get; set; }
        //public virtual List<PlayerPerDayStatisticEntity> StatisticPerDay { get; set; }

        //public int LastTournamentPosition { get; set; }

        [NotMapped]
        public int ArmourLevel => InventoryProfileList.Where(s => s.IsEnabled).Select(s => s.Level).DefaultIfEmpty(0).Max();

        public virtual QueueEntity QueueEntity { get; set; }
        public bool IsSquadLeader => QueueEntity != null && QueueEntity.IsLeader;
        public DateTime LastAttackedDate { get; set; }

        public IEnumerable<PlayerProfileEntityContainer> GetProfileContainers() => InventoryProfileList.OrderBy(p => p.ProfileSlotId).Select(playerPreSet => new PlayerProfileEntityContainer(playerPreSet)
        {
            Items = playerPreSet.Items.Select(i => new PlayerInventoryItemContainer(i.ItemId, i.SlotId)).ToArray(),
        }).ToArray();

        [SuppressMessage("ReSharper", "ConvertIfStatementToConditionalTernaryExpression")]
        public void UpdateLastAttackDate(bool defender)
        {
            LastAttackedDate = DateTime.UtcNow.AddMinutes(1);

            //if (defender)
            //{
            //    //  Защита от нападения в зависимости от уровня экипировки
            //    LastAttackedDate = NextPlayerSafeTime();
            //}
            //else
            //{
            //    //  Защита от нападения во время нападения =)
            //    LastAttackedDate = DateTime.UtcNow.AddMinutes(5);
            //}
        }

        public DateTime NextPlayerSafeTime()
        {
            if(ArmourLevel <= 1) return DateTime.UtcNow.AddHours(8);
            if(ArmourLevel <= 3) return DateTime.UtcNow.AddHours(4);
            if(ArmourLevel <= 5) return DateTime.UtcNow.AddHours(2);
            return DateTime.UtcNow.AddHours(2);
        }

        public static PlayerEntity Factory(Guid playerId, string playerName)
        {
            return new PlayerEntity
            {
                PlayerId = playerId,
                PlayerName = playerName,
                MigrationAction = AccountMigrationTool.LastMigrationVersion,
                Experience = new PlayerExperience
                {
                    Experience = 0,
                    Level = 0
                },

                WeekBonus = new PlayerWeekBonus()
                {
                    JoinDate = DateTime.UtcNow,
                    JoinCount = 1
                },

                CashEntities = new List<PlayerCashEntity>()
                {
                    new PlayerCashEntity(GameCurrency.Money, DefaultMoneyAmount),
                    new PlayerCashEntity(GameCurrency.Donate, 10),
                    new PlayerCashEntity(GameCurrency.Coin, 0),
                },

                //  Защита новичков на 8 часов от нападений
                LastAttackedDate = DateTime.UtcNow.AddHours(8),
                RegistrationDate = DateTime.UtcNow,
                LastActivityDate = DateTime.UtcNow,
                PremiumEndDate = DateTime.UtcNow.AddDays(3),
                SubscribeEndDate = DateTime.UtcNow.AddDays(-7),
                //SubscribeEndDate = null,    // TEMP: For devs
                ReputationList = new HashSet<PlayerFractionReputationEntity>()
                {
                    PlayerFractionReputationEntity.Factory(playerId, FractionTypeId.Keepers),
                    PlayerFractionReputationEntity.Factory(playerId, FractionTypeId.RIFT),
                    PlayerFractionReputationEntity.Factory(playerId, FractionTypeId.Keepers_Friend),
                    PlayerFractionReputationEntity.Factory(playerId, FractionTypeId.RIFT_Friend),
                },
                Statistic = new PlayerStatisticEntity(),
                WorldBuildingEntities = new List<WorldBuildingItemEntity>()
                {
                    WorldBuildingItemEntity.Factory(BuildingModelId.LobbyMainBuilding)
                }
                //QueueEntity = QueueEntity.Factory(playerId, new SearchSessionOptions())
                //LastTournamentPosition = -1
            };
        }

        public override string ToString()
        {
            return $"{PlayerId}: {PlayerName} [{Experience.Level}] | arm: {ArmourLevel}";
        }

        public void UpdateProfileStatus()
        {
            if (PremiumEndDate.HasValue || PremiumEndDate < DateTime.UtcNow)
            {
                foreach (var profile in InventoryProfileList.Where(p => p.IsUnlocked == false))
                {
                    profile.IsEnabled = false;
                }
            }
        }


        public PlayerFriendStatus Status()
        {
            PlayerFriendStatus status;
	        if (QueueEntity == null)
	        {
		        if (IsOnline)
		        {
			        status = PlayerFriendStatus.Online;
		        }
		        else
		        {
					status = PlayerFriendStatus.Offline;
				}
			}
            else
            {
	            if (IsOnline)
	            {
		            if (QueueEntity.Ready.HasFlag(QueueState.Searching))
		            {
			            status = PlayerFriendStatus.SearchGame;
		            }
		            else
		            {
			            var match = QueueEntity.CurrentMatchMember;
			            if (match?.MatchEntity == null)
			            {
							status = PlayerFriendStatus.Online;
						}
						else if (match.MatchEntity.HasBuildingGameMode)
			            {
							status = PlayerFriendStatus.Building;
						}
			            else
			            {
							status = PlayerFriendStatus.PlayGame;
						}
					}
	            }
	            else
	            {
					status = PlayerFriendStatus.Offline;
				}
			}
            return status;
        }

        public Guid? CurrentHoverBoardItemId { get; set; }
        public virtual PlayerInventoryItemEntity CurrentHoverBoardItemEntity { get; set; }

        public Guid? CurrentPlayerWorldId { get; set; }
        public virtual PlayerWorlEntity CurrentPlayerWorldEntity { get; set; }

        public virtual ICollection<PlayerWorlEntity> PlayerWorlEntities { get; set; } = new List<PlayerWorlEntity>(4);
        public virtual ICollection<WorldBuildingItemEntity> WorldBuildingEntities { get; set; } = new List<WorldBuildingItemEntity>(1024);
        public virtual ICollection<BaseTemplateEntity> TemplateEntities { get; } = new List<BaseTemplateEntity>(8);
        public virtual ICollection<PlayerTemplateEntity> PlayerTemplateEntities { get; } = new List<PlayerTemplateEntity>(8);
        

        public IEnumerable<WorldBuildingStorageView> UpdatePlayerStorage()
        {
            var result = new List<WorldBuildingStorageView>();
            //====================================================================
            {
                var starages = WorldBuildingEntities.Where(p => p.ModelEntity.Flags.HasFlag(ItemInstanceFlags.Miner)).ToArray();
                foreach (var s in starages)
                {
                    //============================================================
                    var storage = s.Get<long>(ItemInstancePropertyName.Storage);

                    //============================================================
                    if (storage == null)
                    {
                        continue;
                    }

                    //============================================================
                    var delta = DateTime.UtcNow - storage.Property.EditDate;
                    if(delta.TotalDays > 3)
                    {
                        delta = TimeSpan.Zero;
                    }

                    var elapsed = Math.Max(0, delta.TotalHours);
                    var income = elapsed * s.ModelEntity.Get<long>(ItemInstancePropertyName.Perfomance).Default;
                    var amount = Convert.ToInt64(income);


                    //============================================================
                    storage.Property.Value += amount;
                    storage.Property.Value = Math.Min(Math.Max(storage.Instance.Value, 1000), storage.Property.Value);
                    
                    //============================================================
                    storage.Property.EditDate = DateTime.UtcNow;

                    //============================================================
                    result.Add(s.AsStorageView());
                }
            }

            //====================================================================
            {
                MoneyLimit = 0;
                var money = TakeCash(GameCurrency.Money).Amount - MoneyLimit;
                var starages = WorldBuildingEntities.Where(p => p.ModelEntity.Flags.HasFlag(ItemInstanceFlags.Storage)).ToArray();
                foreach (var s in starages)
                {                  
                    //============================================================
                    var storage = s.Get<long>(ItemInstancePropertyName.Storage);
                    
                    //============================================================
                    if (storage == null)
                    {
                        continue;
                    }

                    //====================================
                    var capacity = storage.Instance.Value;
                    MoneyLimit += capacity;

                    //====================================
                    if (money > 0)
                    {
                        var delta = money - capacity;
                        money -= capacity;

                        if (delta > 0)
                        {
                            storage.Property.Value = capacity;
                        }
                        else
                        {
                            storage.Property.Value = capacity + money;
                        }
                    }
                    else
                    {
                        storage.Property.Value = 0;
                    }

                    //====================================
                    result.Add(s.AsStorageView());
                }
            }

            return result;
        }

        public Guid RowVersion { get; set; }
        public DateTime? SubscribeEndDate { get; set; }

        public class Configuration : EntityTypeConfiguration<PlayerEntity>
        {
            public Configuration()
            {
                HasKey(p => p.PlayerId);
                ToTable("players.PlayerEntity");
                HasOptional(f => f.CurrentPlayerWorldEntity).WithMany().HasForeignKey(f => f.CurrentPlayerWorldId);

                HasOptional(f => f.CurrentHoverBoardItemEntity).WithMany().HasForeignKey(f => f.CurrentHoverBoardItemId);


                HasOptional(l => l.LeagueMemberEntity).WithRequired(l => l.PlayerEntity).WillCascadeOnDelete(true);
                HasOptional(f => f.CurrentFractionEntity).WithMany().HasForeignKey(f => f.CurrentFractionId);
                HasOptional(f => f.CurrentTutorialEntity).WithMany().HasForeignKey(f => f.CurrentTutorialId);
                HasMany(p => p.PlayerTutorialEntities).WithRequired(p => p.PlayerEntity).HasForeignKey(m => m.PlayerId).WillCascadeOnDelete(true);
                HasMany(p => p.CashEntities).WithRequired(p => p.PlayerEntity).HasForeignKey(m => m.PlayerId).WillCascadeOnDelete(true);


                HasMany(i => i.InventoryItemList).WithRequired(i => i.PlayerInstance).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);
                HasMany(i => i.AchievementList).WithRequired(i => i.PlayerInstance).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);

                HasMany(i => i.PromoCodes).WithRequired(i => i.PlayerEntity).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);

                HasMany(i => i.PvEMatchStatisticEntities).WithRequired(i => i.PlayerEntity).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);
                HasMany(i => i.SocialTaskList).WithRequired(i => i.PlayerEntity).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);
                HasMany(p => p.MatchMembers).WithRequired(m => m.PlayerEntity).HasForeignKey(m => m.PlayerId).WillCascadeOnDelete(true);
                Property(p => p.PlayerName).HasMaxLength(64);

                HasMany(p => p.VoteEntities).WithRequired(p => p.PlayerEntity).HasForeignKey(m => m.PlayerId).WillCascadeOnDelete(true);
                HasMany(p => p.SentenceVoteEntities).WithRequired().HasForeignKey(m => m.PlayerId).WillCascadeOnDelete(true);
                HasMany(p => p.MissionEntities).WithRequired(p => p.PlayerEntity).HasForeignKey(m => m.PlayerId).WillCascadeOnDelete(true);
                HasMany(p => p.WorldBuildingEntities).WithRequired(p => p.PlayerEntity).HasForeignKey(m => m.PlayerId).WillCascadeOnDelete(true);
                HasMany(p => p.TemplateEntities).WithRequired(p => p.CreatorEntity).HasForeignKey(m => m.CreatorId).WillCascadeOnDelete(true);
                HasMany(p => p.PlayerTemplateEntities).WithRequired(p => p.PlayerEntity).HasForeignKey(m => m.PlayerId).WillCascadeOnDelete(true);

                Property(p => p.RowVersion).IsConcurrencyToken(false);  
            }
        }
    }
}
