﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Statistic.Match;
using Loka.Server.Player.Models;

namespace Loka.Common.Player.Statistic.Global
{
    public class PlayerPerDayStatisticEntity
    {
        public Guid PlayerStatisticId { get; set; }
        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public DateTime CreatedDate { get; set; }
        public PlayerStatisticEntity Statistic { get; set; }
    }
}
