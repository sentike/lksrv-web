﻿using System;
using Loka.Common.Match.GameMode;

namespace Loka.Common.Player.Statistic.PvE
{
    public class PlayerPvEMatchStatisticView
    {
        public PlayerPvEMatchStatisticView()
        {
            
        }

        public PlayerPvEMatchStatisticView(PlayerPvEMatchStatisticEntity entity)
        {
            PlayerId = entity.PlayerId;
            GameModeTypeId = entity.GameModeTypeId;
            Level = entity.Level;
            PlayerName = entity.PlayerEntity?.PlayerName;
            Score = entity.Score;
        }

        public Guid PlayerId { get; set; }
        public string PlayerName { get; set; }
        public GameModeTypeId GameModeTypeId { get; set; }
        public short Level { get; set; }
        public long Score { get; set; }
    }
}
