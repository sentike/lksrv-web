﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Match.GameMode;
using Loka.Server.Player.Models;
using System.Data.Entity.Infrastructure.Annotations;

namespace Loka.Common.Player.Statistic.PvE
{
    public class PlayerPvEMatchStatisticEntity
    {
        public Guid StatisticEntityId { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public GameModeTypeId GameModeTypeId { get; set; }
        public short Level { get; set; }
        public int FriendlyLevel => Level + 1;

        public long Score { get; set; }
        public DateTime PlayDate { get; set; }

        public override string ToString()
        {
            return $"{StatisticEntityId}: {PlayerId} [{PlayerEntity?.PlayerName}] | {GameModeTypeId} [Level: {Level}] | Score: {Score} [{PlayDate}]";
        }

        public static PlayerPvEMatchStatisticEntity Factory(PlayerPvEMatchStatisticView view)
        {
            return new PlayerPvEMatchStatisticEntity
            {
                PlayerId = view.PlayerId,
                Level = view.Level,
                GameModeTypeId = view.GameModeTypeId,
                Score = view.Score,
                PlayDate = DateTime.UtcNow,
                StatisticEntityId = Guid.NewGuid()
            };
        }

        public static PlayerPvEMatchStatisticEntity Factory(PlayerEntity playerEntity, GameModeTypeId gameModeTypeId, short level, long score = 0)
        {
            return new PlayerPvEMatchStatisticEntity
            {
                StatisticEntityId = Guid.NewGuid(),
                PlayerId = playerEntity.PlayerId,
                GameModeTypeId = gameModeTypeId,
                Level = level,
                Score = score,
                PlayDate = DateTime.UtcNow
            };
        }

        public class Configuration : EntityTypeConfiguration<PlayerPvEMatchStatisticEntity>
        {
            public Configuration()
            {
                HasKey(p => p.StatisticEntityId);
                ToTable("players.PlayerPvEMatchStatisticEntity");

                //---------------------------------------------------------------------------------------------
                //  Restriction game mode and level to the player
                {
                    Property(p => p.GameModeTypeId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("GameModeTypeId_PlayerId", 0) {IsUnique = true}));
                    Property(p => p.PlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("GameModeTypeId_PlayerId", 1) {IsUnique = true}));
                    Property(p => p.Level).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("GameModeTypeId_PlayerId", 2) {IsUnique = true}));
                }

                ////---------------------------------------------------------------------------------------------
                ////  Indexing on the game mode and the level
                //{
                //    Property(p => p.GameModeTypeId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("GameModeTypeId_Level", 0)));
                //    Property(p => p.Level).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("GameModeTypeId_Level", 1)));
                //}
                //
                ////---------------------------------------------------------------------------------------------
                ////  Indexing on the game mode, the level and score for level
                //{
                //    Property(p => p.GameModeTypeId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("GameModeTypeId_Score_Level", 0)));
                //    Property(p => p.Score).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("GameModeTypeId_Score_Level", 1)));
                //    Property(p => p.Level).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("GameModeTypeId_Score_Level", 2)));
                //}
            }
        }
    }
}
