﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Statistic.Match;

namespace Loka.Common.Player.Statistic
{
    public enum PlayerThreatLevel
    {
        Green,  //  From 0 to 2
        Bronze, //  From 2 to 4
        Silver, //  From 4 to 5
        Gold    //  From 5 to 7
    }

    public static class ThreatLevelHelper
    {
        public static PlayerThreatLevel GetThreatLevel(this PlayerStatisticEntity statistic)
        {
            if (statistic.Threat <= 2) return PlayerThreatLevel.Green;
            if (statistic.Threat <= 4) return PlayerThreatLevel.Bronze;
            if (statistic.Threat <= 5) return PlayerThreatLevel.Silver;
            return PlayerThreatLevel.Gold;
        }
    }
}
