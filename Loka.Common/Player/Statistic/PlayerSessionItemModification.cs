﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using JetBrains.Annotations;
using Loka.Common.Player.Inventory.Modification;

namespace Loka.Common.Player.Statistic
{
    [ComplexType]
    public class PlayerSessionItemModification
    {
        public PlayerSessionItemModification() { }

        public PlayerSessionItemModification(Guid itemId, PlayerItemModificationEntity[] PlayerItemModificationEntity)
        {
            ItemId = itemId;
            Modification1 = PlayerItemModificationEntity[00];
            Modification2 = PlayerItemModificationEntity[01];
            Modification3 = PlayerItemModificationEntity[02];
           // Modification4 = PlayerItemModificationEntity[03];
           // Modification5 = PlayerItemModificationEntity[04];
        }

        public Guid? ItemId { get; set; }
        public PlayerItemModificationEntity Modification1 { get; set; }
        public PlayerItemModificationEntity Modification2 { get; set; }
        public PlayerItemModificationEntity Modification3 { get; set; }
        //public PlayerItemModificationEntity Modification4 { get; set; }
        //public PlayerItemModificationEntity Modification5 { get; set; }

        public IEnumerable<PlayerItemModificationEntity> Modifications => new PlayerItemModificationEntity[]
        {
            Modification1,Modification2,Modification3//,Modification4,Modification5,
        };

    }
}