﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Player.Statistic.Match
{
    public class PlayerStatisticView : IPlayerStatisticInterface
    {
        public PlayerStatisticView(IPlayerStatisticInterface statistic)
        {
            Kills = statistic.Kills;
            Deads = statistic.Deads;
            Assists = statistic.Assists;
            TeamKills = statistic.TeamKills;

            SmallSeriesKill = statistic.SmallSeriesKill;
            ShortSeriesKill = statistic.ShortSeriesKill;
            LargeSeriesKill = statistic.LargeSeriesKill;
            EpicSeriesKill = statistic.EpicSeriesKill;

            Threat = statistic.Threat;
            ThreatLevel = statistic.ThreatLevel;
            LastThreatLevel = statistic.LastThreatLevel;
            Shots = statistic.Shots;
            Hits = statistic.Hits;
        }

        #region Kills
        //------------------------------------------------------------
        //                            [ Kills ]
        public long Kills { get; set; }
        /// <summary>
        /// Series of 3 Kill
        /// </summary>
        public long SmallSeriesKill { get; set; }

        /// <summary>
        /// Series of 5 Kill
        /// </summary>
        public long ShortSeriesKill { get; set; }

        /// <summary>
        /// Series of 7 Kill
        /// </summary>
        public long LargeSeriesKill { get; set; }

        /// <summary>
        /// Series of more 7 Kill
        /// </summary>
        public long EpicSeriesKill { get; set; }


        //------------------------------------------------------------
        //                            [ Deads ]
        public long Deads { get; set; }

        //------------------------------------------------------------
        //                            [ Assists ]
        public long Assists { get; set; }


        //------------------------------------------------------------
        //                            [ TeamKills ]
        public long TeamKills { get; set; }
        #endregion

        //================================================================================

        #region Damage
        //------------------------------------------------------------
        //                            [ Shots ]
        public long Shots { get; set; }

        //------------------------------------------------------------
        //                            [ Hits ]
        public long Hits { get; set; }


        //------------------------------------------------------------
        //                            [ IncomingDamage ]
        public long IncomingDamage { get; set; }

        //------------------------------------------------------------
        //                            [ OutgoingDamage ]
        public long OutgoingDamage { get; set; }
        #endregion

        //================================================================================

        #region Matches
        public long Wins { get; set; }

        public long Loses { get; set; }

        public long Draws { get; set; }
        #endregion

        //================================================================================

        public double Threat { get; set; }
        public PlayerThreatLevel ThreatLevel { get; set; }
        public PlayerThreatLevel LastThreatLevel { get; set; }

    }
}
