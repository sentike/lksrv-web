﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Loka.Common.Match.Member;


namespace Loka.Common.Player.Statistic.Match
{
    [ComplexType]
    public class PlayerStatisticEntity: IPlayerStatisticInterface
    {
        #region Kills
        //------------------------------------------------------------
        //                            [ Kills ]
        public long Kills { get; set; }
        public double ClampOfKills => Math.Max(Kills, 1.0);
        public double KillsPerMatch => ClampOfKills / CountOfMatches;

        /// <summary>
        /// Series of 3 Kill
        /// </summary>
        public long SmallSeriesKill { get; set; }
        public double ClampOfSmallSeriesKill => Math.Max(SmallSeriesKill, 1.0);
        public double SmallSeriesKillPerMatch => 1 * ClampOfSmallSeriesKill / CountOfMatches;

        /// <summary>
        /// Series of 5 Kill
        /// </summary>
        public long ShortSeriesKill { get; set; }
        public double ClampOfShortSeriesKill => Math.Max(ShortSeriesKill, 1.0);
        public double ShortSeriesKillPerMatch => 3 * ClampOfShortSeriesKill / CountOfMatches;

        /// <summary>
        /// Series of 7 Kill
        /// </summary>
        public long LargeSeriesKill { get; set; }
        public double ClampOfLargeSeriesKill => Math.Max(LargeSeriesKill, 1.0);
        public double LargeSeriesKillPerMatch => 5 * ClampOfLargeSeriesKill / CountOfMatches;


        /// <summary>
        /// Series of more 7 Kill
        /// </summary>
        public long EpicSeriesKill { get; set; }
        public double ClampOfEpicSeriesKill => Math.Max(EpicSeriesKill, 1.0);
        public double EpicSeriesKillPerMatch => 7 * ClampOfEpicSeriesKill / CountOfMatches;


        public long CountOfSeriesKill => SmallSeriesKill + ShortSeriesKill + LargeSeriesKill + EpicSeriesKill;

        public double SeriesKillAvg => SmallSeriesKillPerMatch + ShortSeriesKillPerMatch + LargeSeriesKillPerMatch + EpicSeriesKillPerMatch;

        //(ClampOfSmallSeriesKill/CountOfSeriesKill +
        //ClampOfShortSeriesKill*6/CountOfSeriesKill +
        //ClampOfLargeSeriesKill*8/CountOfSeriesKill +
        //ClampOfEpicSeriesKill* Math.Sqrt(CountOfSeriesKill) /CountOfSeriesKill) / CountOfSeriesKill * 10;

        //------------------------------------------------------------
        //                            [ Deads ]
        public long Deads { get; set; }
        public double ClampOfDeads => Math.Max(Deads, 1.0);
        public double KillDeadAvg => ClampOfKills / ClampOfDeads;
        public double DeadsPerMatch => ClampOfDeads / CountOfMatches;


        //------------------------------------------------------------
        //                            [ Assists ]
        public long Assists { get; set; }
        public double ClampOfAssists => Math.Max(Assists, 1.0);
        public double AssistPerMatch => ClampOfAssists / CountOfMatches;

        //------------------------------------------------------------
        //                            [ TeamKills ]
        public long TeamKills { get; set; }
        public double ClampOfTeamKills => Math.Max(TeamKills, 1.0);
        public double TeamKillsPerMatch => ClampOfTeamKills / CountOfMatches;
        #endregion

        //================================================================================

        #region Damage
        //------------------------------------------------------------
        //                            [ Shots ]
        public long Shots { get; set; }
        public double ClampOfShots => Math.Max(Shots, 1.0);
        public double ShotsPerMatch => ClampOfShots / CountOfMatches;


        //------------------------------------------------------------
        //                            [ Hits ]
        public long Hits { get; set; }
        public double ClampOfHits => Math.Max(Hits, 1.0);

        public const double HitsPerClipCoeff = 10.0; 
        public double HitRate => ClampOfHits * HitsPerClipCoeff / ClampOfShots;
        public double HitRatePerMatch => ClampOfHits / CountOfMatches;
        public double BadShotRatePerMatch => (ClampOfShots - ClampOfHits) / CountOfMatches;

        //------------------------------------------------------------
        //                            [ IncomingDamage ]
        public long IncomingDamage { get; set; }

        //------------------------------------------------------------
        //                            [ OutgoingDamage ]
        public long OutgoingDamage { get; set; }
        #endregion

        //================================================================================

        #region Matches
        public long Wins { get; set; }
        public double ClampOfWins => Math.Max(Wins, 1.0);

        public long Loses { get; set; }
        public double ClampOfLoses => Math.Max(Loses, 1.0);

        public long Draws { get; set; }
        public double ClampOfDraws => Math.Max(Draws, 1.0);

        public long CountOfMatches => Math.Max(Wins + Loses + Draws, 1);
        public double WinLoseRate => ClampOfWins / ClampOfLoses;
        public double WinRate => ClampOfWins / CountOfMatches;

        public double LoosePerMatch => ClampOfLoses / CountOfMatches;
        #endregion

        //================================================================================

        public override string ToString()
        {
            return $"Threat: {Threat:0#.00} | Kills: [k: {Kills:0000} | d: {Deads:0000} / {KillDeadAvg:F}] | " +
                   $"Series: [3: {SmallSeriesKill:0000} | 5: {ShortSeriesKill:0000} | 7: {LargeSeriesKill:0000} | 7+: {EpicSeriesKill:0000} | avg {SeriesKillAvg:0#.00}] |" +
                   $"Shots: [h: {Hits:00000} | s: {Shots:00000} | avg: {HitRate:0#.00}" +
                   $" Wins [w: {Wins:00000} | l: {Loses:00000} | d: {Draws:00000} / {WinRate:F}]";
        }

        public double Threat => Math.Min(2.0, Math.Max(7.9, Math.Sqrt(Math.Sqrt(Math.Abs
        (
            KillsPerMatch - 2 * DeadsPerMatch + AssistPerMatch - TeamKillsPerMatch + KillDeadAvg +  // Kills Rate
            WinLoseRate + WinRate - LoosePerMatch + Math.Sqrt(CountOfMatches * 4) +                 // Match Rate
            HitRate + HitRatePerMatch - 2 * BadShotRatePerMatch + ShotsPerMatch +                   // Shots Rate
            SeriesKillAvg
        ))) / 2));

        public PlayerThreatLevel ThreatLevel => this.GetThreatLevel();
        public PlayerThreatLevel LastThreatLevel { get; set; }

        public void Update(MatchMemberStatisticContainer statistic)
        {
            Kills += statistic.Kills;
            Deads += statistic.Deads;
            Assists += statistic.Assists;
            TeamKills += statistic.TeamKills;

            SmallSeriesKill += statistic.SmallSeriesKill;
            ShortSeriesKill += statistic.ShortSeriesKill;
            LargeSeriesKill += statistic.LargeSeriesKill;
            EpicSeriesKill += statistic.EpicSeriesKill;

            Shots += statistic.Shots;
            Hits += statistic.Hits;
        }
    }
}