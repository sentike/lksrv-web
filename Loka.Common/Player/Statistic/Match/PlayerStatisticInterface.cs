﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Player.Statistic.Match
{
    public interface IPlayerStatisticInterface
    {
        #region Kills
        //------------------------------------------------------------
        //                            [ Kills ]
        long Kills { get; set; }
        /// <summary>
        /// Series of 3 Kill
        /// </summary>
        long SmallSeriesKill { get; set; }

        /// <summary>
        /// Series of 5 Kill
        /// </summary>
        long ShortSeriesKill { get; set; }

        /// <summary>
        /// Series of 7 Kill
        /// </summary>
        long LargeSeriesKill { get; set; }

        /// <summary>
        /// Series of more 7 Kill
        /// </summary>
        long EpicSeriesKill { get; set; }


        //------------------------------------------------------------
        //                            [ Deads ]
        long Deads { get; set; }

        //------------------------------------------------------------
        //                            [ Assists ]
        long Assists { get; set; }


        //------------------------------------------------------------
        //                            [ TeamKills ]
        long TeamKills { get; set; }
        #endregion

        //================================================================================

        #region Damage
        //------------------------------------------------------------
        //                            [ Shots ]
        long Shots { get; set; }

        //------------------------------------------------------------
        //                            [ Hits ]
        long Hits { get; set; }


        //------------------------------------------------------------
        //                            [ IncomingDamage ]
        long IncomingDamage { get; set; }

        //------------------------------------------------------------
        //                            [ OutgoingDamage ]
        long OutgoingDamage { get; set; }
        #endregion

        //================================================================================

        #region Matches
        long Wins { get; set; }

        long Loses { get; set; }

        long Draws { get; set; }
        #endregion

        //================================================================================

        double Threat { get; }
        PlayerThreatLevel ThreatLevel { get; }
        PlayerThreatLevel LastThreatLevel { get; set; }
       
    }
}
