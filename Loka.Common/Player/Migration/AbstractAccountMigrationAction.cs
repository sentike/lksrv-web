﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Player.Models;

namespace Loka.Server.Infrastructure.Migration
{
    public abstract class AbstractAccountMigrationAction
    {
        public readonly DateTime MigrationDate;
        public readonly AccountMigrationAction MigrationId;
        public readonly bool IsCriticalMigration = true;

        public override string ToString()
        {
            return $"Migration {MigrationId} at {MigrationDate}";
        }

        protected AbstractAccountMigrationAction(AccountMigrationAction migrationId, string migrationDate)
        {
            MigrationId = migrationId;
            MigrationDate = DateTime.ParseExact(migrationDate, "dd.MM.yyyy HH:mm", new DateTimeFormatInfo()); ;
        }

        public virtual bool Migrate(DataRepository.DataRepository repository, PlayerEntity player, out Exception error, bool enableMigrationInc = true)
        {
            try
            {
                MigrateProcess(repository, player, enableMigrationInc);
                repository.SaveChanges();
            }
            catch (Exception exception)
            {
                error = exception;
                if (IsCriticalMigration)
                {
                    throw new AccountMigrationException(this, player, exception);
                }
                return false;
            }

            error = null;
            if (player.MigrationAction < MigrationId && enableMigrationInc)
            {
                player.MigrationAction++;
            }
            return true;
        }

        protected abstract void MigrateProcess(DataRepository.DataRepository repository, PlayerEntity player, bool enableMigrationInc = true);
    }

    public class NullableAccountMigrationAction : AbstractAccountMigrationAction
    {
        public NullableAccountMigrationAction() 
            : base(AccountMigrationAction.None, DateTime.UtcNow.ToString("dd.MM.yyyy HH:mm"))
        {
        }

        protected override void MigrateProcess(DataRepository.DataRepository repository, PlayerEntity player, bool enableMigrationInc = true)
        {
            throw new NotImplementedException();
        }
    }
}
