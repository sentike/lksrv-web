﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Player.Models;

namespace Loka.Server.Infrastructure.Migration
{
    public class AccountMigrationException : Exception
    {
#if DEBUG
        public readonly AbstractAccountMigrationAction MigrationAction;
        public readonly PlayerEntity PlayerEntity;
#endif
        public AccountMigrationException(AbstractAccountMigrationAction migration, PlayerEntity player, Exception exception)
            : base($"Failed migrate {player.PlayerName} [{player.PlayerId}] from {player.MigrationAction} to {migration} | Reason: {exception.Message}", exception)
        {
#if DEBUG
            MigrationAction = migration;
            PlayerEntity = player;
#endif
        }

        public AccountMigrationException(AbstractAccountMigrationAction migration, PlayerEntity player, string exception)
            : this(migration, player, new Exception(exception))
        {
            
        }

        public AccountMigrationException(AbstractAccountMigrationAction migration, PlayerEntity player, string message, Exception exception)
            : this(migration, player, new Exception(message, exception))
        {

        }

    }
}
