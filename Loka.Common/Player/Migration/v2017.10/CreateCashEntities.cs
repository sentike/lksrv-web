﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Migration;
using Loka.Server.Player.Models;

namespace Loka.Common.Player.Migration.v2017._10
{
    public class CreateCashEntities : AbstractAccountMigrationAction
    {
        public CreateCashEntities()
            : base(AccountMigrationAction.CreateCashEntities, "17.10.2017 09:41")
        {

        }

        protected override void MigrateProcess(DataRepository repository, PlayerEntity player, bool enableMigrationInc = true)
        {
            player.Give(GameCurrency.Money, PlayerEntity.DefaultMoneyAmount);
            player.Give(GameCurrency.Donate, 10);
        }
    }

}
