﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Migration.v2017._10;
using Loka.Server.Infrastructure.Migration.v2016_11;
using Loka.Server.Player.Models;

namespace Loka.Server.Infrastructure.Migration
{
    public class AccountMigrationTool
    {
        public static AccountMigrationAction LastMigrationVersion => Migration.Max(m => m.MigrationId);
        public static readonly AbstractAccountMigrationAction[] Migration = new AbstractAccountMigrationAction[]
        {
            new AddNotExistProfilesAndCharacters(),
            new AddBonus500Money(), 
            new CreateCashEntities(), 
        }.OrderByDescending(m => m.MigrationId).ToArray();
    }
}
