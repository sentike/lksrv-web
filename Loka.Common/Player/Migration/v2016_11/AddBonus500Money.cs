﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Store;
using Loka.Server.Player.Models;

namespace Loka.Server.Infrastructure.Migration.v2016_11
{
    public class AddBonus500Money : AbstractAccountMigrationAction
    {
        public AddBonus500Money() 
            : base(AccountMigrationAction.AddBonus500Money, "15.11.2016 21:51")
        {
        }

        protected override void MigrateProcess(DataRepository.DataRepository repository, PlayerEntity player, bool enableMigrationInc = true)
        {
            player.Give(GameCurrency.Money, 500);
        }
    }
}