﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Loka.Common.Player.Profile.Profile;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Store;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Migration;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;
using Loka.Common.Store.Item.Character;


namespace Loka.Server.Infrastructure.Migration.v2016_11
{
    public class AddNotExistProfilesAndCharacters : AbstractAccountMigrationAction
    {
        public AddNotExistProfilesAndCharacters() 
            : base(AccountMigrationAction.AddNotExistProfilesAndCharacters, "15.11.2016 03:40")
        {

        }

        const bool AllowUnlockAllCharacters = true;
        protected override void MigrateProcess(DataRepository.DataRepository repository, PlayerEntity player, bool enableMigrationInc = true)
        {
            //==================================================================================================
            var playerAvalibleCharacters = player.CharacterList.Select(c => c.ItemModelEntity).ToArray();
            var playerAvalibleCharacterIds = playerAvalibleCharacters.Select(c => c.ModelId).ToArray();
            var storeAvalibleCharacters = repository.StoreItemInstance.Where(c => c.CategoryTypeId == CategoryTypeId.Character && c.Level == 0).ToArray().ToArray();
            var requiredCharaterInstances = storeAvalibleCharacters.Where(i => playerAvalibleCharacterIds.Contains(i.ModelId) == false).ToArray().OrderBy(c => c.TargetProfileId).ToArray();

            //==================================================================================================
            foreach (var characterInstance in requiredCharaterInstances)
            {
                var character = PlayerInventoryItemEntity.Factory(player.PlayerId, characterInstance);
                if (player.InventoryProfileList.Count < 3 || (AllowUnlockAllCharacters && player.InventoryProfileList.Count <= 4))
                {
                    player.InventoryProfileList.Add(PlayerProfileEntity.Factory(player.PlayerId, character.ItemId, (PlayerProfileTypeId) player.InventoryProfileList.Count, true));
                }
                player.InventoryItemList.Add(character);
            }
            repository.SaveChanges();

            //==================================================================================================
            //  Add Weapons to Profiles
            {
                player.FillEmptyProfilesByWeapons();
            }
            repository.SaveChanges();

            //==================================================================================================
        }
    }
}