﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Achievements;
using Loka.Common.Player.Achievements;
using Loka.Common.Store.Fraction;
using Loka.Server.Player.Models;

namespace Loka.Common.Player.Fraction
{
    public class PlayerFractionReputationEntity
    {
        public Guid ReputationId { get; set; }
        public virtual Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public FractionTypeId FractionId { get; set; }
        public virtual FractionEntity FractionEntity { get; set; }

        public short CurrentRank { get; set; }
        public short LastRank { get; set; }
        public long Reputation { get; set; }


        public override string ToString()
        {
            return $"{FractionId}: {CurrentRank}, reputation: {Reputation} / {ReputationNextLevel(CurrentRank)} ";
        }

        /// <summary>
        /// Counts the number of reputation to the next level
        /// </summary>
        /// <param name="level">target level</param>
        public static long ReputationNextLevel(int level) => 2500L + 10500L * level;

        public static readonly long MaxFractionReputation = ReputationNextLevel(11);

        public static PlayerFractionReputationEntity Factory(Guid playerId, FractionTypeId fractionTypeId, short rank = 0)
        {
            return new PlayerFractionReputationEntity
            {
                FractionId = fractionTypeId,
                ReputationId = Guid.NewGuid(),
                CurrentRank = rank,
                PlayerId = playerId
            };
        }

        public class Configuration : EntityTypeConfiguration<PlayerFractionReputationEntity>
        {
            public Configuration()
            {
                HasKey(p => p.ReputationId);
                ToTable("players.PlayerFractionReputationEntity");
                HasRequired(i => i.PlayerEntity).WithMany(i => i.ReputationList).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);
                HasRequired(i => i.FractionEntity).WithMany().HasForeignKey(i => i.FractionId).WillCascadeOnDelete(true);
            }
        }

        public void Update()
        {
            while (Reputation >= ReputationNextLevel(CurrentRank))
            {
                Reputation -= ReputationNextLevel(CurrentRank);
                LastRank = CurrentRank;
                CurrentRank++;
            }

            if (FractionEntity.FractionAchievementId > AchievementTypeId.None && FractionEntity.FractionAchievementId < AchievementTypeId.End)
            {
                var achievement = PlayerEntity.AchievementList.SingleOrDefault(a => a.AchievementTypeId == FractionEntity.FractionAchievementId) ??
                                  PlayerAchievementContainerEntity.Factory(PlayerId, FractionEntity.FractionAchievementId, AchievementCategoryId.Settable, CurrentRank);

                achievement.UpdateValue(CurrentRank);
            }
        }
    }
}
