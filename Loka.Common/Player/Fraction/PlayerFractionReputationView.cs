﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store.Fraction;

namespace Loka.Common.Player.Fraction
{
    public class PlayerFractionReputationView
    {
        public PlayerFractionReputationView(PlayerFractionReputationEntity exp)
        {
            exp.Update();
            Id = exp.FractionId;
            EntityId = exp.ReputationId;
            NextReputation = PlayerFractionReputationEntity.ReputationNextLevel(exp.CurrentRank);
            Reputation = exp.Reputation;
            CurrentRank = exp.CurrentRank;
            LastRank = exp.LastRank;
            exp.LastRank = exp.CurrentRank;
        }

        public readonly Guid EntityId;
        public readonly FractionTypeId Id;
        public readonly long NextReputation;
        public readonly long Reputation;
        public readonly short CurrentRank;
        public readonly short LastRank;

    }
}
