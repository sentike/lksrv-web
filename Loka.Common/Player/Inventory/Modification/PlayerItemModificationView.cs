﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Player.Inventory.Modification
{
    public class PlayerItemModificationView
    {
        public byte type { get; set; }
        public double value { get; set; }
        
    }
}
