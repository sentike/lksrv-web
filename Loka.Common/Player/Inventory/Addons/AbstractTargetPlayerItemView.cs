﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store;

namespace Loka.Common.Player.Inventory.Addons
{
    public class AbstractTargetPlayerItemView
    {
        public Guid EntityId { get; set; }
        public short ModelId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }

        public AbstractTargetPlayerItemView(AbstractTargetPlayerItem item)
        {
            EntityId = item.ItemId;
            ModelId = item.ModelId;
            CategoryTypeId = item.CategoryTypeId;
        }
    }
}
