﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Server.Player.Models;


namespace Loka.Common.Player.Inventory
{
    public class AbstractTargetPlayerItem
    {
        public AbstractTargetPlayerItem() { }

        public AbstractTargetPlayerItem(Guid playerId, short modelId, CategoryTypeId category)
        {
            ItemId = Guid.NewGuid();
            PlayerId = playerId;
            ModelId = modelId;
            CategoryTypeId = category;
        }


        public Guid ItemId { get; set; }
        public short ModelId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }
        public virtual ItemInstanceEntity ModelInstance { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerInstance { get; set; }
        
        public Guid TargetPlayerItemId { get; set; }
        public virtual PlayerInventoryItemEntity TargetPlayerInventoryItemEntity { get; set; }

        public override string ToString()
        {
            return $"{ItemId}: {ModelInstance?.ModelIdName ?? ModelId.ToString()} {CategoryTypeId}, [owner {PlayerInstance?.PlayerName} {PlayerId}] | TargetPlayerItemId: {TargetPlayerItemId}";
        }

        public class Configuration : EntityTypeConfiguration<AbstractTargetPlayerItem>
        {
            public Configuration()
            {
                HasKey(s => s.ItemId);
                ToTable("players.InstanceOfTargetItem");
                HasRequired(p => p.PlayerInstance).WithMany().HasForeignKey(f => f.PlayerId);
                HasRequired(s => s.TargetPlayerInventoryItemEntity).WithMany(s => s.AvalibleAddonList).HasForeignKey(s => s.TargetPlayerItemId);


                HasRequired(i => i.ModelInstance).WithMany().HasForeignKey(i => new {i.ModelId, i.CategoryTypeId});
            }
        }
    }
}