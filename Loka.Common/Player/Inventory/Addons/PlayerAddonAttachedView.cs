﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Player.Inventory.Addons
{
    public class PlayerAddonAttachedView
    {
        public Guid AddonId { get; set; }
        public EquipAddonSlot AddonSlot { get; set; }

        public PlayerAddonAttachedView(PlayerAddonAttachedEntity item)
        {
            AddonId = item.AddonId;
            AddonSlot = item.AddonSlot;
        }
    }
}
