﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;


namespace Loka.Common.Player.Inventory
{
    public enum EquipAddonSlot
    {
        None,
        Skin = 100,
        Sniper,
        End
    }

    public class PlayerAddonAttachedEntity
    {
        public Guid ContainerId { get; set; }

        public Guid AddonId { get; set; }
        public virtual AbstractTargetPlayerItem AddonEntity { get; set; }

        public Guid ItemId { get; set; }
        public virtual PlayerInventoryItemEntity InventoryItemEntity { get; set; }

        public EquipAddonSlot AddonSlot { get; set; }

        public class Configuration : EntityTypeConfiguration<PlayerAddonAttachedEntity>
        {
            public Configuration()
            {
                HasKey(a => a.ContainerId);
                ToTable("players.InstanceOfInstalledAddon");
                HasRequired(a => a.AddonEntity).WithMany().HasForeignKey(a => a.AddonId);
            }
        }
    }


}
