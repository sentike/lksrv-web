﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using JetBrains.Annotations;

using System.Data.Entity.Infrastructure.Annotations;
using Loka.Common.Infrastructure;
using Loka.Common.Player.Inventory.Modification;
using Loka.Common.Player.Inventory.Service;
using Loka.Common.Player.Statistic;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Item.Modifications;
using Loka.Common.Store.Service;
using Loka.Infrastructure;
using Loka.Server.Player.Models;
using VRS.Infrastructure;
using Loka.Common.Transaction;

namespace Loka.Common.Player.Inventory
{
    public class PlayerInventoryItemEntity 
        : IVersionedEntity, ICountable
    {
        public PlayerInventoryItemEntity() { }
        public PlayerInventoryItemEntity(Guid playerId, ItemInstanceEntity entity)
        {
            ModelId = entity.ModelId;
            PlayerId = playerId;
            ItemId = Guid.NewGuid();
            CategoryTypeId = entity.CategoryTypeId;
            modification_1 = new PlayerItemModificationEntity();
            modification_2 = new PlayerItemModificationEntity();
            modification_3 = new PlayerItemModificationEntity();
            modification_4 = new PlayerItemModificationEntity();
            modification_5 = new PlayerItemModificationEntity();
            PurchaseDate = DateTime.UtcNow;
            Amount = entity.DefaultAmount;
            if (Amount < 0) Amount = 0;
        }

        /// <summary>
        /// The unique identifier in the object database
        /// </summary>
        public Guid ItemId { get; set; }
        



        /// <summary>
        /// Object Identifier on the client
        /// </summary>
        public short ModelId { get; set; }

        /// <summary>
        /// Object instance
        /// </summary>
        public CategoryTypeId CategoryTypeId { get; set; }


        /// <summary>
        /// Date when the object was purchased
        /// </summary>
        public DateTime PurchaseDate { get; set; }
       
        /// <summary>
        /// Date the last time the object was used. (The last time I wore a character)
        /// </summary>
        public DateTime? LastUseDate { get; set; }
       
        /// <summary>
        /// expiration date of the lease on the subject.
        /// </summary>
        public DateTime? ValidityDate { get; set; }
   

        public virtual ItemInstanceEntity ItemModelEntity { get; set; }

        /// <summary>
        /// Player Identifier owner
        /// </summary>
        public Guid PlayerId { get; set; }

        /// <summary>
        /// Player instance
        /// </summary>
        public virtual PlayerEntity PlayerInstance { get; set; }

        /// <summary>
        /// Duration in Hourse
        /// </summary>
        public long Duration { get; set; }

        public long LastAmount { get; set; }

        public Guid EntityId => ItemId;
        public long Count { get; set; }
        public long Hash { get; set; }

        public long Amount
        {
            get
            {
                return Count;
            }
            set
            {
                Hash = value.Crc64Hash();
                LastAmount = Amount;
                Count = value;
            }
        }

        public long LastHash => LastAmount.Crc64Hash();
        public bool IsValidHash => Hash == LastHash;

        public bool IsAny => Amount > 0;

        private PlayerUsingServiceEntity ActivateService(long amount)
        {
            return new PlayerUsingServiceEntity
            {
                ServiceInstanceId = Guid.NewGuid(),
                PlayerId = PlayerId,
                ServiceTypeId = ((ServiceModelId)ModelId).AsType(),
                ExpirationDate = DateTime.UtcNow.AddHours(amount)
            };
        }

        private void OnServiceActivate(long amount)
        {
            var avalible = Math.Min(Amount, amount);
            var model = (ServiceModelId)ModelId;

            var service = PlayerInstance.UsingServiceEntities.FirstOrDefault(s => s.ServiceTypeId == model.AsType());
            if (service == null)
            {
                PlayerInstance.UsingServiceEntities.Add(ActivateService(avalible));
            }
            else
            {
                // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
                if (service.ExpirationDate < DateTime.UtcNow)
                {
                    service.ExpirationDate = DateTime.UtcNow.AddHours(avalible);
                }
                else
                {
                    service.ExpirationDate = service.ExpirationDate.AddHours(avalible);
                }
            }

            Amount -= avalible;
        }

        private void OnDonateActivate(long amount)
        {
            var avalible = Math.Min(Amount, amount);
            PlayerInstance.Give(ItemModelEntity.Cost.Currency, avalible);
            Amount -= avalible;
        }

        public void Activate(long amount)
        {
            if (CategoryTypeId == CategoryTypeId.Service)
            {
                OnServiceActivate(amount);
            }
            else if (CategoryTypeId == CategoryTypeId.Currency)
            {
                OnDonateActivate(amount);
            }
            //else throw new NotImplementedException($"Invalid CategoryTypeId[{CategoryTypeId}] for activate");
        }



        public virtual List<AbstractTargetPlayerItem> AvalibleAddonList { get; protected set; } = new List<AbstractTargetPlayerItem>(2);

        public virtual List<PlayerAddonAttachedEntity> InstalledAddons { get; protected set; } = new List<PlayerAddonAttachedEntity>(4); 

        public PlayerItemModificationEntity modification_1 { get; set; }
        public PlayerItemModificationEntity modification_2 { get; set; }
        public PlayerItemModificationEntity modification_3 { get; set; }
        public PlayerItemModificationEntity modification_4 { get; set; }
        public PlayerItemModificationEntity modification_5 { get; set; }

        [NotMapped, NotNull]
        public IEnumerable<PlayerItemModificationEntity> ModificationList => new List<PlayerItemModificationEntity>
        {
            modification_1, modification_2, modification_3, modification_4, modification_5
        };

        public bool ApplyModification(byte modificationId, PlayerSessionItemModification modification)
        {
            var modificationInstance = ModificationList.FirstOrDefault(m => m.type == (int)WeaponModificationTypeId.None);
            if (modificationInstance == null) return false;

            var targetModification = modification.Modifications.SingleOrDefault(m => m.type == modificationId);
            if (targetModification == null) return false;

            modificationInstance.value = targetModification.value;
            modificationInstance.type = targetModification.type;
            modification.ItemId = Guid.Empty;
            return true;
        }

        public static readonly Random Random = new Random();
        public bool RandomizeModificationList(bool isDonate, out PlayerSessionItemModification modification)
        {
            if (isDonate || Random.Next(100) >= 50)
            {
                modification = new PlayerSessionItemModification(ItemId, WeaponModification.List.Where(s => ModificationList.Select(m => m.type).Contains((byte)s.Key) == false).PickRandom(3).Select(m => new PlayerItemModificationEntity { type = (byte)m.Key, value = CMath.GetRandomNumber(m.Value.Min, m.Value.Max) }).ToArray());
                return true;
            }
            modification = null;
            return false;
        }

        public override string ToString()
        {
            return $"{ItemModelEntity.ModelIdName} {CategoryTypeId} {ItemId} | owner {PlayerInstance?.PlayerName} {PlayerId}";
        }

        public static PlayerInventoryItemEntity Factory(Guid playerId, ItemInstanceEntity entity)
        {
            return new PlayerInventoryItemEntity(playerId, entity)
            {
                AvalibleAddonList = new List<AbstractTargetPlayerItem>(2)
                {
                    new AbstractTargetPlayerItem(playerId, (short)entity.DefaultSkinId, CategoryTypeId.Material)
                },ItemModelEntity = entity
            };
        }

        public Guid RowVersion { get; set; }


        public class Configuration : EntityTypeConfiguration<PlayerInventoryItemEntity>
        {
            public Configuration()
            {
                ToTable("players.InstanceOfPlayerItem");
                HasKey(p => p.ItemId);

                HasRequired(p => p.PlayerInstance).WithMany(i => i.InventoryItemList).HasForeignKey(f => f.PlayerId);
                HasRequired(i => i.ItemModelEntity).WithMany().HasForeignKey(i => new { i.ModelId, i.CategoryTypeId });

                Property(x => x.PlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_instanceofplayerItem_itemsingleton", 1){IsUnique = true}));
                Property(x => x.ModelId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_instanceofplayerItem_itemsingleton", 2) { IsUnique = true }));
                Property(x => x.CategoryTypeId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_instanceofplayerItem_itemsingleton", 3) { IsUnique = true }));

                Property(p => p.RowVersion).IsConcurrencyToken(false);

                Ignore(p => p.Amount);
                Ignore(p => p.LastHash);
                Ignore(p => p.IsValidHash);

                //HasMany(s => s.InstalledAddons).WithRequired(s => s.ItemEntity).HasForeignKey(s => s.ItemId);

                //Ignore(p => p.LastUseDate);
                //Ignore(p => p.PurchaseDate);
                //Ignore(p => p.ValidityDate);

            }
        }
    }
}