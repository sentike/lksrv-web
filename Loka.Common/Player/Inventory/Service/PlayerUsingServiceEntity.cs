﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store.Service;
using Loka.Server.Player.Models;
using VRS.Infrastructure;

namespace Loka.Common.Player.Inventory.Service
{
    public class PlayerUsingServiceEntity : IVersionedEntity
    {
        public Guid ServiceInstanceId { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public DateTime ExpirationDate { get; set; }
        public DateTime NotifyDate { get; set; }

        public ServiceTypeId ServiceTypeId { get; set; }
        public Guid RowVersion { get; set; }

        public override string ToString()
        {
            return $"{ServiceInstanceId} | time left: {ExpirationDate - DateTime.UtcNow} | {ExpirationDate}";
        }

        public class Configuration : EntityTypeConfiguration<PlayerUsingServiceEntity>
        {
            public Configuration()
            {
                HasKey(i => i.ServiceInstanceId);
                ToTable("players.PlayerUsingServiceEntity");
                HasRequired(p => p.PlayerEntity).WithMany(p => p.UsingServiceEntities).HasForeignKey(p => p.PlayerId).WillCascadeOnDelete(true);
                Property(p => p.PlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_ServiceTypeId", 0) { IsUnique = true }));
                Property(p => p.ServiceTypeId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_ServiceTypeId", 1) { IsUnique = true }));
                Property(p => p.RowVersion).IsConcurrencyToken(false);
            }
        }
    }
}
