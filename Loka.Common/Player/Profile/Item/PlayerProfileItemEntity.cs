﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Profile.Profile;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Player.Inventory;

namespace Loka.Common.Player.Profile.Item
{
    public class PlayerProfileItemEntity
    {
        public Guid ProfileItemId { get; set; }
        public Guid PlayerProfileTypeId { get; set; }
        public virtual PlayerProfileEntity ProfileEntity { get; set; }

        public Guid ItemId { get; set; }
        public virtual PlayerInventoryItemEntity InventoryItemEntity { get; set; }

        public PlayerProfileInstanceSlotId SlotId { get; set; }

        public class Configuration : EntityTypeConfiguration<PlayerProfileItemEntity>
        {
            public Configuration()
            {
                HasKey(p => p.ProfileItemId);
                ToTable("players.PlayerProfileItemEntity");

                HasRequired(p => p.InventoryItemEntity).WithMany().HasForeignKey(p => p.ItemId).WillCascadeOnDelete(true);
                HasRequired(p => p.ProfileEntity).WithMany(p => p.Items).HasForeignKey(p => p.PlayerProfileTypeId).WillCascadeOnDelete(true);

                Property(p => p.PlayerProfileTypeId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ProfileItemSlot", 0) { IsUnique = true }));
                Property(p => p.SlotId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ProfileItemSlot", 1) { IsUnique = true }));
            }
        }

        public static PlayerProfileItemEntity Factory(PlayerInventoryItemEntity inventoryItem, PlayerProfileInstanceSlotId slotId)
        {
            return new PlayerProfileItemEntity
            {
                ProfileItemId = Guid.NewGuid(),
                ItemId = inventoryItem.ItemId,
                InventoryItemEntity = inventoryItem,
                SlotId = slotId
            };
        }
    }
}
