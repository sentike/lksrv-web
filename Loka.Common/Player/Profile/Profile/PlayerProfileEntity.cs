﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Profile.Item;
using Loka.Common.Player.Profile.Slot;
using Loka.Player;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;
using VRS.Infrastructure;

namespace Loka.Common.Player.Profile.Profile
{
    /// <summary>
    /// Is a player profile, which contains the list of installed items. In particular, weapons, armor, character and special tools. Keeps the profile states his name and expiration date.
    /// </summary>
    public class PlayerProfileEntity : IAbstractPlayerProfile, IVersionedEntity
    {
        public static PlayerProfileEntity Factory(Guid playerId, Guid characterId, PlayerProfileTypeId profileSlotId, bool isUnlocked)
        {
            return new PlayerProfileEntity
            {
                IsEnabled = isUnlocked,
                AssetId = Guid.NewGuid(),
                PlayerId = playerId,
                CharacterId = characterId,
                ProfileSlotId = profileSlotId,
            };
        }


        public Guid AssetId { get; set; }
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsUnlocked { get; set; }
        public PlayerProfileTypeId ProfileSlotId { get; set; }

        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public Guid CharacterId { get; set; }
        public int CharacterLevel => CharacterItemEntity?.ItemModelEntity.Level ?? 0;
        public virtual PlayerInventoryItemEntity CharacterItemEntity { get; set; }

        public virtual HashSet<PlayerProfileItemEntity> Items { get; private set; } = new HashSet<PlayerProfileItemEntity>();
        public IReadOnlyDictionary<PlayerProfileInstanceSlotId, PlayerProfileItemEntity> ItemSlotDictionary => Items.ToDictionary(i => i.SlotId, i => i);

        public int Level => Items.Where(i => i.InventoryItemEntity.ItemModelEntity != null).Select(i => i.InventoryItemEntity.ItemModelEntity.Level).DefaultIfEmpty(0).Max();

        public override string ToString()
        {
            return $"Profile {AssetId} | Owner: {PlayerEntity?.PlayerName} [{PlayerId}] | Level: {Level} / Enabled: {IsEnabled} | Character: {CharacterItemEntity?.ModelId} [{CharacterId}] | Items: {Items.Count}";
        }

        public PlayerProfileItemEntity Equip(PlayerInventoryItemEntity inventoryItem, PlayerProfileInstanceSlotId slotId)
        {
            PlayerProfileItemEntity slotEntity;
            if (inventoryItem == null)
            {
                slotEntity = Items.SingleOrDefault(i => i.SlotId == slotId);
                Items.Remove(slotEntity);
            }
            else
            {
                slotEntity = Items.SingleOrDefault(i => i.SlotId == slotId);
                if (slotEntity == null)
                {
                    Items.Add(slotEntity = PlayerProfileItemEntity.Factory(inventoryItem, slotId));
                }
                else
                {
                    slotEntity.ItemId = inventoryItem.ItemId;
                    slotEntity.InventoryItemEntity = inventoryItem;
                }
            }
            return slotEntity;
        }

        public Guid RowVersion { get; set; }
        public class Configuration : EntityTypeConfiguration<PlayerProfileEntity>
        {
            public Configuration()
            {
                HasKey(p => p.AssetId);
                ToTable("players.PlayerProfileEntity");
                HasRequired(i => i.PlayerEntity).WithMany(i => i.InventoryProfileList).HasForeignKey(i => i.PlayerId).WillCascadeOnDelete(true);
                HasRequired(i => i.CharacterItemEntity).WithMany().HasForeignKey(i => i.CharacterId);
                Property(p => p.RowVersion).IsConcurrencyToken(false);

                // Property(p => p.PlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerProfileSlot", 0) {IsUnique = true}));
                // Property(p => p.ProfileSlotId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerProfileSlot", 1) { IsUnique = true }));
            }
        }
    }
}
