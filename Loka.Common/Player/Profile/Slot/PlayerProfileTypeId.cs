﻿using System.ComponentModel;

namespace Loka.Common.Player.Profile.Slot
{
    /// <summary>
    /// It is used to sort the profiles in the lobby and game.
    /// <para>Also used to "limit" the number of profiles and clarity what a profile.</para>
    /// </summary>
    public enum PlayerProfileTypeId
    {
        /// <summary>
        /// First player profile
        /// </summary>
        One,

        /// <summary>
        /// Second player profile
        /// </summary>
        Two,

        /// <summary>
        /// Third player profile
        /// </summary>
        Three,

        /// <summary>
        /// Fourth player profile
        /// </summary>
        Four,

        /// <summary>
        /// Fifth player profile
        /// </summary>
        Five,

        /// <summary>
        /// End of profile list
        /// </summary>
        [Browsable(false)]
        End
    }
}
