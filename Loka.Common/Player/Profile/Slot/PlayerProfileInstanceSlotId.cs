﻿namespace Loka.Common.Player.Profile.Slot
{
    /// <summary>
    /// It specifies the object slot
    /// </summary>
    public enum PlayerProfileInstanceSlotId
    {
        None,

        Character,

        Helmet,
        Mask,

        Gloves,
        Backpack,
        Body,

        Pants,
        Boots,

        PrimaryWeapon,
        SecondaryWeapon,

        Grenade,
        Kit,

        End
	};
}
