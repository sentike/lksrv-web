﻿using System.Diagnostics.CodeAnalysis;

namespace Loka.Common.Player.Profile.Slot
{
    /// <summary>
    /// It defines an object in your inventory slot.
    /// Need to install special equipment and several grenades
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum PlayerProfileItemSlotId
    {
        PrimaryWeapon = PlayerProfileInstanceSlotId.PrimaryWeapon,
        SecondaryWeapon = PlayerProfileInstanceSlotId.SecondaryWeapon,
        Helmet = PlayerProfileInstanceSlotId.Helmet,
        Mask = PlayerProfileInstanceSlotId.Mask,
        Gloves = PlayerProfileInstanceSlotId.Gloves,
        Body = PlayerProfileInstanceSlotId.Body,
        Backpack = PlayerProfileInstanceSlotId.Backpack,
        Pants = PlayerProfileInstanceSlotId.Pants,
        Boots = PlayerProfileInstanceSlotId.Boots,
        Explosive_1 = PlayerProfileInstanceSlotId.End,
        Explosive_2,
        Explosive_3,
        Other_1,
        Other_2,
        Other_3,
        End
    }
}
