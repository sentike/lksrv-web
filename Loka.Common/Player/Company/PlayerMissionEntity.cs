﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Company.Task;
using Loka.Server.Player.Models;

namespace Loka.Common.Player.Company
{
    public class PlayerMissionEntity
    {
        public Guid PlayerMissionId { get; set; }

        //------------------------------------------------
        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        //------------------------------------------------
        public long MissionTaskId { get; set; }
        public virtual ScenarioTaskEntity MissionTaskEntity { get; set; }

        //------------------------------------------------
        public PlayerMissionProgress MissionProgress { get; set; }
        public PlayerMissionState MissionState { get; set; }


        //------------------------------------------------
        public DateTime StartDate { get; set; }
        public DateTime? CompleteDate { get; set; }

        public class Configuration : EntityTypeConfiguration<PlayerMissionEntity>
        {
            public Configuration()
            {
                HasKey(p => p.PlayerMissionId);
                ToTable("players.PlayerMissionEntity");
                HasRequired(l => l.MissionTaskEntity).WithMany().HasForeignKey(p => p.MissionTaskId).WillCascadeOnDelete(true);

                Property(p => p.PlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_MissionTaskId", 0) { IsUnique = true }));
                Property(p => p.MissionTaskId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_MissionTaskId", 1) { IsUnique = true }));
            }
        }
    }
}
