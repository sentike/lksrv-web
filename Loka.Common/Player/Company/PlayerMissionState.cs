﻿namespace Loka.Common.Player.Company
{
    public enum PlayerMissionState
    {
        /// <summary>
        /// The mission in progress
        /// </summary>
        Process,

        /// <summary>
        /// The mission was successfully completed
        /// </summary>
        Complete,

        /// <summary>
        /// The mission was canceled, it is expected to re-execution
        /// </summary>
        Idle,
    }
}