﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Loka.Common.Player.Company
{
    [ComplexType]
    public class PlayerMissionProgress
    {
        public int Current { get; set; }
        public int Last { get; set; }
    }
}