﻿using System;
using System.Data.Entity.ModelConfiguration;
using Loka.Common.Achievements;
using Loka.Common.Achievements.Container;
using Loka.Common.Achievements.Instance;
using Loka.Server.Player.Models;

namespace Loka.Common.Player.Achievements
{
    public class PlayerAchievementContainerEntity
    {
        public Guid PlayerAchievementId { get; set; }
        public int Amount { get; set; }


        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerInstance { get; set; }

        public AchievementTypeId AchievementTypeId { get; set; }
        public AchievementCategoryId CategoryId { get; set; }


        public virtual AchievementContainerEntity AchievementEntity { get; set; }

        public int? LastAchievementInstanceEntityId { get; set; }
        public virtual AchievementInstanceEntity LastAchievementInstanceEntityEntity { get; set; }

        public void UpdateValue(int value)
        {
            if (CategoryId == AchievementCategoryId.Settable) Amount = value;
            else Amount += value;
        }

        public static PlayerAchievementContainerEntity Factory(Guid playerId, AchievementContainerEntity instance, int amount = 0)
        {
            return new PlayerAchievementContainerEntity
            {
                PlayerId = playerId,
                PlayerAchievementId = Guid.NewGuid(),
                AchievementTypeId = instance.AchievementTypeId,
                CategoryId = instance.CategoryId,
                Amount = amount
            };
        }

        public static PlayerAchievementContainerEntity Factory(Guid playerId, AchievementTypeId achievementTypeId, AchievementCategoryId achievementCategoryId, int amount = 0 )
        {
            return new PlayerAchievementContainerEntity
            {
                PlayerId = playerId,
                PlayerAchievementId = Guid.NewGuid(),
                AchievementTypeId = achievementTypeId,
                CategoryId = achievementCategoryId,
                Amount = amount
            };
        }


        public class Configuration : EntityTypeConfiguration<PlayerAchievementContainerEntity>
        {
            public Configuration()
            {
                ToTable("players.PlayerAchievement");
                HasKey(a => a.PlayerAchievementId);
                HasRequired(a => a.AchievementEntity).WithMany().HasForeignKey(a => a.AchievementTypeId).WillCascadeOnDelete(true);
                HasOptional(a => a.LastAchievementInstanceEntityEntity).WithMany().HasForeignKey(a => a.LastAchievementInstanceEntityId);
            }
        }
    }
}