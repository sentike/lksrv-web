﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Loka.Server.Models.Queue
{
    [ComplexType]
    public class QueueRange
    {
        public QueueRange()
        {
            
        }

        public QueueRange(int value)
        {
            Min = Max = value;
        }

        public bool InRange(int value) => value >= Min && value <= Max;

        public override string ToString()
        {
            return $"Range [{Min} - {Max}]: {Max - Min + 1}";
        }

        public int Min { get; set; }
        public int Max { get; set; }
    }
}