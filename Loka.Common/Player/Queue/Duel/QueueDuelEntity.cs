﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;

using Loka.Common.Match.GameMap;
using Loka.Common.Store;
using Loka.Server.Infrastructure.Queue;
using VRS.Infrastructure;


namespace Loka.Server.Models.Queue
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum DuelWeaponType
    {
        None,
        AssaultRifle,
        SniperRifle,
        GrenadeLauncher,
        RocketLauncher,
        Shootgun,
        Pistol,
        LightMachineGun,
        SubMachineGun,
        End
    }

    [Flags]
    public enum DuelWeaponFlags
    {
        None = 1 << DuelWeaponType.None,
        AssaultRifle = 1 << DuelWeaponType.AssaultRifle,
        SniperRifle = 1 << DuelWeaponType.SniperRifle,
        GrenadeLauncher = 1 << DuelWeaponType.GrenadeLauncher,
        RocketLauncher = 1 << DuelWeaponType.RocketLauncher,
        Shootgun = 1 << DuelWeaponType.Shootgun,
        Pistol = 1 << DuelWeaponType.Pistol,
        LightMachineGun = 1 << DuelWeaponType.LightMachineGun,
        SubMachineGun = 1 << DuelWeaponType.SubMachineGun,
        End = 1 << DuelWeaponType.End,
        All = AssaultRifle | SniperRifle | GrenadeLauncher | RocketLauncher | Shootgun | Pistol | LightMachineGun | SubMachineGun
    }

}