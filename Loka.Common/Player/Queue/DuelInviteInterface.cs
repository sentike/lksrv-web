﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Match.GameMap;
using Loka.Common.Store;
using Loka.Infrastructure;
using Loka.Server.Models.Queue;


namespace Loka.Server.Infrastructure.Queue
{
    public interface IDuelInviteInterface
    {
        StoreItemCost Bet { get; set; }
        DuelWeaponType WeaponType { get; set; }
        GameMapTypeId GameMapTypeId { get; set; }
        short NumberOfLives { get; set; }
    }
}
