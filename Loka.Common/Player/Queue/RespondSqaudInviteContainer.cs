﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Models.Queue;

namespace Loka.Server.Infrastructure.Queue
{

    public struct RespondSqaudInviteContainer
    {
        public Guid InviteId { get; set; }
        public bool IsApproved { get; set; }
    }
}
