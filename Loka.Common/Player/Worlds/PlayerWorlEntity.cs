﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Building.Player;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Server.Player.Models;

namespace Loka.Common.Player.Worlds
{
    public class PlayerWorlEntity
    {
        public Guid EntityId { get; set; }
        public string Name { get; set; }

        //=================================================
        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        //=================================================
        public DateTime CreatedDate { get; set; }
        public DateTime LastAttackDate { get; set; }

        //=================================================
        public short WorldModelId { get; set; }
        public CategoryTypeId WorldModelCategory { get; set; }
        public virtual ItemInstanceEntity WorldModelEntity { get; set; }

        //=================================================
        public virtual ICollection<WorldBuildingItemEntity> WorldBuildins { get; set; } = new List<WorldBuildingItemEntity>(8192);

        //public override string ToString()
        //{
        //    return $"{ItemId} | ModelId: {BuildingModelId}[{ModelId}] | Position: {Position} | {Rotation}";
        //}

        //=================================================
        public class Configuration : EntityTypeConfiguration<PlayerWorlEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                ToTable("building.PlayerWorlEntity");

                Property(p => p.Name).IsOptional().HasMaxLength(64);
                HasRequired(p => p.WorldModelEntity).WithMany().HasForeignKey(p => new { p.WorldModelId, p.WorldModelCategory }).WillCascadeOnDelete(true);
                HasRequired(p => p.PlayerEntity).WithMany(p => p.PlayerWorlEntities).HasForeignKey(p => p.PlayerId).WillCascadeOnDelete(true);
                HasMany(p => p.WorldBuildins).WithOptional(p => p.WorlEntity).HasForeignKey(p => p.WorldId).WillCascadeOnDelete(true);


            }
        }

    }
}
