﻿using System;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Web.Hosting;
using Loka.Common.Match.Match;
using Loka.Infrastructure;
using Loka.Server.ClusterManager.Infrastructures;
using Loka.Server.Infrastructure.DataRepository;
using VRS.Infrastructure;

namespace Loka.Common.Cluster
{
    public class ClusterNodePrincipal 
        : GenericPrincipal
        , IDisposable
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public MatchEntity SessionInstance { get; private set; }
        public DataRepository Db { get; private set; }

        public ClusterNodePrincipal(DataRepository db, MatchEntity matchEntity)
            : base(new GenericIdentity(matchEntity.MatchId.ToString()), new string[] {})
        {
            Db = db;
            SessionInstance = matchEntity;
        }

        public void Dispose()
        {
            try
            {
                using (Db)
                {
                    var ch = Db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }

        }
    }

    [ComplexType]
    public class HostAddressContainer
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public IPEndPoint EndPoint => new IPEndPoint(IPAddress.Parse(Host), Port);

        public static IPAddress IpAddressFromCidr(string host)
        {
            //return IPAddress.Parse("78.46.107.13");

            try
            {
                var cidr = host.Split(new char[] { '/' }, 2);
                var address = cidr[0];
                return IPAddress.Parse(address);
            }
            catch (Exception) { }
            return IPAddress.Parse(host);
        }

        public HostAddressContainer(string host, short port)
        {
            Host = IpAddressFromCidr(host).ToString();
            Port = port;
        }

        public override string ToString()
        {
            return $"{Host}:{Port}";
        }
    }

    //public class ClusterNodeInstance : IVersionedEntity
    //{
    //    public Guid MatchId { get; set; }
    //    public virtual MatchEntity SessionInstance { get; set; }
    //
    //    public string MachineAddress { get; set; }
    //    public virtual ClusterInstance MachineInstance { get; set; }
    //    public int ProcessId { get; set; }
    //    public HostAddressContainer Address { get; set; }
    //
    //
    //    public DateTime? ShutDownDate { get; set; }
    //    public int? ShutDownCode { get; set; }
    //
    //    public DateTime RegistrationDate { get; set; }
    //    public DateTime? LastActivityDate { get; set; }
    //    public DateTime? LastCheckDate { get; set; }
    //
    //
    //    public DateTime LastSessionInstanceActivityDate { get; set; }
    //    public long ElapsedSecondsFromLastActivitySessionInstance => LastSessionInstanceActivityDate.ToElapsedSeconds();
    //
    //    public bool IsStoped => State == NodeInstanceState.StopedExit || State == NodeInstanceState.StopedCrash || State == NodeInstanceState.StopedUpdate;
    //    public NodeInstanceState State { get; set; }
    //    public long ElapsedSecondsFromLastActivity => (LastActivityDate?? DateTime.UtcNow).ToElapsedSeconds();
    //
    //    public bool IsOnline => ElapsedSecondsFromLastActivity < 15;
    //    public Guid RowVersion { get; set; }
    //
    //
    //
    //
    //    public override string ToString()
    //    {
    //        return $"Node {MatchId}, host: {Address} {State}, ElapsedFromLastActivity: {ElapsedSecondsFromLastActivity} | {SessionInstance?.GameModeTypeId} -- {SessionInstance?.GameMapTypeId}";
    //    }
    //
    //    public class Configuration : EntityTypeConfiguration<ClusterNodeInstance>
    //    {
    //        public Configuration()
    //        {
    //            HasKey(p => p.MatchId);
    //            ToTable("public.ClusterNodeEntity");
    //            HasOptional(p => p.SessionInstance).WithOptionalDependent(p => p.NodeEntity);
    //            Property(p => p.RowVersion).IsConcurrencyToken(false);
    //        }
    //    }
    //}
}