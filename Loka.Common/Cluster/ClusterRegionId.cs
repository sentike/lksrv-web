﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Cluster
{
    [Flags]
    public enum ClusterRegionId : byte
    {
        None = 0,
        RuEast = 1,
        RuCentral = 2,
        RuSouth = 4,

        UsNorth = 8,
        UsSouth = 16,

        Cn = 32,

        Jp = 8,

        Au = 64,
    }
}
