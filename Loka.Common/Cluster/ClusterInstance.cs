﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Loka.Common.Match.Match;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using VRS.Infrastructure;


namespace Loka.Common.Cluster
{
    [ComplexType]
    public class ClusterPerformanceProperty
    {
        public long CurrentAvalibleMemory { get; set; }
        public long TotalAvalibleMemory { get; set; }
        public byte NumberOfCores { get; set; }
        public byte ProcessorLoad { get; set; }
    }

    public static class ProcessHelper
    {
        public static Process TakeServerProcess(int processId)
        {
            return Process.GetProcesses().SingleOrDefault(p => p.Id == processId && p.ProcessName.StartsWith("LOka", StringComparison.OrdinalIgnoreCase));
        }

        public static bool IsExistServerProcess(this MatchEntity node)
        {
            return IsExistServerProcess(node.ProcessId);
        }

        public static bool IsExistServerProcess(int processId)
        {
            return TakeServerProcess(processId) != null;
        }

    };


    public class ClusterInstance : IVersionedEntity
    {
        protected static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public string MachineAddress { get; set; }
        public string MachineName { get; set; }

        public ClusterRegionId RegionTypeId { get; set; }
        public int LimitActiveNodes { get; set; }
        public ClusterPerformanceProperty Performance { get; set; }

        public DateTime RegistrationDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public long ElapsedSecondsFromLastActivity => LastActivityDate.ToSecondsTime();
        public bool IsOnline => ElapsedSecondsFromLastActivity < 10;


        public virtual List<MatchEntity> MatchEntities { get; set; } = new List<MatchEntity>(256);
        //public int InitializedSlots => NodeList.Count(n => n.State == NodeInstanceState.WaitingStartGame /*&& n.ElapsedSecondsFromLastActivitySessionInstance > 1500*/);
        //public int NonInitializedSlots => LimitActiveNodes - NodeList.Count;
        //public int AvalibleFreeSlots => InitializedSlots + NonInitializedSlots;

        public Guid RowVersion { get; set; }

        public static readonly IPAddress ThisMachineAddress = GetRemoteIpAddress();
        public static IPAddress GetRemoteIpAddress()
        {
            //return IPAddress.Parse("78.46.46.148");
            try
            {
                var addressList = Dns.GetHostEntry(Dns.GetHostName()).AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork);
                IPAddress address = addressList.FirstOrDefault(a => a.ToString().Contains("192.168") == false);
                return address ?? IPAddress.Parse("127.0.0.1");
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage("[GetRemoteIpAddress]", e);
                return IPAddress.Parse("127.0.0.1");
            }
        }

        public static void OnTick()
        {
            LoggerContainer.ClusterLogger.Info("============ MatchMakingJob =====================\n");
            try
            {
                using (var db = new DataRepository())
                {
                    var address = GetRemoteIpAddress().ToString();
                    var cluster = db.ClusterInstance.FirstOrDefault(c => c.MachineAddress == address);
                    if (cluster == null)
                    {
                        LoggerContainer.ClusterLogger.Info($"Added new Cluster {address}");
                        cluster = db.ClusterInstance.Add(new ClusterInstance
                        {
                            MachineAddress = address,
                            RegionTypeId = ClusterRegionId.RuCentral,
                            LastActivityDate = DateTime.UtcNow,
                            RegistrationDate = DateTime.UtcNow,
                            MachineName = Environment.MachineName,
                            Performance = new ClusterPerformanceProperty()
                        });
                        db.SaveChanges();
                    }

                    cluster.OnCheckNodes();
                }
            }
            catch (Exception e)
            {
                LoggerContainer.MatchMakingLogger.WriteExceptionMessage("[OnTickNodes]", e);
            }
        }

        //==============================================================================================
        //                                          StartCreatedNodes
        #region StartCreatedNodes
        public void StartCreatedNodes()
        {
            try
            {
                using (var db = new DataRepository())
                {
                    List<Task> saves = new List<Task>();
                    var nodes = NodesToStart(db);
                    foreach (var node in nodes)
                    {
                        node.OnStartNode();
                        saves.Add(db.SaveChangesAsync());
                    }
                    Task.WaitAll(saves.ToArray());
                }
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage("[StartCreatedNodes]", e);
            }
        }

        public MatchEntity[] NodesToStart(DataRepository db)
        {
            var nodes = db.MatchEntity.Where(p => p.State == MatchGameState.Created && p.MachineAddress == MachineAddress).Take(5).ToArray();
            LoggerContainer.ClusterLogger.Trace($"[NodesToStart][MachineAddress: {MachineAddress}][Nodes: {nodes.Length}]");
            return nodes;
        }
        #endregion

        //==============================================================================================
        //                                          PrepareToShutdownNodes
        #region PrepareToShutdownNodes
        public void PrepareToShutdownNodes()
        {
            try
            {
                using (var db = new DataRepository())
                {
                    List<Task> saves = new List<Task>();
                    var nodes = NodesToShutdownPrepare(db);
                    foreach (var node in nodes)
                    {
                        node.State = MatchGameState.FinishedToShutdown;
                        saves.Add(db.SaveChangesAsync());
                    }
                    Task.WaitAll(saves.ToArray());
                }
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage("[ShutdownCompletedNodes]", e);
            }
        }

        public MatchEntity[] NodesToShutdownPrepare(DataRepository db)
        {
            var nodes = db.MatchEntity.Where(p => p.State == MatchGameState.Finished && p.MachineAddress == MachineAddress).ToArray();
            var avalible = nodes.Where(p => p.Finished != null && p.Finished.Value.ToElapsedSeconds() > 30).ToArray();
            LoggerContainer.ClusterLogger.Trace($"[NodesToStart][MachineAddress: {MachineAddress}][Nodes: {nodes.Length}] [avalible: {avalible.Length}]");
            return avalible;
        }

        #endregion


        //==============================================================================================
        //                                          ShutdownCompletedNodes
        #region ShutdownCompletedNodes
        public void ShutdownCompletedNodes()
        {
            try
            {
                using (var db = new DataRepository())
                {
                    List<Task> saves = new List<Task>();
                    var nodes = NodesToShutdown(db);
                    foreach (var node in nodes)
                    {
                        node.OnStopNode();
                        saves.Add(db.SaveChangesAsync());
                    }
                    Task.WaitAll(saves.ToArray());
                }
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage("[ShutdownCompletedNodes]", e);
            }
        }

        public MatchEntity[] NodesToShutdown(DataRepository db)
        {
            var nodes = db.MatchEntity.Where(p => p.State == MatchGameState.FinishedToShutdown && p.MachineAddress == MachineAddress).ToArray();
            LoggerContainer.ClusterLogger.Trace($"[NodesToStart][MachineAddress: {MachineAddress}][Nodes: {nodes.Length}]");
            return nodes;
        }

        #endregion

        //==============================================================================================
        //                                          ShutdownCompletedNodes
        #region ShutdownCompletedNodes
        public void ShutdownFreezedNodes()
        {
            try
            {
                using (var db = new DataRepository())
                {
                    List<Task> saves = new List<Task>();
                    var nodes = FreezedNodesToShutdown(db);
                    foreach (var node in nodes)
                    {
                        node.OnStopNode();
                        saves.Add(db.SaveChangesAsync());
                    }
                    Task.WaitAll(saves.ToArray());
                }
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage("[ShutdownFreezedNodes]", e);
            }
        }

        public MatchEntity[] FreezedNodesToShutdown(DataRepository db)
        {
            var now = DateTime.UtcNow.AddHours(-2);
            var nodes = db.MatchEntity.Where(p => p.MachineAddress == MachineAddress && p.State != MatchGameState.Shutdown && p.Created <= now).ToArray();
            LoggerContainer.ClusterLogger.Trace($"[FreezedNodesToShutdown][MachineAddress: {MachineAddress}][Nodes: {nodes.Length}]");
            return nodes;
        }

        #endregion


        //==============================================================================================
        //                                          OnCheckNodes
        public void OnCheckNodes()
        {
            //=====================================================================================
            LoggerContainer.ClusterLogger.Info($"================= OnCheckNodes [{MachineAddress} / {RegionTypeId}] ==================");
            LoggerContainer.ClusterLogger.Info($"ContentIsExist: {MatchEntity.IsExistContent()} | ServerRoot: {MatchEntity.ServerRootDirectoryLocation}");

            //=====================================================================================
            StartCreatedNodes();
            ShutdownFreezedNodes();
            PrepareToShutdownNodes();
            ShutdownCompletedNodes();
        }


        public class Configuration : EntityTypeConfiguration<ClusterInstance>
        {
            public Configuration()
            {
                HasKey(p => p.MachineAddress);
                ToTable("public.ClusterInstanceEntity");
                HasMany(c => c.MatchEntities).WithRequired(c => c.MachineInstance).HasForeignKey(c => c.MachineAddress).WillCascadeOnDelete(false);
                Property(p => p.MachineName).HasMaxLength(64);
                Property(p => p.MachineAddress).HasMaxLength(64);
                Property(p => p.RowVersion).IsConcurrencyToken(false);

            }
        }
    }
}
