﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Server.Models.Promo
{
    public class SocialGroupEntity
    {
        public SocialGroupId SocialGroupId { get; set; }
        public long PostIntevalSeconds { get; set; }
        public TimeSpan PostInteval => TimeSpan.FromSeconds(PostIntevalSeconds);

        public virtual List<SocialTaskEntity> TaskList { get; private set; } = new List<SocialTaskEntity>(32); 

        public class Configuration : EntityTypeConfiguration<SocialGroupEntity>
        {
            public Configuration()
            {
                HasKey(p => p.SocialGroupId);
                ToTable("promo.SocialGroupEntity");
                HasMany(p => p.TaskList).WithRequired(p => p.SocialGroupEntity).HasForeignKey(p => p.SocialGroupId);
            }
        }
    }
}
