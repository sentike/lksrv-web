﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store;

namespace Loka.Server.Models.Promo
{
    public class SocialTaskEntity
    {
        public Guid TaskId { get; set; }
        public SocialGroupId SocialGroupId { get; set; }
        public virtual SocialGroupEntity SocialGroupEntity { get; set; }
        public string UrlAddress { get; set; }
        public Uri Url => new Uri(UrlAddress);
        public StoreItemCost Bonus { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public class Configuration : EntityTypeConfiguration<SocialTaskEntity>
        {
            public Configuration()
            {
                HasKey(p => p.TaskId);
                ToTable("promo.SocialTaskEntity");
                Property(p => p.UrlAddress).IsRequired().HasMaxLength(256);
            }
        }
    }
}
