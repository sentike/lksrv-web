﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Server.Models.Promo
{
    public enum SocialGroupId
    {
        None,
        Vkontakte,
        FaceBook,
        Twitter,
        Steam,
        GooglePlus,

        End
    }
}
