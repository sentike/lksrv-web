﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Server.Partner
{
    public class PartnerCodeEntity
    {
        public Guid PromoId { get; set; }
        public Guid PartnerId { get; set; }
        public virtual PartnerAccountEntity PartnerAccount { get; set; }

        public string Name { get; set; }
        public DateTime Validity { get; set; }
        public bool Confirmed { get; set; }

        public class Configuration : EntityTypeConfiguration<PartnerCodeEntity>
        {
            public Configuration()
            {
                HasKey(p => p.PromoId);
                ToTable("promo.Codes");
            }
        }
    }
}
