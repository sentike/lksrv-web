﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;


namespace Loka.Server.Models.Partner
{
    public class PromoItemEntity
    {
        public int ItemId { get; set; }
        public short ModelId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }


        
        
        public virtual ItemInstanceEntity ModelInstance { get; set; }

        public class Configuration : EntityTypeConfiguration<PromoItemEntity>
        {
            public Configuration()
            {
                HasKey(p => p.ItemId);
                ToTable("promo.PromoItemEntity");
                HasRequired(i => i.ModelInstance).WithMany().HasForeignKey(i => new {i.ModelId, i.CategoryTypeId});
            }
        }

    }
}