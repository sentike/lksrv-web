﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Loka.Server.Partner
{
    public class PartnerAccountEntity
    {
        public Guid PartnerId { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public DateTime RegistrationDate { get; set; }

        public virtual HashSet<PartnerCodeEntity> PromoCodeList { get; set; } = new HashSet<PartnerCodeEntity>();
        public class Configuration : EntityTypeConfiguration<PartnerAccountEntity>
        {
            public Configuration()
            {
                HasKey(p => p.PartnerId);
                ToTable("promo.Partners");
                HasMany(p => p.PromoCodeList).WithRequired(p => p.PartnerAccount).HasForeignKey(p => p.PartnerId);
            }
        }
    }
}