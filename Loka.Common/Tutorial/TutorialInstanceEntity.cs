﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Tutorial
{
    public class TutorialInstanceEntity
    {
        public long TutorialId { get; set; }
        public string TutorialName { get; set; }
        public virtual ICollection<TutorialStepEntity> StepEntities { get; set; } = new List<TutorialStepEntity>(8);

        //==================================================================
        public string AdminComment { get; set; }
        public DateTime CreatedDate { get; set; }
    }


    public class Configuration : EntityTypeConfiguration<TutorialInstanceEntity>
    {
        public Configuration()
        {
            HasKey(i => i.TutorialId);
            ToTable("store.TutorialInstanceEntity");
            Property(p => p.TutorialName).IsRequired().HasMaxLength(128);
            Property(p => p.TutorialName).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute { IsUnique = true }));
            HasMany(p => p.StepEntities).WithRequired(p => p.TutorialEntity).HasForeignKey(p => p.TutorialId).WillCascadeOnDelete(true);
            Property(p => p.AdminComment).IsOptional().HasMaxLength(128);
        }
    }

}
