﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Loka.Common.Store;

namespace Loka.Common.Tutorial
{
    public class TutorialShortView
    {
        [Display(Name = "#")]
        public long TutorialId { get; set; }

        [Display(Name = "Название урока в Blueprint")]
        public string TutorialName { get; set; }

        [Display(Name = "Этапы")]
        public ICollection<TutorialStepView> Steps { get; set; } = new List<TutorialStepView>();
    }

    public class TutorialStepView
    {
        public long StepId { get; set; }

        [Display(Name = "Вознагражение. Категория предмета")]
        public CategoryTypeId RewardCategoryId { get; set; }

        [Display(Name = "Вознагражение. Модель предмета")]
        public short RewardModelId { get; set; }

        public long RewardGive { get; set; }
    }


    public static class ViewHelper
    {
        private static readonly IMapper Mapper = new MapperConfiguration(c =>
            {
                c.CreateMap<TutorialStepEntity, TutorialStepView>()
                    .ForMember(p => p.RewardCategoryId, p => p.MapFrom(m => m.RewardCategoryId ?? CategoryTypeId.End))
                    .ForMember(p => p.RewardModelId, p => p.MapFrom(m => m.RewardModelId ?? 0));

                c.CreateMap<TutorialInstanceEntity, TutorialShortView>()
                    .ForMember(p => p.Steps, p => p.MapFrom(m => m.StepEntities.AsView()));
            }
        ).CreateMapper();

        public static TutorialStepView AsView(this TutorialStepEntity entity)
        {
            return Mapper.Map<TutorialStepView>(entity);
        }

        public static List<TutorialStepView> AsView(this ICollection<TutorialStepEntity> entity)
        {
            return entity.Select(p => p.AsView()).ToList();
        }

        public static TutorialShortView AsView(this TutorialInstanceEntity entity)
        {
            return Mapper.Map<TutorialShortView>(entity);
        }

        public static List<TutorialShortView> AsView(this ICollection<TutorialInstanceEntity> entity)
        {
            return entity.Select(p => p.AsView()).ToList();
        }
    }

}
