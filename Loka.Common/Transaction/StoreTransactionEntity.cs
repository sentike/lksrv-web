﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store;

namespace Loka.Common.Transaction
{
    /// <summary>
    /// Оплата покупки в магазине
    /// </summary>
    public class StoreTransactionEntity : BasicTransactionEntity
    {
        public Guid SessionId { get; set; }

        /// <summary>
        /// Кому покупает
        /// </summary>
        public Guid RecipientId { get; set; }

        //=====================================
        public long Cost { get; set; }
        public long Balanace { get; set; }
        public GameCurrency Currency { get; set; }

        //=====================================
        public bool Notified { get; set; }

        //=====================================

        
        public StoreTransactionEntity() { }
        public StoreTransactionEntity(Guid account, short model, CategoryTypeId category, long amount, long last)
            : base(account, model, category, amount, last)
        {

        }

        //=====================================
        public new class Configuration : EntityTypeConfiguration<StoreTransactionEntity>
        {
            public Configuration()
            {
                Map(configuration =>
                {
                    configuration.ToTable("transaction.StoreTransactionEntity");
                    configuration.MapInheritedProperties();
                });

                Property(p => p.RecipientId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            }
        }
    }
}
