﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store;
using Loka.Server.Player.Models;

namespace Loka.Common.Transaction
{
    public abstract class BasicTransactionEntity
    {        
        //=====================================
        public Guid EntityId { get; set; }
        public Guid AccountId { get; set; }
        public virtual PlayerEntity AccountEntity { get; set; }

        public DateTime CreatedDate { get; set; }


        //=====================================
        public short ModelId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }

        public long Amount { get; set; }
        public long LastAmount { get; set; }

        //=====================================
        protected BasicTransactionEntity() { }
        protected BasicTransactionEntity(Guid account, short model, CategoryTypeId category, long amount, long last)
        {
            EntityId = Guid.NewGuid();
            AccountId = account;
            CreatedDate= DateTime.UtcNow;
            CategoryTypeId = category;
            ModelId = model;

            LastAmount = last;
            Amount = amount;
        }


        //=====================================
        public class Configuration : EntityTypeConfiguration<BasicTransactionEntity>
        {
            public Configuration()
            {
                HasKey(p => p.EntityId);
                HasRequired(p => p.AccountEntity).WithMany().HasForeignKey(p => p.AccountId);
                Property(p => p.AccountId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            }
        }
    }
}
