﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Transaction
{
    public interface ICountable
    {
        Guid EntityId { get; }
        long Count { get; }
    }
}
