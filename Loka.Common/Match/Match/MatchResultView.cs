﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Match.Match
{
    public class MatchResultView
    {
        public Guid WinnerTeamId { get; set; }
        public List<MatchResultMemberView> Members { get; set; }
    }
}
