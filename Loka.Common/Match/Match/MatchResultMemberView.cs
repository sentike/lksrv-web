﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Achievements;
using Loka.Common.Match.Member;

namespace Loka.Common.Match.Match
{
    public struct MatchResultMemberAchievementView
    {
        public AchievementTypeId AchievementTypeId { get; set; }
        public int Count { get; set; }
    }

    public class MatchResultMemberView
    {
        public Guid MemberId { get; set; }
        public MatchMemberAward Award { get; set; }
        public MatchMemberStatisticContainer Statistic { get; set; }
        public List<MatchResultMemberAchievementView> Achievements { get; set; }
    }
}
