﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Building.Player;

namespace Loka.Common.Match.Match
{
    public class MatchBuildingInformation
    {
        public Guid? OwnerId { get; set; }
        public string OwnerName { get; set; }
        public List<WorldBuildingItemView> Buildings { get; set; } = new List<WorldBuildingItemView>();
    }

    public class MatchBuildingUpdateContainer
    {
        public List<MatchBuildingView> Items { get; set; }
    }

    public class MatchBuildingView
    {
        public Guid ItemId { get; set; }

        /// <summary>
        /// Сколько осталось здоровья
        /// </summary>
        public long Health { get; set; }

        /// <summary>
        /// Сколько было украдено из хранилища
        /// </summary>
        public long Storage { get; set; }
    }
}
