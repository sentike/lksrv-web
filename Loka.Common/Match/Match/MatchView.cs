﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.Member;
using Loka.Server.Infrastructure.Session;

namespace Loka.Common.Match.Match
{
    public class MatchView
    {
        public MatchView(MatchEntity match)
        {
            NodeId = match.MatchId;

            MatchId = match.MatchId;
            Options = match.Options;
            TeamList = match.TeamList.Select(m => m.TeamId).ToList();
            Members = match.MemberList.Select(m => new MatchMemberView(m)).ToList();
        }

        public Guid NodeId{ get; set; }
        public Guid MatchId{ get; set; }

        public SearchSessionOptions Options { get; set; }

        public IEnumerable<Guid> TeamList{ get; set; }
        public IEnumerable<MatchMemberView> Members{ get; set; }
    }
}
