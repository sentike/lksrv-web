﻿using Loka.Server.Player.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AutoMapper;
using Loka.Common.Match.Match;
using Loka.Common.Match.Team;
using Loka.Common.Store.Service;

using Loka.Server.Models.Account;

namespace Loka.Common.Match.Member
{
    public class MatchMemberEntity : IMatchMemberEntityInterface
    {
        public static MatchMemberEntity Factory(PlayerEntity player, ref int? squadIndex, Guid session)
        {
            if (squadIndex != null && player.QueueEntity.Members.Count > 1) squadIndex++;
            return new MatchMemberEntity
            {
                MemberId = Guid.NewGuid(),
                Award = new MatchMemberAward(),
                PlayerEntity = player,
                PlayerId = player.PlayerId,
                Statistic = new MatchMemberStatisticContainer(),
                State = MatchMemberState.Started,
                Squad = squadIndex ?? 0,
                Level = 1,
                DifficultyFloat = player.Statistic.Threat,
                SessionToken = session
            };
        }

        public Guid MemberId { get; set; }
        public Guid? NextMemberId { get; set; }
        public Guid? PreviewMemberId { get; set; }

        public virtual Guid MatchId { get; set; }
        public virtual MatchEntity MatchEntity { get; set; }

        public virtual Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public virtual Guid TeamId { get; set; }
        public virtual MatchTeamEntity TeamEntity { get; set; }

        public virtual List<MatchMemberAchievements> Achievements { get; set; } = new List<MatchMemberAchievements>(2);

        public byte Level { get; set; }
        public MatchMemberAward Award { get; set; }

        public MatchMemberStatisticContainer Statistic { get; set; }
        public int Squad { get; set; }

        /// <summary>
        /// Difficulty in Normalized
        /// </summary>
        public double Difficulty { get; set; }

        public double DifficultyFloat
        {
            get { return Difficulty/10.0f; }
            set { Difficulty = Convert.ToByte(Math.Ceiling(value*10)); }
        }

        public MatchMemberState State { get; set; }
        public bool Finished => State == MatchMemberState.Finished || State == MatchMemberState.Deserted;

        public override string ToString()
        {
            return $"{MemberId} | {PlayerEntity?.PlayerName} | Squad: {Squad} | Level: {Level} | Team: {TeamId}";
        }

        public Guid SessionToken { get; set; }

        public class Configuration : EntityTypeConfiguration<MatchMemberEntity>
        {
            public Configuration()
            {
                HasKey(p => p.MemberId);
                ToTable("matches.MatchMember");
                HasMany(p => p.Achievements).WithRequired(p => p.MemberEntity).HasForeignKey(p => p.MemberId);
                HasRequired(p => p.PlayerEntity).WithMany().HasForeignKey(p => p.PlayerId);
                Ignore(p => p.DifficultyFloat);

                Ignore(p => p.NextMemberId);
                Ignore(p => p.PreviewMemberId);

            }
        }


    }
}
