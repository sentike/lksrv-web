﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store.Fraction;
using Loka.Player;

namespace Loka.Common.Match.Member
{
    public class MatchMemberView
    {
        public Guid PlayerId { get; set; }
        public Guid TeamId { get; set; }

        public string PlayerName { get; set; }
        public bool ExistPremiumAccount { get; set; }
        public IEnumerable<PlayerProfileEntityContainer> Profiles { get; set; }
        //public Response Loot { get; set; }
        public FractionTypeId FractionId { get; set; }

        public MatchMemberView(MatchMemberEntity member)
        {

            var entity = member.PlayerEntity;
            PlayerId = member.MemberId;
            TeamId = member.TeamId;
            PlayerName = entity.PlayerName;
            FractionId = entity.CurrentFractionEntity?.FractionId ?? FractionTypeId.Keepers;
            ExistPremiumAccount = entity.PremiumEndDate.HasValue && entity.PremiumEndDate.Value >= DateTime.UtcNow;

            Profiles = entity.GetProfileContainers() /*.Where(p => p.IsEnabled)*/;
            //Loot = new RequestLootData.Response(entity, true);
        }
    }
}
