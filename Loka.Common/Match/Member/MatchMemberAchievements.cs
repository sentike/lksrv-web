using System;
using System.Data.Entity.ModelConfiguration;
using Loka.Common.Achievements;
using Loka.Common.Match.Match;

namespace Loka.Common.Match.Member
{
    public class MatchMemberAchievements
    {
        public MatchMemberAchievements()
        {
            
        }

        public MatchMemberAchievements(MatchResultMemberAchievementView achievement)
        {
            AchievementId = Guid.NewGuid();
            AchievementTypeId = achievement.AchievementTypeId;
            AchievementCount = achievement.Count;
        }

        public Guid AchievementId { get; set; }
        public AchievementTypeId AchievementTypeId { get; set; }
        public int AchievementCount { get; set; }
        public Guid MemberId { get; set; }
        public virtual MatchMemberEntity MemberEntity { get; set; }


        public class Configuration : EntityTypeConfiguration<MatchMemberAchievements>
        {
            public Configuration()
            {
                HasKey(p => p.AchievementId);
                ToTable("matches.MemberAchievements");
            }
        }
    }
}