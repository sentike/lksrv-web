﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Common.Store.Abstract;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Queue;
using Loka.Server.Player.Models;
using VRS.Infrastructure;

namespace Loka.Common.Match.GameMode
{
    public class HarvestGameMode : GameModeEntity
    {
        public HarvestGameMode() { }
        public HarvestGameMode(short score) : base(GameModeTypeId.Attack, score) { }

        public const int PlayersToSearch = 50;
        public static Guid? TakeTargetPlayer(DataRepository db, List<QueueEntity> members, int range = PlayersToSearch)
        {
            var attackers = members.Select(p => p.PartyId).ToArray();
            var friends = db.PlayerFriendEntity.AsNoTracking().Where(p => attackers.Contains(p.FriendId)).Select(p => p.PlayerId).ToArray();
            var ignores = attackers.Concat(friends).Distinct().ToArray();
            return TakeTargetPlayer(db, ignores);
        }

        public static Guid? TakeTargetPlayer(DataRepository db, ICollection<Guid> ignores, int range = PlayersToSearch)
        {
            //var now = DateTime.UtcNow.AddHours(-1);
            var now = DateTime.UtcNow.AddHours(-48);
            var players = db.AccountPlayerEntity.AsNoTracking().Where(p => p.LastActivityDate >= now && ignores.Contains(p.PlayerId) == false)
                .Where(p => p.WorldBuildingEntities.Any(/*b => b.Storage > 1000*/))
                .OrderByDescending(p => p.LastActivityDate).Take(range)
#if DEBUG
                .Select(p => new { p.PlayerId, p.PlayerName}).ToArray();
#else
                .Select(p => new { p.PlayerId}).ToArray();
#endif
            if (players.Any())
            {
//#if DEBUG
                return players.Select(p => p.PlayerId).PickRandom();
//#else
//            return players.PickRandom();
//#endif
            }
            return null;
        }

        protected override GameMapEntity TakeRandomMap()
        {
            throw new NotSupportedException();
        }

        //protected override QueueEntity[] TakeQueueEntities(DataRepository repository)
        //{
        //    Guid sentike = Guid.Parse("41a5de8d-ace1-44dc-8891-e5bae823a121");
        //    return repository.QueueEntity.Where(q => q.PartyId == sentike).ToArray();
        //}

        public override bool AttackTo(DataRepository db, Guid attacker, Guid target)
        {
            //try
            //{
            //
            //    //----------------------------------------------------------------------------
            //    var attackers = db.QueueEntity.Where(q => q.PartyId == attacker).ToList();
            //    var defender = TakeTargetPlayer(db, target, attackers, 1);
            //
            //    //----------------------------------------------------------------------------
            //    if (attackers.Any() == false)
            //    {
            //        return false;
            //    }
            //
            //    //----------------------------------------------------------------------------
            //    if (defender == null)
            //    {
            //        return false;
            //    }
            //
            //    //----------------------------------------------------------------------------
            //    var defenders = defender.QueueEntity.Members.Where(p =>
            //            p.MatchEntity == null
            //            || p.MatchEntity.GameModeTypeId == GameModeTypeId.BuildingMode && p.MatchEntity.UniversalId ==
            //            defender.QueueEntity.MatchEntity.UniversalId
            //            || p.MatchEntity.GameModeTypeId == GameModeTypeId.BuildingMode &&
            //            defender.QueueEntity.MatchEntity.GameModeTypeId == GameModeTypeId.BuildingMode
            //            || p.MatchEntity.IsCompleteState
            //            || p.MatchMember == null
            //            || p.MatchMember.State == MatchMemberState.Deserted)
            //        .ToList();
            //
            //    if (defenders.Contains(defender.QueueEntity) == false)
            //    {
            //        defenders.Add(defender.QueueEntity);
            //    }
            //
            //    //----------------------------------------------------------------------------
            //    defender.UpdatePlayerStorage();
            //
            //    //----------------------------------------------------------------------------
            //    var teamAttacker = new MatchTeamEntity
            //    {
            //        TeamId = Guid.NewGuid(),
            //        MemberList = attackers.Select(m => MatchMemberEntity.Factory(m.PlayerEntity)).ToList()
            //    };
            //
            //    //----------------------------------------------------------------------------
            //    var teamDefender = new MatchTeamEntity
            //    {
            //        TeamId = Guid.NewGuid(),
            //        MemberList = defenders.Select(m => MatchMemberEntity.Factory(m.PlayerEntity)).ToList()
            //    };
            //
            //    //----------------------------------------------------------------------------
            //    var members = attackers.Concat(defenders).ToList();
            //    foreach (var m in members)
            //    {
            //        LoggerContainer.TutorialLogger.Info($"[members][{m.PlayerEntity?.PlayerName}]");
            //    }
            //
            //    LoggerContainer.TutorialLogger.Info(
            //        $"{Environment.NewLine}---------------------------------------------------");
            //    foreach (var m in attackers)
            //    {
            //        LoggerContainer.TutorialLogger.Info($"[attackers][{m.PlayerEntity?.PlayerName}]");
            //    }
            //
            //    foreach (var m in defenders)
            //    {
            //        LoggerContainer.TutorialLogger.Info($"[defenders][{m.PlayerEntity?.PlayerName}]");
            //    }
            //
            //
            //    //----------------------------------------------------------------------------
            //    var match = new MatchEntity(new List<MatchTeamEntity>
            //    {
            //        teamAttacker,
            //        teamDefender
            //    }, GameMapTypeId.Lobby, 50, TimeSpan.FromMinutes(10).TotalSeconds)
            //    {
            //        UniversalId = teamDefender.TeamId,
            //        GameModeTypeId = GameModeId,
            //        NumberOfBots = 0
            //    };
            //
            //    //----------------------------------------------------------------------------
            //    db.MatchEntity.Add(match);
            //
            //    //----------------------------------------------------------------------------
            //    defender.UpdateLastAttackDate(false);
            //    foreach (var a in attackers)
            //    {
            //        a?.PlayerEntity?.UpdateLastAttackDate(false);
            //    }
            //
            //
            //    //----------------------------------------------------------------------------
            //    LoggerContainer.MatchMakingLogger.Warn($"[HarvestGameMode]:: match[{match.MatchId}] | Difficulty {match.Difficulty} | {match.GameMapTypeId}");
            //    ApplyMatchPropertyForMember(members, match);
            //    db.SaveChanges();
            //}
            //catch (Exception e)
            //{
            //    LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"[HarvestGameMode]", e);
            //    return false;
            //}
            return true;
        }

        public PlayerEntity TakeTargetPlayer(DataRepository db, Guid? id, List<QueueEntity> members, int range = PlayersToSearch, int attemps = 5)
        {
            if (id == null)
            {
                id = TakeTargetPlayer(db, members);
            }

            //===================================================================================
            for (var i = 1; i <= attemps; i++)
            {
                var player = db.AccountPlayerEntity.SingleOrDefault(p => p.PlayerId == id);
                if (player == null)
                {
                    id = TakeTargetPlayer(db, members, PlayersToSearch * i);
                }
                else
                {
                    return player;
                }
            }

            //===================================================================================
            return null;
        }

        public override void Tick()
        {
            while (true)
            {
                using (var db = new DataRepository())
                {
                    var queues = TakeQueueEntities(db);
                    if (queues.Any() == false) return;
                    Logger.Info($"[HarvestGameMode] Begin make match, gm: {GameModeFlag} | queues: {queues.Length}");

                    //=================================================
                    var party = queues.First(q => q.IsLeader);
                    var attackers = party.Members;
                    var defender = TakeTargetPlayer(db, Guid.Empty, attackers);

                    //=================================================
                    AttackTo(db, party.PartyId, defender.PlayerId);
                }
            }
        }


        public new class Configuration : EntityTypeConfiguration<HarvestGameMode>
        {
            public Configuration()
            {
                ToTable("gamemode.HarvestGameMode");
            }
        }

    }
}
