﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using JetBrains.Annotations;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Common.Store;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Queue;

namespace Loka.Common.Match.GameMode.Duel
{
    public class DuelGameModeEntity : GameModeEntity
    {
        public DuelGameModeEntity() { }
        public DuelGameModeEntity(short score) : base(GameModeTypeId.DuelMatch, score) { }

        public override void Tick()
        {
            using (var db = new DataRepository())
            {
                var queues = TakeQueueEntities(db);
                Logger.Info($"[DuelGameModeEntity] Begin make match, gm: {GameModeFlag} | queues: {queues.Length}");
                if (queues.Length < 2) return;

                var players = queues.OrderBy(q => q.PlayerEntity.ArmourLevel).ToList();
                var playersInSquad = queues.Where(p => p.InSquad && p.Members.Count == 2).OrderBy(p => p.PartyId).ToList();
                players.RemoveAll(p => playersInSquad.Contains(p));

                int? squad = null;
                List<MatchMemberEntity> members = players.Select(member => FactoryMember(db, ref squad, member)).ToList();
                List<MatchMemberEntity> membersInSquad = playersInSquad.Select(member => FactoryMember(db, ref squad, member)).ToList();

                CreateMatch(db, ref members);
                CreateMatch(db, ref membersInSquad);

                db.SaveChanges();
            }
        }

        private void CreateMatch
            (
                [NotNull] DataRepository db,
                [NotNull] ref List<MatchMemberEntity> members
            )
        {
            while (members.Count >= 2)
            {
                GameMapEntity gameMap;

                //=======================================================
                var teamMembers = members.Take(2).ToArray();


                var duelEnity = members.First().PlayerEntity.QueueEntity;
                //=======================================================
                if (teamMembers.Any(p => p.PlayerEntity.IsAllowPay(duelEnity.Options.Bet)) == false)
                {
                    foreach (var member in teamMembers)
                    {
                        member.PlayerEntity.QueueEntity.Ready = QueueState.NotEnouthMoney;
                    }
                    return;
                }

                //=======================================================
                var teams = new List<MatchTeamEntity>(2);
                teams.AddRange(teamMembers.Select(t => new MatchTeamEntity
                {
                    TeamId = Guid.NewGuid(),
                    MemberList = { t },
                }));


                //=======================================================
                if (duelEnity.Options.GameMap == GameMapFlags.End 
                    || duelEnity.Options.GameMap <= GameMapFlags.None)
                {
                    gameMap = TakeRandomMap();
                }
                else
                {
                    gameMap = Maps.FirstOrDefault(m => m.MapAsFlag.HasFlag(duelEnity.Options.GameMap));
                }

                if (gameMap == null)
                {
                    gameMap = TakeRandomMap();
                }

                //=======================================================
                if (duelEnity.Options.WeaponType == DuelWeaponFlags.End ||
                    duelEnity.Options.WeaponType <= DuelWeaponFlags.None)
                {
                    duelEnity.Options.WeaponType = Enum.GetValues(typeof(DuelWeaponFlags)).Cast<DuelWeaponFlags>().ToArray().PickRandom();
                }

                //=======================================================
                duelEnity.Options.GameMode = GameModeFlags.DuelMatch;
                duelEnity.Options.GameMap = gameMap.MapAsFlag;
	            duelEnity.Options.AdditionalFlag = Math.Max( 1, duelEnity.Options.AdditionalFlag);
	            duelEnity.Options.AdditionalFlag = Math.Min(15, duelEnity.Options.AdditionalFlag);
				duelEnity.Options.RoundTimeInSeconds = Convert.ToInt16(duelEnity.Options.AdditionalFlag * 90);

	            //=======================================================
	            if (duelEnity.Options.Bet.Amount <= 0)
	            {
		            duelEnity.Options.Bet.Amount = 500;
		            duelEnity.Options.Bet.Currency = GameCurrency.Money;

	            }

	            //=======================================================

				var match = new MatchEntity(teams, duelEnity.Options);
                ApplyMatchPropertyForMember(members.Select(m => m.PlayerEntity.QueueEntity), match);
                members.RemoveAll(m => teamMembers.Contains(m));
                db.MatchEntity.Add(match);
                db.SaveChanges();
            }
        }

        public override void Finish(MatchEntity session, MatchResultView result, DataRepository db)
        {
            //try
            //{
            //    var winnerMember = session.MemberList.FirstOrDefault(m => m.TeamId == result.WinnerTeamId);
            //    var looseMember = session.MemberList.FirstOrDefault(m => m.TeamId != result.WinnerTeamId);
            //
            //    Logger.Error($"Duel: winnerTeam: {result.WinnerTeamId} | winnerMember: {winnerMember} | looseMember: {looseMember}");
            //
            //    if (winnerMember != null && looseMember != null)
            //    {
            //        var bet = session.Options.Bet;
            //        winnerMember.PlayerEntity.Give(bet);
            //        looseMember.PlayerEntity.Give(bet.Invert());
            //    }
            //    else
            //    {
            //        foreach (var member in session.MemberList)
            //        {
            //            Logger.Error($"Meber {member.PlayerEntity.PlayerName} in {member.TeamId}, memberId: {member.MemberId}");
            //        }
            //    }
            //
            //}
            //catch (Exception exception)
            //{
            //    Logger.Error(exception);
            //}

            try
            {
                Logger.Error($"Finish duel, members: {session.MemberList.Count} | {session.MatchId}");
                foreach (var member in session.MemberList.Where(p => p.State == MatchMemberState.Started).ToArray().Select(p => p.PlayerEntity.QueueEntity).ToArray())
                {
                    member.SetDefaultSquad();
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }

            base.Finish(session, result, db);
        }

        protected override MatchEntity TakeAvalibleMatch(DataRepository repository)
        {
            return null;
        }


        public new class Configuration : EntityTypeConfiguration<DuelGameModeEntity>
        {
            public Configuration()
            {
                ToTable("gamemode.DuelGameModeEntity");
            }
        }

    }
}