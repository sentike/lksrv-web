using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Threading;

using Loka.Common.Cluster;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Session;
using VRS.Infrastructure;


namespace Loka.Common.Match.GameMode.PvP
{
    public class TeamGameModeEntity : GameModeEntity
    {
        public TeamGameModeEntity() { }
        public TeamGameModeEntity(GameModeTypeId gm, short score) : base(gm, score) { }

        public new class Configuration : EntityTypeConfiguration<TeamGameModeEntity>
        {
            public Configuration()
            {
                ToTable("gamemode.TeamGameModeEntity");
            }
        }

        public override void Tick()
        {
            if (PrepareTimeInSeconds < 10) PrepareTimeInSeconds = 10;
            //if(GameModeFlag != GameModeFlags.ResearchMatch) return;
            //if(GameModeId != GameModeTypeId.TeamDeadMatch && GameModeId != GameModeTypeId.DuelMatch && GameModeId != GameModeTypeId.LostDeadMatch) return;
            //for (var l = 1; l <= 10; l++)
            try
            {
                var l = 1;
                using (var db = new DataRepository())
                {
                    var queues = TakeQueueEntities(db);
                    LoggerContainer.MatchMakingLogger.Info($"Begin make match[1], gm: {GameModeFlag} | queues: {queues.Length}");

                    if (queues.Any() == false)
                    {
                        LoggerContainer.MatchMakingLogger.Info($"Begin make match[1.5], gm: {GameModeFlag} | queues: {queues.Length}");
                        return;
                    }

                    LoggerContainer.MatchMakingLogger.Info($"Begin make match[2], gm: {GameModeFlag} | queues: {queues.Length}");

                    //
                    var startDateTime = DateTime.UtcNow;
	                GameMapEntity gameMap = TakeRandomMap(db, queues);

                    LoggerContainer.MatchMakingLogger.Info($"Begin make match[3], gm: {GameModeFlag} - {gameMap.MapId} | queues: {queues.Length}");

                    byte numberOfBots = 0;

                    var team1 = new MatchTeamEntity() {TeamId = Guid.NewGuid()};
                    var team2 = new MatchTeamEntity() {TeamId = Guid.NewGuid()};
                    var buffer = new MatchTeamEntity() {MemberList = new List<MatchMemberEntity>(256)};

                    //  List of squads that can play at the current level of equipment
                    var squads = TakeSquadEntities(queues);
                    var members = squads.SelectMany(s => s.Members).ToArray();

                    LoggerContainer.MatchMakingLogger.Info($"Level: {l} | Map: {gameMap.MapId} | Players: {members.Count()} of {gameMap.Members.Min}[{gameMap.Members.Max}]");


                    //  If the selected map does not have enough players who are looking for a game to the next level
                    if (members.Any(m => m.ElsapsedSecondsFromLastCheck > PrepareTimeInSeconds) == false)
                    {
                        if (squads.Sum(s => s.Members.Count) < gameMap.Members.Min)
                            return;
                        //continue;
                    }

                    //  Adding all members in the first team
                    {
                        int? squadCount = 0;
                        team1.MemberList.AddRange(members.Select(m =>
                            FactoryMember(db, ref squadCount, m)));
                    }

                    if (team1.MemberList.Count >= 2)
                    {
                        var memberCount = team1.MemberList.Count;
                        while (true)
                        {
                            if (team1.Level > team2.Level)
                            {
                                // Merge We reserve on the team players only in the squads
                                if (team1.MemberList.Any(m => m.Squad > 0))
                                {
                                    team1.MergeMember(buffer, team1.MemberList.Where(m => m.Squad == 0).ToList());

                                    var squadId =
                                        team1.MemberList.First(s => s.Level == team1.MemberList.Max(p => p.Level))
                                            .Squad;
                                    team1.MergeMember(team2, team1.MemberList.Where(m => m.Squad == squadId).ToList());
                                }
                                else
                                {
                                    team1.MergeMember(team2,
                                        team1.MemberList.First(s => s.Level == team1.MemberList.Max(p => p.Level)));
                                }
                            }
                            else
                            {
                                if (team2.MemberList.Any())
                                {
                                    if (team2.MemberList.Any(m => m.Squad > 0))
                                    {
                                        var squadId =
                                            team2.MemberList.First(s => s.Level == team2.MemberList.Max(p => p.Level))
                                                .Squad;
                                        team2.MergeMember(team1,
                                            team1.MemberList.Where(m => m.Squad == squadId).ToList());
                                    }
                                    else
                                    {
                                        team2.MergeMember(team1,
                                            team2.MemberList.First(s => s.Level == team2.MemberList.Max(p => p.Level)));
                                        team1.MergeMember(team2,
                                            team1.MemberList.First(s => s.Level == team1.MemberList.Min(p => p.Level)));
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }

                            if (buffer.MemberList.Any())
                            {
                                if (team2.Lenght > team1.Lenght)
                                {
                                    buffer.MergeMember(team1, buffer.MemberList.First());
                                }
                                else
                                {
                                    buffer.MergeMember(team2, buffer.MemberList.First());
                                }
                            }

                            if (team1.Lenght == team2.Lenght && team1.Lenght >= gameMap.Members.HalthMin)
                                break;

                            if (team1.Lenght + team2.Lenght == memberCount && team1.Level != team2.Level &&
                                Math.Abs(team1.Level - team2.Level) < 3)
                            {
                                numberOfBots = Convert.ToByte(gameMap.Members.Min - (team1.Lenght + team2.Lenght));
                                break;
                            }

                            if (startDateTime.ToElapsedSeconds() > HalthPrepareTimeInSeconds)
                            {
                                numberOfBots = Convert.ToByte(gameMap.Members.Min - (team1.Lenght + team2.Lenght));
                                break;
                            }
                        }
                    }

                    if (members.Any(m => m.ElsapsedSecondsFromLastCheck > PrepareTimeInSeconds))
                    {
                        numberOfBots = Convert.ToByte(gameMap.Members.Min - (team1.Lenght + team2.Lenght));
                    }





                    var match = TakeAvalibleMatch(db);
                    if (match == null)
                    {
                        match = new MatchEntity(new List<MatchTeamEntity> {team1, team2},
                            new SearchSessionOptions(GameModeFlag, gameMap.MapAsFlag,
                                TimeSpan.FromSeconds(gameMap.RoundTimeInSeconds), ScoreLimit)
                            {
                                NumberOfBots = numberOfBots,
                            });

                        LoggerContainer.MatchMakingLogger.Info("========================== TeamGameModeEntity ==================");
                        foreach (var team in match.TeamList)
                        {
                            LoggerContainer.MatchMakingLogger.Info($"[Team][{team.TeamId}]");
                            foreach (var member in team.MemberList)
                            {
                                LoggerContainer.MatchMakingLogger.Info($"> [{member.PlayerEntity.PlayerName}] - Squad: {member.Squad}");
                            }
                        }


                        LoggerContainer.MatchMakingLogger.Info($"Match: {match} {Environment.NewLine}");
                        db.MatchEntity.Add(match);
                    }
                    else
                    {
                        LoggerContainer.MatchMakingLogger.Info(
                            $"Match founded: {match.TeamList.Count} teams | created: {match.Created} | state: {match.State}");

                        //  Move the players added to the queue for entry into battle
                        foreach (var member in team1.MemberList.Concat(team2.MemberList).ToArray())
                        {
                            member.MatchId = match.MatchId;
                            member.State = MatchMemberState.Created;
                        }


                        match.TeamList.Last().MemberList.AddRange(team1.MemberList);
                        match.TeamList.First().MemberList.AddRange(team2.MemberList);
                    }

                    ApplyMatchPropertyForMember(members, match);

                    LoggerContainer.MatchMakingLogger.Info("^^^^^^^^^^^^^^^^^^^^^^^^^^ TeamGameModeEntity ^^^^^^^^^^^^^^^^^^^^^^^^^");
                    db.SaveChanges();
                }
                Thread.Sleep(150);
            }
            catch (Exception e)
            {
                LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"TeamGameModeEntity[{GameModeFlag}]", e);
            }
        }
    }
}