﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Match.GameMode.PvP
{
    public class DominationGameMode : TeamGameModeEntity
    {
        public new class Configuration : EntityTypeConfiguration<DominationGameMode>
        {
            public Configuration()
            {
                ToTable("gamemode.DominationGameMode");
            }
        }
    }
}
