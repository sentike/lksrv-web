﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Match.GameMode.PvP
{
    public class CaptureTheFlagGameMode : TeamGameModeEntity
    {
        public new class Configuration : EntityTypeConfiguration<CaptureTheFlagGameMode>
        {
            public Configuration()
            {
                ToTable("gamemode.CaptureTheFlagGameMode");
            }
        }
    }
}
