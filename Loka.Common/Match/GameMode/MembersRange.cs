﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace Loka.Common.Match.GameMode
{
    [ComplexType]
    public class MembersRange
    {
        public int Min { get; set; }
        public int Max { get; set; }

        public override string ToString()
        {
            return $"Members: {Min} / {Max}";
        }

        public int HalthMin => Min/2;
        public int HalthMax => Max / 2;

    }
}