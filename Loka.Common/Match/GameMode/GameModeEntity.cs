﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

using Loka.Common.Cluster;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Player.Achievements;
using Loka.Common.Player.Company;
using Loka.Common.Player.Statistic;
using Loka.Common.Store;
using Loka.Infrastructure;

using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Queue;
using MoreLinq;
using VRS.Infrastructure;

namespace Loka.Common.Match.GameMode
{
    public abstract class GameModeEntity
    {        
        public GameModeTypeId GameModeId { get; set; } 
        public GameModeFlags GameModeFlag => GameModeId.AsFlag();

        public MembersRange Level { get; set; }
        public int PrepareTimeInSeconds { get; set; }
        public int HalthPrepareTimeInSeconds => PrepareTimeInSeconds/2;
        public StoreItemCost JoinCost { get; set; }
        public short Priority { get; set; }
        public short ScoreLimit { get; set; }
        //public DateTime LastMatchMakingDate { get; set; }


        public virtual List<GameMapEntity> Maps { get; private set; } = new List<GameMapEntity>(10);
        protected readonly object SyncObject = new object();
        public abstract void Tick();

        public GameModeEntity() { }

        protected GameModeEntity(GameModeTypeId gm, short score)
        {
            GameModeId = gm;
            Level = new MembersRange();
            PrepareTimeInSeconds = 10;
            JoinCost = new StoreItemCost();
            ScoreLimit = score;
        }

        public void AddMaps()
        {
            Maps = Enum.GetValues(typeof(GameMapTypeId)).Cast<GameMapTypeId>().ToArray()
                .Select(p => new GameMapEntity(GameModeId, p)).ToList();
        }

        public virtual void Finish(MatchEntity session, MatchResultView result, DataRepository db)
        {
            try
            {
                var achievementsInstanceCollection = db.Achievements.ToArray();
                //-------------------------------------------------------------
                var winnerTeam = session.TeamList.SingleOrDefault(t => t.TeamId == result.WinnerTeamId);
                if (winnerTeam != null)
                {
                    session.WinnerTeamId = result.WinnerTeamId;
                }

                //Logger.Info($"Match ended on {session.NodeEntity.MatchId} | {session.GameMapTypeId} - {session.GameModeTypeId} | WinnerTeamId:{session.WinnerTeamId} | Players {result.Members.Count}");
                //-------------------------------------------------------------
                foreach (var m in result.Members.Where(m => m.MemberId != Guid.Empty))
                {
                    MatchMemberEntity member = session.MemberList.SingleOrDefault(p => p.MemberId == m.MemberId && p.State != MatchMemberState.Deserted);
                    if (member != null)
                    {
                        //======================================================================================================================
                        member.Statistic = m.Statistic;
                        member.Achievements.AddRange(m.Achievements.Select(a => new MatchMemberAchievements(a)));

                        //======================================================================================================================
                        member.PlayerEntity.GiveReputaion(m.Award.FractionId, m.Award.Reputation);
                        {
                            member.PlayerEntity.QueueEntity.Ready = QueueState.None;
                            //member.PlayerEntity.Statistic.Update(m.Statistic);
                        }

                        //======================================================================================================================
                        member.PlayerEntity.Experience.WeekExperience += m.Award.Experience;


                        //======================================================================================================================
                        LoggerContainer.SessionLogger.Info(
                            $"{member.PlayerEntity.PlayerName} -- Money:  {m.Award.Money} |  Donate: {m.Award.Donate} |  Experience: {m.Award.Experience}");

                        member.PlayerEntity.Experience.Experience += m.Award.Experience;
                        member.PlayerEntity.Give(GameCurrency.Money, m.Award.Money);
	                    member.PlayerEntity.Give(GameCurrency.Coin, m.Award.Bitcoin);
						member.PlayerEntity.Give(GameCurrency.Donate, m.Award.Donate.Clamp(-1000, 1000));

                        //======================================================================================================================
                        member.State = MatchMemberState.Finished;
                        member.Award = m.Award;
                    }
                    else
                    {
                        Logger.Error($"Member not found, memberId: {m.MemberId} | match: {session.MatchId}");
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        public override string ToString()
        {
            return $"{GameModeFlag} | {Maps.Count} maps: { string.Join(", ", Maps.Select(m => $"{m.MapId} [{m.Members}]"))}";
        }

        public class Configuration : EntityTypeConfiguration<GameModeEntity>
        {
            public Configuration()
            {
                HasKey(p => p.GameModeId);
                ToTable("gamemode.GameModeEntity");
                HasMany(p => p.Maps).WithRequired(p => p.GameModeEntityEntity).HasForeignKey(p => p.GameModeId);
                //Property(p => p.LastMatchMakingDate).IsConcurrencyToken();
                //Ignore(p => p.SyncObject);
            }
        }

        protected virtual MatchMemberEntity FactoryMember(DataRepository db, ref int? squad, QueueEntity entity)
        {
            //-----------------------------------------
            var session = db.GameSessionEntity.Add(new PlayerGameSessionEntity(entity.QueueId, PlayerSessionAttach.Server));
            var member = MatchMemberEntity.Factory(entity.PlayerEntity, ref squad, session.SessionId);

            //-----------------------------------------
            return member;
        }


        protected virtual void ApplyMatchPropertyForMember(IEnumerable<QueueEntity> members, MatchEntity match , QueueState state = QueueState.Playing)
        {
            foreach (var member in members)
            {
                member.Ready = state;
            }
        }

	    protected virtual GameMapEntity TakeRandomMap(DataRepository repository, QueueEntity[] queue)
	    {
			//==================================================
		    var ignore = new GameMapFlags[]
		    {
			    GameMapFlags.None,
			    GameMapFlags.End,
			    GameMapFlags.Lobby
			};

			//==================================================
			var maps = queue.Select(p => p.Options.GameMap).ToArray()
				.Where(p => ignore.Contains(p) == false).ToArray();

		    if (maps.Length > 1)
		    {
			    var groups = maps.GroupBy(p => p).ToArray();

			    // Генерируем случайную карту
				var randomed = TakeRandomMap();

			    // Получаем информацию о популярных картах
				var populate = groups.Max();

			    // Получаем информацию о не популярных картах
				var recomend = groups.Min();

				//	Если популярная и рекомендованная карта одинаковы,
				//	то ищем другую карту
			    if (populate.Key == recomend.Key)
			    {
				    if (randomed.MapAsFlag == recomend.Key)
				    {
						randomed = TakeRandomMap();
					}
				    return randomed;
			    }

				//	Если есть непопулярная карта, то возвращаем её
				//	если нет, то выдаем случайную
			    return Maps.FirstOrDefault(p => p.MapAsFlag == recomend.Key) ?? randomed;
		    }
		    return TakeRandomMap();
	    }

		protected virtual GameMapEntity TakeRandomMap()
        {
            return Maps.PickRandom();
        }

        protected virtual QueueEntity[] TakeQueueEntities(DataRepository repository)
        {
            return repository.QueueEntity.Where
            (
                q => q.Options.GameMode.HasFlag(GameModeFlag) && (q.Ready.HasFlag(QueueState.Searching) || q.Ready.HasFlag(QueueState.WaitingPlayers))
            ).ToArray().Where(q => q.IsOnline && q.PartyReady).ToArray();
        }

        protected virtual QueueEntity[] TakeSquadEntities(QueueEntity[] queueEntities)
        {
            return queueEntities/*.Where(m => m.Level.InRange(l))*/.ToArray();
        }

        protected virtual MatchEntity TakeAvalibleMatch(DataRepository repository)
        {
            return repository.MatchEntity.ToArray().Where(m => 
            m.State == MatchGameState.Started &&
            m.Options.GameMode.HasFlag(GameModeFlag) &&
            m.StartedOfMatch != null &&
            m.Finished == null &&
            m.WinnerTeamId == null &&
            m.TimeLeftInSeconds >= m.Options.RoundTimeInSeconds / 2 &&
            m.IsAllowJoinNewPlayers
            ).ToArray().Where(m=> 
            m.Created.ToElapsedSeconds() < 300 &&
            m.LastActivityDate.HasValue &&
            m.LastActivityDate.Value.ToElapsedSeconds() < 15
            ).ToArray().PickRandom();
        }

        protected static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public virtual bool AttackTo(DataRepository db, Guid attacker, Guid target)
        {
            throw new NotImplementedException("Only for Harvest game mode");
        }
    }
}