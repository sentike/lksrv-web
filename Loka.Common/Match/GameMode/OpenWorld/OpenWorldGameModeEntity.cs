﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Loka.Common.Match.GameMode.PvP;

namespace Loka.Common.Match.GameMode.OpenWorld
{
    public class OpenWorldGameModeEntity : TeamGameModeEntity
    {
        public override void Tick()
        {
            base.Tick();
        }



        public new class Configuration : EntityTypeConfiguration<OpenWorldGameModeEntity>
        {
            public Configuration()
            {
                ToTable("gamemode.OpenWorldGameModeEntity");
            }
        }
    }
}