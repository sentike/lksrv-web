﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Models.Queue;
using VRS.Infrastructure;

namespace Loka.Common.Match.GameMode
{
	public class BuildingGameMode : GameModeEntity
	{
		public BuildingGameMode() { }
		public BuildingGameMode(short score) : base(GameModeTypeId.BuildingMode, score) { }

		public new class Configuration : EntityTypeConfiguration<BuildingGameMode>
		{
			public Configuration()
			{
				ToTable("gamemode.BuildingGameMode");
			}
		}

		public override void Finish(MatchEntity session, MatchResultView result, DataRepository db)
		{
			session.OnStopNode();
			db.SaveChanges();
		}

		private void ReadySquadTick()
		{
			using (var db = new DataRepository())
			{
				var queues = TakeQueueEntities(db);
				//===================================================================================
				//LoggerContainer.MatchMakingLogger.Error($"[BuildingGameMode] Begin make match, gm: {GameModeFlag} | queues: {queues.Length}");
				if (queues.Length < 1)
				{
					return;
				}

				//===================================================================================
				var party = queues.FirstOrDefault(p => p.IsLeader);
				if (party == null)
				{
					return;
				}

				//===================================================================================

				int? squad = null;
				MatchEntity match = null;
				var members = party.Members;

				//----------------------------------------------------------------------------
				var team = new MatchTeamEntity()
				{
					TeamId = Guid.NewGuid(),
					MemberList = members.Select(m => FactoryMember(db, ref squad, m)).ToList()
				};

				//===================================================================================
				match = new MatchEntity(new List<MatchTeamEntity>
				{
					team
				}, new SearchSessionOptions(GameModeFlags.BuildingMode, GameMapFlags.Lobby, TimeSpan.FromMinutes(120))
				{
					UniversalId = party.PartyId
				});

				//===================================================================================
				db.MatchEntity.Add(match);

				//===================================================================================
				ApplyMatchPropertyForMember(members, match, QueueState.Playing);
				db.SaveChanges();
			}
		}

		private void SecondSquadTick(DataRepository db)
		{
			int? squad = null;
			var now = DateTime.UtcNow.AddSeconds(-10);

			//==========================================================
			//	Получаем список онлайн игроков в очереди
			var queues = db.QueueEntity
				.Include(p => p.PartyEntity)
				.Include(p => p.PartyEntity.Members)
				.Where(q => q.Options.GameMode.HasFlag(GameModeFlag)
				&& q.Ready.HasFlag(QueueState.Searching)).ToArray().Where(q => q.IsOnline).ToList();


			LoggerContainer.MatchMakingLogger.Error($"[SecondSquadTick]: queues: {queues.Count} | {string.Join(",", queues.Select(p => $"{p.PlayerEntity.PlayerName} - {p.Ready}"))}");
			//==========================================================
			//while (queues.Any())
			{
				var member = queues.FirstOrDefault();
				if (member == null)
				{
					return;
				}

				//==========================================================
				var party = member.PartyEntity;
				var leader = party.PlayerEntity;

				//==========================================================
				var matches = leader.MatchMembers.Select(p => p.MatchEntity).ToArray();

				//==========================================================
				var match = matches.FirstOrDefault(p => p.HasBuildingGameMode && p.IsServerInOnline);
				LoggerContainer.MatchMakingLogger.Error($"[SecondSquadTick]: match: {match}");


				if (match == null)
				{
					return;
				}

				//==========================================================
				var partymembers = party.Members;
				queues.RemoveAll(p => partymembers.Contains(p));

				//==========================================================
				db.Entry(match).Collection(p => p.TeamList).Load();
				db.Entry(match).Collection(p => p.MemberList).Load();

				//==========================================================
				var team = match.TeamList.First();
				var keys = match.MemberList.Select(p => p.PlayerId).ToArray();
				LoggerContainer.MatchMakingLogger.Error($"[SecondSquadTick] MemberList: {string.Join(",", match.MemberList.Select(p => $"{p.PlayerEntity.PlayerName} - {p.MemberId}"))}");


				var players = partymembers.Where(p => keys.Contains(p.QueueId) == false).ToArray();
				var members = players.Select(m => FactoryMember(db, ref squad, m)).ToArray();

				LoggerContainer.MatchMakingLogger.Error($"[SecondSquadTick] team: {team.TeamId} | keys: {keys.Length} | players: {players.Length} | members: {members.Length}");

				//==========================================================
				foreach (var m in members)
				{
					m.State = MatchMemberState.Created;
					m.TeamId = team.TeamId;
					m.TeamEntity = team;
				}
				
				//==========================================================
				match.MemberList.AddRange(members);
				db.MatchMember.AddRange(members);
				
				//==========================================================
				ApplyMatchPropertyForMember(players, match);
				db.SaveChanges();
			}
		}



		public override void Tick()
		{
			ReadySquadTick();

			using (var db = new DataRepository())
			{
				SecondSquadTick(db);
			}
		}
	}
}
