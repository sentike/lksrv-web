﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Match.GameMode.PvE
{
    public class PvEDefenseGameMode : GameModeEntity
    {
        public override void Tick()
        {

        }

        public new class Configuration : EntityTypeConfiguration<PvEDefenseGameMode>
        {
            public Configuration()
            {
                ToTable("gamemode.PvEDefenseGameMode");
            }
        }
    }
}
