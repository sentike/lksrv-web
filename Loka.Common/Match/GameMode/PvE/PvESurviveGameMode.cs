﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Cluster;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Match.Team;
using Loka.Common.Player.Statistic;
using Loka.Common.Player.Statistic.PvE;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Queue;

namespace Loka.Common.Match.GameMode.PvE
{
    /// <summary>
    /// Survival mode, where the player and his squad must hold a certain time
    /// </summary>
    public class PvESurviveGameMode : GameModeEntity
    {
        const int MiniminNumberOfPlayers = 1;
        const int MaximumNumberOfPlayers = 5;
        const int MaximumRoundTime = 10*3*60;

        public override void Finish(MatchEntity session, MatchResultView result, DataRepository db)
        {
            base.Finish(session, result, db);

            throw new NotImplementedException("PvESurviveGameMode::NotImplemented");
            //foreach (var m in result.Members.Where(m => m.MemberId != Guid.Empty))
            //{
            //    var member = session.MemberList.SingleOrDefault(p => p.MemberId == m.MemberId);
            //    if (member != null && result.WinnerTeamId == member.TeamId)
            //    {
            //        var player = member.PlayerEntity;
            //        var matchWithCurrentGameMode = player.PvEMatchStatisticEntities.SingleOrDefault(p => p.GameModeTypeId == session.GameModeTypeId && p.Level == session.AbstractMatchFlag);
            //        if (matchWithCurrentGameMode != null)
            //        {
            //            if (m.Statistic.Score > matchWithCurrentGameMode.Score)
            //            {
            //                matchWithCurrentGameMode.Score = m.Statistic.Score;
            //            }
            //        }
            //        else
            //        {
            //            player.PvEMatchStatisticEntities.Add(PlayerPvEMatchStatisticEntity.Factory(player, session.GameModeTypeId, (short) session.AbstractMatchFlag, m.Statistic.Score));
            //        }
            //    }
            //}
            
        }

        public override void Tick()
        {
            if (PrepareTimeInSeconds < 10) PrepareTimeInSeconds = 10;

            //while (true)
            //{
            //    using (var db = new DataRepository())
            //    {
            //        try
            //        {
            //            //----------------------------------------------------------------------------
            //            var queues = TakeQueueEntities(db);
            //            Logger.Info($"Begin make match, gm: {GameModeId} | queues: {queues.Length}");
            //            if (queues.Count() < MiniminNumberOfPlayers)
            //            {
            //                return;
            //            }
            //                                   
            //            //----------------------------------------------------------------------------
            //            var map = TakeRandomMap();
            //            var members = queues.First(q => q.IsLeader).Members;
            //            var team = new MatchTeamEntity()
            //            {
            //                TeamId = Guid.NewGuid(),
            //                MemberList = members.Select(m => MatchMemberEntity.Factory(m.PlayerEntity)).ToList()
            //            };
            //            
            //            var match = new MatchEntity(new List<MatchTeamEntity> { team }, map.MapId, 300, MaximumRoundTime);
            //            var leader = members.First(m => m.IsLeader);
            //            var targetLevel = leader.Level.Max;
            //            if (targetLevel < 0) targetLevel = 0;
            //            else
            //            {
            //                var maxAvalibleLevel = leader.PlayerEntity.PvEMatchStatisticEntities.Where(m => m.GameModeTypeId == GameModeId).Select(m => (int)m.Level).DefaultIfEmpty(0).Max();
            //                if (targetLevel > maxAvalibleLevel) targetLevel = maxAvalibleLevel;
            //            }
            //            
            //            //match.AbstractMatchFlag = members.First(m => m.IsLeader).Level.Max;
            //            //match.GameModeTypeId = GameModeId;
            //            //match.NumberOfBots = 0;
            //            //db.MatchEntity.Add(match);
            //            //
            //            ////----------------------------------------------------------------------------
            //            //Logger.Info($"PvESurviveGameMode:: match[{match.MatchId}] | Difficulty {match.Difficulty} | {match.GameMapTypeId}");
            //            //ApplyMatchPropertyForMember(members, match);
            //            //db.SaveChanges();
            //            break;
            //
            //        }
            //        catch (System.Exception exception)
            //        {
            //            Logger.Error($"PvESurviveGameMode: {exception}");
            //            throw;
            //        }
            //    }
            //}
        }

        public new class Configuration : EntityTypeConfiguration<PvESurviveGameMode>
        {
            public Configuration()
            {
                ToTable("gamemode.PvESurviveGameMode");
            }
        }
    }
}
