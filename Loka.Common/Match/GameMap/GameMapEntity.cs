﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Match.GameMode;

namespace Loka.Common.Match.GameMap
{
    public class GameMapEntity
    {
        public int InstanceId { get; set; }
        public GameModeTypeId GameModeId { get; set; }
        public virtual GameModeEntity GameModeEntityEntity { get; set; }

        public GameMapTypeId MapId { get; set; }
        public GameMapFlags MapAsFlag => (GameMapFlags)(1 << (int)MapId);


        public MembersRange Members { get; set; }
        public int RoundTimeInSeconds { get; set; }

        public GameMapEntity() { }

        public GameMapEntity(GameModeTypeId gm, GameMapTypeId map)
        {
            GameModeId = gm;
            MapId = map;
            Members = new MembersRange();
            RoundTimeInSeconds = 600;
        }

        public override string ToString()
        {
            return $"{InstanceId}:{MapId}  : {Members} | {GameModeId} : {RoundTimeInSeconds} soconds";
        }

        public class Configuration : EntityTypeConfiguration<GameMapEntity>
        {
            public Configuration()
            {
                HasKey(p => p.InstanceId);
                ToTable("gamemode.GameMapEntity");
            }
        }
    }
}
