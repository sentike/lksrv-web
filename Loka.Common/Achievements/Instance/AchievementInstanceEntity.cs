﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Achievements.Container;


namespace Loka.Common.Achievements.Instance
{
    public class AchievementInstanceEntity
    {
        public int AchievementBonusId { get; set; }
        public AchievementTypeId AchievementTypeId { get; set; }
        public AchievementCategoryId CategoryId { get; set; }

        public virtual AchievementContainerEntity AchievementEntity { get; set; }

        public int Amount { get; set; }
        public int BonusPackId { get; set; }

        public class Configuration : EntityTypeConfiguration<AchievementInstanceEntity>
        {
            public Configuration()
            {
                ToTable("achievements.AchievementInstanceEntity");
                HasKey(a => a.AchievementBonusId);
                Property(a => a.AchievementBonusId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            }
        }
    }
}
