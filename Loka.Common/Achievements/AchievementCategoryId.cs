﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Achievements
{
    public enum AchievementCategoryId
    {
        /// <summary>
        /// Used for repeated actions. For example: the number of murders, the number of fights, wins
        /// </summary>
        Incrementable,

        /// <summary>
        /// Used for actions that can be performed once, but have a lot of steps. For example a series of murders
        /// </summary>
        Settable,

        End
    }
}
