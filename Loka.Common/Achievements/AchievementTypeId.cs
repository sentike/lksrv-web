﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Achievements
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum AchievementTypeId
    {
        None,

        #region Kill By
        /// <summary>
        /// kill players
        /// </summary>
        Kill_Total,

        /// <summary>
        /// kill players from rifles
        /// </summary>
        Kill_By_Rifle,

        /// <summary>
        /// kill players from sniper rifles
        /// </summary>          
        Kill_By_SniperRifle,

        /// <summary>
        /// kill players from minigun
        /// </summary>          
        Kill_By_MiniGun,

        /// <summary>
        /// kill players from shotgun
        /// </summary>          
        Kill_By_ShotGun,

        /// <summary>
        /// kill players from Pistol
        /// </summary>          
        Kill_By_Pistol,

        /// <summary>
        /// kill players from submachine gun(SMG)
        /// </summary>      
        Kill_By_SMG,

        /// <summary>
        /// kill players from Knife
        /// </summary>          
        Kill_By_Knife,

        /// <summary>
        /// kill players from Grenade
        /// </summary>          
        Kill_By_Grenade,

        /// <summary>
        /// kill players from Mine
        /// </summary>          
        Kill_By_Mine,
#endregion

        #region Series Kill By
        /// <summary>
        /// SeriesKill players
        /// </summary>
        SeriesKill_Total,

        /// <summary>
        /// SeriesKill players from rifles
        /// </summary>
        SeriesKill_By_Rifle,

        /// <summary>
        /// SeriesKill players from sniper rifles
        /// </summary>          
        SeriesKill_By_SniperRifle,

        /// <summary>
        /// SeriesKill players from minigun
        /// </summary>          
        SeriesKill_By_MiniGun,

        /// <summary>
        /// SeriesKill players from shotgun
        /// </summary>          
        SeriesKill_By_ShotGun,

        /// <summary>
        /// SeriesKill players from Pistol
        /// </summary>          
        SeriesKill_By_Pistol,

        /// <summary>
        /// SeriesKill players from submachine gun(SMG)
        /// </summary>      
        SeriesKill_By_SMG,

        /// <summary>
        /// SeriesKill players from Knife
        /// </summary>          
        SeriesKill_By_Knife,

        /// <summary>
        /// SeriesKill players from Grenade
        /// </summary>          
        SeriesKill_By_Grenade,

        /// <summary>
        /// SeriesKill players from Mine
        /// </summary>          
        SeriesKill_By_Mine,
        #endregion


        #region Fraction Reputation

        Reputation_RIFT,
        Reputation_RIFT_Friend,

        Reputation_Keepers,
        Reputation_Keepers_Friend,

        #endregion

        #region Player Experience
        Experience_Level,
        #endregion

        #region Match Medals
        Medal_Match_Best_Shooter,
        Medal_Match_Best_Player,
        Medal_Match_Best_Killer,
        Medal_Match_Best_Hero,
        #endregion

        End
    }
}
