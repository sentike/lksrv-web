﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Achievements.Instance;
using Loka.Common.Player.Achievements;
using Loka.Common.Store;

namespace Loka.Common.Achievements.Container
{
    public class AchievementContainerEntity
    {
        public AchievementContainerEntity()
        {
        }

        public AchievementContainerEntity(AchievementTypeId id, AchievementCategoryId category)
        {
            AchievementTypeId = id;
            CategoryId = category;
        }

        public AchievementTypeId AchievementTypeId { get; set; }
        public AchievementCategoryId CategoryId { get; set; }
        public virtual List<AchievementInstanceEntity> Achievement { get; set; } = new List<AchievementInstanceEntity>(5);

        public void TryGiveArchivement(PlayerAchievementContainerEntity player)
        {
            foreach (var achievement in Achievement.Where(a => a.AchievementBonusId > (player.LastAchievementInstanceEntityId ?? -1) && player.Amount >= a.Amount))
            {
                player.PlayerInstance.Give(GameCurrency.Money, achievement.BonusPackId);
                player.LastAchievementInstanceEntityId = achievement.AchievementBonusId;
            }
        }

        public class Configuration : EntityTypeConfiguration<AchievementContainerEntity>
        {
            public Configuration()
            {
                ToTable("achievements.AchievementContainerEntity");
                HasKey(a => a.AchievementTypeId);
                Property(p => p.CategoryId).IsRequired();
                HasMany(a => a.Achievement).WithRequired(a => a.AchievementEntity).HasForeignKey(a => a.AchievementTypeId);
            }
        }
    }
}
