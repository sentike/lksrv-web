﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Loka.Common.Improvements.Category;
using Loka.Common.Improvements.Instance;
using Loka.Common.Improvements.Sentence;
using Loka.Infrastructure;

using VRS.Infrastructure.Environment;

namespace Loka.Common.Improvements
{
    public class ImprovementViewModel
    {
        public long DateOfEndVotes { get; set; }
        public List<ImprovementView> ImprovementViews { get; set; }
        public List<ImprovementView> SentenceViews { get; set; }


        public override string ToString()
        {
            return $"Improvements: {ImprovementViews.Count} |  Sentences: {SentenceViews.Count} | DateOfEndVotes: {CMath.UnixTimeStampToDateTime(DateOfEndVotes)} [{DateOfEndVotes}]";
        }
    }

    public class ImprovementView
    {
        public Guid ImprovementId { get; set; }

        [DisplayName("Состояние")]
        public ImprovementProgressState ProgressState { get; set; }

        [DisplayName("Категория")]
        public ImprovementCategoryId CategoryId { get; set; }

        [DisplayName("Дата создания")]
        public long CreatedDate { get; set; }

        [DisplayName("Количество голосов")]
        public long Count { get; set; }

        [DisplayName("Заголовок")]
        public string Title { get; set; }

        [DisplayName("Описание")]
        public string Message { get; set; }

        public bool IsVoted { get; set; }

        public override string ToString()
        {
            return $"{ImprovementId} | {CategoryId} | {ProgressState} | {Title} | {Message} | IsVoted: {IsVoted}";
        }
    }

    public static class ViewHelper
    {
        public static List<ImprovementView> AsView(this IEnumerable<ImprovementEntity> entity, LanguageTypeId language, Guid viewerId)
        {
            return entity.Select(e => e.AsView(language, viewerId)).ToList();
        }

        public static ImprovementView AsView(this ImprovementEntity entity, LanguageTypeId language, Guid viewerId)
        {
            return new MapperConfiguration
            (
                c => c.CreateMap<ImprovementEntity, ImprovementView>()
                    .ForSourceMember(m => m.Localization, m => m.Ignore())
                    .ForMember(m => m.Count, m => m.MapFrom(r => r.VoteEntities.Count(v => v.Step == entity.Step)))
                    .ForMember(m => m.Title, m => m.MapFrom(r => r.LocalizationById(language).Title))
                    .ForMember(m => m.Message, m => m.MapFrom(r => r.LocalizationById(language).Message))
                    .ForMember(m => m.CreatedDate, m => m.MapFrom(r => r.CreatedDate.ToUnixTime()))
                    .ForMember(m => m.IsVoted, m => m.MapFrom(r => r.VoteEntities.Any(v => r.Step == v.Step && v.PlayerId == viewerId)))
            ).CreateMapper().Map<ImprovementView>(entity);
        }

        //===========================================================================================


        public static List<ImprovementView> AsView(this IEnumerable<SentenceEntity> entity, Guid viewerId)
        {
            return entity.Select(e => e.AsView(viewerId)).ToList();
        }

        public static ImprovementView AsView(this SentenceEntity entity, Guid viewerId)
        {
            return new MapperConfiguration
            (
                c => c.CreateMap<SentenceEntity, ImprovementView>()
                    .ForMember(m => m.Title, m => m.Ignore())
                    .ForMember(m => m.ImprovementId, m => m.MapFrom(r => r.SentenceId))
                    .ForMember(m => m.Count, m => m.MapFrom(r => r.VoteEntities.LongCount(v => v.IsApprove == true) - r.VoteEntities.LongCount(v => v.IsApprove == false)))
                    .ForMember(m => m.CreatedDate, m => m.MapFrom(r => r.CreatedDate.ToUnixTime()))
                    .ForMember(m => m.IsVoted, m => m.MapFrom(r => r.VoteEntities.Any(v => v.PlayerId == viewerId)))
            ).CreateMapper().Map<ImprovementView>(entity);
        }

    }
}
