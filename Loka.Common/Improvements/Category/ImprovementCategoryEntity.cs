﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Loka.Common.Improvements.Instance;
using Loka.Common.Improvements.Sentence;

namespace Loka.Common.Improvements.Category
{
    public enum ImprovementCategoryId
    {
        None,
        GamePlay,
        Lobby,
        Store,
    }
}