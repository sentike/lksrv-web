﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Loka.Common.Improvements.Category;
using Loka.Common.Improvements.Vote;
using VRS.Infrastructure.Environment;


namespace Loka.Common.Improvements.Instance
{
    public enum ImprovementProgressState
    {
        Voting,
        Development,
        Release,
    }

    public class ImprovementEntity
    {
        public Guid ImprovementId { get; set; }
        public ImprovementProgressState ProgressState { get; set; }

        public ImprovementCategoryId CategoryId { get; set; }

        public virtual List<ImprovementLocalizationEntity> LocalizationEntities { get; set; } = new List<ImprovementLocalizationEntity>(2);
        public Dictionary<LanguageTypeId, ImprovementLocalizationEntity> Localization => LocalizationEntities.ToDictionary(x => x.LanguageId, x => x);
        public short Step { get; set; }


        public ImprovementLocalizationEntity LocalizationById(LanguageTypeId language)
        {
            if (Localization.ContainsKey(language)) return Localization[language];
            else if (Localization.ContainsKey(LanguageTypeId.en)) return Localization[LanguageTypeId.en];
            else return LocalizationEntities.FirstOrDefault();
        }

        public virtual List<ImprovementVoteEntity> VoteEntities { get; set; } = new List<ImprovementVoteEntity>(32);
        public long CurrentVoteCount => VoteEntities.LongCount(v => v.Step == Step);

        public DateTime CreatedDate { get; set; }

        public override string ToString()
        {
            return $"{ImprovementId} | {ProgressState} | Step {Step} | {CategoryId} | {LocalizationById(LanguageTypeId.ru)?.Title} | CreatedDate: {CreatedDate.ToShortDateString()} | Localizations: {string.Join(", ", Localization.Keys)}";
        }

        public class Configuration : EntityTypeConfiguration<ImprovementEntity>
        {
            public Configuration()
            {
                HasKey(p => p.ImprovementId);

                ToTable("improvement.ImprovementEntity");
                HasMany(i => i.LocalizationEntities).WithRequired(i => i.ImprovementEntity).HasForeignKey(i => i.ImprovementId).WillCascadeOnDelete(true);
                HasMany(i => i.VoteEntities).WithRequired(i => i.ImprovementEntity).HasForeignKey(i => i.ImprovementId).WillCascadeOnDelete(true);

            }
        }
    }
}