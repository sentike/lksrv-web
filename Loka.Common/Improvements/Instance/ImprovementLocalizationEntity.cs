﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VRS.Infrastructure.Environment;

namespace Loka.Common.Improvements.Instance
{
    public class ImprovementLocalizationEntity
    {
        public Guid LocalizationId { get; set; }
        public LanguageTypeId LanguageId { get; set; }

        public Guid ImprovementId { get; set; }
        public virtual ImprovementEntity ImprovementEntity { get; set; }

        public string Title { get; set; }
        public string Message { get; set; }

        public class Configuration : EntityTypeConfiguration<ImprovementLocalizationEntity>
        {
            public Configuration()
            {
                HasKey(p => p.LocalizationId);

                ToTable("improvement.ImprovementLocalizationEntity");
                Property(p => p.Title).HasMaxLength(128);
                Property(p => p.Message).HasMaxLength(32 * 1024);

                Property(p => p.LanguageId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("LanguageId_ImprovementId", 0) { IsUnique = true }));
                Property(p => p.ImprovementId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("LanguageId_ImprovementId", 1) { IsUnique = true }));

            }
        }

    }
}
