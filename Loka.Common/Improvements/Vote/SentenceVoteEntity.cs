﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using AutoMapper;
using Loka.Common.Improvements.Instance;
using Loka.Common.Improvements.Sentence;

namespace Loka.Common.Improvements.Vote
{
    public class SentenceVoteEntity
    {
        public Guid VoteId { get; set; }

        public Guid SentenceId { get; set; }
        public virtual SentenceEntity SentenceEntity { get; set; }
        public ImprovementProgressState ImprovementProgressState { get; set; }


        public Guid PlayerId { get; set; }
        public DateTime VoteDate { get; set; }
        public bool IsApprove { get; set; }

        public class Configuration : EntityTypeConfiguration<SentenceVoteEntity>
        {
            public Configuration()
            {
                HasKey(p => p.VoteId);
                ToTable("improvement.SentenceVoteEntity");

                Property(p => p.SentenceId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("SentenceId_PlayerId", 0) { IsUnique = true }));
                Property(p => p.PlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("SentenceId_PlayerId", 1) { IsUnique = true }));
            }
        }

        public static SentenceVoteEntity Factory(ImprovementVoteCreateView view)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ImprovementVoteCreateView, SentenceVoteEntity>()
                    .ForMember(m => m.VoteDate, m => m.MapFrom(r => DateTime.UtcNow))
                    .ForMember(m => m.VoteId, m => m.MapFrom(r => Guid.NewGuid()))
                    .ForMember(m => m.SentenceId, m => m.MapFrom(r => r.ImprovementId))
            );
            return Mapper.Map<SentenceVoteEntity>(view);
        }
    }
}