﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using AutoMapper;
using Loka.Common.Improvements.Instance;

using Loka.Server.Player.Models;
using VRS.Infrastructure.Environment;

namespace Loka.Common.Improvements.Vote
{
    public class ImprovementVoteEntity
    {
        public Guid VoteId { get; set; }

        public Guid ImprovementId { get; set; }
        public virtual ImprovementEntity ImprovementEntity { get; set; }
        public ImprovementProgressState ImprovementProgressState { get; set; }


        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public DateTime VoteDate { get; set; }
        public short Step { get; set; }

        public override string ToString()
        {
            return $"{VoteId} | Step: {Step} | ImprovementId: {ImprovementId} | {ImprovementEntity.LocalizationById(LanguageTypeId.en)?.Title}";
        }

        public class Configuration : EntityTypeConfiguration<ImprovementVoteEntity>
        {
            public Configuration()
            {
                HasKey(p => p.VoteId);
                ToTable("improvement.ImprovementVoteEntity");

                Property(p => p.PlayerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_ImprovementId_Step", 0) { IsUnique = true }));
                Property(p => p.ImprovementId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_ImprovementId_Step", 1) { IsUnique = true }));
                Property(p => p.Step).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("PlayerId_ImprovementId_Step", 2) { IsUnique = true }));

            }
        }

        public static ImprovementVoteEntity Factory(ImprovementVoteCreateView view, ImprovementEntity entity/*, Guid playerId*/)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ImprovementVoteCreateView, ImprovementVoteEntity>()
                    .ForMember(m => m.VoteDate, m => m.MapFrom(r => DateTime.UtcNow))
                    .ForMember(m => m.VoteId, m => m.MapFrom(r => Guid.NewGuid()))
                    .ForMember(m => m.Step, m => m.UseValue(entity.Step))
                   // .ForMember(m => m.PlayerId, m => m.UseValue(playerId))
                    .ForMember(m => m.ImprovementProgressState, m => m.UseValue(entity.ProgressState))
            );
            return Mapper.Map<ImprovementVoteEntity>(view);
        }
    }
}