﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Improvements.Vote
{
    public class ImprovementVoteCreateView
    {
        public Guid ImprovementId { get; set; }
        public bool IsApprove { get; set; }

    }
}
