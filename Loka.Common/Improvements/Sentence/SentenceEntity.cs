﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Loka.Common.Improvements.Category;
using Loka.Common.Improvements.Instance;
using Loka.Common.Improvements.Vote;
using Loka.Server.Player.Models;

namespace Loka.Common.Improvements.Sentence
{
    public enum SentenceProgressState
    {
        Voting,
        Development,
        Rejected,
    }

    public class SentenceEntity
    {
        public Guid SentenceId { get; set; }
        public SentenceProgressState ProgressState { get; set; }
        public ImprovementCategoryId CategoryId { get; set; }

        public string Message { get; set; }
        public string RejectReason { get; set; }

        public Guid CreatorId { get; set; }
        public PlayerEntity CreatorEntity { get; set; }

        public DateTime CreatedDate { get; set; }

        public virtual List<SentenceVoteEntity> VoteEntities { get; set; } = new List<SentenceVoteEntity>(32);

        //
        public static SentenceEntity Factory(ImprovementCreateView view)
        {
            view.Title = view.Title.Replace("\\", "/").Replace("\"", "\\\"");
            view.Message = view.Message.Replace("\\", "/").Replace("\"", "\\\"");
            Mapper.Initialize
            (
                c => c.CreateMap<ImprovementCreateView, SentenceEntity>()
                    .ForMember(m => m.CreatedDate, m => m.MapFrom(r => DateTime.UtcNow))
                    .ForMember(m => m.SentenceId, m => m.MapFrom(r => Guid.NewGuid()))
            );
            return Mapper.Map<SentenceEntity>(view);
        }

        public class Configuration : EntityTypeConfiguration<SentenceEntity>
        {
            public Configuration()
            {
                HasKey(p => p.SentenceId);
                ToTable("improvement.SentenceEntity");

                HasRequired(l => l.CreatorEntity).WithMany(l => l.SentenceEntities).HasForeignKey(p => p.CreatorId).WillCascadeOnDelete(true);
                Property(p => p.Message).IsRequired().HasMaxLength(32 * 1024);
                Property(p => p.RejectReason).IsOptional().HasMaxLength(1024);

            }
        }

    }
}
