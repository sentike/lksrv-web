﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Improvements.Category;

namespace Loka.Common.Improvements
{
    public class ImprovementCreateView
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public ImprovementCategoryId CategoryId { get; set; }
    }
}
