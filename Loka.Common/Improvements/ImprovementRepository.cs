﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Improvements.Instance;
using Loka.Server.Infrastructure.DataRepository;

namespace Loka.Common.Improvements
{
    public class ImprovementRepository
    {
        public static void OnVoteCounting()
        {
            using (var db = new DataRepository())
            {
                var improvements = db.ImprovementEntities.Where(e => e.ProgressState == ImprovementProgressState.Voting).ToList();
                var sorterImprovements = improvements.OrderByDescending(e => e.CurrentVoteCount);
                var bestImprovement = sorterImprovements.FirstOrDefault();

                foreach (var improvement in improvements)
                {
                    improvement.Step++;
                }

                if (bestImprovement != null)
                {
                    bestImprovement.ProgressState = ImprovementProgressState.Development;
                }

                db.SaveChanges();
            }
        }

    }
}
