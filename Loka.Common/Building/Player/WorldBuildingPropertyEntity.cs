﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store.Property;
using Loka.Server.Player.Models;

namespace Loka.Common.Building.Player
{
    public class WorldBuildingPropertyEntity
    {
        public WorldBuildingPropertyEntity() { }


        public static WorldBuildingPropertyEntity Factory(WorldBuildingItemEntity entity, ItemInstancePropertyEntity instance)
        {
            return new WorldBuildingPropertyEntity
            {
                EntityId = Guid.NewGuid(),
                BuildingId = entity.ItemId,
                PlayerId = entity.PlayerId,
                PlayerEntity = entity.PlayerEntity,
                PropertyId = instance.EntityId,
                BuildingEntity = entity,
                PropertyEntity = instance,
                Value = instance.Default,
                EditDate = DateTime.UtcNow,
                Last = instance.Value,
                Hash = instance.Hash
            };
        }

        public Guid EntityId { get; set; }
		public Guid BuildingId { get; set; }
		public virtual WorldBuildingItemEntity BuildingEntity { get; set; }


        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

        public Guid PropertyId { get; set; }
		public virtual ItemInstancePropertyEntity PropertyEntity { get; set; }

		public ItemInstancePropertyName Key => PropertyEntity?.Key ?? ItemInstancePropertyName.None;
		public string Default => PropertyEntity?.Value;
		public string Value { get; set; }
		public string Last { get; set; }
		public long Hash { get; set; }
		public DateTime EditDate { get; set; }

		public class Configuration : EntityTypeConfiguration<WorldBuildingPropertyEntity>
		{
			public Configuration()
			{
				HasKey(p => p.EntityId);
				ToTable("building.WorldBuildingPropertyEntity");

				Property(x => x.Value).HasMaxLength(64);
                Property(x => x.Last).HasMaxLength(64);

                HasRequired(p => p.PlayerEntity).WithMany().HasForeignKey(p => p.PlayerId).WillCascadeOnDelete(true);
				HasRequired(p => p.PropertyEntity).WithMany().HasForeignKey(p => p.PropertyId).WillCascadeOnDelete(true);
				Property(x => x.BuildingId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_building_key", 1) { IsUnique = true }));
				Property(x => x.PropertyId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_building_key", 2) { IsUnique = true }));
			}
		}
	}
}
