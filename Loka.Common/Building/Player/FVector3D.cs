using System.ComponentModel.DataAnnotations.Schema;

namespace Loka.Common.Building.Player
{
    [ComplexType]
    public class FVector3D
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public override string ToString()
        {
            return $"X {X:F} | Y {Y:F} | Z {Z:F}";
        }
    }
}