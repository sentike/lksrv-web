﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Building.Player
{
    public class WorldBuildingRepairContainer
    {
        public List<WorldBuildingRepairView> Items { get; set; }
    }

    public class WorldBuildingRepairView
    {
        public Guid ItemId { get; set; }
        public long Health { get; set; }

		public WorldBuildingRepairView() { }

	    public WorldBuildingRepairView(Guid id, long health)
	    {
		    ItemId = id;
		    Health = health;
	    }

	}
}
