﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Building.Template;
using Loka.Common.Player.Worlds;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Property;
using Loka.Server.Player.Models;

namespace Loka.Common.Building.Player
{
    public class WorldBuildingItemEntity 
	{
        public static WorldBuildingItemEntity Factory(BuildingModelId model)
        {
            return new WorldBuildingItemEntity()
            {
                ItemId = Guid.NewGuid(),
                ModelId = (short)model,
                CategoryTypeId = CategoryTypeId.Building,
                Position = new FVector3D(),
                Rotation = new FRotator(),
            };
        }

        public Guid ItemId { get; set; }
        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }

	    public Guid? WorldId { get; set; }
	    public virtual PlayerWorlEntity WorlEntity { get; set; }


        public short ModelId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; } = CategoryTypeId.Building;
        public virtual ItemInstanceEntity ModelEntity { get; set; }
        public BuildingModelId BuildingModelId => (BuildingModelId)ModelId;

        public FVector3D Position { get; set; }
        public FRotator Rotation { get; set; }


		public virtual List<WorldBuildingPropertyEntity> PropertyEntities { get; } = new List<WorldBuildingPropertyEntity>();



        public long Harvest(long free)
        {
            //=================================
            var amount = this.Get<long>(ItemInstancePropertyName.Storage);
            
            //==============================
            //  1000 - 2000
            var harvested = Math.Min(free, amount.Property.Value);

            //==============================
            amount.Property.Value -= harvested;

            //==============================
            return harvested;
        }

        public override string ToString()
        {
            return $"{ItemId} | ModelId: {BuildingModelId}[{ModelId}] | Position: {Position} | {Rotation}";
        }

        public class Configuration : EntityTypeConfiguration<WorldBuildingItemEntity>
        {
            public Configuration()
            {
                HasKey(p => p.ItemId)/*.Property(p => p.ItemId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.)*/;
                ToTable("building.WorldBuildingItemEntity");

                HasRequired(p => p.ModelEntity).WithMany().HasForeignKey(p => new { p.ModelId, p.CategoryTypeId}).WillCascadeOnDelete(true);

	            HasMany(p => p.PropertyEntities).WithRequired(p => p.BuildingEntity).HasForeignKey(p => p.BuildingId).WillCascadeOnDelete(true);


			}
		}
    }
}
