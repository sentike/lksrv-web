﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Loka.Common.Building.Template;
using Loka.Common.Store;
using Loka.Common.Store.Property;

namespace Loka.Common.Building.Player
{
    public class FWorldBuildingProperty
    {
        public FWorldBuildingProperty()
        {

        }

        public FWorldBuildingProperty(string value, string @default)
        {
            Value = value;
            Default = @default;
        }

        public string Value { get; set; }
        public string Default { get; set; }
    }

    public class WorldBuildingItemView
    {
        public Guid ItemId { get; set; }
        public Guid? NewItemId { get; set; }
        public BuildingModelId ModelId { get; set; }
        public FVector3D Position { get; set; }
        public FRotator Rotation { get; set; }

	    public Dictionary<ItemInstancePropertyName, FWorldBuildingProperty> Properties { get; set; } = new Dictionary<ItemInstancePropertyName, FWorldBuildingProperty>();

		public override string ToString()
        {
            return $"ItemId: {ItemId} | {ModelId} | {Position} | {Rotation}";
        }
    }

    public class WorldBuildingStorageView
    {
        public Guid ItemId { get; set; }
        public long Storage { get; set; }
    }


    public static partial class ViewHelper
    {
        //-------------------------------------------------------------------------------------------------
        private static readonly IMapper Mapper;
        static ViewHelper()
        {
            Mapper = new MapperConfiguration(c =>
            {
                //==========================================================================
                c.CreateMap<WorldBuildingItemEntity, WorldBuildingStorageView>()
                    .ForMember(m => m.Storage, m => m.MapFrom(r => r.Get<long>(ItemInstancePropertyName.Storage).Property.Value));


                c.CreateMap<WorldBuildingItemEntity, WorldBuildingItemView>()
		            .ForSourceMember(m => m.ModelId, m => m.Ignore())
		            .ForMember(m => m.ModelId, m => m.MapFrom(r => r.BuildingModelId))
		            .ForMember(m => m.Properties, m => m.MapFrom(r => r.PropertyEntities.ToDictionary(p => p.Key, p => new FWorldBuildingProperty(p.Value, p.Default))));


				//.ForMember(m => m.Sockets, m => m.MapFrom(r => r.Sockets.AsView()));

				//==========================================================================
				//  To template
                c.CreateMap<WorldBuildingItemEntity, TemplateBuildingEntity>()
                    .ForSourceMember(p => p.PlayerEntity, p => p.Ignore())
                    .ForSourceMember(p => p.ModelEntity, p => p.Ignore())
                    .ForSourceMember(p => p.PlayerId, p => p.Ignore())
                    .ForMember(p => p.ModelEntity, p => p.Ignore());


                //==========================================================================
                //  From template
                c.CreateMap<TemplateBuildingEntity, WorldBuildingItemEntity>()
                    .ForSourceMember(m => m.Sockets, m => m.Ignore())
                    .ForSourceMember(m => m.Childs, m => m.Ignore())
                    .ForSourceMember(p => p.ModelEntity, p => p.Ignore())
                    .ForMember(p => p.ModelEntity, p => p.Ignore());

                //==========================================================================
            }).CreateMapper();
        }


        //==========================================================================
        //  Template
        public static TemplateBuildingEntity AsTemplate(this WorldBuildingItemEntity entity)
        {
            return Mapper.Map<TemplateBuildingEntity>(entity);
        }

        public static List<TemplateBuildingEntity> AsTemplate(this ICollection<WorldBuildingItemEntity> entity)
        {
            return entity.Select(AsTemplate).ToList();
        }




        public static WorldBuildingItemEntity FromTemplate(this TemplateBuildingEntity entity) => Mapper.Map<WorldBuildingItemEntity>(entity);
        public static List<WorldBuildingItemEntity> FromTemplate(this ICollection<TemplateBuildingEntity> template) => template.Select(FromTemplate).ToList();
        //==========================================================================

        public static List<WorldBuildingItemView> AsView(this ICollection<WorldBuildingItemEntity> entity)
        {
            return entity.Select(m => m.AsView()).ToList();
        }

        public static bool IsInit = false;
        public static WorldBuildingItemView AsView(this WorldBuildingItemEntity entity)
        {
            return Mapper.Map<WorldBuildingItemView>(entity);
        }

        public static WorldBuildingStorageView AsStorageView(this WorldBuildingItemEntity entity)
        {
            return Mapper.Map<WorldBuildingStorageView>(entity);
        }



        //-------------------------------------------------------------------------------------------------

        public static WorldBuildingItemEntity AsCreate(this WorldBuildingItemView entity, Guid userId)
        {
            return new MapperConfiguration(c =>
            {
                c.CreateMap<WorldBuildingItemView, WorldBuildingItemEntity>()
                    .ForMember(m => m.PlayerId, m => m.UseValue(userId))
                    .ForMember(m => m.CategoryTypeId, m => m.UseValue(CategoryTypeId.Building))
                    .ForMember(m => m.ModelId, m => m.MapFrom(r => (short) r.ModelId));

            }).CreateMapper().Map<WorldBuildingItemEntity>(entity);
        }
    }


}
