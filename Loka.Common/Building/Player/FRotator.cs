﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Loka.Common.Building.Player
{
    [ComplexType]
    public class FRotator
    {
        public float Pitch { get; set; }
        public float Roll { get; set; }
        public float Yaw { get; set; }

        public override string ToString()
        {
            return $"Pitch {Pitch:F} | Roll {Roll:F} | Yaw {Yaw:F}";
        }
    }
}