﻿using System;
using Loka.Common.Store;

namespace Loka.Common.Building.Template
{
    public interface IWorldBuildingInterface
    {
        Guid ItemId { get; set; }
        Guid OItemId { get; set; }

	    short ModelId { get; set; }
        CategoryTypeId CategoryTypeId { get; set; }
        BuildingModelId BuildingModelId { get; }
    }
}