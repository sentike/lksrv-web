﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store;
using Loka.Server.Player.Models;

namespace Loka.Common.Building.Template
{
    public class BaseTemplateEntity
    {
        public BaseTemplateEntity() {}

        public BaseTemplateEntity(SaveTemplateView view, Guid player)
            : this(view)
        {
            CreatorId = player;
        }

        public BaseTemplateEntity(SaveTemplateView view)
        {
            Name = view.Name;
            Cost = view.Cost;
            AllowSell = view.AllowSell;
            TemplateId = Guid.NewGuid();
        }

        //=========================================
        public Guid TemplateId { get; set; }
        public string Name { get; set; }
        public StoreItemCost Cost { get; set; }

        //=========================================
        public Guid CreatorId { get; set; }
        public virtual PlayerEntity CreatorEntity { get; set; }
        public virtual ICollection<TemplateBuildingEntity> BuildingEntities { get; set; } = new List<TemplateBuildingEntity>(1024);
        public virtual ICollection<PlayerTemplateEntity> PlayerTemplateEntities { get; set; } = new List<PlayerTemplateEntity>();

        //=========================================
        public long Downloads { get; set; }
        public long Likes { get; set; }
        public long Views { get; set; }

        //=========================================
        public bool AllowSell { get; set; }

        public override string ToString()
        {
            return $"{TemplateId} -- {Name} | Creator: {CreatorEntity?.PlayerName} [{CreatorId}]";
        }

        public class Configuration : EntityTypeConfiguration<BaseTemplateEntity>
        {
            public Configuration()
            {
                HasKey(p => p.TemplateId);
                ToTable("tps.BaseTemplateEntity");
                Property(p => p.Name).IsRequired().HasMaxLength(64);
                HasMany(p => p.BuildingEntities).WithRequired(p => p.TemplateEntity).HasForeignKey(p => p.TemplateId).WillCascadeOnDelete(true);
                HasMany(p => p.PlayerTemplateEntities).WithRequired(p => p.TemplateEntity).HasForeignKey(p => p.TemplateId).WillCascadeOnDelete(true);
            }
        }

    }
}
