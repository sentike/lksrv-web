﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Player.Models;

namespace Loka.Common.Building.Template
{
    public class PlayerTemplateEntity
    {
        public PlayerTemplateEntity()
        {
            
        }

        public PlayerTemplateEntity(BaseTemplateEntity template)
        {
            PlayerTemplateId = Guid.NewGuid();
            TemplateId = template.TemplateId;
            CreatedDate = DateTime.UtcNow;
        }

        public PlayerTemplateEntity(BaseTemplateEntity template, Guid player)
            : this(template)
        {
            PlayerId = player;
        }

        public Guid PlayerTemplateId { get; set; }

        //===================================================================
        public Guid PlayerId { get; set; }
        public virtual PlayerEntity PlayerEntity { get; set; }


        public Guid TemplateId { get; set; }
        public virtual BaseTemplateEntity TemplateEntity { get; set; }

        //===================================================================
        public DateTime CreatedDate { get; set; }

        public class Configuration : EntityTypeConfiguration<PlayerTemplateEntity>
        {
            public Configuration()
            {
                HasKey(p => p.PlayerTemplateId);
                ToTable("tps.PlayerTemplateEntity");
            }
        }

    }
}
