﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Building.Player;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Server.Player.Models;

namespace Loka.Common.Building.Template
{
    public class TemplateBuildingEntity
    {
        public virtual Guid ItemId { get; set; }
        //public Guid OItemId { get; set; }
        public Guid TemplateId { get; set; }
        public virtual BaseTemplateEntity TemplateEntity { get; set; }
        

        public short ModelId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; } = CategoryTypeId.Building;

        public virtual ItemInstanceEntity ModelEntity { get; set; }
        public BuildingModelId BuildingModelId => (BuildingModelId)ModelId;

        public virtual ICollection<TemplateBuildingAttachEntity> Sockets { get; set; } = new List<TemplateBuildingAttachEntity>();
        public virtual ICollection<TemplateBuildingAttachEntity> Childs { get; set; } = new List<TemplateBuildingAttachEntity>();

        public FVector3D Position { get; set; }
        public FRotator Rotation { get; set; }
 
        public override string ToString()
        {
            return $"{ItemId} | ModelId: {BuildingModelId}[{ModelId}] | Position: {Position} | {Rotation} | Sockets: {/*Sockets.Count*/0} | {/*Childs.Count*/0}";
        }

        public class Configuration : EntityTypeConfiguration<TemplateBuildingEntity>
        {
            public Configuration()
            {
                //Ignore(p => p.OItemId);
                HasKey(p => p.ItemId);
                ToTable("tps.tpsbuild");
                HasRequired(p => p.ModelEntity).WithMany().HasForeignKey(p => new { p.ModelId, p.CategoryTypeId }).WillCascadeOnDelete(true);

                HasMany(p => p.Sockets).WithRequired(p => p.ItemEntity).HasForeignKey(p => p.ItemId).WillCascadeOnDelete(true);
                HasMany(p => p.Childs).WithRequired(p => p.TargetEntity).HasForeignKey(p => p.TargetId).WillCascadeOnDelete(true);
            }
        }
    }
}
