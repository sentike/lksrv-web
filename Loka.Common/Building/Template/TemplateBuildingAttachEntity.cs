﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Building.Player;

namespace Loka.Common.Building.Template
{
    public interface IWorldBuildingAttachInterface
    {
        Guid AttachId { get; set; }
        Guid ItemId { get; set; }
        Guid TargetId { get; set; }

        Guid OAttachId { get; set; }
        Guid OItemId { get; set; }
        Guid OTargetId { get; set; }

        byte AttachFrom { get; set; }
        byte AttachTo { get; set; }

    }

    public class TemplateBuildingAttachEntity 
    {
        public Guid AttachId { get; set; }
    
        public virtual Guid ItemId { get; set; }
        public virtual TemplateBuildingEntity ItemEntity { get; set; }
    
        public virtual Guid TargetId { get; set; }
        public virtual TemplateBuildingEntity TargetEntity { get; set; }
    
        public byte AttachFrom { get; set; }
        public byte AttachTo { get; set; }

        public override string ToString()
        {
            return $"{AttachId} | Target: {TargetId} | Item: {ItemId}";
        }

        public class Configuration : EntityTypeConfiguration<TemplateBuildingAttachEntity>
        {
            public Configuration()
            {
                //Ignore(p => p.OItemId);
                //Ignore(p => p.OAttachId);
                //Ignore(p => p.OTargetId);

                HasKey(p => p.AttachId).Property(p => p.AttachId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                ToTable("tps.tpsbuildattach");
            }
        }
    }
}
