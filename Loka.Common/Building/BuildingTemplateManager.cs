﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Building.Player;
using Loka.Common.Building.Template;
using Loka.Common.Player.Inventory;
using Loka.Common.Store;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Player.Models;
using MoreLinq;

namespace Loka.Common.Building
{
    public class SaveTemplateView
    {
        public string Name { get; set; }
        public StoreItemCost Cost { get; set; }
        public bool AllowSell { get; set; }
    }

    public static class BuildingTemplateManager
    {
        public static bool ExistTemplate(this PlayerEntity player, Guid id)
        {
            return player.PlayerTemplateEntities.Any(p => p.TemplateId == id);
        }

        public static void UnlockTemplate(this PlayerEntity player, DataRepository db, Guid id)
        {
            var template = db.BaseTemplateEntities.SingleOrDefault(p => p.TemplateId == id);

            //===============================================================================
            if (template == null)
            {
                throw new ArgumentNullException(nameof(template), "Template not found");
            }

            //===============================================================================
            if (player.ExistTemplate(id))
            {
                throw new DuplicateNameException("Template was exist");
            }

            player.PlayerTemplateEntities.Add(new PlayerTemplateEntity(template, player.PlayerId));
            db.SaveChanges();
        }

        public struct KeyPair
        {
            public Dictionary<Guid, Guid> Buldings { get; set; }
            public Dictionary<Guid, Guid> Attaches { get; set; }

        }



        public static void SaveTemplate(this SaveTemplateView view, PlayerEntity player, DataRepository db)
        {
            var instance = player.WorldBuildingEntities.ToList();
            var prototypes = instance.AsTemplate();
   

            var template = new BaseTemplateEntity(view, player.PlayerId)
            {
                BuildingEntities = prototypes
            };

            var purchase = new PlayerTemplateEntity(template, player.PlayerId);
            var keys = prototypes.ToDictionary(p => p.ItemId, p => Guid.NewGuid());
            var map = prototypes.ToDictionary(p => p.ItemId, p => p);
            foreach (var p in prototypes)
            {
                p.ItemId = keys[p.ItemId];
            }

            foreach (var p in prototypes)
            {
                var sss = p.Sockets.ToArray();

                p.Sockets.Clear();
                p.Childs.Clear();
                p.Sockets = new List<TemplateBuildingAttachEntity>();
                p.Childs = new List<TemplateBuildingAttachEntity>();

                foreach (var s in sss)
                {
                    p.Sockets.Add(new TemplateBuildingAttachEntity
                    {
                        AttachId = Guid.NewGuid(),

                        ItemEntity = map[s.ItemId],
                        ItemId = map[s.ItemId].ItemId,

                        TargetEntity = map[s.TargetId],
                        TargetId = map[s.TargetId].ItemId,

                        AttachFrom = s.AttachFrom,
                        AttachTo = s.AttachTo,
                    });
                }
            }

            db.BaseTemplateEntities.Add(template);
            db.PlayerTemplateEntities.Add(purchase);
            db.SaveChanges();
        }

        public static bool ApplyTemplate(this PlayerEntity player, DataRepository db, Guid id, bool force, out Dictionary<BuildingModelId, long> depends)
        {
            var template = db.BaseTemplateEntities.SingleOrDefault(p => p.TemplateId == id);
            //===============================================================================
            if (template == null)
            {
                throw new ArgumentNullException(nameof(template), "Template not found");
            }

            //===============================================================================
            //if (player.ExistTemplate(id) == false)
            //{
            //    throw new DuplicateNameException("Template not paid");
            //}

            //===============================================================================
            //  Создаем соотношение модель - количество
            Dictionary<BuildingModelId, long> amounts = new Dictionary<BuildingModelId, long>(32);
            Dictionary<BuildingModelId, PlayerInventoryItemEntity> items = new Dictionary<BuildingModelId, PlayerInventoryItemEntity>(32);

            //===============================================================================
            //  Считаем количество объектов в инвентаре
            foreach (var b in player.BuildingList)
            {
                amounts.Add((BuildingModelId)b.ModelId, b.Count);
                items.Add((BuildingModelId)b.ModelId, b);
            }

            //===============================================================================
            //  Считаем количество построенных объектов
            foreach (var b in player.WorldBuildingEntities.GroupBy(p => p.BuildingModelId))
            {
                amounts[b.Key] += b.Count();
            }

            //===============================================================================
            depends = new Dictionary<BuildingModelId, long>(32);
            Dictionary<BuildingModelId, long> requirements = new Dictionary<BuildingModelId, long>(32);

            //===============================================================================
            //  Считаем количество объектов необходимых для постройки
            foreach (var b in template.BuildingEntities.GroupBy(p => p.BuildingModelId))
            {
                requirements.Add(b.Key, b.Count());
            }

            //===============================================================================
            if (force == false)
            {
                //  Составляем список зависимостей
                foreach (var require in requirements)
                {
                    if (amounts.ContainsKey(require.Key) == false || amounts[require.Key] < require.Value)
                    {
                        depends.Add(require.Key, require.Value);
                    }
                }

                if (depends.Any())
                {
                    return false;
                }
            }

            //===============================================================================
            //  Удаляем постройки игрока
            db.BuildingItemEntities.RemoveRange(player.WorldBuildingEntities);

            //===============================================================================
            //  Обновляем инвентарь игрока, добавляем уничтоженные постройки с учетом новых построек
            foreach (var b in amounts)
            {
                var dep = depends.ContainsKey(b.Key) ? depends[b.Key] : 0;
                items[b.Key].Amount = Math.Min(0, b.Value - dep);
            }

            //===============================================================================
            var prototypes = player.WorldBuildingEntities = template.BuildingEntities.FromTemplate();
            var keys = prototypes.ToDictionary(p => p.ItemId, p => Guid.NewGuid());
            var map = prototypes.ToDictionary(p => p.ItemId, p => p);
            foreach (var p in prototypes)
            {
                p.ItemId = keys[p.ItemId];
            }

            //===============================================================================
            db.SaveChanges();

            return true;
        }
    }
}
