﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Building.Player;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Item.Addon;
using Loka.Common.Store.Property;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Player.Models;

namespace Loka.Common.Building.Handlers
{
	public class RepairBuildingHandler
	{
		private static long GetRepairCost(WorldBuildingItemEntity entity)
		{
			long cost = 1000;
			//==================================================
			if (entity.ModelEntity.Any(ItemInstancePropertyName.RepairCost))
			{
                cost = entity.ModelEntity.Get<long>(ItemInstancePropertyName.RepairCost).Value;
			}

            //==================================================
            else if (entity.ModelEntity.SubscribeCost.Currency == GameCurrency.Money)
			{
				cost = entity.ModelEntity.SubscribeCost.Amount;
			}

			//==================================================
			return cost;
		}
		public static bool Handler(PlayerEntity player, WorldBuildingItemEntity entity, out long amount)
		{
			var health = entity.Get<long>(ItemInstancePropertyName.Health);

            //--------------------------------
            amount = health.Property.Value;
            
            //--------------------------------
            if (health.Property.Value < health.Instance.Value)
			{
				//--------------------------------
				var damaged = health.Property.Value / Math.Max(1.0, health.Instance.Value);

				//--------------------------------
				var cost = GetRepairCost(entity);
				var price = (cost / 4.0) + (cost * health.Instance.Value - damaged);

				//--------------------------------
				var sub = price.Clamp(cost / 4.0, cost);
				var total = Convert.ToInt64(sub);

				//--------------------------------
				if (player.Pay(GameCurrency.Money, total))
				{
					amount = health.Property.Value = health.Instance.Value;
					return true;
				}
				return false;
				//--------------------------------
			}
			return true;
		}
	}
}
