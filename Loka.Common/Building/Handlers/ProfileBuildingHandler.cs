﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Building.Player;
using Loka.Common.Store.Property;
using Loka.Server.Player.Models;
using VRS.Infrastructure;

namespace Loka.Common.Building.Handlers
{
	public class UpdateBuildingProperty
	{
		public Guid BuildingId { get; set; }
		public ItemInstancePropertyName Key { get; set; }
		public string Value { get; set; }

		public TData GetAs<TData>()
		{
			if (string.IsNullOrWhiteSpace(Value))
			{
				return default(TData);
			}

			try
			{
				return (TData)TypeDescriptor.GetConverter(typeof(TData)).ConvertFromInvariantString(Value);
				//return (TData)Convert.ChangeType(Value, typeof(TData));
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return default(TData);
			}
		}
	}


	public static class ProfileBuildingHandler
	{
		public static bool Handler(PlayerEntity player, UpdateBuildingProperty property)
		{
			//========================================================================
			var building = player.WorldBuildingEntities.SingleOrDefault(p => p.ItemId == property.BuildingId);
			if (building == null)
			{
				LoggerContainer.BuildingLogger.Error($"[ProfileBuildingHandler][{player.PlayerName}][Value: {property.Value}][BuildingId: {property.BuildingId}][building was null]");
				return false;
			}

			if (player.InventoryProfileList.Any(p => p.AssetId == property.GetAs<Guid>()) == false)
			{
				LoggerContainer.BuildingLogger.Error($"[ProfileBuildingHandler][{player.PlayerName}][Value: {property.Value} | {property.GetAs<Guid>()}][BuildingId: {property.BuildingId}][profile was null]");
				return false;
			}

			//========================================================================
			if (building.ModelEntity.Any(ItemInstancePropertyName.Profile) == false)
			{
				LoggerContainer.BuildingLogger.Error($"[ProfileBuildingHandler][{player.PlayerName}][Value: {property.Value}][BuildingId: {property.BuildingId}][building not Profile]");
				return false;
			}

			//========================================================================
			var profile = building.Get<Guid>(ItemInstancePropertyName.Profile);
			profile.Property.Value = property.GetAs<Guid>();

			//========================================================================
			return true;
		}
	}
}
