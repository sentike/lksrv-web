namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedpromo1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("promo.PromoItemEntity", new[] { "ModelInstance_ModelId", "ModelInstance_CategoryTypeId" }, "store.AbstractItemInstance");
            DropIndex("promo.PromoItemEntity", new[] { "ModelInstance_ModelId", "ModelInstance_CategoryTypeId" });
            DropColumn("promo.PromoItemEntity", "ModelId");
            RenameColumn(table: "promo.PromoItemEntity", name: "ModelInstance_ModelId", newName: "ModelId");
            RenameColumn(table: "promo.PromoItemEntity", name: "ModelInstance_CategoryTypeId", newName: "CategoryTypeId");
            AlterColumn("promo.PromoItemEntity", "ModelId", c => c.Short(nullable: false));
            AlterColumn("promo.PromoItemEntity", "CategoryTypeId", c => c.Int(nullable: false));
            CreateIndex("promo.PromoItemEntity", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("promo.PromoItemEntity", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" }, cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("promo.PromoItemEntity", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropIndex("promo.PromoItemEntity", new[] { "ModelId", "CategoryTypeId" });
            AlterColumn("promo.PromoItemEntity", "CategoryTypeId", c => c.Int());
            AlterColumn("promo.PromoItemEntity", "ModelId", c => c.Short());
            RenameColumn(table: "promo.PromoItemEntity", name: "CategoryTypeId", newName: "ModelInstance_CategoryTypeId");
            RenameColumn(table: "promo.PromoItemEntity", name: "ModelId", newName: "ModelInstance_ModelId");
            AddColumn("promo.PromoItemEntity", "ModelId", c => c.Short(nullable: false));
            CreateIndex("promo.PromoItemEntity", new[] { "ModelInstance_ModelId", "ModelInstance_CategoryTypeId" });
            AddForeignKey("promo.PromoItemEntity", new[] { "ModelInstance_ModelId", "ModelInstance_CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" });
        }
    }
}
