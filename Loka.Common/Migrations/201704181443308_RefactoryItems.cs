namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoryItems : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("store.WeaponItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.ServiceInstanceEntity", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.SubsribeServiceEntity", new[] { "ModelId", "CategoryTypeId" }, "store.ServiceInstanceEntity");
            DropForeignKey("store.SkinItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.GrenadeItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.KitItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.CharacterItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.ArmourItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.AmmoItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("players.InstanceOfItemAddon", "ItemId", "players.InstanceOfTargetItem");
            DropForeignKey("players.InstanceOfItemSkin", "ItemId", "players.InstanceOfTargetItem");
            DropForeignKey("players.InstanceOfPlayerWeapon", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerCharacter", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerArmour", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerKit", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerGrenade", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.PlayerServiceEntity", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("store.BuildingItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("players.PlayerBuildingItemEntity", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.PlayerProfileEntity", "CharacterId", "players.InstanceOfPlayerCharacter");
            DropIndex("store.WeaponItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.ServiceInstanceEntity", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.SubsribeServiceEntity", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.SkinItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.GrenadeItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.KitItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.CharacterItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.ArmourItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.AmmoItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("players.InstanceOfItemAddon", new[] { "ItemId" });
            DropIndex("players.InstanceOfItemSkin", new[] { "ItemId" });
            DropIndex("players.InstanceOfPlayerWeapon", new[] { "ItemId" });
            DropIndex("players.InstanceOfPlayerCharacter", new[] { "ItemId" });
            DropIndex("players.InstanceOfPlayerArmour", new[] { "ItemId" });
            DropIndex("players.InstanceOfPlayerKit", new[] { "ItemId" });
            DropIndex("players.InstanceOfPlayerGrenade", new[] { "ItemId" });
            DropIndex("players.PlayerServiceEntity", new[] { "ItemId" });
            DropIndex("store.BuildingItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("players.PlayerBuildingItemEntity", new[] { "ItemId" });
            AddColumn("store.AbstractItemInstance", "DefaultAmount", c => c.Short(nullable: false));
            AddColumn("store.AbstractItemInstance", "Performance", c => c.Long());
            AddColumn("store.AbstractItemInstance", "Storage", c => c.Long());
            AddColumn("building.WorldBuildingItemEntity", "Storage", c => c.Long(nullable: false));
            AlterColumn("building.WorldBuildingItemEntity", "Health", c => c.Long(nullable: false));
            AddForeignKey("players.PlayerProfileEntity", "CharacterId", "players.InstanceOfPlayerItem", "ItemId", cascadeDelete: true);
            DropTable("store.WeaponItemInstance");
            DropTable("store.ServiceInstanceEntity");
            DropTable("store.SubsribeServiceEntity");
            DropTable("store.SkinItemInstance");
            DropTable("store.GrenadeItemInstance");
            DropTable("store.KitItemInstance");
            DropTable("store.CharacterItemInstance");
            DropTable("store.ArmourItemInstance");
            DropTable("store.AmmoItemInstance");
            DropTable("players.InstanceOfItemAddon");
            DropTable("players.InstanceOfItemSkin");
            DropTable("players.InstanceOfPlayerWeapon");
            DropTable("players.InstanceOfPlayerCharacter");
            DropTable("players.InstanceOfPlayerArmour");
            DropTable("players.InstanceOfPlayerKit");
            DropTable("players.InstanceOfPlayerGrenade");
            DropTable("players.PlayerServiceEntity");
            DropTable("store.BuildingItemInstance");
            DropTable("players.PlayerBuildingItemEntity");
        }
        
        public override void Down()
        {
            CreateTable(
                "players.PlayerBuildingItemEntity",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                        Amount = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "store.BuildingItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "players.PlayerServiceEntity",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                        Amount = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerGrenade",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerKit",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerArmour",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerCharacter",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerWeapon",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfItemSkin",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfItemAddon",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "store.AmmoItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.ArmourItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.CharacterItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.KitItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.GrenadeItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.SkinItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.SubsribeServiceEntity",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.ServiceInstanceEntity",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                        SubCategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.WeaponItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId });
            
            DropForeignKey("players.PlayerProfileEntity", "CharacterId", "players.InstanceOfPlayerItem");
            AlterColumn("building.WorldBuildingItemEntity", "Health", c => c.Short(nullable: false));
            DropColumn("building.WorldBuildingItemEntity", "Storage");
            DropColumn("store.AbstractItemInstance", "Storage");
            DropColumn("store.AbstractItemInstance", "Performance");
            DropColumn("store.AbstractItemInstance", "DefaultAmount");
            CreateIndex("players.PlayerBuildingItemEntity", "ItemId");
            CreateIndex("store.BuildingItemInstance", new[] { "ModelId", "CategoryTypeId" });
            CreateIndex("players.PlayerServiceEntity", "ItemId");
            CreateIndex("players.InstanceOfPlayerGrenade", "ItemId");
            CreateIndex("players.InstanceOfPlayerKit", "ItemId");
            CreateIndex("players.InstanceOfPlayerArmour", "ItemId");
            CreateIndex("players.InstanceOfPlayerCharacter", "ItemId");
            CreateIndex("players.InstanceOfPlayerWeapon", "ItemId");
            CreateIndex("players.InstanceOfItemSkin", "ItemId");
            CreateIndex("players.InstanceOfItemAddon", "ItemId");
            CreateIndex("store.AmmoItemInstance", new[] { "ModelId", "CategoryTypeId" });
            CreateIndex("store.ArmourItemInstance", new[] { "ModelId", "CategoryTypeId" });
            CreateIndex("store.CharacterItemInstance", new[] { "ModelId", "CategoryTypeId" });
            CreateIndex("store.KitItemInstance", new[] { "ModelId", "CategoryTypeId" });
            CreateIndex("store.GrenadeItemInstance", new[] { "ModelId", "CategoryTypeId" });
            CreateIndex("store.SkinItemInstance", new[] { "ModelId", "CategoryTypeId" });
            CreateIndex("store.SubsribeServiceEntity", new[] { "ModelId", "CategoryTypeId" });
            CreateIndex("store.ServiceInstanceEntity", new[] { "ModelId", "CategoryTypeId" });
            CreateIndex("store.WeaponItemInstance", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("players.PlayerProfileEntity", "CharacterId", "players.InstanceOfPlayerCharacter", "ItemId");
            AddForeignKey("players.PlayerBuildingItemEntity", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
            AddForeignKey("store.BuildingItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("players.PlayerServiceEntity", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
            AddForeignKey("players.InstanceOfPlayerGrenade", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
            AddForeignKey("players.InstanceOfPlayerKit", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
            AddForeignKey("players.InstanceOfPlayerArmour", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
            AddForeignKey("players.InstanceOfPlayerCharacter", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
            AddForeignKey("players.InstanceOfPlayerWeapon", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
            AddForeignKey("players.InstanceOfItemSkin", "ItemId", "players.InstanceOfTargetItem", "ItemId");
            AddForeignKey("players.InstanceOfItemAddon", "ItemId", "players.InstanceOfTargetItem", "ItemId");
            AddForeignKey("store.AmmoItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("store.ArmourItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("store.CharacterItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("store.KitItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("store.GrenadeItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("store.SkinItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("store.SubsribeServiceEntity", new[] { "ModelId", "CategoryTypeId" }, "store.ServiceInstanceEntity", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("store.ServiceInstanceEntity", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("store.WeaponItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" });
        }
    }
}
