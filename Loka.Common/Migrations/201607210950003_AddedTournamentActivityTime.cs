namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTournamentActivityTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Experience_WeekActivityTime", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "Experience_WeekActivityTime");
        }
    }
}
