namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMatchState : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchEntity", "State", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("matches.MatchEntity", "State");
        }
    }
}
