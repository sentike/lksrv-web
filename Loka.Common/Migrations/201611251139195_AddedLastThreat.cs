namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedLastThreat : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Statistic_LastThreatLevel", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "Statistic_LastThreatLevel");
        }
    }
}
