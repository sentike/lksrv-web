namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTargetProfileIdForCharacters : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.CharacterItemInstance", "TargetProfileId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("store.CharacterItemInstance", "TargetProfileId");
        }
    }
}
