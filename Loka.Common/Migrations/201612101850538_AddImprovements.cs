namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImprovements : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "improvement.SentenceEntity",
                c => new
                    {
                        SentenceId = c.Guid(nullable: false),
                        ProgressState = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 128),
                        Message = c.String(nullable: false, maxLength: 32768),
                        RejectReason = c.String(maxLength: 1024),
                        CreatorId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SentenceId)
                .ForeignKey("improvement.ImprovementCategoryEntity", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.CreatorId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.CreatorId);
            
            CreateTable(
                "improvement.ImprovementCategoryEntity",
                c => new
                    {
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "improvement.ImprovementEntity",
                c => new
                    {
                        ImprovementId = c.Guid(nullable: false),
                        ProgressState = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ImprovementId)
                .ForeignKey("improvement.ImprovementCategoryEntity", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "improvement.ImprovementLocalizationEntity",
                c => new
                    {
                        LocalizationId = c.Guid(nullable: false),
                        LanguageId = c.Int(nullable: false),
                        ImprovementId = c.Guid(nullable: false),
                        Title = c.String(maxLength: 128),
                        Message = c.String(maxLength: 32768),
                    })
                .PrimaryKey(t => t.LocalizationId)
                .ForeignKey("improvement.ImprovementEntity", t => t.ImprovementId, cascadeDelete: true)
                .Index(t => new { t.LanguageId, t.ImprovementId }, unique: true, name: "LanguageId_ImprovementId");
            
            CreateTable(
                "improvement.ImprovementVoteEntity",
                c => new
                    {
                        VoteId = c.Guid(nullable: false),
                        ImprovementId = c.Guid(nullable: false),
                        ImprovementProgressState = c.Int(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        VoteDate = c.DateTime(nullable: false),
                        IsApprove = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.VoteId)
                .ForeignKey("improvement.ImprovementEntity", t => t.ImprovementId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.ImprovementId)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "improvement.SentenceVoteEntity",
                c => new
                    {
                        VoteId = c.Guid(nullable: false),
                        SentenceId = c.Guid(nullable: false),
                        ImprovementProgressState = c.Int(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        VoteDate = c.DateTime(nullable: false),
                        IsApprove = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.VoteId)
                .ForeignKey("improvement.SentenceEntity", t => t.SentenceId, cascadeDelete: true)
                .Index(t => t.SentenceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("improvement.ImprovementVoteEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("improvement.SentenceVoteEntity", "SentenceId", "improvement.SentenceEntity");
            DropForeignKey("improvement.SentenceEntity", "CreatorId", "players.PlayerEntity");
            DropForeignKey("improvement.SentenceEntity", "CategoryId", "improvement.ImprovementCategoryEntity");
            DropForeignKey("improvement.ImprovementVoteEntity", "ImprovementId", "improvement.ImprovementEntity");
            DropForeignKey("improvement.ImprovementLocalizationEntity", "ImprovementId", "improvement.ImprovementEntity");
            DropForeignKey("improvement.ImprovementEntity", "CategoryId", "improvement.ImprovementCategoryEntity");
            DropIndex("improvement.SentenceVoteEntity", new[] { "SentenceId" });
            DropIndex("improvement.ImprovementVoteEntity", new[] { "PlayerId" });
            DropIndex("improvement.ImprovementVoteEntity", new[] { "ImprovementId" });
            DropIndex("improvement.ImprovementLocalizationEntity", "LanguageId_ImprovementId");
            DropIndex("improvement.ImprovementEntity", new[] { "CategoryId" });
            DropIndex("improvement.SentenceEntity", new[] { "CreatorId" });
            DropIndex("improvement.SentenceEntity", new[] { "CategoryId" });
            DropTable("improvement.SentenceVoteEntity");
            DropTable("improvement.ImprovementVoteEntity");
            DropTable("improvement.ImprovementLocalizationEntity");
            DropTable("improvement.ImprovementEntity");
            DropTable("improvement.ImprovementCategoryEntity");
            DropTable("improvement.SentenceEntity");
        }
    }
}
