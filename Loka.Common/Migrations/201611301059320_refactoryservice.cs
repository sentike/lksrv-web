namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class refactoryservice : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("players.PlayerUsingServiceEntity", "ServiceInstanceId", "players.PlayerServiceEntity");
            DropIndex("players.PlayerUsingServiceEntity", new[] { "ServiceInstanceId" });
            DropIndex("players.PlayerUsingServiceEntity", new[] { "PlayerId" });
            AddColumn("players.PlayerUsingServiceEntity", "ServiceTypeId", c => c.Int(nullable: false));
            CreateIndex("players.PlayerUsingServiceEntity", new[] { "PlayerId", "ServiceTypeId" }, unique: true, name: "PlayerId_ServiceTypeId");
            DropColumn("players.PlayerServiceEntity", "SubCategoryTypeId");
        }
        
        public override void Down()
        {
            AddColumn("players.PlayerServiceEntity", "SubCategoryTypeId", c => c.Int(nullable: false));
            DropIndex("players.PlayerUsingServiceEntity", "PlayerId_ServiceTypeId");
            DropColumn("players.PlayerUsingServiceEntity", "ServiceTypeId");
            CreateIndex("players.PlayerUsingServiceEntity", "PlayerId");
            CreateIndex("players.PlayerUsingServiceEntity", "ServiceInstanceId");
            AddForeignKey("players.PlayerUsingServiceEntity", "ServiceInstanceId", "players.PlayerServiceEntity", "ItemId");
        }
    }
}
