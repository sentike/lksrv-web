namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAdminComment : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.AbstractItemInstance", "AdminComment", c => c.String(maxLength: 128));
            AddColumn("store.TutorialStepEntity", "AdminComment", c => c.String(maxLength: 128));
            AddColumn("store.TutorialInstanceEntity", "AdminComment", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("store.TutorialInstanceEntity", "AdminComment");
            DropColumn("store.TutorialStepEntity", "AdminComment");
            DropColumn("store.AbstractItemInstance", "AdminComment");
        }
    }
}
