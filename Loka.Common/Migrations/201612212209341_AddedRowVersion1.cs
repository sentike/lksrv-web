namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRowVersion1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.ClusterNodeEntity", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("public.ClusterInstanceEntity", "RowVersion", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("public.ClusterInstanceEntity", "RowVersion");
            DropColumn("public.ClusterNodeEntity", "RowVersion");
        }
    }
}
