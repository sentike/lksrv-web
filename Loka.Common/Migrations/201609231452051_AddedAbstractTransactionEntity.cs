namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAbstractTransactionEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "transcaction.AbstractTransactionEntity",
                c => new
                    {
                        TransactionId = c.Guid(nullable: false),
                        TransactionType = c.Int(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        ExecuteDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TransactionId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "transcaction.DepositTransactionEntity",
                c => new
                    {
                        TransactionId = c.Guid(nullable: false),
                        Amount_Amount = c.Int(nullable: false),
                        Amount_IsDonate = c.Boolean(nullable: false),
                        Source = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TransactionId)
                .ForeignKey("transcaction.AbstractTransactionEntity", t => t.TransactionId, true)
                .Index(t => t.TransactionId);
            
            CreateTable(
                "transcaction.PaymentTransactionEntity",
                c => new
                    {
                        TransactionId = c.Guid(nullable: false),
                        AbstractItemId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TransactionId)
                .ForeignKey("transcaction.AbstractTransactionEntity", t => t.TransactionId, true)
                .ForeignKey("store.AbstractItemInstance", t => new { t.AbstractItemId, t.CategoryTypeId }, cascadeDelete: false)
                .Index(t => t.TransactionId)
                .Index(t => new { t.AbstractItemId, t.CategoryTypeId });
            
        }
        
        public override void Down()
        {
            DropForeignKey("transcaction.PaymentTransactionEntity", new[] { "AbstractItemId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("transcaction.PaymentTransactionEntity", "TransactionId", "transcaction.AbstractTransactionEntity");
            DropForeignKey("transcaction.DepositTransactionEntity", "TransactionId", "transcaction.AbstractTransactionEntity");
            DropForeignKey("transcaction.AbstractTransactionEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("transcaction.PaymentTransactionEntity", new[] { "AbstractItemId", "CategoryTypeId" });
            DropIndex("transcaction.PaymentTransactionEntity", new[] { "TransactionId" });
            DropIndex("transcaction.DepositTransactionEntity", new[] { "TransactionId" });
            DropIndex("transcaction.AbstractTransactionEntity", new[] { "PlayerId" });
            DropTable("transcaction.PaymentTransactionEntity");
            DropTable("transcaction.DepositTransactionEntity");
            DropTable("transcaction.AbstractTransactionEntity");
        }
    }
}
