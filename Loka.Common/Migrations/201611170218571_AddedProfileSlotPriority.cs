namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProfileSlotPriority : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerProfileEntity", "ProfileSlotId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerProfileEntity", "ProfileSlotId");
        }
    }
}
