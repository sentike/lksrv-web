namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsingDoubleTutStepId2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("store.PlayerTutorialEntity", new[] { "TutorialId", "StepId" }, "store.TutorialStepEntity");
            DropIndex("store.PlayerTutorialEntity", new[] { "TutorialId", "StepId" });
            DropPrimaryKey("store.TutorialStepEntity");
            AddColumn("store.TutorialStepEntity", "StepKey", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("store.TutorialStepEntity", "StepKey");
            CreateIndex("store.PlayerTutorialEntity", "TutorialId");
            CreateIndex("store.PlayerTutorialEntity", "StepId");
            AddForeignKey("store.PlayerTutorialEntity", "TutorialId", "store.TutorialInstanceEntity", "TutorialId", cascadeDelete: true);
            AddForeignKey("store.PlayerTutorialEntity", "StepId", "store.TutorialStepEntity", "StepKey", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("store.PlayerTutorialEntity", "StepId", "store.TutorialStepEntity");
            DropForeignKey("store.PlayerTutorialEntity", "TutorialId", "store.TutorialInstanceEntity");
            DropIndex("store.PlayerTutorialEntity", new[] { "StepId" });
            DropIndex("store.PlayerTutorialEntity", new[] { "TutorialId" });
            DropPrimaryKey("store.TutorialStepEntity");
            DropColumn("store.TutorialStepEntity", "StepKey");
            AddPrimaryKey("store.TutorialStepEntity", new[] { "TutorialId", "StepId" });
            RenameColumn(table: "store.PlayerTutorialEntity", name: "StepId", newName: "TutorialId");
            AddColumn("store.PlayerTutorialEntity", "StepId", c => c.Long(nullable: false));
            CreateIndex("store.PlayerTutorialEntity", new[] { "TutorialId", "StepId" });
            AddForeignKey("store.PlayerTutorialEntity", new[] { "TutorialId", "StepId" }, "store.TutorialStepEntity", new[] { "TutorialId", "StepId" }, cascadeDelete: true);
        }
    }
}
