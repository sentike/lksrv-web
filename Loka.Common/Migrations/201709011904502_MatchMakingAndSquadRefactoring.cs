namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MatchMakingAndSquadRefactoring : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.QueueDuelEntity", "QueueId", "public.QueueEntity");
            DropForeignKey("matches.MatchEntity", "GameModeTypeId", "gamemode.GameModeEntity");
            DropIndex("public.QueueDuelEntity", new[] { "QueueId" });
            DropIndex("matches.MatchEntity", new[] { "GameModeTypeId" });
            AddColumn("public.SquadInviteEntity", "Options_GameMap", c => c.Int(nullable: false));
            AddColumn("public.SquadInviteEntity", "Options_GameMode", c => c.Int(nullable: false));
            AddColumn("public.SquadInviteEntity", "Options_Region", c => c.Short(nullable: false));
            AddColumn("public.SquadInviteEntity", "Options_RoundTimeInSeconds", c => c.Short(nullable: false));
            AddColumn("public.SquadInviteEntity", "Options_Bet_Amount", c => c.Int(nullable: false));
            AddColumn("public.SquadInviteEntity", "Options_Bet_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("public.SquadInviteEntity", "Options_WeaponType", c => c.Int(nullable: false));
            AddColumn("public.SquadInviteEntity", "Options_Difficulty", c => c.Double(nullable: false));
            AddColumn("public.SquadInviteEntity", "Options_AdditionalFlag", c => c.Int(nullable: false));
            AddColumn("public.SquadInviteEntity", "Options_NumberOfBots", c => c.Short(nullable: false));
            AddColumn("public.QueueEntity", "Options_GameMap", c => c.Int(nullable: false));
            AddColumn("public.QueueEntity", "Options_GameMode", c => c.Int(nullable: false));
            AddColumn("public.QueueEntity", "Options_Region", c => c.Short(nullable: false));
            AddColumn("public.QueueEntity", "Options_RoundTimeInSeconds", c => c.Short(nullable: false));
            AddColumn("public.QueueEntity", "Options_Bet_Amount", c => c.Int(nullable: false));
            AddColumn("public.QueueEntity", "Options_Bet_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("public.QueueEntity", "Options_WeaponType", c => c.Int(nullable: false));
            AddColumn("public.QueueEntity", "Options_Difficulty", c => c.Double(nullable: false));
            AddColumn("public.QueueEntity", "Options_AdditionalFlag", c => c.Int(nullable: false));
            AddColumn("public.QueueEntity", "Options_NumberOfBots", c => c.Short(nullable: false));
            AddColumn("matches.MatchEntity", "Options_GameMap", c => c.Int(nullable: false));
            AddColumn("matches.MatchEntity", "Options_GameMode", c => c.Int(nullable: false));
            AddColumn("matches.MatchEntity", "Options_Region", c => c.Short(nullable: false));
            AddColumn("matches.MatchEntity", "Options_RoundTimeInSeconds", c => c.Short(nullable: false));
            AddColumn("matches.MatchEntity", "Options_Bet_Amount", c => c.Int(nullable: false));
            AddColumn("matches.MatchEntity", "Options_Bet_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("matches.MatchEntity", "Options_WeaponType", c => c.Int(nullable: false));
            AddColumn("matches.MatchEntity", "Options_Difficulty", c => c.Double(nullable: false));
            AddColumn("matches.MatchEntity", "Options_AdditionalFlag", c => c.Int(nullable: false));
            AddColumn("matches.MatchEntity", "Options_NumberOfBots", c => c.Short(nullable: false));
            AlterColumn("matches.MatchMember", "Difficulty", c => c.Double(nullable: false));
            DropColumn("public.QueueEntity", "GameModeTypeId");
            DropColumn("public.QueueEntity", "RegionTypeId");
            DropColumn("public.QueueEntity", "LastAttempSearchSession");
            DropColumn("public.QueueEntity", "RowVersion");
            DropColumn("matches.MatchEntity", "GameModeTypeId");
            DropColumn("matches.MatchEntity", "AbstractMatchFlag");
            DropColumn("matches.MatchEntity", "NumberOfLives");
            DropColumn("matches.MatchEntity", "GameMapTypeId");
            DropColumn("matches.MatchEntity", "NumberOfBots");
            DropColumn("matches.MatchEntity", "ScoreLimit");
            DropColumn("matches.MatchEntity", "Difficulty");
            DropColumn("matches.MatchEntity", "RoundTimeInSeconds");
            DropTable("public.QueueDuelEntity");
        }
        
        public override void Down()
        {
            CreateTable(
                "public.QueueDuelEntity",
                c => new
                    {
                        QueueId = c.Guid(nullable: false),
                        Bet_Amount = c.Int(nullable: false),
                        Bet_IsDonate = c.Boolean(nullable: false),
                        WeaponType = c.Int(nullable: false),
                        NumberOfLives = c.Short(nullable: false),
                        GameMapTypeId = c.Int(nullable: false),
                        RowVersion = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.QueueId);
            
            AddColumn("matches.MatchEntity", "RoundTimeInSeconds", c => c.Int(nullable: false));
            AddColumn("matches.MatchEntity", "Difficulty", c => c.Short(nullable: false));
            AddColumn("matches.MatchEntity", "ScoreLimit", c => c.Short(nullable: false));
            AddColumn("matches.MatchEntity", "NumberOfBots", c => c.Short(nullable: false));
            AddColumn("matches.MatchEntity", "GameMapTypeId", c => c.Int(nullable: false));
            AddColumn("matches.MatchEntity", "NumberOfLives", c => c.Short(nullable: false));
            AddColumn("matches.MatchEntity", "AbstractMatchFlag", c => c.Int(nullable: false));
            AddColumn("matches.MatchEntity", "GameModeTypeId", c => c.Int(nullable: false));
            AddColumn("public.QueueEntity", "RowVersion", c => c.Guid(nullable: false));
            AddColumn("public.QueueEntity", "LastAttempSearchSession", c => c.DateTime());
            AddColumn("public.QueueEntity", "RegionTypeId", c => c.Short(nullable: false));
            AddColumn("public.QueueEntity", "GameModeTypeId", c => c.Int(nullable: false));
            AlterColumn("matches.MatchMember", "Difficulty", c => c.Short(nullable: false));
            DropColumn("matches.MatchEntity", "Options_NumberOfBots");
            DropColumn("matches.MatchEntity", "Options_AdditionalFlag");
            DropColumn("matches.MatchEntity", "Options_Difficulty");
            DropColumn("matches.MatchEntity", "Options_WeaponType");
            DropColumn("matches.MatchEntity", "Options_Bet_IsDonate");
            DropColumn("matches.MatchEntity", "Options_Bet_Amount");
            DropColumn("matches.MatchEntity", "Options_RoundTimeInSeconds");
            DropColumn("matches.MatchEntity", "Options_Region");
            DropColumn("matches.MatchEntity", "Options_GameMode");
            DropColumn("matches.MatchEntity", "Options_GameMap");
            DropColumn("public.QueueEntity", "Options_NumberOfBots");
            DropColumn("public.QueueEntity", "Options_AdditionalFlag");
            DropColumn("public.QueueEntity", "Options_Difficulty");
            DropColumn("public.QueueEntity", "Options_WeaponType");
            DropColumn("public.QueueEntity", "Options_Bet_IsDonate");
            DropColumn("public.QueueEntity", "Options_Bet_Amount");
            DropColumn("public.QueueEntity", "Options_RoundTimeInSeconds");
            DropColumn("public.QueueEntity", "Options_Region");
            DropColumn("public.QueueEntity", "Options_GameMode");
            DropColumn("public.QueueEntity", "Options_GameMap");
            DropColumn("public.SquadInviteEntity", "Options_NumberOfBots");
            DropColumn("public.SquadInviteEntity", "Options_AdditionalFlag");
            DropColumn("public.SquadInviteEntity", "Options_Difficulty");
            DropColumn("public.SquadInviteEntity", "Options_WeaponType");
            DropColumn("public.SquadInviteEntity", "Options_Bet_IsDonate");
            DropColumn("public.SquadInviteEntity", "Options_Bet_Amount");
            DropColumn("public.SquadInviteEntity", "Options_RoundTimeInSeconds");
            DropColumn("public.SquadInviteEntity", "Options_Region");
            DropColumn("public.SquadInviteEntity", "Options_GameMode");
            DropColumn("public.SquadInviteEntity", "Options_GameMap");
            CreateIndex("matches.MatchEntity", "GameModeTypeId");
            CreateIndex("public.QueueDuelEntity", "QueueId");
            AddForeignKey("matches.MatchEntity", "GameModeTypeId", "gamemode.GameModeEntity", "GameModeId", cascadeDelete: true);
            AddForeignKey("public.QueueDuelEntity", "QueueId", "public.QueueEntity", "QueueId", cascadeDelete: true);
        }
    }
}
