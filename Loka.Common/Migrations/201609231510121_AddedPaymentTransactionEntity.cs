namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPaymentTransactionEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("transcaction.PaymentTransactionEntity", "PaymentType", c => c.Int(nullable: false));
            AddColumn("transcaction.PaymentTransactionEntity", "Amount_Amount", c => c.Int(nullable: false));
            AddColumn("transcaction.PaymentTransactionEntity", "Amount_IsDonate", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("transcaction.PaymentTransactionEntity", "Amount_IsDonate");
            DropColumn("transcaction.PaymentTransactionEntity", "Amount_Amount");
            DropColumn("transcaction.PaymentTransactionEntity", "PaymentType");
        }
    }
}
