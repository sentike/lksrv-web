namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProfileUniqueItemSlot : DbMigration
    {
        public override void Up()
        {
            //DropIndex("players.PlayerProfileItemEntity", new[] { "ProfileId" });
            CreateIndex("players.PlayerProfileItemEntity", new[] { "ProfileId", "SlotId" }, unique: true, name: "ProfileItemSlot");
        }
        
        public override void Down()
        {
            DropIndex("players.PlayerProfileItemEntity", "ProfileItemSlot");
            //CreateIndex("players.PlayerProfileItemEntity", "ProfileId");
        }
    }
}
