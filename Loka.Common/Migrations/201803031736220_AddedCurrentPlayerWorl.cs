namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCurrentPlayerWorl : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "CurrentPlayerWorldId", c => c.Guid());
            CreateIndex("players.PlayerEntity", "CurrentPlayerWorldId");
            AddForeignKey("players.PlayerEntity", "CurrentPlayerWorldId", "building.PlayerWorlEntity", "EntityId");
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerEntity", "CurrentPlayerWorldId", "building.PlayerWorlEntity");
            DropIndex("players.PlayerEntity", new[] { "CurrentPlayerWorldId" });
            DropColumn("players.PlayerEntity", "CurrentPlayerWorldId");
        }
    }
}
