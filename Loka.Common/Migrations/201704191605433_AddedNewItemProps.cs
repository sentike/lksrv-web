namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNewItemProps : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.AbstractItemInstance", "Flags", c => c.Int(nullable: false));
            AddColumn("players.InstanceOfPlayerItem", "Count", c => c.Long(nullable: false));
            DropColumn("store.AbstractItemInstance", "IsAvalible");
            DropColumn("store.AbstractItemInstance", "IsSubscribe");
        }
        
        public override void Down()
        {
            AddColumn("store.AbstractItemInstance", "IsSubscribe", c => c.Boolean(nullable: false));
            AddColumn("store.AbstractItemInstance", "IsAvalible", c => c.Boolean(nullable: false));
            DropColumn("players.InstanceOfPlayerItem", "Count");
            DropColumn("store.AbstractItemInstance", "Flags");
        }
    }
}
