namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeValueChecks : DbMigration
    {
        public override void Up()
        {
            AlterColumn("building.WorldBuildingPropertyEntity", "Value", c => c.String(maxLength: 64));
        }
        
        public override void Down()
        {
            AlterColumn("building.WorldBuildingPropertyEntity", "Value", c => c.String(nullable: false, maxLength: 64));
        }
    }
}
