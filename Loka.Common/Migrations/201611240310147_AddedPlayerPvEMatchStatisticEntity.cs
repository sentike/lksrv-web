namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerPvEMatchStatisticEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "players.PlayerPvEMatchStatisticEntity",
                c => new
                    {
                        StatisticEntityId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        GameModeTypeId = c.Int(nullable: false),
                        Level = c.Short(nullable: false),
                        Score = c.Long(nullable: false),
                        PlayDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.StatisticEntityId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => new { t.GameModeTypeId, t.PlayerId, t.Level }, unique: true, name: "GameModeTypeId_PlayerId");
            
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerPvEMatchStatisticEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("players.PlayerPvEMatchStatisticEntity", "GameModeTypeId_PlayerId");
            DropTable("players.PlayerPvEMatchStatisticEntity");
        }
    }
}
