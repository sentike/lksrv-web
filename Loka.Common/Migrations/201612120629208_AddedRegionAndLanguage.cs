namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRegionAndLanguage : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "ClusterRegion", c => c.Short(nullable: false));
            AddColumn("players.PlayerEntity", "Language", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "Language");
            DropColumn("players.PlayerEntity", "ClusterRegion");
        }
    }
}
