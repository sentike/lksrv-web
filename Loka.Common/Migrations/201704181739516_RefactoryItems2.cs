namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoryItems2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("building.WorldBuildingItemEntity", "CategoryTypeId", c => c.Int(nullable: false, defaultValue:10, defaultValueSql:"10"));
            AddColumn("building.WorldBuildingItemEntity", "LastStorageUpdateDate", c => c.DateTime());
            AlterColumn("building.WorldBuildingItemEntity", "ModelId", c => c.Short(nullable: false));
            AlterColumn("building.WorldBuildingItemEntity", "Storage", c => c.Long());
            CreateIndex("building.WorldBuildingItemEntity", new[] { "ModelId", "CategoryTypeId" });
            AddForeignKey("building.WorldBuildingItemEntity", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" }, cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("building.WorldBuildingItemEntity", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropIndex("building.WorldBuildingItemEntity", new[] { "ModelId", "CategoryTypeId" });
            AlterColumn("building.WorldBuildingItemEntity", "Storage", c => c.Long(nullable: false));
            AlterColumn("building.WorldBuildingItemEntity", "ModelId", c => c.Int(nullable: false));
            DropColumn("building.WorldBuildingItemEntity", "LastStorageUpdateDate");
            DropColumn("building.WorldBuildingItemEntity", "CategoryTypeId");
        }
    }
}
