namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoryAchievents : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("achievements.ExperienceAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropForeignKey("achievements.ReputationAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropForeignKey("achievements.ReputationAchievementContainer", "FractionTypeId", "store.FractionEntity");
            DropForeignKey("achievements.RestrictedAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropForeignKey("achievements.SummableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropIndex("achievements.ExperienceAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            DropIndex("achievements.ReputationAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            DropIndex("achievements.ReputationAchievementContainer", new[] { "FractionTypeId" });
            DropIndex("achievements.RestrictedAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            DropIndex("achievements.SummableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            CreateTable(
                "achievements.SettableAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId })
                .ForeignKey("achievements.AchievementContainer", t => new { t.AchievementTypeId, t.CategoryId }, true)
                .Index(t => new { t.AchievementTypeId, t.CategoryId });
            
            CreateTable(
                "achievements.IncrementableAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId })
                .ForeignKey("achievements.AchievementContainer", t => new { t.AchievementTypeId, t.CategoryId }, true)
                .Index(t => new { t.AchievementTypeId, t.CategoryId });
            
            DropTable("achievements.ExperienceAchievementContainer");
            DropTable("achievements.ReputationAchievementContainer");
            DropTable("achievements.RestrictedAchievementContainer");
            DropTable("achievements.SummableAchievementContainer");
        }
        
        public override void Down()
        {
            CreateTable(
                "achievements.SummableAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId });
            
            CreateTable(
                "achievements.RestrictedAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId });
            
            CreateTable(
                "achievements.ReputationAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        FractionTypeId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId });
            
            CreateTable(
                "achievements.ExperienceAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId });
            
            DropForeignKey("achievements.IncrementableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropForeignKey("achievements.SettableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropIndex("achievements.IncrementableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            DropIndex("achievements.SettableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            DropTable("achievements.IncrementableAchievementContainer");
            DropTable("achievements.SettableAchievementContainer");
            CreateIndex("achievements.SummableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            CreateIndex("achievements.RestrictedAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            CreateIndex("achievements.ReputationAchievementContainer", "FractionTypeId");
            CreateIndex("achievements.ReputationAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            CreateIndex("achievements.ExperienceAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            AddForeignKey("achievements.SummableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            AddForeignKey("achievements.RestrictedAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            AddForeignKey("achievements.ReputationAchievementContainer", "FractionTypeId", "store.FractionEntity", "FractionId", cascadeDelete: true);
            AddForeignKey("achievements.ReputationAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            AddForeignKey("achievements.ExperienceAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
        }
    }
}
