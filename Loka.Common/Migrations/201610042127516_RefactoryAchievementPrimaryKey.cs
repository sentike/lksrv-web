namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoryAchievementPrimaryKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("players.PlayerAchievement", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropForeignKey("achievements.AchievementInstance", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropIndex("players.PlayerAchievement", new[] { "AchievementTypeId", "CategoryId" });
            DropIndex("achievements.AchievementInstance", new[] { "AchievementTypeId", "CategoryId" });
            DropPrimaryKey("achievements.AchievementContainer");
            AddPrimaryKey("achievements.AchievementContainer", "AchievementTypeId");
            CreateIndex("players.PlayerAchievement", "AchievementTypeId");
            CreateIndex("achievements.AchievementInstance", "AchievementTypeId");
            AddForeignKey("players.PlayerAchievement", "AchievementTypeId", "achievements.AchievementContainer", "AchievementTypeId", cascadeDelete: true);
            AddForeignKey("achievements.AchievementInstance", "AchievementTypeId", "achievements.AchievementContainer", "AchievementTypeId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("achievements.AchievementInstance", "AchievementTypeId", "achievements.AchievementContainer");
            DropForeignKey("players.PlayerAchievement", "AchievementTypeId", "achievements.AchievementContainer");
            DropIndex("achievements.AchievementInstance", new[] { "AchievementTypeId" });
            DropIndex("players.PlayerAchievement", new[] { "AchievementTypeId" });
            DropPrimaryKey("achievements.AchievementContainer");
            AddPrimaryKey("achievements.AchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            CreateIndex("achievements.AchievementInstance", new[] { "AchievementTypeId", "CategoryId" });
            CreateIndex("players.PlayerAchievement", new[] { "AchievementTypeId", "CategoryId" });
            AddForeignKey("achievements.AchievementInstance", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, cascadeDelete: true);
            AddForeignKey("players.PlayerAchievement", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, cascadeDelete: true);
        }
    }
}
