namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDonateForMatchMemberAward : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchMember", "Award_Donate", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("matches.MatchMember", "Award_Donate");
        }
    }
}
