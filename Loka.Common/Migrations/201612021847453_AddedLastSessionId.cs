namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedLastSessionId : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "LastSessionId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "LastSessionId");
        }
    }
}
