namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedVRSProWebSiteFields1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("vrs.UserLogins", "RegistrationDate", c => c.DateTime(nullable: false));
            AddColumn("vrs.UserLogins", "AllowIdentity", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("vrs.UserLogins", "AllowIdentity");
            DropColumn("vrs.UserLogins", "RegistrationDate");
        }
    }
}
