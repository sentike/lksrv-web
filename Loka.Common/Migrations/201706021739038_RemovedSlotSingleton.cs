namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedSlotSingleton : DbMigration
    {
        public override void Up()
        {
            DropIndex("store.CharacterProfileTemplate", "ProfileItemSlot");
        }
        
        public override void Down()
        {
            CreateIndex("store.CharacterProfileTemplate", new[] { "CharacterModelId", "ProfileSlot" }, unique: true, name: "ProfileItemSlot");
        }
    }
}
