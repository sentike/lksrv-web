namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedCascadeDeleteForMatchAndCluster : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("matches.MatchEntity", "MachineAddress", "public.ClusterInstanceEntity");
            AddForeignKey("matches.MatchEntity", "MachineAddress", "public.ClusterInstanceEntity", "MachineAddress");
        }
        
        public override void Down()
        {
            DropForeignKey("matches.MatchEntity", "MachineAddress", "public.ClusterInstanceEntity");
            AddForeignKey("matches.MatchEntity", "MachineAddress", "public.ClusterInstanceEntity", "MachineAddress", cascadeDelete: true);
        }
    }
}
