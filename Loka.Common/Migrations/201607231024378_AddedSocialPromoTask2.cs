namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSocialPromoTask2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerSocialTaskEntity", "ConfirmDate", c => c.DateTime(nullable: false, defaultValueSql:"Now()"));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerSocialTaskEntity", "ConfirmDate");
        }
    }
}
