namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BuildingGameMode : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "gamemode.BuildingGameMode",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.GameModeEntity", t => t.GameModeId, true)
                .Index(t => t.GameModeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("gamemode.BuildingGameMode", "GameModeId", "gamemode.GameModeEntity");
            DropIndex("gamemode.BuildingGameMode", new[] { "GameModeId" });
            DropTable("gamemode.BuildingGameMode");
        }
    }
}
