namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCharacterProfileTemplate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "store.CharacterProfileTemplate",
                c => new
                    {
                        TemplateId = c.Int(nullable: false, identity: true),
                        CharacterModelId = c.Short(nullable: false),
                        CharacterCategoryId = c.Int(nullable: false),
                        ItemModelId = c.Short(nullable: false),
                        ItemCategoryId = c.Int(nullable: false),
                        ProfileSlot = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TemplateId)
                .ForeignKey("store.AbstractItemInstance", t => new { t.CharacterModelId, t.CharacterCategoryId }, cascadeDelete: true)
                .ForeignKey("store.AbstractItemInstance", t => new { t.ItemModelId, t.ItemCategoryId }, cascadeDelete: true)
                .Index(t => new { t.CharacterModelId, t.ProfileSlot }, unique: true, name: "ProfileItemSlot")
                .Index(t => new { t.CharacterModelId, t.CharacterCategoryId })
                .Index(t => new { t.ItemModelId, t.ItemCategoryId });
            
        }
        
        public override void Down()
        {
            DropForeignKey("store.CharacterProfileTemplate", new[] { "ItemModelId", "ItemCategoryId" }, "store.AbstractItemInstance");
            DropForeignKey("store.CharacterProfileTemplate", new[] { "CharacterModelId", "CharacterCategoryId" }, "store.AbstractItemInstance");
            DropIndex("store.CharacterProfileTemplate", new[] { "ItemModelId", "ItemCategoryId" });
            DropIndex("store.CharacterProfileTemplate", new[] { "CharacterModelId", "CharacterCategoryId" });
            DropIndex("store.CharacterProfileTemplate", "ProfileItemSlot");
            DropTable("store.CharacterProfileTemplate");
        }
    }
}
