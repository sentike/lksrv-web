namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerCashLimit : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Cash_Limit", c => c.Long(nullable: false));
            AddColumn("league.LeagueEntity", "Cash_Limit", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("league.LeagueEntity", "Cash_Limit");
            DropColumn("players.PlayerEntity", "Cash_Limit");
        }
    }
}
