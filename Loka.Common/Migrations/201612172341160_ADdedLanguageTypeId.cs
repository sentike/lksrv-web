namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ADdedLanguageTypeId : DbMigration
    {
        public override void Up()
        {
            DropIndex("improvement.ImprovementLocalizationEntity", "LanguageId_ImprovementId");
            AlterColumn("players.PlayerEntity", "Language", c => c.Byte(nullable: false));
            AlterColumn("improvement.ImprovementLocalizationEntity", "LanguageId", c => c.Byte(nullable: false));
            AlterColumn("public.GlobalChatMessage", "LanguageId", c => c.Byte(nullable: false));
            CreateIndex("improvement.ImprovementLocalizationEntity", new[] { "LanguageId", "ImprovementId" }, unique: true, name: "LanguageId_ImprovementId");
        }
        
        public override void Down()
        {
            DropIndex("improvement.ImprovementLocalizationEntity", "LanguageId_ImprovementId");
            AlterColumn("public.GlobalChatMessage", "LanguageId", c => c.Int(nullable: false));
            AlterColumn("improvement.ImprovementLocalizationEntity", "LanguageId", c => c.Int(nullable: false));
            AlterColumn("players.PlayerEntity", "Language", c => c.Int(nullable: false));
            CreateIndex("improvement.ImprovementLocalizationEntity", new[] { "LanguageId", "ImprovementId" }, unique: true, name: "LanguageId_ImprovementId");
        }
    }
}
