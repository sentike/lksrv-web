namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPremiumServiceAndTimeItems : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "players.PlayerUsingServiceEntity",
                c => new
                    {
                        ServiceInstanceId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ServiceInstanceId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("players.PlayerServiceEntity", t => t.ServiceInstanceId, true)
                .Index(t => t.ServiceInstanceId)
                .Index(t => t.PlayerId);

            CreateTable(
                "players.PlayerServiceEntity",
                c => new
                {
                    ItemId = c.Guid(nullable: false),
                    SubCategoryTypeId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.ItemId, true)
                .Index(t => t.ItemId);
            
            CreateTable(
                "store.ServiceInstanceEntity",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                        SubCategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId }, true)
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
            AddColumn("store.AbstractItemInstance", "Duration", c => c.Long(nullable: false));
            AddColumn("players.InstanceOfPlayerItem", "Duration", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            CreateTable(
                "players.InstanceOfPlayerGrenade",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerKit",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerArmour",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerWeapon",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerCharacter",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            DropForeignKey("store.ServiceInstanceEntity", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("players.PlayerServiceEntity", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerGrenade", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerKit", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerArmour", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerWeapon", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerCharacter", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.PlayerUsingServiceEntity", "ServiceInstanceId", "players.PlayerServiceEntity");
            DropForeignKey("players.PlayerUsingServiceEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("store.ServiceInstanceEntity", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("players.PlayerServiceEntity", "PlayerServiceEntity_SubCategory_Player");
            DropIndex("players.PlayerServiceEntity", new[] { "ItemId" });
            DropIndex("players.PlayerUsingServiceEntity", new[] { "PlayerId" });
            DropIndex("players.PlayerUsingServiceEntity", new[] { "ServiceInstanceId" });
            DropIndex("players.InstanceOfPlayerItem", "PlayerServiceEntity_SubCategory_Player");
            DropColumn("players.InstanceOfPlayerItem", "Duration");
            DropColumn("store.AbstractItemInstance", "Duration");
            DropTable("store.ServiceInstanceEntity");
            DropTable("players.PlayerServiceEntity");
            DropTable("players.InstanceOfPlayerGrenade");
            DropTable("players.InstanceOfPlayerKit");
            DropTable("players.InstanceOfPlayerArmour");
            DropTable("players.InstanceOfPlayerWeapon");
            DropTable("players.InstanceOfPlayerCharacter");
            DropTable("players.PlayerUsingServiceEntity");
            AddForeignKey("players.InstanceOfPlayerGrenade", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
            AddForeignKey("players.InstanceOfPlayerKit", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
            AddForeignKey("players.InstanceOfPlayerArmour", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
            AddForeignKey("players.InstanceOfPlayerWeapon", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
            AddForeignKey("players.InstanceOfPlayerCharacter", "ItemId", "players.InstanceOfPlayerItem", "ItemId");
        }
    }
}
