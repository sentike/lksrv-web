namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMoreGameModes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "gamemode.CaptureTheFlagGameMode",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.TeamGameModeEntity", t => t.GameModeId, true)
                .Index(t => t.GameModeId);
            
            CreateTable(
                "gamemode.DefenseTheFlagGameMode",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.TeamGameModeEntity", t => t.GameModeId, true)
                .Index(t => t.GameModeId);
            
            CreateTable(
                "gamemode.DominationGameMode",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.TeamGameModeEntity", t => t.GameModeId, true)
                .Index(t => t.GameModeId);
            
            CreateTable(
                "gamemode.PvEDefenseGameMode",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.GameModeEntity", t => t.GameModeId, true)
                .Index(t => t.GameModeId);
            
            CreateTable(
                "gamemode.PvESurviveGameMode",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.GameModeEntity", t => t.GameModeId, true)
                .Index(t => t.GameModeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("gamemode.PvESurviveGameMode", "GameModeId", "gamemode.GameModeEntity");
            DropForeignKey("gamemode.PvEDefenseGameMode", "GameModeId", "gamemode.GameModeEntity");
            DropForeignKey("gamemode.DominationGameMode", "GameModeId", "gamemode.TeamGameModeEntity");
            DropForeignKey("gamemode.DefenseTheFlagGameMode", "GameModeId", "gamemode.TeamGameModeEntity");
            DropForeignKey("gamemode.CaptureTheFlagGameMode", "GameModeId", "gamemode.TeamGameModeEntity");
            DropIndex("gamemode.PvESurviveGameMode", new[] { "GameModeId" });
            DropIndex("gamemode.PvEDefenseGameMode", new[] { "GameModeId" });
            DropIndex("gamemode.DominationGameMode", new[] { "GameModeId" });
            DropIndex("gamemode.DefenseTheFlagGameMode", new[] { "GameModeId" });
            DropIndex("gamemode.CaptureTheFlagGameMode", new[] { "GameModeId" });
            DropTable("gamemode.PvESurviveGameMode");
            DropTable("gamemode.PvEDefenseGameMode");
            DropTable("gamemode.DominationGameMode");
            DropTable("gamemode.DefenseTheFlagGameMode");
            DropTable("gamemode.CaptureTheFlagGameMode");
        }
    }
}
