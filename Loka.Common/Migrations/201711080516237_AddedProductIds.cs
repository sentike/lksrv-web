namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProductIds : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.AddonItemContainer", "ProductId", c => c.Int(nullable: false));
            AddColumn("store.AddonItemContainer", "TargetProductId", c => c.Int(nullable: false));
            AddColumn("store.CharacterProfileTemplate", "CharacterId", c => c.Int(nullable: false));
            AddColumn("store.CharacterProfileTemplate", "ProductId", c => c.Int(nullable: false));
            AddColumn("store.TutorialStepEntity", "ProductId", c => c.Int());
            AddColumn("players.InstanceOfTargetItem", "ProductId", c => c.Int(nullable: false));
            AddColumn("promo.PromoItemEntity", "ProductId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("promo.PromoItemEntity", "ProductId");
            DropColumn("players.InstanceOfTargetItem", "ProductId");
            DropColumn("store.TutorialStepEntity", "ProductId");
            DropColumn("store.CharacterProfileTemplate", "ProductId");
            DropColumn("store.CharacterProfileTemplate", "CharacterId");
            DropColumn("store.AddonItemContainer", "TargetProductId");
            DropColumn("store.AddonItemContainer", "ProductId");
        }
    }
}
