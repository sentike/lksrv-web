namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AchievementCleanUp : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("achievements.SettableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropForeignKey("achievements.IncrementableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropIndex("achievements.SettableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            DropIndex("achievements.IncrementableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            
            CreateIndex("players.PlayerReputationEntity", "FractionId");
            AddForeignKey("players.PlayerReputationEntity", "FractionId", "store.FractionEntity", "FractionId", cascadeDelete: true);
            DropTable("achievements.SettableAchievementContainer");
            DropTable("achievements.IncrementableAchievementContainer");
        }
        
        public override void Down()
        {
            CreateTable(
                "achievements.IncrementableAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId });
            
            CreateTable(
                "achievements.SettableAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId });
            
            CreateTable(
                "achievements.AchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId });
            
            DropForeignKey("players.PlayerReputationEntity", "FractionId", "store.FractionEntity");
            DropIndex("players.PlayerReputationEntity", new[] { "FractionId" });
            CreateIndex("achievements.IncrementableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            CreateIndex("achievements.SettableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            AddForeignKey("achievements.IncrementableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            AddForeignKey("achievements.SettableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
        }
    }
}
