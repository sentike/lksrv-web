namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBasicItemEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "store.BasicItemEntity",
                c => new
                    {
                        EntityId = c.Int(nullable: false, identity: true),
                        DisplayPosition = c.Int(nullable: false),
                        DisplayName = c.String(maxLength: 128),
                        AdminComment = c.String(maxLength: 128),
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                        Flags = c.Int(nullable: false),
                        FractionId = c.Short(),
                        Level = c.Short(nullable: false),
                        Duration = c.Time(precision: 6),
                        AmountDefault = c.Long(nullable: false),
                        AmountInStack = c.Long(nullable: false),
                        AmountMinimum = c.Long(nullable: false),
                        AmountMaximum = c.Long(),
                        Cost_Amount = c.Int(nullable: false),
                        Cost_Currency = c.Short(nullable: false),
                        SubscribeCost_Amount = c.Int(nullable: false),
                        SubscribeCost_Currency = c.Short(nullable: false),
                        Performance = c.Long(),
                        Storage = c.Long(),
                    })
                .PrimaryKey(t => t.EntityId)
                .Index(t => new { t.ModelId, t.CategoryTypeId }, unique: true, name: "ModelId_CategoryTypeId");
            
        }
        
        public override void Down()
        {
            DropIndex("store.BasicItemEntity", "ModelId_CategoryTypeId");
            DropTable("store.BasicItemEntity");
        }
    }
}
