namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerSessionAttach : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchMember", "SessionToken", c => c.Guid(nullable: false));
            AddColumn("public.GameSessionEntity", "SessionAttach", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("public.GameSessionEntity", "SessionAttach");
            DropColumn("matches.MatchMember", "SessionToken");
        }
    }
}
