namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoryDuels : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.QueueDuelEntity", "QueueId", "public.QueueEntity");
            AddColumn("public.QueueDuelEntity", "GameMapTypeId", c => c.Int(nullable: false));
            AddColumn("public.SquadDuelInvite", "GameMapTypeId", c => c.Int(nullable: false));
            AddForeignKey("public.QueueDuelEntity", "QueueId", "public.QueueEntity", "QueueId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("public.QueueDuelEntity", "QueueId", "public.QueueEntity");
            DropColumn("public.SquadDuelInvite", "GameMapTypeId");
            DropColumn("public.QueueDuelEntity", "GameMapTypeId");
            AddForeignKey("public.QueueDuelEntity", "QueueId", "public.QueueEntity", "QueueId");
        }
    }
}
