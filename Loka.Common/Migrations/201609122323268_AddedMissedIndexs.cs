namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMissedIndexs : DbMigration
    {
        public override void Up()
        {
            //====================================================
            AddPrimaryKey("public.QueueEntity", "QueueId");

            CreateIndex("public.QueueEntity", "GameModeTypeId");
            CreateIndex("public.QueueEntity", "RegionTypeId");


            //====================================================
            CreateIndex("public.SquadInviteEntity", "Type");
         
            
            //====================================================
            CreateIndex("players.PlayerEntity", "PlayerName", true);
            CreateIndex("players.PlayerEntity", "Experience_Level");
            CreateIndex("players.PlayerEntity", "Experience_WeekExperience");
            CreateIndex("players.PlayerEntity", "Statistic_Kills");
            CreateIndex("players.PlayerEntity", "Statistic_Deads");

            //====================================================
            CreateIndex("matches.MatchEntity", "GameMapTypeId");
            CreateIndex("matches.MatchEntity", "Created");
            CreateIndex("matches.MatchEntity", "Started");
            CreateIndex("matches.MatchEntity", "State");
        }

        public override void Down()
        {
        }
    }
}
