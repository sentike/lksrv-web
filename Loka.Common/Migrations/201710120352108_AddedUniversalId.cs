namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUniversalId : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.SquadInviteEntity", "Options_UniversalId", c => c.Guid());
            AddColumn("public.QueueEntity", "Options_UniversalId", c => c.Guid());
            AddColumn("matches.MatchEntity", "Options_UniversalId", c => c.Guid());
            DropColumn("matches.MatchEntity", "UniversalId");
        }
        
        public override void Down()
        {
            AddColumn("matches.MatchEntity", "UniversalId", c => c.Guid());
            DropColumn("matches.MatchEntity", "Options_UniversalId");
            DropColumn("public.QueueEntity", "Options_UniversalId");
            DropColumn("public.SquadInviteEntity", "Options_UniversalId");
        }
    }
}
