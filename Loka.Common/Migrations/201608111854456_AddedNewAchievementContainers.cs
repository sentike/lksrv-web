namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNewAchievementContainers : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("players.PlayerAchievement", "AchievementTypeId", "achievements.AchievementContainer");
            DropForeignKey("achievements.AchievementInstance", "AchievementTypeId", "achievements.AchievementContainer");
            DropIndex("players.PlayerAchievement", new[] { "AchievementTypeId" });
            DropIndex("achievements.AchievementInstance", new[] { "AchievementTypeId" });

            DropPrimaryKey("achievements.AchievementContainer", "PK_achievements.AchievementContainer");
            AddColumn("achievements.AchievementContainer", "CategoryId", c => c.Int(nullable: false));
            AddPrimaryKey("achievements.AchievementContainer", new []{ "AchievementTypeId", "CategoryId" });
                
            CreateTable(
                "achievements.ExperienceAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId })
                .ForeignKey("achievements.AchievementContainer", t => new { t.AchievementTypeId, t.CategoryId }, true)
                .Index(t => new { t.AchievementTypeId, t.CategoryId });
            
            CreateTable(
                "achievements.ReputationAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        FractionTypeId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId })
                .ForeignKey("achievements.AchievementContainer", t => new { t.AchievementTypeId, t.CategoryId }, true)
                .ForeignKey("store.FractionEntity", t => t.FractionTypeId, cascadeDelete: true)
                .Index(t => new { t.AchievementTypeId, t.CategoryId })
                .Index(t => t.FractionTypeId);
            
            CreateTable(
                "achievements.RestrictedAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId })
                .ForeignKey("achievements.AchievementContainer", t => new { t.AchievementTypeId, t.CategoryId }, true)
                .Index(t => new { t.AchievementTypeId, t.CategoryId });
            
            CreateTable(
                "achievements.SummableAchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AchievementTypeId, t.CategoryId })
                .ForeignKey("achievements.AchievementContainer", t => new { t.AchievementTypeId, t.CategoryId }, true)
                .Index(t => new { t.AchievementTypeId, t.CategoryId });
            
            AddColumn("players.PlayerAchievement", "CategoryId", c => c.Int(nullable: false));
            AddColumn("achievements.AchievementInstance", "CategoryId", c => c.Int(nullable: false));
            CreateIndex("players.PlayerAchievement", new[] { "AchievementTypeId", "CategoryId" });
            CreateIndex("achievements.AchievementInstance", new[] { "AchievementTypeId", "CategoryId" });
            AddForeignKey("players.PlayerAchievement", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, cascadeDelete: true);
            AddForeignKey("achievements.AchievementInstance", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, cascadeDelete: true);
        }
        
        public override void Down()
        {
            CreateTable(
                "achievements.AchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AchievementTypeId);
            
            DropForeignKey("achievements.AchievementInstance", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropForeignKey("players.PlayerAchievement", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropForeignKey("achievements.SummableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropForeignKey("achievements.RestrictedAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropForeignKey("achievements.ReputationAchievementContainer", "FractionTypeId", "store.FractionEntity");
            DropForeignKey("achievements.ReputationAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropForeignKey("achievements.ExperienceAchievementContainer", new[] { "AchievementTypeId", "CategoryId" }, "achievements.AchievementContainer");
            DropIndex("achievements.SummableAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            DropIndex("achievements.RestrictedAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            DropIndex("achievements.ReputationAchievementContainer", new[] { "FractionTypeId" });
            DropIndex("achievements.ReputationAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            DropIndex("achievements.ExperienceAchievementContainer", new[] { "AchievementTypeId", "CategoryId" });
            DropIndex("achievements.AchievementInstance", new[] { "AchievementTypeId", "CategoryId" });
            DropIndex("players.PlayerAchievement", new[] { "AchievementTypeId", "CategoryId" });
            DropColumn("achievements.AchievementInstance", "CategoryId");
            DropColumn("players.PlayerAchievement", "CategoryId");
            DropTable("achievements.SummableAchievementContainer");
            DropTable("achievements.RestrictedAchievementContainer");
            DropTable("achievements.ReputationAchievementContainer");
            DropTable("achievements.ExperienceAchievementContainer");
            DropTable("achievements.AchievementContainer");
            CreateIndex("achievements.AchievementInstance", "AchievementTypeId");
            CreateIndex("players.PlayerAchievement", "AchievementTypeId");
            AddForeignKey("achievements.AchievementInstance", "AchievementTypeId", "achievements.AchievementContainer", "AchievementTypeId", cascadeDelete: true);
            AddForeignKey("players.PlayerAchievement", "AchievementTypeId", "achievements.AchievementContainer", "AchievementTypeId", cascadeDelete: true);
        }
    }
}
