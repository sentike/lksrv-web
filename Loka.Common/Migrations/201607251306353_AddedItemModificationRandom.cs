namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedItemModificationRandom : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.GameSessionEntity", "Modification_ItemId", c => c.Guid());
            AddColumn("public.GameSessionEntity", "Modification_Modification1_type", c => c.Short(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification1_value", c => c.Double(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification2_type", c => c.Short(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification2_value", c => c.Double(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification3_type", c => c.Short(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification3_value", c => c.Double(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification4_type", c => c.Short(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification4_value", c => c.Double(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification5_type", c => c.Short(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification5_value", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("public.GameSessionEntity", "Modification_Modification5_value");
            DropColumn("public.GameSessionEntity", "Modification_Modification5_type");
            DropColumn("public.GameSessionEntity", "Modification_Modification4_value");
            DropColumn("public.GameSessionEntity", "Modification_Modification4_type");
            DropColumn("public.GameSessionEntity", "Modification_Modification3_value");
            DropColumn("public.GameSessionEntity", "Modification_Modification3_type");
            DropColumn("public.GameSessionEntity", "Modification_Modification2_value");
            DropColumn("public.GameSessionEntity", "Modification_Modification2_type");
            DropColumn("public.GameSessionEntity", "Modification_Modification1_value");
            DropColumn("public.GameSessionEntity", "Modification_Modification1_type");
            DropColumn("public.GameSessionEntity", "Modification_ItemId");
        }
    }
}
