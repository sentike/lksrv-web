namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSquadDuelInvite : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.SquadDuelInvite",
                c => new
                    {
                        InviteId = c.Guid(nullable: false),
                        Bet_Amount = c.Int(nullable: false),
                        Bet_IsDonate = c.Boolean(nullable: false),
                        WeaponType = c.Int(nullable: false),
                        NumberOfLives = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InviteId)
                .ForeignKey("public.SquadInviteEntity", t => t.InviteId, true)
                .Index(t => t.InviteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("public.SquadDuelInvite", "InviteId", "public.SquadInviteEntity");
            DropIndex("public.SquadDuelInvite", new[] { "InviteId" });
            DropTable("public.SquadDuelInvite");
        }
    }
}
