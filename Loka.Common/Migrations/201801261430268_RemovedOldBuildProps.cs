namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedOldBuildProps : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("building.WorldBuildingAttachEntity", "TargetId", "building.WorldBuildingItemEntity");
            DropForeignKey("building.WorldBuildingAttachEntity", "ItemId", "building.WorldBuildingItemEntity");
            DropIndex("building.WorldBuildingAttachEntity", new[] { "ItemId" });
            DropIndex("building.WorldBuildingAttachEntity", new[] { "TargetId" });
            DropColumn("store.AbstractItemInstance", "Performance");
            DropColumn("store.AbstractItemInstance", "Storage");
            DropColumn("building.WorldBuildingItemEntity", "Health");
            DropColumn("building.WorldBuildingItemEntity", "Storage");
            DropColumn("building.WorldBuildingItemEntity", "LastStorageUpdateDate");
            DropTable("building.WorldBuildingAttachEntity");
        }
        
        public override void Down()
        {
            CreateTable(
                "building.WorldBuildingAttachEntity",
                c => new
                    {
                        AttachId = c.Guid(nullable: false, identity: true),
                        ItemId = c.Guid(nullable: false),
                        TargetId = c.Guid(nullable: false),
                        AttachFrom = c.Short(nullable: false),
                        AttachTo = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.AttachId);
            
            AddColumn("building.WorldBuildingItemEntity", "LastStorageUpdateDate", c => c.DateTime());
            AddColumn("building.WorldBuildingItemEntity", "Storage", c => c.Long(nullable: false));
            AddColumn("building.WorldBuildingItemEntity", "Health", c => c.Long(nullable: false));
            AddColumn("store.AbstractItemInstance", "Storage", c => c.Long(nullable: false));
            AddColumn("store.AbstractItemInstance", "Performance", c => c.Long(nullable: false));
            CreateIndex("building.WorldBuildingAttachEntity", "TargetId");
            CreateIndex("building.WorldBuildingAttachEntity", "ItemId");
            AddForeignKey("building.WorldBuildingAttachEntity", "ItemId", "building.WorldBuildingItemEntity", "ItemId", cascadeDelete: true);
            AddForeignKey("building.WorldBuildingAttachEntity", "TargetId", "building.WorldBuildingItemEntity", "ItemId", cascadeDelete: true);
        }
    }
}
