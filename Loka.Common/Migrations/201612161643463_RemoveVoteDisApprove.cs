namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveVoteDisApprove : DbMigration
    {
        public override void Up()
        {
            DropColumn("improvement.ImprovementVoteEntity", "IsApprove");
        }
        
        public override void Down()
        {
            AddColumn("improvement.ImprovementVoteEntity", "IsApprove", c => c.Boolean(nullable: false));
        }
    }
}
