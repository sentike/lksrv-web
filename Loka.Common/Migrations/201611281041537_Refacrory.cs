namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Refacrory : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerUsingServiceEntity", "ExpirationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerUsingServiceEntity", "ExpirationDate");
        }
    }
}
