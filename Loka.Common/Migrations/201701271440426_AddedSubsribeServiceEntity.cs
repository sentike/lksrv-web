namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSubsribeServiceEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "store.SubsribeServiceEntity",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.ServiceInstanceEntity", t => new { t.ModelId, t.CategoryTypeId }, true)
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
            AddColumn("players.PlayerEntity", "SubscribeEndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropForeignKey("store.SubsribeServiceEntity", new[] { "ModelId", "CategoryTypeId" }, "store.ServiceInstanceEntity");
            DropIndex("store.SubsribeServiceEntity", new[] { "ModelId", "CategoryTypeId" });
            DropColumn("players.PlayerEntity", "SubscribeEndDate");
            DropTable("store.SubsribeServiceEntity");
        }
    }
}
