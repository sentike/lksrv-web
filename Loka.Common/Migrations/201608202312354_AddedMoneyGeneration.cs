namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMoneyGeneration : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.GameSessionEntity", "LastMoneyGenerate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("public.GameSessionEntity", "LastMoneyGenerate");
        }
    }
}
