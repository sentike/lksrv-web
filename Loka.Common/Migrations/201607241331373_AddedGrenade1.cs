namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGrenade1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "players.InstanceOfPlayerGrenade",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.ItemId)
                .Index(t => t.ItemId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("players.InstanceOfPlayerGrenade", "ItemId", "players.InstanceOfPlayerItem");
            DropIndex("players.InstanceOfPlayerGrenade", new[] { "ItemId" });
            DropTable("players.InstanceOfPlayerGrenade");
        }
    }
}
