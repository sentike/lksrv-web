namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedItemProps : DbMigration
    {
        public override void Up()
        {
            DropColumn("players.InstanceOfPlayerItem", "Amount");
        }
        
        public override void Down()
        {
            AddColumn("players.InstanceOfPlayerItem", "Amount", c => c.Long(nullable: false));
        }
    }
}
