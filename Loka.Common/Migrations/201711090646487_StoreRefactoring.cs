namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StoreRefactoring : DbMigration
    {
        public override void Up()
        {
            DropIndex("store.BasicItemEntity", "ModelId_CategoryTypeId");
            DropIndex("transaction.MatchTransactionEntity", new[] { "CreatedDate" });
            DropIndex("transaction.MatchTransactionEntity", new[] { "ProductId" });
            DropIndex("transaction.OrderTransactionEntity", new[] { "CreatedDate" });
            DropIndex("transaction.OrderTransactionEntity", new[] { "ProductId" });
            DropIndex("transaction.StoreTransactionEntity", new[] { "CreatedDate" });
            DropIndex("transaction.StoreTransactionEntity", new[] { "ProductId" });
            AddColumn("transaction.MatchTransactionEntity", "ModelId", c => c.Short(nullable: false));
            AddColumn("transaction.MatchTransactionEntity", "CategoryTypeId", c => c.Int(nullable: false));
            AddColumn("transaction.OrderTransactionEntity", "ModelId", c => c.Short(nullable: false));
            AddColumn("transaction.OrderTransactionEntity", "CategoryTypeId", c => c.Int(nullable: false));
            AddColumn("transaction.StoreTransactionEntity", "ModelId", c => c.Short(nullable: false));
            AddColumn("transaction.StoreTransactionEntity", "CategoryTypeId", c => c.Int(nullable: false));
            AddColumn("store.OrderProductDeliveryEntity", "CategoryTypeId", c => c.Int(nullable: false));
            DropColumn("store.AbstractItemInstance", "ProductId");
            DropColumn("store.AddonItemContainer", "ProductId");
            DropColumn("store.CharacterProfileTemplate", "ProductId");
            DropColumn("players.InstanceOfPlayerItem", "ProductId");
            DropColumn("players.InstanceOfTargetItem", "ProductId");
            DropColumn("tps.tpsbuild", "ProductId");
            DropColumn("building.WorldBuildingItemEntity", "ProductId");
            DropColumn("transaction.MatchTransactionEntity", "ProductId");
            DropColumn("transaction.OrderTransactionEntity", "ProductId");
            DropColumn("transaction.StoreTransactionEntity", "ProductId");
            DropColumn("promo.PromoItemEntity", "ProductId");
            DropTable("store.BasicItemEntity");
        }
        
        public override void Down()
        {
            CreateTable(
                "store.BasicItemEntity",
                c => new
                    {
                        EntityId = c.Int(nullable: false, identity: true),
                        DisplayPosition = c.Int(nullable: false),
                        DisplayName = c.String(maxLength: 128),
                        AdminComment = c.String(maxLength: 128),
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                        Flags = c.Int(nullable: false),
                        FractionId = c.Short(),
                        Level = c.Short(nullable: false),
                        Duration = c.Time(precision: 6),
                        AmountDefault = c.Long(nullable: false),
                        AmountInStack = c.Long(nullable: false),
                        AmountMinimum = c.Long(nullable: false),
                        AmountMaximum = c.Long(),
                        Cost_Amount = c.Int(nullable: false),
                        Cost_Currency = c.Short(nullable: false),
                        SubscribeCost_Amount = c.Int(nullable: false),
                        SubscribeCost_Currency = c.Short(nullable: false),
                        Performance = c.Long(),
                        Storage = c.Long(),
                    })
                .PrimaryKey(t => t.EntityId);
            
            AddColumn("promo.PromoItemEntity", "ProductId", c => c.Int(nullable: false));
            AddColumn("transaction.StoreTransactionEntity", "ProductId", c => c.Int(nullable: false));
            AddColumn("transaction.OrderTransactionEntity", "ProductId", c => c.Int(nullable: false));
            AddColumn("transaction.MatchTransactionEntity", "ProductId", c => c.Int(nullable: false));
            AddColumn("building.WorldBuildingItemEntity", "ProductId", c => c.Int(nullable: false));
            AddColumn("tps.tpsbuild", "ProductId", c => c.Int(nullable: false));
            AddColumn("players.InstanceOfTargetItem", "ProductId", c => c.Int(nullable: false));
            AddColumn("players.InstanceOfPlayerItem", "ProductId", c => c.Int(nullable: false));
            AddColumn("store.CharacterProfileTemplate", "ProductId", c => c.Int(nullable: false));
            AddColumn("store.AddonItemContainer", "ProductId", c => c.Int(nullable: false));
            AddColumn("store.AbstractItemInstance", "ProductId", c => c.Int(nullable: false));
            DropColumn("store.OrderProductDeliveryEntity", "CategoryTypeId");
            DropColumn("transaction.StoreTransactionEntity", "CategoryTypeId");
            DropColumn("transaction.StoreTransactionEntity", "ModelId");
            DropColumn("transaction.OrderTransactionEntity", "CategoryTypeId");
            DropColumn("transaction.OrderTransactionEntity", "ModelId");
            DropColumn("transaction.MatchTransactionEntity", "CategoryTypeId");
            DropColumn("transaction.MatchTransactionEntity", "ModelId");
            CreateIndex("transaction.StoreTransactionEntity", "ProductId");
            CreateIndex("transaction.StoreTransactionEntity", "CreatedDate");
            CreateIndex("transaction.OrderTransactionEntity", "ProductId");
            CreateIndex("transaction.OrderTransactionEntity", "CreatedDate");
            CreateIndex("transaction.MatchTransactionEntity", "ProductId");
            CreateIndex("transaction.MatchTransactionEntity", "CreatedDate");
            CreateIndex("store.BasicItemEntity", new[] { "ModelId", "CategoryTypeId" }, unique: true, name: "ModelId_CategoryTypeId");
        }
    }
}
