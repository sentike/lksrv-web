namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTutorialEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "store.PlayerTutorialEntity",
                c => new
                    {
                        ProgressId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        TutorialId = c.Long(nullable: false),
                        StepId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CompleteDate = c.DateTime(nullable: false),
                        Complete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ProgressId)
                .ForeignKey("store.TutorialStepEntity", t => t.StepId, cascadeDelete: true)
                .ForeignKey("store.TutorialInstanceEntity", t => t.TutorialId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.TutorialId)
                .Index(t => t.StepId);
            
            CreateTable(
                "store.TutorialStepEntity",
                c => new
                    {
                        StepId = c.Long(nullable: false, identity: true),
                        OrderId = c.Long(nullable: false),
                        RewardCategoryId = c.Int(nullable: false),
                        RewardModelId = c.Short(nullable: false),
                        TutorialId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.StepId)
                .ForeignKey("store.AbstractItemInstance", t => new { t.RewardModelId, t.RewardCategoryId }, cascadeDelete: true)
                .ForeignKey("store.TutorialInstanceEntity", t => t.TutorialId, cascadeDelete: true)
                .Index(t => new { t.RewardModelId, t.RewardCategoryId })
                .Index(t => t.TutorialId);
            
            CreateTable(
                "store.TutorialInstanceEntity",
                c => new
                    {
                        TutorialId = c.Long(nullable: false, identity: true),
                        TutorialName = c.String(nullable: false, maxLength: 128),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TutorialId)
                .Index(t => t.TutorialName, unique: true);
            
            AddColumn("players.PlayerEntity", "CurrentTutorialId", c => c.Guid());
            CreateIndex("players.PlayerEntity", "CurrentTutorialId");
            AddForeignKey("players.PlayerEntity", "CurrentTutorialId", "store.PlayerTutorialEntity", "ProgressId");
        }
        
        public override void Down()
        {
            DropForeignKey("store.PlayerTutorialEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.PlayerEntity", "CurrentTutorialId", "store.PlayerTutorialEntity");
            DropForeignKey("store.PlayerTutorialEntity", "TutorialId", "store.TutorialInstanceEntity");
            DropForeignKey("store.PlayerTutorialEntity", "StepId", "store.TutorialStepEntity");
            DropForeignKey("store.TutorialStepEntity", "TutorialId", "store.TutorialInstanceEntity");
            DropForeignKey("store.TutorialStepEntity", new[] { "RewardModelId", "RewardCategoryId" }, "store.AbstractItemInstance");
            DropIndex("store.TutorialInstanceEntity", new[] { "TutorialName" });
            DropIndex("store.TutorialStepEntity", new[] { "TutorialId" });
            DropIndex("store.TutorialStepEntity", new[] { "RewardModelId", "RewardCategoryId" });
            DropIndex("store.PlayerTutorialEntity", new[] { "StepId" });
            DropIndex("store.PlayerTutorialEntity", new[] { "TutorialId" });
            DropIndex("store.PlayerTutorialEntity", new[] { "PlayerId" });
            DropIndex("players.PlayerEntity", new[] { "CurrentTutorialId" });
            DropColumn("players.PlayerEntity", "CurrentTutorialId");
            DropTable("store.TutorialInstanceEntity");
            DropTable("store.TutorialStepEntity");
            DropTable("store.PlayerTutorialEntity");
        }
    }
}
