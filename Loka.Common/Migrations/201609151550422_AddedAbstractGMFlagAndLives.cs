namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAbstractGMFlagAndLives : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchEntity", "NumberOfLives", c => c.Short(nullable: false));
            AlterColumn("public.QueueDuelEntity", "NumberOfLives", c => c.Short(nullable: false));
            AlterColumn("public.SquadDuelInvite", "NumberOfLives", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("public.SquadDuelInvite", "NumberOfLives", c => c.Int(nullable: false));
            AlterColumn("public.QueueDuelEntity", "NumberOfLives", c => c.Int(nullable: false));
            DropColumn("matches.MatchEntity", "NumberOfLives");
        }
    }
}
