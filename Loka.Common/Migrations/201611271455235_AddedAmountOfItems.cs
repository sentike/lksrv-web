namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAmountOfItems : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerServiceEntity", "Amount", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerServiceEntity", "Amount");
        }
    }
}
