namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOrderDeliveredStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("transaction.OrderTransactionEntity", "Notified", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("transaction.OrderTransactionEntity", "Notified");
        }
    }
}
