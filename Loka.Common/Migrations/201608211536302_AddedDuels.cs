namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDuels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "gamemode.DuelGameModeEntity",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.AbstractGameModeEntity", t => t.GameModeId, true)
                .Index(t => t.GameModeId);
        }
        
        public override void Down()
        {
            DropForeignKey("gamemode.DuelGameModeEntity", "GameModeId", "gamemode.AbstractGameModeEntity");
            DropIndex("gamemode.DuelGameModeEntity", new[] { "GameModeId" });
            DropTable("gamemode.DuelGameModeEntity");
        }
    }
}
