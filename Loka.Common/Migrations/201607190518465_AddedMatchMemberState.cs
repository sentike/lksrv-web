namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMatchMemberState : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchMember", "State", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("matches.MatchMember", "State");
        }
    }
}
