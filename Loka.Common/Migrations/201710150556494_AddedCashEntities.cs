namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCashEntities : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.SquadDuelInvite", "InviteId", "public.SquadInviteEntity");
            DropIndex("public.SquadDuelInvite", new[] { "InviteId" });
            CreateTable(
                "players.PlayerCashEntity",
                c => new
                    {
                        CashId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        Amount = c.Long(nullable: false),
                        LastAmount = c.Long(nullable: false),
                        Currency = c.Short(nullable: false),
                        LastChangedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CashId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId);
            
            AddColumn("players.PlayerEntity", "MoneyLimit", c => c.Long(nullable: false));
            AddColumn("store.AbstractItemInstance", "Cost_Currency", c => c.Short(nullable: false));
            AddColumn("store.AbstractItemInstance", "SubscribeCost_Currency", c => c.Short(nullable: false));
            AddColumn("public.QueueEntity", "Options_Bet_Currency", c => c.Short(nullable: false));
            AddColumn("matches.MatchEntity", "Options_Bet_Currency", c => c.Short(nullable: false));
            AddColumn("tps.BaseTemplateEntity", "Cost_Currency", c => c.Short(nullable: false));
            AddColumn("promo.SocialTaskEntity", "Bonus_Currency", c => c.Short(nullable: false));
            AddColumn("transcaction.PaymentTransactionEntity", "Amount_Currency", c => c.Short(nullable: false));
            AddColumn("gamemode.GameModeEntity", "JoinCost_Currency", c => c.Short(nullable: false));
            AddColumn("public.SquadInviteEntity", "Options_Bet_Currency", c => c.Short(nullable: false));
            DropColumn("players.PlayerEntity", "Cash_Money");
            DropColumn("players.PlayerEntity", "Cash_Donate");
            DropColumn("players.PlayerEntity", "Cash_Limit");
            DropColumn("store.AbstractItemInstance", "Cost_IsDonate");
            DropColumn("store.AbstractItemInstance", "SubscribeCost_IsDonate");
            DropColumn("public.QueueEntity", "Options_Bet_IsDonate");
            DropColumn("matches.MatchEntity", "Options_Bet_IsDonate");
            DropColumn("league.LeagueEntity", "Cash_Money");
            DropColumn("league.LeagueEntity", "Cash_Donate");
            DropColumn("league.LeagueEntity", "Cash_Limit");
            DropColumn("tps.BaseTemplateEntity", "Cost_IsDonate");
            DropColumn("promo.SocialTaskEntity", "Bonus_IsDonate");
            DropColumn("transcaction.PaymentTransactionEntity", "Amount_IsDonate");
            DropColumn("gamemode.GameModeEntity", "JoinCost_IsDonate");
            DropColumn("public.SquadInviteEntity", "Options_Bet_IsDonate");
            DropTable("public.SquadDuelInvite");
        }
        
        public override void Down()
        {
            CreateTable(
                "public.SquadDuelInvite",
                c => new
                    {
                        InviteId = c.Guid(nullable: false),
                        Bet_Amount = c.Int(nullable: false),
                        Bet_IsDonate = c.Boolean(nullable: false),
                        WeaponType = c.Int(nullable: false),
                        GameMapTypeId = c.Int(nullable: false),
                        NumberOfLives = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.InviteId);
            
            AddColumn("public.SquadInviteEntity", "Options_Bet_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("gamemode.GameModeEntity", "JoinCost_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("transcaction.PaymentTransactionEntity", "Amount_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("promo.SocialTaskEntity", "Bonus_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("tps.BaseTemplateEntity", "Cost_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("league.LeagueEntity", "Cash_Limit", c => c.Long(nullable: false));
            AddColumn("league.LeagueEntity", "Cash_Donate", c => c.Long(nullable: false));
            AddColumn("league.LeagueEntity", "Cash_Money", c => c.Long(nullable: false));
            AddColumn("matches.MatchEntity", "Options_Bet_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("public.QueueEntity", "Options_Bet_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("store.AbstractItemInstance", "SubscribeCost_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("store.AbstractItemInstance", "Cost_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("players.PlayerEntity", "Cash_Limit", c => c.Long(nullable: false));
            AddColumn("players.PlayerEntity", "Cash_Donate", c => c.Long(nullable: false));
            AddColumn("players.PlayerEntity", "Cash_Money", c => c.Long(nullable: false));
            DropForeignKey("players.PlayerCashEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("players.PlayerCashEntity", new[] { "PlayerId" });
            DropColumn("public.SquadInviteEntity", "Options_Bet_Currency");
            DropColumn("gamemode.GameModeEntity", "JoinCost_Currency");
            DropColumn("transcaction.PaymentTransactionEntity", "Amount_Currency");
            DropColumn("promo.SocialTaskEntity", "Bonus_Currency");
            DropColumn("tps.BaseTemplateEntity", "Cost_Currency");
            DropColumn("matches.MatchEntity", "Options_Bet_Currency");
            DropColumn("public.QueueEntity", "Options_Bet_Currency");
            DropColumn("store.AbstractItemInstance", "SubscribeCost_Currency");
            DropColumn("store.AbstractItemInstance", "Cost_Currency");
            DropColumn("players.PlayerEntity", "MoneyLimit");
            DropTable("players.PlayerCashEntity");
            CreateIndex("public.SquadDuelInvite", "InviteId");
            AddForeignKey("public.SquadDuelInvite", "InviteId", "public.SquadInviteEntity", "InviteId");
        }
    }
}
