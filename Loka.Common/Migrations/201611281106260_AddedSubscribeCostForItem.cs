namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSubscribeCostForItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.AbstractItemInstance", "IsSubscribe", c => c.Boolean(nullable: false));
            AddColumn("store.AbstractItemInstance", "SubscribeCost_Amount", c => c.Int(nullable: false));
            AddColumn("store.AbstractItemInstance", "SubscribeCost_IsDonate", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("store.AbstractItemInstance", "SubscribeCost_IsDonate");
            DropColumn("store.AbstractItemInstance", "SubscribeCost_Amount");
            DropColumn("store.AbstractItemInstance", "IsSubscribe");
        }
    }
}
