namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedchatchannels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "chat.ChatChanelEntity",
                c => new
                    {
                        ChanelId = c.Guid(nullable: false),
                        ChanelName = c.String(maxLength: 256),
                        GroupId = c.Guid(),
                        ChanelGroup = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ChanelId);
            
            CreateTable(
                "chat.ChatChanelMemberEntity",
                c => new
                    {
                        MemberId = c.Guid(nullable: false),
                        MemberFlags = c.Int(nullable: false),
                        ChanelId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.MemberId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("chat.ChatChanelEntity", t => t.ChanelId, cascadeDelete: true)
                .Index(t => t.ChanelId)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "chat.ChatChanelMessageEntity",
                c => new
                    {
                        MessageId = c.Guid(nullable: false),
                        ChanelId = c.Guid(nullable: false),
                        MemberId = c.Guid(nullable: false),
                        Message = c.String(nullable: false, maxLength: 256),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("chat.ChatChanelMemberEntity", t => t.MemberId, cascadeDelete: true)
                .ForeignKey("chat.ChatChanelEntity", t => t.ChanelId, cascadeDelete: true)
                .Index(t => t.ChanelId)
                .Index(t => t.MemberId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("chat.ChatChanelMessageEntity", "ChanelId", "chat.ChatChanelEntity");
            DropForeignKey("chat.ChatChanelMemberEntity", "ChanelId", "chat.ChatChanelEntity");
            DropForeignKey("chat.ChatChanelMemberEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("chat.ChatChanelMessageEntity", "MemberId", "chat.ChatChanelMemberEntity");
            DropIndex("chat.ChatChanelMessageEntity", new[] { "MemberId" });
            DropIndex("chat.ChatChanelMessageEntity", new[] { "ChanelId" });
            DropIndex("chat.ChatChanelMemberEntity", new[] { "PlayerId" });
            DropIndex("chat.ChatChanelMemberEntity", new[] { "ChanelId" });
            DropTable("chat.ChatChanelMessageEntity");
            DropTable("chat.ChatChanelMemberEntity");
            DropTable("chat.ChatChanelEntity");
        }
    }
}
