namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Refactory : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "achievements.AchievementContainer", newName: "AchievementContainerEntity");
            RenameTable(name: "achievements.AchievementInstance", newName: "AchievementInstanceEntity");
            RenameTable(name: "players.PlayerReputationEntity", newName: "PlayerFractionReputationEntity");
            RenameTable(name: "gamemode.AbstractGameModeEntity", newName: "GameModeEntity");
            RenameColumn(table: "players.PlayerProfileItemEntity", name: "ProfileId", newName: "PlayerProfileTypeId");
            RenameColumn(table: "players.PlayerAchievement", name: "LastAchievementInstanceId", newName: "LastAchievementInstanceEntityId");
            RenameIndex(table: "players.PlayerAchievement", name: "IX_LastAchievementInstanceId", newName: "IX_LastAchievementInstanceEntityId");
            DropColumn("store.FractionEntity", "Discount_Armour");
            DropColumn("store.FractionEntity", "Discount_Ammo");
            DropColumn("store.FractionEntity", "Discount_Weapon");
        }
        
        public override void Down()
        {
            AddColumn("store.FractionEntity", "Discount_Weapon", c => c.Int(nullable: false));
            AddColumn("store.FractionEntity", "Discount_Ammo", c => c.Int(nullable: false));
            AddColumn("store.FractionEntity", "Discount_Armour", c => c.Int(nullable: false));
            RenameIndex(table: "players.PlayerAchievement", name: "IX_LastAchievementInstanceEntityId", newName: "IX_LastAchievementInstanceId");
            RenameColumn(table: "players.PlayerAchievement", name: "LastAchievementInstanceEntityId", newName: "LastAchievementInstanceId");
            RenameColumn(table: "players.PlayerProfileItemEntity", name: "PlayerProfileTypeId", newName: "ProfileId");
            RenameTable(name: "gamemode.GameModeEntity", newName: "AbstractGameModeEntity");
            RenameTable(name: "players.PlayerFractionReputationEntity", newName: "PlayerReputationEntity");
            RenameTable(name: "achievements.AchievementInstanceEntity", newName: "AchievementInstance");
            RenameTable(name: "achievements.AchievementContainerEntity", newName: "AchievementContainer");
        }
    }
}
