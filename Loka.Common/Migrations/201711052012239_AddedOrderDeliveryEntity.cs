namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOrderDeliveryEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "store.OrderDeliveryEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        CreatorId = c.Guid(nullable: false),
                        InvoiceId = c.Guid(),
                        CreatedDate = c.DateTime(nullable: false),
                        Exception = c.String(),
                        Status = c.Int(nullable: false),
                        LastStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId);
            
            CreateTable(
                "store.OrderProductDeliveryEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        Exception = c.String(),
                        OrderId = c.Guid(nullable: false),
                        Status = c.Int(nullable: false),
                        ItemCategory = c.Int(nullable: false),
                        ItemModel = c.Short(nullable: false),
                        Amount = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.OrderDeliveryEntity", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.OrderId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("store.OrderProductDeliveryEntity", "OrderId", "store.OrderDeliveryEntity");
            DropIndex("store.OrderProductDeliveryEntity", new[] { "OrderId" });
            DropTable("store.OrderProductDeliveryEntity");
            DropTable("store.OrderDeliveryEntity");
        }
    }
}
