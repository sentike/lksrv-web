namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMatchAndGMScore : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchEntity", "ScoreLimit", c => c.Short(nullable: false));
            AddColumn("gamemode.AbstractGameModeEntity", "ScoreLimit", c => c.Short(nullable: false));
            AlterColumn("matches.MatchEntity", "NumberOfBots", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("matches.MatchEntity", "NumberOfBots", c => c.Int(nullable: false));
            DropColumn("gamemode.AbstractGameModeEntity", "ScoreLimit");
            DropColumn("matches.MatchEntity", "ScoreLimit");
        }
    }
}
