namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGameModeTypeReference : DbMigration
    {
        public override void Up()
        {
            CreateIndex("matches.MatchEntity", "GameModeTypeId");
            AddForeignKey("matches.MatchEntity", "GameModeTypeId", "gamemode.AbstractGameModeEntity", "GameModeId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("matches.MatchEntity", "GameModeTypeId", "gamemode.AbstractGameModeEntity");
            DropIndex("matches.MatchEntity", new[] { "GameModeTypeId" });
        }
    }
}
