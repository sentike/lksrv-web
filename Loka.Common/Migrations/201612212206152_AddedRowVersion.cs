namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRowVersion : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("public.QueueEntity", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("public.QueueDuelEntity", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("players.InstanceOfPlayerItem", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("players.PlayerProfileEntity", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("players.PlayerUsingServiceEntity", "RowVersion", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerUsingServiceEntity", "RowVersion");
            DropColumn("players.PlayerProfileEntity", "RowVersion");
            DropColumn("players.InstanceOfPlayerItem", "RowVersion");
            DropColumn("public.QueueDuelEntity", "RowVersion");
            DropColumn("public.QueueEntity", "RowVersion");
            DropColumn("players.PlayerEntity", "RowVersion");
        }
    }
}
