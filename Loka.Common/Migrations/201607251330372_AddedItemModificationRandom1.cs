namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedItemModificationRandom1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("public.GameSessionEntity", "Modification_Modification4_type");
            DropColumn("public.GameSessionEntity", "Modification_Modification4_value");
            DropColumn("public.GameSessionEntity", "Modification_Modification5_type");
            DropColumn("public.GameSessionEntity", "Modification_Modification5_value");
        }
        
        public override void Down()
        {
            AddColumn("public.GameSessionEntity", "Modification_Modification5_value", c => c.Double(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification5_type", c => c.Short(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification4_value", c => c.Double(nullable: false));
            AddColumn("public.GameSessionEntity", "Modification_Modification4_type", c => c.Short(nullable: false));
        }
    }
}
