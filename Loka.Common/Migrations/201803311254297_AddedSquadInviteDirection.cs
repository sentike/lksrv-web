namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSquadInviteDirection : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.SquadInviteEntity", "Direction", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("public.SquadInviteEntity", "Direction");
        }
    }
}
