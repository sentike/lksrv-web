namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDefenceExpiryDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "LastAttackedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "LastAttackedDate");
        }
    }
}
