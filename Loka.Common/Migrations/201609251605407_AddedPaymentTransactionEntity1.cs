namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPaymentTransactionEntity1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("transcaction.AbstractTransactionEntity", "Amount_Amount", c => c.Int(nullable: false));
            AddColumn("transcaction.AbstractTransactionEntity", "Amount_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("transcaction.DepositTransactionEntity", "Status", c => c.Int(nullable: false));
            AddColumn("transcaction.DepositTransactionEntity", "ConfirmationDate", c => c.DateTime());
            AddColumn("transcaction.DepositTransactionEntity", "PaymentId", c => c.Long());
            DropColumn("transcaction.DepositTransactionEntity", "Amount_Amount");
            DropColumn("transcaction.DepositTransactionEntity", "Amount_IsDonate");
            DropColumn("transcaction.PaymentTransactionEntity", "Amount_Amount");
            DropColumn("transcaction.PaymentTransactionEntity", "Amount_IsDonate");
        }
        
        public override void Down()
        {
            AddColumn("transcaction.PaymentTransactionEntity", "Amount_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("transcaction.PaymentTransactionEntity", "Amount_Amount", c => c.Int(nullable: false));
            AddColumn("transcaction.DepositTransactionEntity", "Amount_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("transcaction.DepositTransactionEntity", "Amount_Amount", c => c.Int(nullable: false));
            DropColumn("transcaction.DepositTransactionEntity", "PaymentId");
            DropColumn("transcaction.DepositTransactionEntity", "ConfirmationDate");
            DropColumn("transcaction.DepositTransactionEntity", "Status");
            DropColumn("transcaction.AbstractTransactionEntity", "Amount_IsDonate");
            DropColumn("transcaction.AbstractTransactionEntity", "Amount_Amount");
        }
    }
}
