namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClusterRefactory : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("matches.MatchEntity", "NodeId", "public.ClusterNodeEntity");
            DropForeignKey("public.ClusterNodeEntity", "ClusterId", "public.ClusterInstanceEntity");
            DropIndex("matches.MatchEntity", new[] { "NodeId" });
            DropIndex("public.ClusterNodeEntity", new[] { "ClusterId" });
            DropIndex("public.ClusterNodeEntity", new[] { "ActiveMatchId" });
            DropColumn("public.ClusterNodeEntity", "ActiveMatchId");
            RenameColumn(table: "public.ClusterNodeEntity", name: "NodeId", newName: "SessionInstance_MatchId");
            RenameColumn(table: "public.ClusterNodeEntity", name: "ClusterId", newName: "MachineAddress");
            DropPrimaryKey("public.ClusterNodeEntity");
            DropPrimaryKey("public.ClusterInstanceEntity");
            AddColumn("public.ClusterNodeEntity", "MatchId", c => c.Guid(nullable: false));
            AddColumn("public.ClusterNodeEntity", "ProcessId", c => c.Int(nullable: false));
            AddColumn("public.ClusterNodeEntity", "ShutDownCode", c => c.Int());
            AddColumn("public.ClusterNodeEntity", "LastCheckDate", c => c.DateTime());
            AddColumn("public.ClusterInstanceEntity", "Performance_CurrentAvalibleMemory", c => c.Long(nullable: false));
            AddColumn("public.ClusterInstanceEntity", "Performance_TotalAvalibleMemory", c => c.Long(nullable: false));
            AddColumn("public.ClusterInstanceEntity", "Performance_NumberOfCores", c => c.Short(nullable: false));
            AddColumn("public.ClusterInstanceEntity", "Performance_ProcessorLoad", c => c.Short(nullable: false));
            AlterColumn("public.ClusterNodeEntity", "SessionInstance_MatchId", c => c.Guid());

            DropColumn("public.ClusterNodeEntity", "MachineAddress");

            AddColumn("public.ClusterNodeEntity", "MachineAddress", c => c.String(nullable: false, maxLength: 64, storeType: "\"cidr\"", defaultValue: "78.46.46.148"));
            AlterColumn("public.ClusterNodeEntity", "ShutDownDate", c => c.DateTime());
            AlterColumn("public.ClusterNodeEntity", "LastActivityDate", c => c.DateTime());

            DropColumn("public.ClusterInstanceEntity", "MachineAddress");
            AddColumn("public.ClusterInstanceEntity", "MachineAddress", c => c.String(nullable: false, maxLength: 64, storeType: "\"cidr\"", defaultValue: "78.46.46.148"));

            CreateIndex("public.ClusterNodeEntity", "MatchId");
            AddPrimaryKey("public.ClusterInstanceEntity", "MachineAddress");
            CreateIndex("public.ClusterNodeEntity", "MachineAddress");
            CreateIndex("public.ClusterNodeEntity", "SessionInstance_MatchId");
           // AddForeignKey("public.ClusterNodeEntity", "MachineAddress", "public.ClusterInstanceEntity", "MachineAddress", cascadeDelete: true);
            DropColumn("matches.MatchEntity", "NodeId");
            DropColumn("public.ClusterInstanceEntity", "ClusterId");
            DropColumn("public.ClusterInstanceEntity", "State");
            DropColumn("public.ClusterInstanceEntity", "ShoutDownReaspon");
            DropColumn("public.ClusterInstanceEntity", "UpdateStatus");

            Sql("INSERT INTO \"ClusterInstanceEntity\" (\"MachineName\", \"RegionTypeId\", \"RegistrationDate\", \"LastActivityDate\", \"LimitActiveNodes\", \"RowVersion\", \"Performance_CurrentAvalibleMemory\", \"Performance_TotalAvalibleMemory\", \"Performance_NumberOfCores\", \"Performance_ProcessorLoad\", \"MachineAddress\") VALUES ('WIN-12782K5KS1B', 64, '2017-01-04 13:00:26.475343', '2017-01-04 13:00:26.475343', 0, '9aed0756-3759-4d26-b682-51d4251214c7', 0, 0, 0, 0, '78.46.107.13/32');");
        }
        
        public override void Down()
        {
            AddColumn("public.ClusterInstanceEntity", "UpdateStatus", c => c.Int(nullable: false));
            AddColumn("public.ClusterInstanceEntity", "ShoutDownReaspon", c => c.Int(nullable: false));
            AddColumn("public.ClusterInstanceEntity", "State", c => c.Int(nullable: false));
            AddColumn("public.ClusterInstanceEntity", "ClusterId", c => c.Guid(nullable: false));
            AddColumn("matches.MatchEntity", "NodeId", c => c.Guid());
            DropForeignKey("public.ClusterNodeEntity", "MachineAddress", "public.ClusterInstanceEntity");
            DropIndex("public.ClusterNodeEntity", new[] { "SessionInstance_MatchId" });
            DropIndex("public.ClusterNodeEntity", new[] { "MachineAddress" });
            DropPrimaryKey("public.ClusterInstanceEntity");
            DropPrimaryKey("public.ClusterNodeEntity");
            AlterColumn("public.ClusterInstanceEntity", "MachineAddress", c => c.String(maxLength: 64));
            AlterColumn("public.ClusterNodeEntity", "LastActivityDate", c => c.DateTime(nullable: false));
            AlterColumn("public.ClusterNodeEntity", "ShutDownDate", c => c.DateTime(nullable: false));
            AlterColumn("public.ClusterNodeEntity", "MachineAddress", c => c.Guid(nullable: false));
            AlterColumn("public.ClusterNodeEntity", "SessionInstance_MatchId", c => c.Guid(nullable: false));
            DropColumn("public.ClusterInstanceEntity", "Performance_ProcessorLoad");
            DropColumn("public.ClusterInstanceEntity", "Performance_NumberOfCores");
            DropColumn("public.ClusterInstanceEntity", "Performance_TotalAvalibleMemory");
            DropColumn("public.ClusterInstanceEntity", "Performance_CurrentAvalibleMemory");
            DropColumn("public.ClusterNodeEntity", "LastCheckDate");
            DropColumn("public.ClusterNodeEntity", "ShutDownCode");
            DropColumn("public.ClusterNodeEntity", "ProcessId");
            DropColumn("public.ClusterNodeEntity", "MatchId");
            AddPrimaryKey("public.ClusterInstanceEntity", "ClusterId");
            AddPrimaryKey("public.ClusterNodeEntity", "NodeId");
            RenameColumn(table: "public.ClusterNodeEntity", name: "MachineAddress", newName: "ClusterId");
            RenameColumn(table: "public.ClusterNodeEntity", name: "SessionInstance_MatchId", newName: "ActiveMatchId");
            RenameColumn(table: "public.ClusterNodeEntity", name: "SessionInstance_MatchId", newName: "NodeId");
            CreateIndex("public.ClusterNodeEntity", "ActiveMatchId");
            CreateIndex("public.ClusterNodeEntity", "ClusterId");
            CreateIndex("matches.MatchEntity", "NodeId");
            AddForeignKey("public.ClusterNodeEntity", "ClusterId", "public.ClusterInstanceEntity", "ClusterId", cascadeDelete: true);
            AddForeignKey("matches.MatchEntity", "NodeId", "public.ClusterNodeEntity", "NodeId");
        }
    }
}
