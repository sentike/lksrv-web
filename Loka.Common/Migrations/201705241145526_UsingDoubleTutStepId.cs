namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsingDoubleTutStepId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("store.PlayerTutorialEntity", "TutorialId", "store.TutorialInstanceEntity");
            DropForeignKey("store.PlayerTutorialEntity", "StepId", "store.TutorialStepEntity");
            DropIndex("store.PlayerTutorialEntity", new[] { "TutorialId" });
            DropIndex("store.PlayerTutorialEntity", new[] { "StepId" });
            DropIndex("store.TutorialStepEntity", new[] { "TutorialId" });
            DropPrimaryKey("store.TutorialStepEntity");
            AddPrimaryKey("store.TutorialStepEntity", new[] { "TutorialId", "StepId" });
            CreateIndex("store.PlayerTutorialEntity", new[] { "TutorialId", "StepId" });
            CreateIndex("store.TutorialStepEntity", new[] { "TutorialId", "StepId" }, unique: true, name: "Tutorial_Step");
            AddForeignKey("store.PlayerTutorialEntity", new[] { "TutorialId", "StepId" }, "store.TutorialStepEntity", new[] { "TutorialId", "StepId" }, cascadeDelete: true);
            DropColumn("store.TutorialStepEntity", "OrderId");
        }
        
        public override void Down()
        {
            AddColumn("store.TutorialStepEntity", "OrderId", c => c.Long(nullable: false));
            DropForeignKey("store.PlayerTutorialEntity", new[] { "TutorialId", "StepId" }, "store.TutorialStepEntity");
            DropIndex("store.TutorialStepEntity", "Tutorial_Step");
            DropIndex("store.PlayerTutorialEntity", new[] { "TutorialId", "StepId" });
            DropPrimaryKey("store.TutorialStepEntity");
            AlterColumn("store.TutorialStepEntity", "StepId", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("store.TutorialStepEntity", "StepId");
            RenameColumn(table: "store.PlayerTutorialEntity", name: "TutorialId", newName: "StepId");
            AddColumn("store.PlayerTutorialEntity", "TutorialId", c => c.Long(nullable: false));
            CreateIndex("store.TutorialStepEntity", "TutorialId");
            CreateIndex("store.PlayerTutorialEntity", "StepId");
            CreateIndex("store.PlayerTutorialEntity", "TutorialId");
            AddForeignKey("store.PlayerTutorialEntity", "StepId", "store.TutorialStepEntity", "StepId", cascadeDelete: true);
            AddForeignKey("store.PlayerTutorialEntity", "TutorialId", "store.TutorialInstanceEntity", "TutorialId", cascadeDelete: true);
        }
    }
}
