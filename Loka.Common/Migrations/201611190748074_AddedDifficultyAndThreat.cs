namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDifficultyAndThreat : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Statistic_SmallSeriesKill", c => c.Long(nullable: false));
            AddColumn("players.PlayerEntity", "Statistic_ShortSeriesKill", c => c.Long(nullable: false));
            AddColumn("players.PlayerEntity", "Statistic_LargeSeriesKill", c => c.Long(nullable: false));
            AddColumn("players.PlayerEntity", "Statistic_EpicSeriesKill", c => c.Long(nullable: false));
            AddColumn("players.PlayerEntity", "Statistic_Wins", c => c.Long(nullable: false));
            AddColumn("players.PlayerEntity", "Statistic_Loses", c => c.Long(nullable: false));
            AddColumn("players.PlayerEntity", "Statistic_Draws", c => c.Long(nullable: false));
            AddColumn("matches.MatchMember", "Statistic_SmallSeriesKill", c => c.Long(nullable: false));
            AddColumn("matches.MatchMember", "Statistic_ShortSeriesKill", c => c.Long(nullable: false));
            AddColumn("matches.MatchMember", "Statistic_LargeSeriesKill", c => c.Long(nullable: false));
            AddColumn("matches.MatchMember", "Statistic_EpicSeriesKill", c => c.Long(nullable: false));
            AddColumn("matches.MatchMember", "Difficulty", c => c.Short(nullable: false));
            DropColumn("matches.MatchMember", "Statistic_IncomingDamage");
            DropColumn("matches.MatchMember", "Statistic_OutgoingDamage");
        }
        
        public override void Down()
        {
            AddColumn("matches.MatchMember", "Statistic_OutgoingDamage", c => c.Long(nullable: false));
            AddColumn("matches.MatchMember", "Statistic_IncomingDamage", c => c.Long(nullable: false));
            DropColumn("matches.MatchMember", "Difficulty");
            DropColumn("matches.MatchMember", "Statistic_EpicSeriesKill");
            DropColumn("matches.MatchMember", "Statistic_LargeSeriesKill");
            DropColumn("matches.MatchMember", "Statistic_ShortSeriesKill");
            DropColumn("matches.MatchMember", "Statistic_SmallSeriesKill");
            DropColumn("players.PlayerEntity", "Statistic_Draws");
            DropColumn("players.PlayerEntity", "Statistic_Loses");
            DropColumn("players.PlayerEntity", "Statistic_Wins");
            DropColumn("players.PlayerEntity", "Statistic_EpicSeriesKill");
            DropColumn("players.PlayerEntity", "Statistic_LargeSeriesKill");
            DropColumn("players.PlayerEntity", "Statistic_ShortSeriesKill");
            DropColumn("players.PlayerEntity", "Statistic_SmallSeriesKill");
        }
    }
}
