namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsingAutoInc : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("players.PlayerMissionEntity", "MissionTaskId", "company.ScenarioTaskEntity");
            DropForeignKey("company.ScenarioMissionEntity", "CompanyId", "company.ScenarioCompanyEntity");
            DropIndex("players.PlayerMissionEntity", "PlayerId_MissionTaskId");
            DropIndex("company.ScenarioMissionEntity", "CompanyId_AbstractMissionId");
            DropPrimaryKey("company.ScenarioTaskEntity");
            DropPrimaryKey("company.ScenarioCompanyEntity");
            AlterColumn("players.PlayerMissionEntity", "MissionTaskId", c => c.Long(nullable: false));

            DropColumn("company.ScenarioTaskEntity", "TaskId");
            AddColumn("company.ScenarioTaskEntity", "TaskId", c => c.Long(nullable: false, identity: true));
            AlterColumn("company.ScenarioMissionEntity", "CompanyId", c => c.Long(nullable: false));

            DropColumn("company.ScenarioCompanyEntity", "CompanyId");
            AddColumn("company.ScenarioCompanyEntity", "CompanyId", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("company.ScenarioTaskEntity", "TaskId");
            AddPrimaryKey("company.ScenarioCompanyEntity", "CompanyId");
            CreateIndex("players.PlayerMissionEntity", new[] { "PlayerId", "MissionTaskId" }, unique: true, name: "PlayerId_MissionTaskId");
            CreateIndex("company.ScenarioMissionEntity", new[] { "CompanyId", "AbstractMissionId" }, unique: true, name: "CompanyId_AbstractMissionId");
            AddForeignKey("players.PlayerMissionEntity", "MissionTaskId", "company.ScenarioTaskEntity", "TaskId", cascadeDelete: true);
            AddForeignKey("company.ScenarioMissionEntity", "CompanyId", "company.ScenarioCompanyEntity", "CompanyId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("company.ScenarioMissionEntity", "CompanyId", "company.ScenarioCompanyEntity");
            DropForeignKey("players.PlayerMissionEntity", "MissionTaskId", "company.ScenarioTaskEntity");
            DropIndex("company.ScenarioMissionEntity", "CompanyId_AbstractMissionId");
            DropIndex("players.PlayerMissionEntity", "PlayerId_MissionTaskId");
            DropPrimaryKey("company.ScenarioCompanyEntity");
            DropPrimaryKey("company.ScenarioTaskEntity");
            AlterColumn("company.ScenarioCompanyEntity", "CompanyId", c => c.Int(nullable: false));
            AlterColumn("company.ScenarioMissionEntity", "CompanyId", c => c.Int(nullable: false));
            AlterColumn("company.ScenarioTaskEntity", "TaskId", c => c.Int(nullable: false, identity: true));
            AlterColumn("players.PlayerMissionEntity", "MissionTaskId", c => c.Int(nullable: false));
            AddPrimaryKey("company.ScenarioCompanyEntity", "CompanyId");
            AddPrimaryKey("company.ScenarioTaskEntity", "TaskId");
            CreateIndex("company.ScenarioMissionEntity", new[] { "CompanyId", "AbstractMissionId" }, unique: true, name: "CompanyId_AbstractMissionId");
            CreateIndex("players.PlayerMissionEntity", new[] { "PlayerId", "MissionTaskId" }, unique: true, name: "PlayerId_MissionTaskId");
            AddForeignKey("company.ScenarioMissionEntity", "CompanyId", "company.ScenarioCompanyEntity", "CompanyId", cascadeDelete: true);
            AddForeignKey("players.PlayerMissionEntity", "MissionTaskId", "company.ScenarioTaskEntity", "TaskId", cascadeDelete: true);
        }
    }
}
