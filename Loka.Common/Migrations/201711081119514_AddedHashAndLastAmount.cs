namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedHashAndLastAmount : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerCashEntity", "Hash", c => c.Long(nullable: false));
            AddColumn("players.InstanceOfPlayerItem", "LastAmount", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.InstanceOfPlayerItem", "LastAmount");
            DropColumn("players.PlayerCashEntity", "Hash");
        }
    }
}
