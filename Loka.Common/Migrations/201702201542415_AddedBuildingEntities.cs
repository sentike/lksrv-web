namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBuildingEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "store.BuildingItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId }, true)
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "players.PlayerBuildingItemEntity",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                        Amount = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.ItemId, true)
                .Index(t => t.ItemId);
            
            AddColumn("store.AbstractItemInstance", "ItemId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerBuildingItemEntity", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("store.BuildingItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropIndex("players.PlayerBuildingItemEntity", new[] { "ItemId" });
            DropIndex("store.BuildingItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropColumn("store.AbstractItemInstance", "ItemId");
            DropTable("players.PlayerBuildingItemEntity");
            DropTable("store.BuildingItemInstance");
        }
    }
}
