namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMigrationAction : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "MigrationAction", c => c.Int(nullable: false));
            AddColumn("store.AbstractItemInstance", "MigrationAction", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("store.AbstractItemInstance", "MigrationAction");
            DropColumn("players.PlayerEntity", "MigrationAction");
        }
    }
}
