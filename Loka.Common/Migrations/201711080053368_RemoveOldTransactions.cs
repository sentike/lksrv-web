namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveOldTransactions : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("transcaction.AbstractTransactionEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("transcaction.DepositTransactionEntity", "TransactionId", "transcaction.AbstractTransactionEntity");
            DropForeignKey("transcaction.WithdrawTransactionEntity", "TransactionId", "transcaction.AbstractTransactionEntity");
            DropForeignKey("transcaction.PaymentTransactionEntity", "TransactionId", "transcaction.AbstractTransactionEntity");
            DropForeignKey("transcaction.PaymentTransactionEntity", new[] { "AbstractItemId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropIndex("transcaction.AbstractTransactionEntity", new[] { "PlayerId" });
            DropIndex("transcaction.DepositTransactionEntity", new[] { "TransactionId" });
            DropIndex("transcaction.WithdrawTransactionEntity", new[] { "TransactionId" });
            DropIndex("transcaction.PaymentTransactionEntity", new[] { "TransactionId" });
            DropIndex("transcaction.PaymentTransactionEntity", new[] { "AbstractItemId", "CategoryTypeId" });
            DropTable("transcaction.AbstractTransactionEntity");
            DropTable("transcaction.DepositTransactionEntity");
            DropTable("transcaction.WithdrawTransactionEntity");
            DropTable("transcaction.PaymentTransactionEntity");
        }
        
        public override void Down()
        {
            CreateTable(
                "transcaction.PaymentTransactionEntity",
                c => new
                    {
                        TransactionId = c.Guid(nullable: false),
                        AbstractItemId = c.Short(nullable: false),
                        PaymentType = c.Int(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                        Amount_Amount = c.Int(nullable: false),
                        Amount_Currency = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.TransactionId);
            
            CreateTable(
                "transcaction.WithdrawTransactionEntity",
                c => new
                    {
                        TransactionId = c.Guid(nullable: false),
                        Status = c.Int(nullable: false),
                        ConfirmationDate = c.DateTime(),
                        PaymentId = c.Long(),
                        Amount = c.Long(nullable: false),
                        To = c.String(nullable: false, maxLength: 96),
                        CurrencyId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.TransactionId);
            
            CreateTable(
                "transcaction.DepositTransactionEntity",
                c => new
                    {
                        TransactionId = c.Guid(nullable: false),
                        Source = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        ConfirmationDate = c.DateTime(),
                        PaymentId = c.Long(),
                        Amount = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.TransactionId);
            
            CreateTable(
                "transcaction.AbstractTransactionEntity",
                c => new
                    {
                        TransactionId = c.Guid(nullable: false),
                        TransactionType = c.Int(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        ExecuteDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TransactionId);
            
            CreateIndex("transcaction.PaymentTransactionEntity", new[] { "AbstractItemId", "CategoryTypeId" });
            CreateIndex("transcaction.PaymentTransactionEntity", "TransactionId");
            CreateIndex("transcaction.WithdrawTransactionEntity", "TransactionId");
            CreateIndex("transcaction.DepositTransactionEntity", "TransactionId");
            CreateIndex("transcaction.AbstractTransactionEntity", "PlayerId");
            AddForeignKey("transcaction.PaymentTransactionEntity", new[] { "AbstractItemId", "CategoryTypeId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" }, cascadeDelete: true);
            AddForeignKey("transcaction.PaymentTransactionEntity", "TransactionId", "transcaction.AbstractTransactionEntity", "TransactionId");
            AddForeignKey("transcaction.WithdrawTransactionEntity", "TransactionId", "transcaction.AbstractTransactionEntity", "TransactionId");
            AddForeignKey("transcaction.DepositTransactionEntity", "TransactionId", "transcaction.AbstractTransactionEntity", "TransactionId");
            AddForeignKey("transcaction.AbstractTransactionEntity", "PlayerId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
        }
    }
}
