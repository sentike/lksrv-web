namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedQueueDuelEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.QueueDuelEntity",
                c => new
                    {
                        QueueId = c.Guid(nullable: false),
                        Bet_Amount = c.Int(nullable: false),
                        Bet_IsDonate = c.Boolean(nullable: false),
                        WeaponType = c.Int(nullable: false),
                        NumberOfLives = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QueueId)
                .ForeignKey("public.QueueEntity", t => t.QueueId, true)
                .Index(t => t.QueueId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("public.QueueDuelEntity", "QueueId", "public.QueueEntity");
            DropIndex("public.QueueDuelEntity", new[] { "QueueId" });
            DropTable("public.QueueDuelEntity");
        }
    }
}
