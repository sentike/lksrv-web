namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUniqSentenceVote : DbMigration
    {
        public override void Up()
        {
            CreateIndex("improvement.SentenceVoteEntity", new[] { "SentenceId", "PlayerId" }, unique: true, name: "SentenceId_PlayerId");
            AddForeignKey("improvement.SentenceVoteEntity", "PlayerId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("improvement.SentenceVoteEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("improvement.SentenceVoteEntity", "SentenceId_PlayerId");
            CreateIndex("improvement.SentenceVoteEntity", "SentenceId");
        }
    }
}
