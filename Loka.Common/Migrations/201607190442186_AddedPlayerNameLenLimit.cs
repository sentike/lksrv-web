namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerNameLenLimit : DbMigration
    {
        public override void Up()
        {
            AlterColumn("players.PlayerEntity", "PlayerName", c => c.String(maxLength: 64));
        }
        
        public override void Down()
        {
            AlterColumn("players.PlayerEntity", "PlayerName", c => c.String());
        }
    }
}
