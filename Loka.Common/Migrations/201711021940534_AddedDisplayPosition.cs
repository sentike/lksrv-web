namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDisplayPosition : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.AbstractItemInstance", "DisplayPosition", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("store.AbstractItemInstance", "DisplayPosition");
        }
    }
}
