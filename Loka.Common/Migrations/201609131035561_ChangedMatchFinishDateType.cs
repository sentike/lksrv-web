namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedMatchFinishDateType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("matches.MatchEntity", "Finished", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("matches.MatchEntity", "Finished", c => c.DateTime(nullable: false));
        }
    }
}
