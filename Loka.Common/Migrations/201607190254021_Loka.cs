namespace Loka.Server.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    public partial class Loka : DbMigration
    {
        public override void Up()
        {
            const string user = "postgres";

            var schemes = new List<string>()
            {
                "achievements",
                "gamemode",
                "league",
                "matches",
                "players",
                "promo",
                "store",
                "public",
                "vrs"
            };

            foreach (var schema in schemes)
            {
                this.Sql($"CREATE SCHEMA IF NOT EXISTS \"{schema}\" AUTHORIZATION \"{user}\";");
            }

            this.Sql("CREATE EXTENSION  IF NOT EXISTS pgcrypto;");
           

            CreateTable(
                "players.PlayerEntity",
                c => new
                    {
                        PlayerId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        PlayerName = c.String(),
                        PremiumEndDate = c.DateTime(),
                        Experience_Level = c.Int(nullable: false),
                        Experience_Experience = c.Int(nullable: false),
                        Experience_WeekExperience = c.Long(nullable: false),
                        WeekBonus_JoinDate = c.DateTime(nullable: false, defaultValueSql:"Now()"),
                        WeekBonus_NotifyDate = c.DateTime(nullable: false, defaultValueSql: "Now()"),
                        WeekBonus_JoinCount = c.Short(nullable: false),
                        Cash_Money = c.Long(nullable: false),
                        Cash_Donate = c.Long(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false, defaultValueSql:"Now()"),
                        LastActivityDate = c.DateTime(nullable: false, defaultValueSql:"Now()"),
                        CurrentFractionId = c.Guid(),
                        FirstPartnerId = c.Guid(),
                        MatchMemberId = c.Guid(),
                        QueueMemberId = c.Guid(),
                        Statistic_Kills = c.Long(nullable: false),
                        Statistic_Deads = c.Long(nullable: false),
                        Statistic_Assists = c.Long(nullable: false),
                        Statistic_TeamKills = c.Long(nullable: false),
                        Statistic_Shots = c.Long(nullable: false),
                        Statistic_Hits = c.Long(nullable: false),
                        Statistic_IncomingDamage = c.Long(nullable: false),
                        Statistic_OutgoingDamage = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PlayerId)
                .ForeignKey("players.PlayerReputationEntity", t => t.CurrentFractionId)
                .ForeignKey("public.SessionQueueMember", t => t.QueueMemberId, cascadeDelete: true)
                .ForeignKey("matches.MatchMember", t => t.MatchMemberId)
                .ForeignKey("vrs.Users", t => t.PlayerId)
                .Index(t => t.PlayerId)
                .Index(t => t.CurrentFractionId)
                .Index(t => t.MatchMemberId)
                .Index(t => t.QueueMemberId);
            
            CreateTable(
                "players.PlayerAchievement",
                c => new
                    {
                        PlayerAchievementId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        Amount = c.Int(nullable: false),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        AchievementTypeId = c.Int(nullable: false),
                        LastAchievementInstanceId = c.Int(),
                    })
                .PrimaryKey(t => t.PlayerAchievementId)
                .ForeignKey("achievements.AchievementContainer", t => t.AchievementTypeId, cascadeDelete: true)
                .ForeignKey("achievements.AchievementInstance", t => t.LastAchievementInstanceId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.AchievementTypeId)
                .Index(t => t.LastAchievementInstanceId);
            
            CreateTable(
                "achievements.AchievementContainer",
                c => new
                    {
                        AchievementTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AchievementTypeId);
            
            CreateTable(
                "achievements.AchievementInstance",
                c => new
                    {
                        AchievementBonusId = c.Int(nullable: false, identity: true),
                        AchievementTypeId = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        BonusPackId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AchievementBonusId)
                .ForeignKey("achievements.AchievementContainer", t => t.AchievementTypeId, cascadeDelete: true)
                .Index(t => t.AchievementTypeId);
            
            CreateTable(
                "players.PlayerReputationEntity",
                c => new
                    {
                        ReputationId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        FractionId = c.Short(nullable: false),
                        CurrentRank = c.Short(nullable: false),
                        LastRank = c.Short(nullable: false),
                        Reputation = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ReputationId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "players.FriendEntity",
                c => new
                    {
                        MemberId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        FriendId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MemberId)
                .ForeignKey("players.PlayerEntity", t => t.FriendId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.FriendId);
            
            CreateTable(
                "players.InstanceOfPlayerItem",
                c => new
                    {
                        ItemId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        modification_1_type = c.Short(nullable: false),
                        modification_1_value = c.Double(nullable: false),
                        modification_2_type = c.Short(nullable: false),
                        modification_2_value = c.Double(nullable: false),
                        modification_3_type = c.Short(nullable: false),
                        modification_3_value = c.Double(nullable: false),
                        modification_4_type = c.Short(nullable: false),
                        modification_4_value = c.Double(nullable: false),
                        modification_5_type = c.Short(nullable: false),
                        modification_5_value = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId }, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => new { t.ModelId, t.CategoryTypeId })
                .Index(t => t.PlayerId);
            
            CreateTable(
                "players.InstanceOfTargetItem",
                c => new
                    {
                        ItemId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        TargetPlayerItemId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId }, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.TargetPlayerItemId, cascadeDelete: true)
                .Index(t => new { t.ModelId, t.CategoryTypeId })
                .Index(t => t.PlayerId)
                .Index(t => t.TargetPlayerItemId);
            
            CreateTable(
                "store.AbstractItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                        IsAvalible = c.Boolean(nullable: false),
                        Level = c.Int(nullable: false),
                        DefaultSkinId = c.Int(nullable: false),
                        FractionId = c.Short(nullable: false),
                        Cost_Amount = c.Int(nullable: false),
                        Cost_IsDonate = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.FractionEntity", t => t.FractionId, cascadeDelete: true)
                .Index(t => t.FractionId);
            
            CreateTable(
                "store.AddonItemContainer",
                c => new
                    {
                        ContainerId = c.Int(nullable: false, identity: true),
                        ModelId = c.Short(nullable: false),
                        ModelCategoryTypeId = c.Int(nullable: false),
                        ItemId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ContainerId)
                .ForeignKey("store.AbstractItemInstance", t => new { t.ItemId, t.CategoryTypeId }, cascadeDelete: true)
                .ForeignKey("store.AddonItemInstance", t => new { t.ModelId, t.ModelCategoryTypeId })
                .Index(t => new { t.ModelId, t.ModelCategoryTypeId })
                .Index(t => new { t.ItemId, t.CategoryTypeId });
            
            CreateTable(
                "store.FractionEntity",
                c => new
                    {
                        FractionId = c.Short(nullable: false),
                        Discount_Armour = c.Int(nullable: false),
                        Discount_Ammo = c.Int(nullable: false),
                        Discount_Weapon = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FractionId);
            
            CreateTable(
                "players.InstanceOfInstalledAddon",
                c => new
                    {
                        ContainerId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        AddonId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        ItemId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        AddonSlot = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ContainerId)
                .ForeignKey("players.InstanceOfTargetItem", t => t.AddonId, cascadeDelete: true)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.AddonId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "players.PlayerProfileEntity",
                c => new
                    {
                        AssetId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        Name = c.String(),
                        IsEnabled = c.Boolean(nullable: false),
                        IsUnlocked = c.Boolean(nullable: false),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        CharacterId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                    })
                .PrimaryKey(t => t.AssetId)
                .ForeignKey("players.InstanceOfPlayerCharacter", t => t.CharacterId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.CharacterId);
            
            CreateTable(
                "players.PlayerProfileItemEntity",
                c => new
                    {
                        ProfileItemId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        ProfileId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        ItemId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        SlotId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProfileItemId)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("players.PlayerProfileEntity", t => t.ProfileId, cascadeDelete: true)
                .Index(t => t.ProfileId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "league.LeagueMemberEntity",
                c => new
                    {
                        PlayerId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        LeagueId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        JoinDate = c.DateTime(nullable: false),
                        Access = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PlayerId)
                .ForeignKey("league.LeagueEntity", t => t.LeagueId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.LeagueId);
            
            CreateTable(
                "league.LeagueEntity",
                c => new
                    {
                        LeagueId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        Info_Name = c.String(),
                        Info_Abbr = c.String(),
                        Info_FoundedDate = c.DateTime(nullable: false),
                        Info_AccessType = c.Int(nullable: false),
                        Info_JoinPrice = c.Long(nullable: false),
                        Cash_Money = c.Long(nullable: false),
                        Cash_Donate = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.LeagueId);
            
            CreateTable(
                "public.PrivateChatMessage",
                c => new
                    {
                        MessageId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        MessageDataId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("public.PrivateChatMessageData", t => t.MessageDataId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.MessageDataId)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "public.PrivateChatMessageData",
                c => new
                    {
                        MessageId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        Message = c.String(),
                        Date = c.DateTime(nullable: false),
                        IsReaded = c.Boolean(nullable: false),
                        SenderId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        ReceiverId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("players.PlayerEntity", t => t.ReceiverId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.SenderId, cascadeDelete: true)
                .Index(t => t.SenderId)
                .Index(t => t.ReceiverId);
            
            CreateTable(
                "public.SessionQueueMember",
                c => new
                    {
                        QueueMemberId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        QueueId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        Ready = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.QueueMemberId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("public.SessionQueueEntity", t => t.QueueId, cascadeDelete: true)
                .Index(t => t.QueueId)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "public.SessionQueueEntity",
                c => new
                    {
                        QueueId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        MatchId = c.Guid(),
                        GameModeTypeId = c.Int(nullable: false),
                        RegionTypeId = c.Short(nullable: false),
                        CheckDate = c.DateTime(nullable: false),
                        JoinDate = c.DateTime(nullable: false),
                        Level_Min = c.Int(nullable: false),
                        Level_Max = c.Int(nullable: false),
                        Ready = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.QueueId)
                .ForeignKey("matches.MatchEntity", t => t.MatchId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.MatchId);
            
            CreateTable(
                "matches.MatchEntity",
                c => new
                    {
                        MatchId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        NodeId = c.Guid(),
                        GameModeTypeId = c.Int(nullable: false),
                        GameMapTypeId = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Started = c.DateTime(nullable: false),
                        Finished = c.DateTime(nullable: false),
                        NumberOfBots = c.Int(nullable: false),
                        WinnerTeamId = c.Guid(),
                        RoundTimeInSeconds = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MatchId)
                .ForeignKey("public.ClusterNodeEntity", t => t.NodeId)
                .ForeignKey("matches.MatchTeam", t => t.WinnerTeamId)
                .Index(t => t.NodeId)
                .Index(t => t.WinnerTeamId);
            
            CreateTable(
                "matches.MatchMember",
                c => new
                    {
                        MemberId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        MatchId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        TeamId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        Level = c.Short(nullable: false),
                        Award_Money = c.Int(nullable: false),
                        Award_Experience = c.Int(nullable: false),
                        Award_Reputation = c.Int(nullable: false),
                        Award_FractionId = c.Short(nullable: false),
                        Statistic_Kills = c.Long(nullable: false),
                        Statistic_Deads = c.Long(nullable: false),
                        Statistic_Assists = c.Long(nullable: false),
                        Statistic_TeamKills = c.Long(nullable: false),
                        Statistic_Shots = c.Long(nullable: false),
                        Statistic_Hits = c.Long(nullable: false),
                        Statistic_IncomingDamage = c.Long(nullable: false),
                        Statistic_OutgoingDamage = c.Long(nullable: false),
                        Squad = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MemberId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("matches.MatchTeam", t => t.TeamId, cascadeDelete: true)
                .ForeignKey("matches.MatchEntity", t => t.MatchId, cascadeDelete: true)
                .Index(t => t.MatchId)
                .Index(t => t.PlayerId)
                .Index(t => t.TeamId);
            
            CreateTable(
                "matches.MemberAchievements",
                c => new
                    {
                        AchievementId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        AchievementTypeId = c.Int(nullable: false),
                        AchievementCount = c.Int(nullable: false),
                        MemberId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                    })
                .PrimaryKey(t => t.AchievementId)
                .ForeignKey("matches.MatchMember", t => t.MemberId, cascadeDelete: true)
                .Index(t => t.MemberId);
            
            CreateTable(
                "matches.MatchTeam",
                c => new
                    {
                        TeamId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        MatchId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                    })
                .PrimaryKey(t => t.TeamId)
                .ForeignKey("matches.MatchEntity", t => t.MatchId, cascadeDelete: true)
                .Index(t => t.MatchId);
            
            CreateTable(
                "public.ClusterNodeEntity",
                c => new
                    {
                        NodeId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        ClusterId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        Address_Host = c.String(),
                        Address_Port = c.Int(nullable: false),
                        ShutDownDate = c.DateTime(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                        ActiveMatchId = c.Guid(),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NodeId)
                .ForeignKey("public.ClusterInstanceEntity", t => t.ClusterId, cascadeDelete: true)
                .ForeignKey("matches.MatchEntity", t => t.ActiveMatchId)
                .Index(t => t.ClusterId)
                .Index(t => t.ActiveMatchId);
            
            CreateTable(
                "public.ClusterInstanceEntity",
                c => new
                    {
                        ClusterId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        MachineName = c.String(maxLength: 64),
                        MachineAddress = c.String(maxLength: 64),
                        RegionTypeId = c.Short(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                        State = c.Int(nullable: false),
                        LimitActiveNodes = c.Int(nullable: false),
                        ShoutDownReaspon = c.Int(nullable: false),
                        UpdateStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClusterId);
            
            CreateTable(
                "vrs.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        Balance = c.Long(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "vrs.UserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("vrs.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "vrs.UserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("vrs.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "vrs.UserRoles",
                c => new
                {
                    UserId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                    RoleId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                })
                .PrimaryKey(t => new {t.UserId, t.RoleId})
                .ForeignKey("vrs.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("vrs.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "gamemode.GameMapEntity",
                c => new
                    {
                        InstanceId = c.Int(nullable: false, identity: true),
                        GameModeId = c.Int(nullable: false),
                        MapId = c.Int(nullable: false),
                        Members_Min = c.Int(nullable: false),
                        Members_Max = c.Int(nullable: false),
                        RoundTimeInSeconds = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InstanceId)
                .ForeignKey("gamemode.AbstractGameModeEntity", t => t.GameModeId, cascadeDelete: true)
                .Index(t => t.GameModeId);
            
            CreateTable(
                "gamemode.AbstractGameModeEntity",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                        Level_Min = c.Int(nullable: false),
                        Level_Max = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId);
            
            CreateTable(
                "public.GameSessionEntity",
                c => new
                    {
                        SessionId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        LastMessageAccess = c.DateTime(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SessionId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "public.GlobalChatMessage",
                c => new
                    {
                        MessageId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        SenderId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        EditorId = c.Guid(),
                        Status = c.Int(nullable: false),
                        LanguageId = c.Int(nullable: false),
                        Message = c.String(),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("players.PlayerEntity", t => t.EditorId)
                .ForeignKey("players.PlayerEntity", t => t.SenderId, cascadeDelete: true)
                .Index(t => t.SenderId)
                .Index(t => t.EditorId);
            
            CreateTable(
                "players.PlayerPromoCode",
                c => new
                    {
                        PlayerCodeId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        CodeId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        ActivationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PlayerCodeId);
            
            CreateTable(
                "vrs.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "gamemode.TeamGameModeEntity",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.AbstractGameModeEntity", t => t.GameModeId)
                .Index(t => t.GameModeId);
            
            CreateTable(
                "players.InstanceOfPlayerCharacter",
                c => new
                    {
                        ItemId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid()"),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.ItemId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerWeapon",
                c => new
                    {
                        ItemId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.ItemId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerArmour",
                c => new
                    {
                        ItemId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.ItemId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfItemSkin",
                c => new
                    {
                        ItemId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("players.InstanceOfTargetItem", t => t.ItemId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfItemAddon",
                c => new
                    {
                        ItemId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("players.InstanceOfTargetItem", t => t.ItemId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "players.InstanceOfPlayerKit",
                c => new
                    {
                        ItemId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid ()"),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("players.InstanceOfPlayerItem", t => t.ItemId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "store.CharacterItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId })
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.ArmourItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId })
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.WeaponItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId })
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.AmmoItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId })
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.SkinItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId })
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.KitItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId })
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
            CreateTable(
                "store.AddonItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId })
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
        }
        
        public override void Down()
        {
            DropForeignKey("store.AddonItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.KitItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.SkinItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.AmmoItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.WeaponItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.ArmourItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.CharacterItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("players.InstanceOfPlayerKit", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfItemAddon", "ItemId", "players.InstanceOfTargetItem");
            DropForeignKey("players.InstanceOfItemSkin", "ItemId", "players.InstanceOfTargetItem");
            DropForeignKey("players.InstanceOfPlayerArmour", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerWeapon", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfPlayerCharacter", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("gamemode.TeamGameModeEntity", "GameModeId", "gamemode.AbstractGameModeEntity");
            DropForeignKey("vrs.UserRoles", "RoleId", "vrs.Roles");
            DropForeignKey("public.GlobalChatMessage", "SenderId", "players.PlayerEntity");
            DropForeignKey("public.GlobalChatMessage", "EditorId", "players.PlayerEntity");
            DropForeignKey("public.GameSessionEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("gamemode.GameMapEntity", "GameModeId", "gamemode.AbstractGameModeEntity");
            DropForeignKey("vrs.UserRoles", "UserId", "vrs.Users");
            DropForeignKey("players.PlayerEntity", "PlayerId", "vrs.Users");
            DropForeignKey("vrs.UserLogins", "UserId", "vrs.Users");
            DropForeignKey("vrs.UserClaims", "UserId", "vrs.Users");
            DropForeignKey("players.PlayerEntity", "MatchMemberId", "matches.MatchMember");
            DropForeignKey("players.PlayerEntity", "QueueMemberId", "public.SessionQueueMember");
            DropForeignKey("public.SessionQueueEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("public.SessionQueueMember", "QueueId", "public.SessionQueueEntity");
            DropForeignKey("public.SessionQueueEntity", "MatchId", "matches.MatchEntity");
            DropForeignKey("matches.MatchEntity", "WinnerTeamId", "matches.MatchTeam");
            DropForeignKey("matches.MatchTeam", "MatchId", "matches.MatchEntity");
            DropForeignKey("matches.MatchEntity", "NodeId", "public.ClusterNodeEntity");
            DropForeignKey("public.ClusterNodeEntity", "ActiveMatchId", "matches.MatchEntity");
            DropForeignKey("public.ClusterNodeEntity", "ClusterId", "public.ClusterInstanceEntity");
            DropForeignKey("matches.MatchMember", "MatchId", "matches.MatchEntity");
            DropForeignKey("matches.MatchMember", "TeamId", "matches.MatchTeam");
            DropForeignKey("matches.MatchMember", "PlayerId", "players.PlayerEntity");
            DropForeignKey("matches.MemberAchievements", "MemberId", "matches.MatchMember");
            DropForeignKey("public.SessionQueueMember", "PlayerId", "players.PlayerEntity");
            DropForeignKey("public.PrivateChatMessage", "PlayerId", "players.PlayerEntity");
            DropForeignKey("public.PrivateChatMessage", "MessageDataId", "public.PrivateChatMessageData");
            DropForeignKey("public.PrivateChatMessageData", "SenderId", "players.PlayerEntity");
            DropForeignKey("public.PrivateChatMessageData", "ReceiverId", "players.PlayerEntity");
            DropForeignKey("league.LeagueMemberEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("league.LeagueMemberEntity", "LeagueId", "league.LeagueEntity");
            DropForeignKey("players.PlayerProfileEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.PlayerProfileItemEntity", "ProfileId", "players.PlayerProfileEntity");
            DropForeignKey("players.PlayerProfileItemEntity", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.PlayerProfileEntity", "CharacterId", "players.InstanceOfPlayerCharacter");
            DropForeignKey("players.InstanceOfPlayerItem", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.InstanceOfPlayerItem", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("players.InstanceOfInstalledAddon", "ItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfInstalledAddon", "AddonId", "players.InstanceOfTargetItem");
            DropForeignKey("players.InstanceOfTargetItem", "TargetPlayerItemId", "players.InstanceOfPlayerItem");
            DropForeignKey("players.InstanceOfTargetItem", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.InstanceOfTargetItem", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("store.AddonItemContainer", new[] { "ModelId", "ModelCategoryTypeId" }, "store.AddonItemInstance");
            DropForeignKey("store.AbstractItemInstance", "FractionId", "store.FractionEntity");
            DropForeignKey("store.AddonItemContainer", new[] { "ItemId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("players.FriendEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.FriendEntity", "FriendId", "players.PlayerEntity");
            DropForeignKey("players.PlayerEntity", "CurrentFractionId", "players.PlayerReputationEntity");
            DropForeignKey("players.PlayerReputationEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.PlayerAchievement", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.PlayerAchievement", "LastAchievementInstanceId", "achievements.AchievementInstance");
            DropForeignKey("players.PlayerAchievement", "AchievementTypeId", "achievements.AchievementContainer");
            DropForeignKey("achievements.AchievementInstance", "AchievementTypeId", "achievements.AchievementContainer");
            DropIndex("store.AddonItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.KitItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.SkinItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.AmmoItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.WeaponItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.ArmourItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("store.CharacterItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("players.InstanceOfPlayerKit", new[] { "ItemId" });
            DropIndex("players.InstanceOfItemAddon", new[] { "ItemId" });
            DropIndex("players.InstanceOfItemSkin", new[] { "ItemId" });
            DropIndex("players.InstanceOfPlayerArmour", new[] { "ItemId" });
            DropIndex("players.InstanceOfPlayerWeapon", new[] { "ItemId" });
            DropIndex("players.InstanceOfPlayerCharacter", new[] { "ItemId" });
            DropIndex("gamemode.TeamGameModeEntity", new[] { "GameModeId" });
            DropIndex("vrs.Roles", "RoleNameIndex");
            DropIndex("public.GlobalChatMessage", new[] { "EditorId" });
            DropIndex("public.GlobalChatMessage", new[] { "SenderId" });
            DropIndex("public.GameSessionEntity", new[] { "PlayerId" });
            DropIndex("gamemode.GameMapEntity", new[] { "GameModeId" });
            DropIndex("vrs.UserRoles", new[] { "RoleId" });
            DropIndex("vrs.UserRoles", new[] { "UserId" });
            DropIndex("vrs.UserLogins", new[] { "UserId" });
            DropIndex("vrs.UserClaims", new[] { "UserId" });
            DropIndex("vrs.Users", "UserNameIndex");
            DropIndex("public.ClusterNodeEntity", new[] { "ActiveMatchId" });
            DropIndex("public.ClusterNodeEntity", new[] { "ClusterId" });
            DropIndex("matches.MatchTeam", new[] { "MatchId" });
            DropIndex("matches.MemberAchievements", new[] { "MemberId" });
            DropIndex("matches.MatchMember", new[] { "TeamId" });
            DropIndex("matches.MatchMember", new[] { "PlayerId" });
            DropIndex("matches.MatchMember", new[] { "MatchId" });
            DropIndex("matches.MatchEntity", new[] { "WinnerTeamId" });
            DropIndex("matches.MatchEntity", new[] { "NodeId" });
            DropIndex("public.SessionQueueEntity", new[] { "MatchId" });
            DropIndex("public.SessionQueueEntity", new[] { "PlayerId" });
            DropIndex("public.SessionQueueMember", new[] { "PlayerId" });
            DropIndex("public.SessionQueueMember", new[] { "QueueId" });
            DropIndex("public.PrivateChatMessageData", new[] { "ReceiverId" });
            DropIndex("public.PrivateChatMessageData", new[] { "SenderId" });
            DropIndex("public.PrivateChatMessage", new[] { "PlayerId" });
            DropIndex("public.PrivateChatMessage", new[] { "MessageDataId" });
            DropIndex("league.LeagueMemberEntity", new[] { "LeagueId" });
            DropIndex("league.LeagueMemberEntity", new[] { "PlayerId" });
            DropIndex("players.PlayerProfileItemEntity", new[] { "ItemId" });
            DropIndex("players.PlayerProfileItemEntity", new[] { "ProfileId" });
            DropIndex("players.PlayerProfileEntity", new[] { "CharacterId" });
            DropIndex("players.PlayerProfileEntity", new[] { "PlayerId" });
            DropIndex("players.InstanceOfInstalledAddon", new[] { "ItemId" });
            DropIndex("players.InstanceOfInstalledAddon", new[] { "AddonId" });
            DropIndex("store.AddonItemContainer", new[] { "ItemId", "CategoryTypeId" });
            DropIndex("store.AddonItemContainer", new[] { "ModelId", "ModelCategoryTypeId" });
            DropIndex("store.AbstractItemInstance", new[] { "FractionId" });
            DropIndex("players.InstanceOfTargetItem", new[] { "TargetPlayerItemId" });
            DropIndex("players.InstanceOfTargetItem", new[] { "PlayerId" });
            DropIndex("players.InstanceOfTargetItem", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("players.InstanceOfPlayerItem", new[] { "PlayerId" });
            DropIndex("players.InstanceOfPlayerItem", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("players.FriendEntity", new[] { "FriendId" });
            DropIndex("players.FriendEntity", new[] { "PlayerId" });
            DropIndex("players.PlayerReputationEntity", new[] { "PlayerId" });
            DropIndex("achievements.AchievementInstance", new[] { "AchievementTypeId" });
            DropIndex("players.PlayerAchievement", new[] { "LastAchievementInstanceId" });
            DropIndex("players.PlayerAchievement", new[] { "AchievementTypeId" });
            DropIndex("players.PlayerAchievement", new[] { "PlayerId" });
            DropIndex("players.PlayerEntity", new[] { "QueueMemberId" });
            DropIndex("players.PlayerEntity", new[] { "MatchMemberId" });
            DropIndex("players.PlayerEntity", new[] { "CurrentFractionId" });
            DropIndex("players.PlayerEntity", new[] { "PlayerId" });
            DropTable("store.AddonItemInstance");
            DropTable("store.KitItemInstance");
            DropTable("store.SkinItemInstance");
            DropTable("store.AmmoItemInstance");
            DropTable("store.WeaponItemInstance");
            DropTable("store.ArmourItemInstance");
            DropTable("store.CharacterItemInstance");
            DropTable("players.InstanceOfPlayerKit");
            DropTable("players.InstanceOfItemAddon");
            DropTable("players.InstanceOfItemSkin");
            DropTable("players.InstanceOfPlayerArmour");
            DropTable("players.InstanceOfPlayerWeapon");
            DropTable("players.InstanceOfPlayerCharacter");
            DropTable("gamemode.TeamGameModeEntity");
            DropTable("vrs.Roles");
            DropTable("players.PlayerPromoCode");
            DropTable("public.GlobalChatMessage");
            DropTable("public.GameSessionEntity");
            DropTable("gamemode.AbstractGameModeEntity");
            DropTable("gamemode.GameMapEntity");
            DropTable("vrs.UserRoles");
            DropTable("vrs.UserLogins");
            DropTable("vrs.UserClaims");
            DropTable("vrs.Users");
            DropTable("public.ClusterInstanceEntity");
            DropTable("public.ClusterNodeEntity");
            DropTable("matches.MatchTeam");
            DropTable("matches.MemberAchievements");
            DropTable("matches.MatchMember");
            DropTable("matches.MatchEntity");
            DropTable("public.SessionQueueEntity");
            DropTable("public.SessionQueueMember");
            DropTable("public.PrivateChatMessageData");
            DropTable("public.PrivateChatMessage");
            DropTable("league.LeagueEntity");
            DropTable("league.LeagueMemberEntity");
            DropTable("players.PlayerProfileItemEntity");
            DropTable("players.PlayerProfileEntity");
            DropTable("players.InstanceOfInstalledAddon");
            DropTable("store.FractionEntity");
            DropTable("store.AddonItemContainer");
            DropTable("store.AbstractItemInstance");
            DropTable("players.InstanceOfTargetItem");
            DropTable("players.InstanceOfPlayerItem");
            DropTable("players.FriendEntity");
            DropTable("players.PlayerReputationEntity");
            DropTable("achievements.AchievementInstance");
            DropTable("achievements.AchievementContainer");
            DropTable("players.PlayerAchievement");
            DropTable("players.PlayerEntity");
        }
    }
}
