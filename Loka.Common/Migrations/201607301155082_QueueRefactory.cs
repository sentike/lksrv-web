namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QueueRefactory : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.QueueMember", "MatchMemberId", "matches.MatchMember");
            DropForeignKey("public.QueueMember", "PlayerId", "players.PlayerEntity");
            DropForeignKey("public.QueueMember", "QueueId", "public.QueueEntity");
            DropIndex("public.QueueMember", new[] { "PlayerId" });
            DropIndex("public.QueueMember", new[] { "QueueId" });
            DropIndex("public.QueueMember", new[] { "MatchMemberId" });
            AddColumn("public.QueueEntity", "MatchMemberId", c => c.Guid());
            AddColumn("public.QueueEntity", "PartyId", c => c.Guid(nullable: false));
            CreateIndex("public.QueueEntity", "MatchMemberId");
            CreateIndex("public.QueueEntity", "PartyId");
            AddForeignKey("public.QueueEntity", "MatchMemberId", "matches.MatchMember", "MemberId");
            AddForeignKey("public.QueueEntity", "PartyId", "public.QueueEntity", "QueueId");
            DropTable("public.QueueMember");
        }
        
        public override void Down()
        {
            CreateTable(
                "public.QueueMember",
                c => new
                    {
                        PlayerId = c.Guid(nullable: false),
                        QueueId = c.Guid(nullable: false),
                        MatchMemberId = c.Guid(),
                        Ready = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PlayerId);
            
            DropForeignKey("public.QueueEntity", "PartyId", "public.QueueEntity");
            DropForeignKey("public.QueueEntity", "MatchMemberId", "matches.MatchMember");
            DropIndex("public.QueueEntity", new[] { "PartyId" });
            DropIndex("public.QueueEntity", new[] { "MatchMemberId" });
            DropColumn("public.QueueEntity", "PartyId");
            DropColumn("public.QueueEntity", "MatchMemberId");
            CreateIndex("public.QueueMember", "MatchMemberId");
            CreateIndex("public.QueueMember", "QueueId");
            CreateIndex("public.QueueMember", "PlayerId");
            AddForeignKey("public.QueueMember", "QueueId", "public.QueueEntity", "QueueId", cascadeDelete: true);
            AddForeignKey("public.QueueMember", "PlayerId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
            AddForeignKey("public.QueueMember", "MatchMemberId", "matches.MatchMember", "MemberId");
        }
    }
}
