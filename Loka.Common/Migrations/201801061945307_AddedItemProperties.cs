namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedItemProperties : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "store.ItemInstancePropertyEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                        Key = c.Int(nullable: false),
                        EditDate = c.DateTime(nullable: false),
                        Hash = c.Long(nullable: false),
                        Last = c.String(),
                        Value = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId }, cascadeDelete: true)
                .Index(t => new { t.ModelId, t.CategoryTypeId, t.Key }, unique: true, name: "ix_model_cat_key");
            
            CreateTable(
                "building.WorldBuildingPropertyEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        BuildingId = c.Guid(nullable: false),
                        PropertyId = c.Guid(nullable: false),
                        Value = c.String(nullable: false, maxLength: 64),
                        Last = c.String(),
                        Hash = c.Long(nullable: false),
                        EditDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("store.ItemInstancePropertyEntity", t => t.PropertyId, cascadeDelete: true)
                .ForeignKey("building.WorldBuildingItemEntity", t => t.BuildingId, cascadeDelete: true)
                .Index(t => new { t.BuildingId, t.PropertyId }, unique: true, name: "ix_building_key");
            
        }
        
        public override void Down()
        {
            DropForeignKey("building.WorldBuildingPropertyEntity", "BuildingId", "building.WorldBuildingItemEntity");
            DropForeignKey("building.WorldBuildingPropertyEntity", "PropertyId", "store.ItemInstancePropertyEntity");
            DropForeignKey("store.ItemInstancePropertyEntity", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropIndex("building.WorldBuildingPropertyEntity", "ix_building_key");
            DropIndex("store.ItemInstancePropertyEntity", "ix_model_cat_key");
            DropTable("building.WorldBuildingPropertyEntity");
            DropTable("store.ItemInstancePropertyEntity");
        }
    }
}
