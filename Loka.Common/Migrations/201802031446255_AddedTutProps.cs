namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTutProps : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.TutorialStepEntity", "RewardActivate", c => c.Long(nullable: false));
            AddColumn("store.TutorialStepEntity", "RewardGive", c => c.Long(nullable: false));
            AddColumn("players.InstanceOfPlayerItem", "Hash", c => c.Long(nullable: false));
            AddColumn("players.InstanceOfPlayerItem", "Amount", c => c.Long(nullable: false));
            DropColumn("store.TutorialStepEntity", "ProductId");
        }
        
        public override void Down()
        {
            AddColumn("store.TutorialStepEntity", "ProductId", c => c.Int());
            DropColumn("players.InstanceOfPlayerItem", "Amount");
            DropColumn("players.InstanceOfPlayerItem", "Hash");
            DropColumn("store.TutorialStepEntity", "RewardGive");
            DropColumn("store.TutorialStepEntity", "RewardActivate");
        }
    }
}
