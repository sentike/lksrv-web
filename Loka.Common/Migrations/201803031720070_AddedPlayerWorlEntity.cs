namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerWorlEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "building.PlayerWorlEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        Name = c.String(maxLength: 64),
                        PlayerId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        LastAttackDate = c.DateTime(nullable: false),
                        WorldModelId = c.Short(nullable: false),
                        WorldModelCategory = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("store.AbstractItemInstance", t => new { t.WorldModelId, t.WorldModelCategory }, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => new { t.WorldModelId, t.WorldModelCategory });
            
            AddColumn("building.WorldBuildingItemEntity", "WorldId", c => c.Guid());
            CreateIndex("building.WorldBuildingItemEntity", "WorldId");
            AddForeignKey("building.WorldBuildingItemEntity", "WorldId", "building.PlayerWorlEntity", "EntityId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("building.PlayerWorlEntity", new[] { "WorldModelId", "WorldModelCategory" }, "store.AbstractItemInstance");
            DropForeignKey("building.WorldBuildingItemEntity", "WorldId", "building.PlayerWorlEntity");
            DropForeignKey("building.PlayerWorlEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("building.WorldBuildingItemEntity", new[] { "WorldId" });
            DropIndex("building.PlayerWorlEntity", new[] { "WorldModelId", "WorldModelCategory" });
            DropIndex("building.PlayerWorlEntity", new[] { "PlayerId" });
            DropColumn("building.WorldBuildingItemEntity", "WorldId");
            DropTable("building.PlayerWorlEntity");
        }
    }
}
