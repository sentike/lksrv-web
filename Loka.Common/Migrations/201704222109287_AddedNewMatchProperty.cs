namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNewMatchProperty : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "gamemode.HarvestGameMode",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.GameModeEntity", t => t.GameModeId)
                .Index(t => t.GameModeId);
            
            AddColumn("matches.MatchEntity", "UniversalId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropForeignKey("gamemode.HarvestGameMode", "GameModeId", "gamemode.GameModeEntity");
            DropIndex("gamemode.HarvestGameMode", new[] { "GameModeId" });
            DropColumn("matches.MatchEntity", "UniversalId");
            DropTable("gamemode.HarvestGameMode");
        }
    }
}
