namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRowVersion2 : DbMigration
    {
        public override void Up()
        {

            DropColumn("players.PlayerEntity", "RowVersion");
            DropColumn("public.QueueEntity", "RowVersion");
            DropColumn("public.QueueDuelEntity", "RowVersion");
            DropColumn("public.ClusterNodeEntity", "RowVersion");
            DropColumn("public.ClusterInstanceEntity", "RowVersion");
            DropColumn("players.InstanceOfPlayerItem", "RowVersion");
            DropColumn("players.PlayerProfileEntity", "RowVersion");
            DropColumn("players.PlayerUsingServiceEntity", "RowVersion");

            AddColumn("players.PlayerEntity", "RowVersion", c => c.Guid(nullable: false));
            AddColumn("public.QueueEntity", "RowVersion", c => c.Guid(nullable: false));
            AddColumn("public.QueueDuelEntity", "RowVersion", c => c.Guid(nullable: false));
            AddColumn("public.ClusterNodeEntity", "RowVersion", c => c.Guid(nullable: false));
            AddColumn("public.ClusterInstanceEntity", "RowVersion", c => c.Guid(nullable: false));
            AddColumn("players.InstanceOfPlayerItem", "RowVersion", c => c.Guid(nullable: false));
            AddColumn("players.PlayerProfileEntity", "RowVersion", c => c.Guid(nullable: false));
            AddColumn("players.PlayerUsingServiceEntity", "RowVersion", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "RowVersion");
            DropColumn("public.QueueEntity", "RowVersion");
            DropColumn("public.QueueDuelEntity", "RowVersion");
            DropColumn("public.ClusterNodeEntity", "RowVersion");
            DropColumn("public.ClusterInstanceEntity", "RowVersion");
            DropColumn("players.InstanceOfPlayerItem", "RowVersion");
            DropColumn("players.PlayerProfileEntity", "RowVersion");
            DropColumn("players.PlayerUsingServiceEntity", "RowVersion");

            AddColumn("players.PlayerUsingServiceEntity", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("players.PlayerProfileEntity", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("players.InstanceOfPlayerItem", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("public.ClusterInstanceEntity", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("public.ClusterNodeEntity", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("public.QueueDuelEntity", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("public.QueueEntity", "RowVersion", c => c.DateTime(nullable: false));
            AddColumn("players.PlayerEntity", "RowVersion", c => c.DateTime(nullable: false));
        }
    }
}
