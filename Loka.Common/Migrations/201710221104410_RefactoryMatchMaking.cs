namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoryMatchMaking : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.QueueEntity", "MatchId", "matches.MatchEntity");
            DropForeignKey("public.QueueEntity", "MatchMemberId", "matches.MatchMember");
            DropIndex("public.QueueEntity", new[] { "MatchId" });
            DropIndex("public.QueueEntity", new[] { "MatchMemberId" });

            DropColumn("public.QueueEntity", "Ready");
            AddColumn("public.QueueEntity", "Ready", c => c.Int(nullable: false));

            DropColumn("players.PlayerEntity", "ClusterRegion");
            DropColumn("public.QueueEntity", "MatchId");
            DropColumn("public.QueueEntity", "MatchMemberId");
        }
        
        public override void Down()
        {
            AddColumn("public.QueueEntity", "MatchMemberId", c => c.Guid());
            AddColumn("public.QueueEntity", "MatchId", c => c.Guid());
            AddColumn("players.PlayerEntity", "ClusterRegion", c => c.Short(nullable: false));
            AlterColumn("public.QueueEntity", "Ready", c => c.Boolean(nullable: false));
            CreateIndex("public.QueueEntity", "MatchMemberId");
            CreateIndex("public.QueueEntity", "MatchId");
            AddForeignKey("public.QueueEntity", "MatchMemberId", "matches.MatchMember", "MemberId");
            AddForeignKey("public.QueueEntity", "MatchId", "matches.MatchEntity", "MatchId");
        }
    }
}
