namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedSentenceTitle : DbMigration
    {
        public override void Up()
        {
            DropColumn("improvement.SentenceEntity", "Title");
        }
        
        public override void Down()
        {
            AddColumn("improvement.SentenceEntity", "Title", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
