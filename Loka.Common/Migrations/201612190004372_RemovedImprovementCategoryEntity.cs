namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedImprovementCategoryEntity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("improvement.ImprovementEntity", "CategoryId", "improvement.ImprovementCategoryEntity");
            DropForeignKey("improvement.SentenceEntity", "CategoryId", "improvement.ImprovementCategoryEntity");
            DropIndex("improvement.SentenceEntity", new[] { "CategoryId" });
            DropIndex("improvement.ImprovementEntity", new[] { "CategoryId" });
            DropTable("improvement.ImprovementCategoryEntity");
        }
        
        public override void Down()
        {
            CreateTable(
                "improvement.ImprovementCategoryEntity",
                c => new
                    {
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateIndex("improvement.ImprovementEntity", "CategoryId");
            CreateIndex("improvement.SentenceEntity", "CategoryId");
            AddForeignKey("improvement.SentenceEntity", "CategoryId", "improvement.ImprovementCategoryEntity", "CategoryId", cascadeDelete: true);
            AddForeignKey("improvement.ImprovementEntity", "CategoryId", "improvement.ImprovementCategoryEntity", "CategoryId", cascadeDelete: true);
        }
    }
}
