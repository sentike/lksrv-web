namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBaseTemplateEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "tamplate.PlayerTemplateEntity",
                c => new
                    {
                        PlayerTemplateId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        TemplateId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PlayerTemplateId)
                .ForeignKey("tamplate.BaseTemplateEntity", t => t.TemplateId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.TemplateId);
            
            CreateTable(
                "tamplate.BaseTemplateEntity",
                c => new
                    {
                        TemplateId = c.Guid(nullable: false),
                        Name = c.String(),
                        Cost_Amount = c.Int(nullable: false),
                        Cost_IsDonate = c.Boolean(nullable: false),
                        CreatorId = c.Guid(nullable: false),
                        Downloads = c.Long(nullable: false),
                        Likes = c.Long(nullable: false),
                        Views = c.Long(nullable: false),
                        AllowSell = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TemplateId)
                .ForeignKey("players.PlayerEntity", t => t.CreatorId, cascadeDelete: true)
                .Index(t => t.CreatorId);
            
            CreateTable(
                "tamplate.TemplateBuildingEntity",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                        TemplateId = c.Guid(nullable: false),
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                        Position_X = c.Single(nullable: false),
                        Position_Y = c.Single(nullable: false),
                        Position_Z = c.Single(nullable: false),
                        Rotation_Pitch = c.Single(nullable: false),
                        Rotation_Roll = c.Single(nullable: false),
                        Rotation_Yaw = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId }, cascadeDelete: true)
                .ForeignKey("tamplate.BaseTemplateEntity", t => t.TemplateId, cascadeDelete: true)
                .Index(t => t.TemplateId)
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
            AlterColumn("building.WorldBuildingAttachEntity", "AttachFrom", c => c.Short(nullable: false));
            AlterColumn("building.WorldBuildingAttachEntity", "AttachTo", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("tamplate.BaseTemplateEntity", "CreatorId", "players.PlayerEntity");
            DropForeignKey("tamplate.PlayerTemplateEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("tamplate.PlayerTemplateEntity", "TemplateId", "tamplate.BaseTemplateEntity");
            DropForeignKey("tamplate.TemplateBuildingEntity", "TemplateId", "tamplate.BaseTemplateEntity");
            DropForeignKey("tamplate.TemplateBuildingEntity", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropIndex("tamplate.TemplateBuildingEntity", new[] { "ModelId", "CategoryTypeId" });
            DropIndex("tamplate.TemplateBuildingEntity", new[] { "TemplateId" });
            DropIndex("tamplate.BaseTemplateEntity", new[] { "CreatorId" });
            DropIndex("tamplate.PlayerTemplateEntity", new[] { "TemplateId" });
            DropIndex("tamplate.PlayerTemplateEntity", new[] { "PlayerId" });
            AlterColumn("building.WorldBuildingAttachEntity", "AttachTo", c => c.Int(nullable: false));
            AlterColumn("building.WorldBuildingAttachEntity", "AttachFrom", c => c.Int(nullable: false));
            DropTable("tamplate.TemplateBuildingEntity");
            DropTable("tamplate.BaseTemplateEntity");
            DropTable("tamplate.PlayerTemplateEntity");
        }
    }
}
