namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedWorldBuildingItemEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "building.WorldBuildingAttachEntity",
                c => new
                    {
                        AttachId = c.Guid(nullable: false),
                        ItemId = c.Guid(nullable: false),
                        TargetId = c.Guid(nullable: false),
                        AttachFrom = c.Int(nullable: false),
                        AttachTo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AttachId)
                .ForeignKey("building.WorldBuildingItemEntity", t => t.TargetId, cascadeDelete: true, name:"fkattach1")
                .ForeignKey("building.WorldBuildingItemEntity", t => t.ItemId, cascadeDelete: true, name: "fkattach2")
                .Index(t => t.ItemId)
                .Index(t => t.TargetId);
            
            CreateTable(
                "building.WorldBuildingItemEntity",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        ModelId = c.Int(nullable: false),
                        Position_X = c.Single(nullable: false),
                        Position_Y = c.Single(nullable: false),
                        Position_Z = c.Single(nullable: false),
                        Rotation_Pitch = c.Single(nullable: false),
                        Rotation_Roll = c.Single(nullable: false),
                        Rotation_Yaw = c.Single(nullable: false),
                        Health = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("building.WorldBuildingAttachEntity", "ItemId", "building.WorldBuildingItemEntity");
            DropForeignKey("building.WorldBuildingItemEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("building.WorldBuildingAttachEntity", "TargetId", "building.WorldBuildingItemEntity");
            DropIndex("building.WorldBuildingItemEntity", new[] { "PlayerId" });
            DropIndex("building.WorldBuildingAttachEntity", new[] { "TargetId" });
            DropIndex("building.WorldBuildingAttachEntity", new[] { "ItemId" });
            DropTable("building.WorldBuildingItemEntity");
            DropTable("building.WorldBuildingAttachEntity");
        }
    }
}
