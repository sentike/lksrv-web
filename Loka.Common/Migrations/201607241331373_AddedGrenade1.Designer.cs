// <auto-generated />
namespace Loka.Server.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddedGrenade1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedGrenade1));
        
        string IMigrationMetadata.Id
        {
            get { return "201607241331373_AddedGrenade1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
