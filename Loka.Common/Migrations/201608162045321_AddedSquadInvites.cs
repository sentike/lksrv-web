namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSquadInvites : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.SquadInviteEntity",
                c => new
                    {
                        InviteId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid()"),
                        PlayerId = c.Guid(nullable: false),
                        SquadId = c.Guid(nullable: false),
                        State = c.Int(nullable: false),
                        SubmittedDate = c.DateTime(nullable: false, defaultValueSql: "Now()"),
                    })
                .PrimaryKey(t => t.InviteId)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("public.QueueEntity", t => t.SquadId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.SquadId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("public.SquadInviteEntity", "SquadId", "public.QueueEntity");
            DropForeignKey("public.SquadInviteEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("public.SquadInviteEntity", new[] { "SquadId" });
            DropIndex("public.SquadInviteEntity", new[] { "PlayerId" });
            DropTable("public.SquadInviteEntity");
        }
    }
}
