namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoryTransactions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "transcaction.WithdrawTransactionEntity",
                c => new
                    {
                        TransactionId = c.Guid(nullable: false),
                        Status = c.Int(nullable: false),
                        ConfirmationDate = c.DateTime(),
                        PaymentId = c.Long(),
                        Amount = c.Long(nullable: false),
                        To = c.String(nullable: false, maxLength: 96),
                        CurrencyId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.TransactionId)
                .ForeignKey("transcaction.AbstractTransactionEntity", t => t.TransactionId, true)
                .Index(t => t.TransactionId);
            
            AddColumn("transcaction.DepositTransactionEntity", "Amount", c => c.Long(nullable: false));
            AddColumn("transcaction.PaymentTransactionEntity", "Amount_Amount", c => c.Int(nullable: false));
            AddColumn("transcaction.PaymentTransactionEntity", "Amount_IsDonate", c => c.Boolean(nullable: false));
            DropColumn("transcaction.AbstractTransactionEntity", "Amount_Amount");
            DropColumn("transcaction.AbstractTransactionEntity", "Amount_IsDonate");
        }
        
        public override void Down()
        {
            AddColumn("transcaction.AbstractTransactionEntity", "Amount_IsDonate", c => c.Boolean(nullable: false));
            AddColumn("transcaction.AbstractTransactionEntity", "Amount_Amount", c => c.Int(nullable: false));
            DropForeignKey("transcaction.WithdrawTransactionEntity", "TransactionId", "transcaction.AbstractTransactionEntity");
            DropIndex("transcaction.WithdrawTransactionEntity", new[] { "TransactionId" });
            DropColumn("transcaction.PaymentTransactionEntity", "Amount_IsDonate");
            DropColumn("transcaction.PaymentTransactionEntity", "Amount_Amount");
            DropColumn("transcaction.DepositTransactionEntity", "Amount");
            DropTable("transcaction.WithdrawTransactionEntity");
        }
    }
}
