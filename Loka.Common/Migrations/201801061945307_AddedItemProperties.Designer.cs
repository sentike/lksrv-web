// <auto-generated />
namespace Loka.Server.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddedItemProperties : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedItemProperties));
        
        string IMigrationMetadata.Id
        {
            get { return "201801061945307_AddedItemProperties"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
