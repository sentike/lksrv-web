namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNodeStartedDates : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchEntity", "StartedOfProcecess", c => c.DateTime());
            RenameColumn("matches.MatchEntity", "Started", "StartedOfMatch");
        }
        
        public override void Down()
        {
            RenameColumn("matches.MatchEntity", "StartedOfMatch", "Started");
            DropColumn("matches.MatchEntity", "StartedOfProcecess");
        }
    }
}
