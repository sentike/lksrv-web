namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedScoreForMatchStatistic : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchMember", "Statistic_Service", c => c.Int(nullable: false));
            AddColumn("matches.MatchMember", "Statistic_Score", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("matches.MatchMember", "Statistic_Score");
            DropColumn("matches.MatchMember", "Statistic_Service");
        }
    }
}
