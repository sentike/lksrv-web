namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsingIsAllowJoinNewPlayers : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchEntity", "IsAllowJoinNewPlayers", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("matches.MatchEntity", "IsAllowJoinNewPlayers");
        }
    }
}
