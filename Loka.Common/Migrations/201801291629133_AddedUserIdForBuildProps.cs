namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUserIdForBuildProps : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.ItemInstancePropertyEntity", "Default", c => c.String(maxLength: 64));
            AddColumn("building.WorldBuildingPropertyEntity", "PlayerId", c => c.Guid(nullable: false));
            AlterColumn("store.ItemInstancePropertyEntity", "Last", c => c.String(maxLength: 64));
            AlterColumn("building.WorldBuildingPropertyEntity", "Last", c => c.String(maxLength: 64));
            CreateIndex("building.WorldBuildingPropertyEntity", "PlayerId");
            AddForeignKey("building.WorldBuildingPropertyEntity", "PlayerId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("building.WorldBuildingPropertyEntity", "PlayerId", "players.PlayerEntity");
            DropIndex("building.WorldBuildingPropertyEntity", new[] { "PlayerId" });
            AlterColumn("building.WorldBuildingPropertyEntity", "Last", c => c.String());
            AlterColumn("store.ItemInstancePropertyEntity", "Last", c => c.String());
            DropColumn("building.WorldBuildingPropertyEntity", "PlayerId");
            DropColumn("store.ItemInstancePropertyEntity", "Default");
        }
    }
}
