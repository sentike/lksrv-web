namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedInstanceOfPlayerItemSingleton : DbMigration
    {
        public override void Up()
        {
            CreateIndex("players.InstanceOfPlayerItem", new[] { "PlayerId", "ModelId", "CategoryTypeId" }, unique: true, name: "ix_instanceofplayerItem_itemsingleton");
        }
        
        public override void Down()
        {
            DropIndex("players.InstanceOfPlayerItem", "ix_instanceofplayerItem_itemsingleton");
        }
    }
}
