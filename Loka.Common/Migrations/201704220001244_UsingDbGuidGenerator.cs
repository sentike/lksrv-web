namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsingDbGuidGenerator : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("building.WorldBuildingAttachEntity", "TargetId", "building.WorldBuildingItemEntity");
            //DropForeignKey("building.WorldBuildingAttachEntity", "ItemId", "building.WorldBuildingItemEntity");
            //DropPrimaryKey("building.WorldBuildingItemEntity");
            //DropPrimaryKey("building.WorldBuildingAttachEntity");
            //AlterColumn("building.WorldBuildingItemEntity", "ItemId", c => c.Guid(nullable: false, identity: true));
            //AlterColumn("building.WorldBuildingAttachEntity", "AttachId", c => c.Guid(nullable: false, identity: true));
            //AddPrimaryKey("building.WorldBuildingItemEntity", "ItemId");
            //AddPrimaryKey("building.WorldBuildingAttachEntity", "AttachId");
            //AddForeignKey("building.WorldBuildingAttachEntity", "TargetId", "building.WorldBuildingItemEntity", "ItemId", cascadeDelete: true);
            //AddForeignKey("building.WorldBuildingAttachEntity", "ItemId", "building.WorldBuildingItemEntity", "ItemId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("building.WorldBuildingAttachEntity", "ItemId", "building.WorldBuildingItemEntity");
            DropForeignKey("building.WorldBuildingAttachEntity", "TargetId", "building.WorldBuildingItemEntity");
            DropPrimaryKey("building.WorldBuildingAttachEntity");
            DropPrimaryKey("building.WorldBuildingItemEntity");
            AlterColumn("building.WorldBuildingAttachEntity", "AttachId", c => c.Guid(nullable: false));
            AlterColumn("building.WorldBuildingItemEntity", "ItemId", c => c.Guid(nullable: false));
            AddPrimaryKey("building.WorldBuildingAttachEntity", "AttachId");
            AddPrimaryKey("building.WorldBuildingItemEntity", "ItemId");
            AddForeignKey("building.WorldBuildingAttachEntity", "ItemId", "building.WorldBuildingItemEntity", "ItemId", cascadeDelete: true);
            AddForeignKey("building.WorldBuildingAttachEntity", "TargetId", "building.WorldBuildingItemEntity", "ItemId", cascadeDelete: true);
        }
    }
}
