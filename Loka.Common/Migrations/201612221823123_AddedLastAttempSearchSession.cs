namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedLastAttempSearchSession : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.QueueEntity", "LastAttempSearchSession", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("public.QueueEntity", "LastAttempSearchSession");
        }
    }
}
