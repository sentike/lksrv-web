namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAbstractMatchFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchEntity", "AbstractMatchFlag", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("matches.MatchEntity", "AbstractMatchFlag");
        }
    }
}
