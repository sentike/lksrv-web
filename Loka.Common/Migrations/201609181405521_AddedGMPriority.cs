namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGMPriority : DbMigration
    {
        public override void Up()
        {
            AddColumn("gamemode.AbstractGameModeEntity", "Priority", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("gamemode.AbstractGameModeEntity", "Priority");
        }
    }
}
