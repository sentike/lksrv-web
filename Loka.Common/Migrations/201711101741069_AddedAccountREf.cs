namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAccountREf : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("transaction.MatchTransactionEntity", "AccountId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
            AddForeignKey("transaction.OrderTransactionEntity", "AccountId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
            AddForeignKey("transaction.StoreTransactionEntity", "AccountId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("transaction.StoreTransactionEntity", "AccountId", "players.PlayerEntity");
            DropForeignKey("transaction.OrderTransactionEntity", "AccountId", "players.PlayerEntity");
            DropForeignKey("transaction.MatchTransactionEntity", "AccountId", "players.PlayerEntity");
        }
    }
}
