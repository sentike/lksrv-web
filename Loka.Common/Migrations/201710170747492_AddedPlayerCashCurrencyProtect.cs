namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerCashCurrencyProtect : DbMigration
    {
        public override void Up()
        {
            //DropIndex("players.PlayerCashEntity", new[] { "PlayerId" });
            CreateIndex("players.PlayerCashEntity", new[] { "PlayerId", "Currency" }, unique: true, name: "PlayerId_Currency");
        }
        
        public override void Down()
        {
            DropIndex("players.PlayerCashEntity", "PlayerId_Currency");
            //CreateIndex("players.PlayerCashEntity", "PlayerId");
        }
    }
}
