namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Refactory1 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "players.ProfileItemEntity", newName: "PlayerProfileItemEntity");
        }
        
        public override void Down()
        {
            RenameTable(name: "players.PlayerProfileItemEntity", newName: "ProfileItemEntity");
        }
    }
}
