namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDuelProps : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.SquadInviteEntity", "Type", c => c.Int(nullable: false));
            AddColumn("gamemode.AbstractGameModeEntity", "JoinCost_Amount", c => c.Int(nullable: false));
            AddColumn("gamemode.AbstractGameModeEntity", "JoinCost_IsDonate", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("gamemode.AbstractGameModeEntity", "JoinCost_IsDonate");
            DropColumn("gamemode.AbstractGameModeEntity", "JoinCost_Amount");
            DropColumn("public.SquadInviteEntity", "Type");
        }
    }
}
