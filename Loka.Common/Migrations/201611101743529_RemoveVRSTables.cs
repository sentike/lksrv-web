namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveVRSTables : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("vrs.UserClaims", "UserId", "vrs.Users");
            DropForeignKey("vrs.UserLogins", "UserId", "vrs.Users");
            DropForeignKey("players.PlayerEntity", "PlayerId", "vrs.Users");
            DropForeignKey("vrs.UserRoles", "UserId", "vrs.Users");
            DropForeignKey("vrs.UserRoles", "RoleId", "vrs.Roles");
            DropIndex("players.PlayerEntity", new[] { "PlayerId" });
            DropIndex("vrs.Users", new[] { "RegistrationDate" });
            DropIndex("vrs.Users", new[] { "LastActivityDate" });
            DropIndex("vrs.Users", new[] { "Email" });
            DropIndex("vrs.Users", new[] { "PhoneNumber" });
            DropIndex("vrs.Users", new[] { "UserName" });
            DropIndex("vrs.UserClaims", new[] { "UserId" });
            DropIndex("vrs.UserLogins", new[] { "UserId" });
            DropIndex("vrs.UserRoles", new[] { "UserId" });
            DropIndex("vrs.UserRoles", new[] { "RoleId" });
            DropIndex("vrs.Roles", "RoleNameIndex");
            DropTable("vrs.Users");
            DropTable("vrs.UserClaims");
            DropTable("vrs.UserLogins");
            DropTable("vrs.UserRoles");
            DropTable("vrs.Roles");
        }
        
        public override void Down()
        {
            CreateTable(
                "vrs.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "vrs.UserRoles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId });
            
            CreateTable(
                "vrs.UserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false),
                        AllowIdentity = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId });
            
            CreateTable(
                "vrs.UserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "vrs.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Balance = c.Long(nullable: false),
                        FirstName = c.String(maxLength: 16),
                        MiddleName = c.String(maxLength: 16),
                        LastName = c.String(maxLength: 16),
                        Gender = c.Int(nullable: false),
                        Country = c.Int(nullable: false),
                        Language = c.Int(nullable: false),
                        City = c.Int(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false),
                        LastActivityDate = c.DateTime(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                        Email = c.String(maxLength: 64),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("vrs.Roles", "Name", unique: true, name: "RoleNameIndex");
            CreateIndex("vrs.UserRoles", "RoleId");
            CreateIndex("vrs.UserRoles", "UserId");
            CreateIndex("vrs.UserLogins", "UserId");
            CreateIndex("vrs.UserClaims", "UserId");
            CreateIndex("vrs.Users", "UserName", unique: true);
            CreateIndex("vrs.Users", "PhoneNumber", unique: true);
            CreateIndex("vrs.Users", "Email", unique: true);
            CreateIndex("vrs.Users", "LastActivityDate");
            CreateIndex("vrs.Users", "RegistrationDate");
            CreateIndex("players.PlayerEntity", "PlayerId");
            AddForeignKey("vrs.UserRoles", "RoleId", "vrs.Roles", "Id", cascadeDelete: true);
            AddForeignKey("vrs.UserRoles", "UserId", "vrs.Users", "Id", cascadeDelete: true);
            AddForeignKey("players.PlayerEntity", "PlayerId", "vrs.Users", "Id");
            AddForeignKey("vrs.UserLogins", "UserId", "vrs.Users", "Id", cascadeDelete: true);
            AddForeignKey("vrs.UserClaims", "UserId", "vrs.Users", "Id", cascadeDelete: true);
        }
    }
}
