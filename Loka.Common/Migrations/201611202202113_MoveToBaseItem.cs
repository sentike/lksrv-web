namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoveToBaseItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.AbstractItemInstance", "TargetProfileId", c => c.Int(nullable: false));
            DropColumn("store.CharacterItemInstance", "TargetProfileId");
        }
        
        public override void Down()
        {
            AddColumn("store.CharacterItemInstance", "TargetProfileId", c => c.Int(nullable: false));
            DropColumn("store.AbstractItemInstance", "TargetProfileId");
        }
    }
}
