namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNewSquadRefs : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("public.SquadInviteEntity", "SquadId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("public.SquadInviteEntity", "SquadId", "players.PlayerEntity");
        }
    }
}
