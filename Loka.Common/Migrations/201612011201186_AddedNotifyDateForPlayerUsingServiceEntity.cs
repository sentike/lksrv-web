namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNotifyDateForPlayerUsingServiceEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerUsingServiceEntity", "NotifyDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerUsingServiceEntity", "NotifyDate");
        }
    }
}
