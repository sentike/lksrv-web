namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProductIdForTWB : DbMigration
    {
        public override void Up()
        {
            AddColumn("tps.tpsbuild", "ProductId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("tps.tpsbuild", "ProductId");
        }
    }
}
