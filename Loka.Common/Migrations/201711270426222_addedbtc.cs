namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedbtc : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchMember", "Award_Bitcoin", c => c.Long(nullable: false));
            AlterColumn("matches.MatchMember", "Award_Money", c => c.Long(nullable: false));
            AlterColumn("matches.MatchMember", "Award_Donate", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("matches.MatchMember", "Award_Donate", c => c.Int(nullable: false));
            AlterColumn("matches.MatchMember", "Award_Money", c => c.Int(nullable: false));
            DropColumn("matches.MatchMember", "Award_Bitcoin");
        }
    }
}
