namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFractionAchievementId : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.FractionEntity", "FractionAchievementId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("store.FractionEntity", "FractionAchievementId");
        }
    }
}
