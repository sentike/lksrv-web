namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveAutoGenerateKey : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("building.WorldBuildingAttachEntity", "TargetId", "building.WorldBuildingItemEntity");
            //DropForeignKey("building.WorldBuildingAttachEntity", "ItemId", "building.WorldBuildingItemEntity");
            //DropPrimaryKey("building.WorldBuildingItemEntity");
            //AlterColumn("building.WorldBuildingItemEntity", "ItemId", c => c.Guid(nullable: false));
            //AddPrimaryKey("building.WorldBuildingItemEntity", "ItemId");
            //AddForeignKey("building.WorldBuildingAttachEntity", "TargetId", "building.WorldBuildingItemEntity", "ItemId", cascadeDelete: true);
            //AddForeignKey("building.WorldBuildingAttachEntity", "ItemId", "building.WorldBuildingItemEntity", "ItemId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("building.WorldBuildingAttachEntity", "ItemId", "building.WorldBuildingItemEntity");
            DropForeignKey("building.WorldBuildingAttachEntity", "TargetId", "building.WorldBuildingItemEntity");
            DropPrimaryKey("building.WorldBuildingItemEntity");
            AlterColumn("building.WorldBuildingItemEntity", "ItemId", c => c.Guid(nullable: false));
            AddPrimaryKey("building.WorldBuildingItemEntity", "ItemId");
            AddForeignKey("building.WorldBuildingAttachEntity", "ItemId", "building.WorldBuildingItemEntity", "ItemId", cascadeDelete: true);
            AddForeignKey("building.WorldBuildingAttachEntity", "TargetId", "building.WorldBuildingItemEntity", "ItemId", cascadeDelete: true);
        }
    }
}
