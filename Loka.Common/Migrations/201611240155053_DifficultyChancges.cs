namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DifficultyChancges : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchEntity", "Difficulty", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("matches.MatchEntity", "Difficulty");
        }
    }
}
