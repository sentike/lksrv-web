namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNullableReward : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("store.TutorialStepEntity", new[] { "RewardModelId", "RewardCategoryId" }, "store.AbstractItemInstance");
            DropIndex("store.TutorialStepEntity", new[] { "RewardModelId", "RewardCategoryId" });
            AlterColumn("store.TutorialStepEntity", "RewardCategoryId", c => c.Int());
            AlterColumn("store.TutorialStepEntity", "RewardModelId", c => c.Short());
            CreateIndex("store.TutorialStepEntity", new[] { "RewardModelId", "RewardCategoryId" });
            AddForeignKey("store.TutorialStepEntity", new[] { "RewardModelId", "RewardCategoryId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" });
        }
        
        public override void Down()
        {
            DropForeignKey("store.TutorialStepEntity", new[] { "RewardModelId", "RewardCategoryId" }, "store.AbstractItemInstance");
            DropIndex("store.TutorialStepEntity", new[] { "RewardModelId", "RewardCategoryId" });
            AlterColumn("store.TutorialStepEntity", "RewardModelId", c => c.Short(nullable: false));
            AlterColumn("store.TutorialStepEntity", "RewardCategoryId", c => c.Int(nullable: false));
            CreateIndex("store.TutorialStepEntity", new[] { "RewardModelId", "RewardCategoryId" });
            AddForeignKey("store.TutorialStepEntity", new[] { "RewardModelId", "RewardCategoryId" }, "store.AbstractItemInstance", new[] { "ModelId", "CategoryTypeId" }, cascadeDelete: true);
        }
    }
}
