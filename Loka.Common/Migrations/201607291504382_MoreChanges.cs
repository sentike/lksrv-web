namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoreChanges : DbMigration
    {
        public override void Up()
        {
            AlterColumn("players.PlayerEntity", "Experience_Experience", c => c.Long(nullable: false));
            AlterColumn("players.PlayerSocialTaskEntity", "ConfirmDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("players.PlayerSocialTaskEntity", "ConfirmDate", c => c.DateTime(nullable: false));
            AlterColumn("players.PlayerEntity", "Experience_Experience", c => c.Int(nullable: false));
        }
    }
}
