namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGameModesAndPrepareTime : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "gamemode.LastHeroGameModeEntity",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.AbstractGameModeEntity", t => t.GameModeId, true)
                .Index(t => t.GameModeId);
            
            CreateTable(
                "gamemode.OpenWorldGameModeEntity",
                c => new
                    {
                        GameModeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameModeId)
                .ForeignKey("gamemode.TeamGameModeEntity", t => t.GameModeId, true)
                .Index(t => t.GameModeId);
            
            AddColumn("gamemode.AbstractGameModeEntity", "PrepareTimeInSeconds", c => c.Int(nullable: false, defaultValue: 30));
        }
        
        public override void Down()
        {
            DropForeignKey("gamemode.OpenWorldGameModeEntity", "GameModeId", "gamemode.TeamGameModeEntity");
            DropForeignKey("gamemode.LastHeroGameModeEntity", "GameModeId", "gamemode.AbstractGameModeEntity");
            DropIndex("gamemode.OpenWorldGameModeEntity", new[] { "GameModeId" });
            DropIndex("gamemode.LastHeroGameModeEntity", new[] { "GameModeId" });
            DropColumn("gamemode.AbstractGameModeEntity", "PrepareTimeInSeconds");
            DropTable("gamemode.OpenWorldGameModeEntity");
            DropTable("gamemode.LastHeroGameModeEntity");
        }
    }
}
