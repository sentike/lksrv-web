namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSessionKostil : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.ClusterNodeEntity", "LastSessionInstanceActivityDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("public.ClusterNodeEntity", "LastSessionInstanceActivityDate");
        }
    }
}
