namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlayerRoleField : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "Role", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("players.PlayerEntity", "Role");
        }
    }
}
