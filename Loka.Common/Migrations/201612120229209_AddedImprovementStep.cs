namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedImprovementStep : DbMigration
    {
        public override void Up()
        {
            AddColumn("improvement.ImprovementEntity", "Step", c => c.Short(nullable: false));
            AddColumn("improvement.ImprovementVoteEntity", "Step", c => c.Short(nullable: false));
            CreateIndex("improvement.ImprovementVoteEntity", new[] { "PlayerId", "ImprovementId", "Step" }, unique: true, name: "PlayerId_ImprovementId_Step");
        }
        
        public override void Down()
        {
            DropIndex("improvement.ImprovementVoteEntity", "PlayerId_ImprovementId_Step");
            DropColumn("improvement.ImprovementVoteEntity", "Step");
            DropColumn("improvement.ImprovementEntity", "Step");
        }
    }
}
