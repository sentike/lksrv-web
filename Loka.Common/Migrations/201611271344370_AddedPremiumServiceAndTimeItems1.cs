namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPremiumServiceAndTimeItems1 : DbMigration
    {
        public override void Up()
        {
            DropIndex("players.InstanceOfPlayerItem", "PlayerServiceEntity_SubCategory_Player");
            DropIndex("players.PlayerServiceEntity", "PlayerServiceEntity_SubCategory_Player");
        }
        
        public override void Down()
        {
            CreateIndex("players.PlayerServiceEntity", "SubCategoryTypeId", unique: true, name: "PlayerServiceEntity_SubCategory_Player");
            CreateIndex("players.InstanceOfPlayerItem", "PlayerId", unique: true, name: "PlayerServiceEntity_SubCategory_Player");
        }
    }
}
