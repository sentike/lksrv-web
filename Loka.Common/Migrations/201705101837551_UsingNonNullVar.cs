namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsingNonNullVar : DbMigration
    {
        public override void Up()
        {
            AlterColumn("store.AbstractItemInstance", "Performance", c => c.Long(nullable: false));
            AlterColumn("store.AbstractItemInstance", "Storage", c => c.Long(nullable: false));
            AlterColumn("building.WorldBuildingItemEntity", "Storage", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("building.WorldBuildingItemEntity", "Storage", c => c.Long());
            AlterColumn("store.AbstractItemInstance", "Storage", c => c.Long());
            AlterColumn("store.AbstractItemInstance", "Performance", c => c.Long());
        }
    }
}
