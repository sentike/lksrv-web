namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProductIdForWB : DbMigration
    {
        public override void Up()
        {
            AddColumn("building.WorldBuildingItemEntity", "ProductId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("building.WorldBuildingItemEntity", "ProductId");
        }
    }
}
