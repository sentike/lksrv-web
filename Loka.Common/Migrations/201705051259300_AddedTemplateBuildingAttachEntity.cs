namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTemplateBuildingAttachEntity : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "tamplate.TemplateBuildingEntity", newName: "tpsbuild");
            MoveTable(name: "tamplate.PlayerTemplateEntity", newSchema: "tps");
            MoveTable(name: "tamplate.BaseTemplateEntity", newSchema: "tps");
            MoveTable(name: "tamplate.tpsbuild", newSchema: "tps");
            CreateTable(
                "tps.tpsbuildattach",
                c => new
                    {
                        AttachId = c.Guid(nullable: false, identity: true),
                        ItemId = c.Guid(nullable: false),
                        TargetId = c.Guid(nullable: false),
                        AttachFrom = c.Short(nullable: false),
                        AttachTo = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.AttachId)
                .ForeignKey("tps.tpsbuild", t => t.TargetId, cascadeDelete: true)
                .ForeignKey("tps.tpsbuild", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.TargetId);
            
            AlterColumn("tps.BaseTemplateEntity", "Name", c => c.String(nullable: false, maxLength: 64));
        }
        
        public override void Down()
        {
            DropForeignKey("tps.tpsbuildattach", "ItemId", "tps.tpsbuild");
            DropForeignKey("tps.tpsbuildattach", "TargetId", "tps.tpsbuild");
            DropIndex("tps.tpsbuildattach", new[] { "TargetId" });
            DropIndex("tps.tpsbuildattach", new[] { "ItemId" });
            AlterColumn("tps.BaseTemplateEntity", "Name", c => c.String());
            DropTable("tps.tpsbuildattach");
            MoveTable(name: "tps.tpsbuild", newSchema: "tamplate");
            MoveTable(name: "tps.BaseTemplateEntity", newSchema: "tamplate");
            MoveTable(name: "tps.PlayerTemplateEntity", newSchema: "tamplate");
            RenameTable(name: "tamplate.tpsbuild", newName: "TemplateBuildingEntity");
        }
    }
}
