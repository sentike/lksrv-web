namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCompanyMissions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "players.PlayerMissionEntity",
                c => new
                    {
                        PlayerMissionId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                        MissionTaskId = c.Int(nullable: false),
                        MissionProgress_Current = c.Int(nullable: false),
                        MissionProgress_Last = c.Int(nullable: false),
                        MissionState = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        CompleteDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.PlayerMissionId)
                .ForeignKey("company.ScenarioTaskEntity", t => t.MissionTaskId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => new { t.PlayerId, t.MissionTaskId }, unique: true, name: "PlayerId_MissionTaskId");
            
            CreateTable(
                "company.ScenarioTaskEntity",
                c => new
                    {
                        TaskId = c.Int(nullable: false, identity: true),
                        TaskType = c.Int(nullable: false),
                        MissionId = c.Short(nullable: false),
                        TargetAction = c.Int(nullable: false),
                        TargetAmount = c.Int(nullable: false),
                        EquipmentLevel = c.Short(nullable: false),
                        PeriodOfExecutionInSeconds = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TaskId)
                .ForeignKey("company.ScenarioMissionEntity", t => t.MissionId, cascadeDelete: true)
                .Index(t => t.MissionId);
            
            CreateTable(
                "company.ScenarioMissionEntity",
                c => new
                    {
                        MissionId = c.Short(nullable: false, identity: true),
                        AbstractMissionId = c.Short(nullable: false),
                        NextMissionId = c.Short(),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MissionId)
                .ForeignKey("company.ScenarioCompanyEntity", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("company.ScenarioMissionEntity", t => t.NextMissionId, cascadeDelete: true)
                .Index(t => new { t.CompanyId, t.AbstractMissionId }, unique: true, name: "CompanyId_AbstractMissionId")
                .Index(t => t.NextMissionId);
            
            CreateTable(
                "company.ScenarioCompanyEntity",
                c => new
                    {
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CompanyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerMissionEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.PlayerMissionEntity", "MissionTaskId", "company.ScenarioTaskEntity");
            DropForeignKey("company.ScenarioTaskEntity", "MissionId", "company.ScenarioMissionEntity");
            DropForeignKey("company.ScenarioMissionEntity", "NextMissionId", "company.ScenarioMissionEntity");
            DropForeignKey("company.ScenarioMissionEntity", "CompanyId", "company.ScenarioCompanyEntity");
            DropIndex("company.ScenarioMissionEntity", new[] { "NextMissionId" });
            DropIndex("company.ScenarioMissionEntity", "CompanyId_AbstractMissionId");
            DropIndex("company.ScenarioTaskEntity", new[] { "MissionId" });
            DropIndex("players.PlayerMissionEntity", "PlayerId_MissionTaskId");
            DropTable("company.ScenarioCompanyEntity");
            DropTable("company.ScenarioMissionEntity");
            DropTable("company.ScenarioTaskEntity");
            DropTable("players.PlayerMissionEntity");
        }
    }
}
