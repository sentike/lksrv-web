namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSocialPromoTask : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "promo.SocialGroupEntity",
                c => new
                    {
                        SocialGroupId = c.Int(nullable: false, identity: true),
                        PostIntevalSeconds = c.Long(nullable: false, defaultValue: (long) new TimeSpan(24, 0, 0).TotalSeconds),
                    })
                .PrimaryKey(t => t.SocialGroupId);
            
            CreateTable(
                "promo.SocialTaskEntity",
                c => new
                    {
                        TaskId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid()"),
                        SocialGroupId = c.Int(nullable: false),
                        UrlAddress = c.String(nullable: false, maxLength: 256),
                        Bonus_Amount = c.Int(nullable: false, defaultValue: 500),
                        Bonus_IsDonate = c.Boolean(nullable: false),
                        StartDate = c.DateTime(nullable: false, defaultValueSql: "Now()"),
                        EndDate = c.DateTime(nullable: false, defaultValueSql: "Now()"),
                    })
                .PrimaryKey(t => t.TaskId)
                .ForeignKey("promo.SocialGroupEntity", t => t.SocialGroupId, cascadeDelete: true)
                .Index(t => t.SocialGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("promo.SocialTaskEntity", "SocialGroupId", "promo.SocialGroupEntity");
            DropIndex("promo.SocialTaskEntity", new[] { "SocialGroupId" });
            DropTable("promo.SocialTaskEntity");
            DropTable("promo.SocialGroupEntity");
        }
    }
}
