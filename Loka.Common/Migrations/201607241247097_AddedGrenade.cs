namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGrenade : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "store.GrenadeItemInstance",
                c => new
                    {
                        ModelId = c.Short(nullable: false),
                        CategoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ModelId, t.CategoryTypeId })
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelId, t.CategoryTypeId })
                .Index(t => new { t.ModelId, t.CategoryTypeId });
            
        }
        
        public override void Down()
        {
            DropForeignKey("store.GrenadeItemInstance", new[] { "ModelId", "CategoryTypeId" }, "store.AbstractItemInstance");
            DropIndex("store.GrenadeItemInstance", new[] { "ModelId", "CategoryTypeId" });
            DropTable("store.GrenadeItemInstance");
        }
    }
}
