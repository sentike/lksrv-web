namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMatchTeamScore : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchTeam", "Score", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("matches.MatchTeam", "Score");
        }
    }
}
