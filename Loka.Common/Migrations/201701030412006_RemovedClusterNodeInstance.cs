namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedClusterNodeInstance : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.ClusterNodeEntity", "MachineAddress", "public.ClusterInstanceEntity");
            DropForeignKey("public.ClusterNodeEntity", "SessionInstance_MatchId", "matches.MatchEntity");
            DropIndex("public.ClusterNodeEntity", new[] { "MachineAddress" });
            DropIndex("public.ClusterNodeEntity", new[] { "SessionInstance_MatchId" });
            AddColumn("matches.MatchEntity", "MachineAddress", c => c.String(nullable: false, maxLength: 64, storeType: "\"cidr\"", defaultValue: "78.46.46.148"));
            AddColumn("matches.MatchEntity", "MachinePort", c => c.Short(nullable: false));
            AddColumn("matches.MatchEntity", "Shutdown", c => c.DateTime());
            AddColumn("matches.MatchEntity", "LastCheckDate", c => c.DateTime());
            AddColumn("matches.MatchEntity", "LastActivityDate", c => c.DateTime());
            AddColumn("matches.MatchEntity", "ShutdownCode", c => c.Int(nullable: false));
            AddColumn("matches.MatchEntity", "ProcessId", c => c.Int(nullable: false));
            AddColumn("matches.MatchEntity", "GameVersion", c => c.Int(nullable: false));
            AlterColumn("matches.MatchEntity", "Started", c => c.DateTime());
            CreateIndex("matches.MatchEntity", "MachineAddress");
            //AddForeignKey("matches.MatchEntity", "MachineAddress", "public.ClusterInstanceEntity", "MachineAddress", cascadeDelete: true);
            DropTable("public.ClusterNodeEntity");
        }
        
        public override void Down()
        {
            CreateTable(
                "public.ClusterNodeEntity",
                c => new
                    {
                        MatchId = c.Guid(nullable: false),
                        MachineAddress = c.String(nullable: false, maxLength: 64),
                        ProcessId = c.Int(nullable: false),
                        Address_Host = c.String(),
                        Address_Port = c.Int(nullable: false),
                        ShutDownDate = c.DateTime(),
                        ShutDownCode = c.Int(),
                        RegistrationDate = c.DateTime(nullable: false),
                        LastActivityDate = c.DateTime(),
                        LastCheckDate = c.DateTime(),
                        LastSessionInstanceActivityDate = c.DateTime(nullable: false),
                        State = c.Int(nullable: false),
                        RowVersion = c.Guid(nullable: false),
                        SessionInstance_MatchId = c.Guid(),
                    })
                .PrimaryKey(t => t.MatchId);
            
            DropForeignKey("matches.MatchEntity", "MachineAddress", "public.ClusterInstanceEntity");
            DropIndex("matches.MatchEntity", new[] { "MachineAddress" });
            AlterColumn("matches.MatchEntity", "Started", c => c.DateTime(nullable: false));
            DropColumn("matches.MatchEntity", "GameVersion");
            DropColumn("matches.MatchEntity", "ProcessId");
            DropColumn("matches.MatchEntity", "ShutdownCode");
            DropColumn("matches.MatchEntity", "LastActivityDate");
            DropColumn("matches.MatchEntity", "LastCheckDate");
            DropColumn("matches.MatchEntity", "Shutdown");
            DropColumn("matches.MatchEntity", "MachinePort");
            DropColumn("matches.MatchEntity", "MachineAddress");
            CreateIndex("public.ClusterNodeEntity", "SessionInstance_MatchId");
            CreateIndex("public.ClusterNodeEntity", "MachineAddress");
            AddForeignKey("public.ClusterNodeEntity", "SessionInstance_MatchId", "matches.MatchEntity", "MatchId");
            AddForeignKey("public.ClusterNodeEntity", "MachineAddress", "public.ClusterInstanceEntity", "MachineAddress", cascadeDelete: true);
        }
    }
}
