namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedpromo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "promo.Partners",
                c => new
                    {
                        PartnerId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid()"),
                        Name = c.String(),
                        Login = c.String(),
                        Password = c.String(),
                        RegistrationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PartnerId);
            
            CreateTable(
                "promo.Codes",
                c => new
                    {
                        PromoId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid()"),
                        PartnerId = c.Guid(nullable: false),
                        Name = c.String(),
                        Validity = c.DateTime(nullable: false),
                        Confirmed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PromoId)
                .ForeignKey("promo.Partners", t => t.PartnerId, cascadeDelete: true)
                .Index(t => t.PartnerId);
            
            CreateTable(
                "promo.PromoItemEntity",
                c => new
                    {
                        ItemId = c.Int(nullable: false, identity: true),
                        ModelId = c.Short(nullable: false),
                        ModelInstance_ModelId = c.Short(),
                        ModelInstance_CategoryTypeId = c.Int(),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("store.AbstractItemInstance", t => new { t.ModelInstance_ModelId, t.ModelInstance_CategoryTypeId })
                .Index(t => new { t.ModelInstance_ModelId, t.ModelInstance_CategoryTypeId });
            
            CreateIndex("players.PlayerPromoCode", "PlayerId");
            AddForeignKey("players.PlayerPromoCode", "PlayerId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("promo.PromoItemEntity", new[] { "ModelInstance_ModelId", "ModelInstance_CategoryTypeId" }, "store.AbstractItemInstance");
            DropForeignKey("promo.Codes", "PartnerId", "promo.Partners");
            DropForeignKey("players.PlayerPromoCode", "PlayerId", "players.PlayerEntity");
            DropIndex("promo.PromoItemEntity", new[] { "ModelInstance_ModelId", "ModelInstance_CategoryTypeId" });
            DropIndex("promo.Codes", new[] { "PartnerId" });
            DropIndex("players.PlayerPromoCode", new[] { "PlayerId" });
            DropTable("promo.PromoItemEntity");
            DropTable("promo.Codes");
            DropTable("promo.Partners");
        }
    }
}
