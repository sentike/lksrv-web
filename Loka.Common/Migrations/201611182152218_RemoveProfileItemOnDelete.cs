namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveProfileItemOnDelete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("players.PlayerProfileItemEntity", "ItemId", "players.InstanceOfPlayerItem");
            DropIndex("players.PlayerProfileItemEntity", new[] { "ItemId" });

            CreateIndex("players.PlayerProfileItemEntity", "ItemId");
            AddForeignKey("players.PlayerProfileItemEntity", "ItemId", "players.InstanceOfPlayerItem", "ItemId", cascadeDelete: true);
        }
        
        public override void Down()
        {
        }
    }
}
