namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCharacterModelsFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("store.AbstractItemInstance", "CharacterModelsFlag", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("store.AbstractItemInstance", "CharacterModelsFlag");
        }
    }
}
