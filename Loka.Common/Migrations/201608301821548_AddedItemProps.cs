//namespace Loka.Server.Migrations
//{
//    using System;
//    using System.Data.Entity.Migrations;
    
//    public partial class AddedItemProps : DbMigration
//    {
//        public override void Up()
//        {
//            AddColumn("players.InstanceOfPlayerItem", "PurchaseDate", c => c.DateTime(nullable: false));
//            AddColumn("players.InstanceOfPlayerItem", "LastUseDate", c => c.DateTime());
//            AddColumn("players.InstanceOfPlayerItem", "ValidityDate", c => c.DateTime());
//        }
        
//        public override void Down()
//        {
//            DropColumn("players.InstanceOfPlayerItem", "ValidityDate");
//            DropColumn("players.InstanceOfPlayerItem", "LastUseDate");
//            DropColumn("players.InstanceOfPlayerItem", "PurchaseDate");
//        }
//    }
//}
