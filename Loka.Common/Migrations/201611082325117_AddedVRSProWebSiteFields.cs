namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedVRSProWebSiteFields : DbMigration
    {
        public override void Up()
        {
            DropIndex("vrs.Users", "UserNameIndex");
            AddColumn("vrs.Users", "FirstName", c => c.String(maxLength: 16));
            AddColumn("vrs.Users", "MiddleName", c => c.String(maxLength: 16));
            AddColumn("vrs.Users", "LastName", c => c.String(maxLength: 16));
            AddColumn("vrs.Users", "Gender", c => c.Int(nullable: false));
            AddColumn("vrs.Users", "Country", c => c.Int(nullable: false));
            AddColumn("vrs.Users", "Language", c => c.Int(nullable: false));
            AddColumn("vrs.Users", "City", c => c.Int(nullable: false));
            AddColumn("vrs.Users", "RegistrationDate", c => c.DateTime(nullable: false));
            AddColumn("vrs.Users", "LastActivityDate", c => c.DateTime(nullable: false));
            AddColumn("vrs.Users", "BirthDate", c => c.DateTime(nullable: false));
            AddColumn("vrs.Users", "Email", c => c.String(maxLength: 64));
            AlterColumn("vrs.Users", "UserName", c => c.String(nullable: false, maxLength: 64));
            CreateIndex("vrs.Users", "RegistrationDate");
            CreateIndex("vrs.Users", "LastActivityDate");
            CreateIndex("vrs.Users", "Email", unique: true);
            CreateIndex("vrs.Users", "PhoneNumber", unique: true);
            CreateIndex("vrs.Users", "UserName", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("vrs.Users", new[] { "UserName" });
            DropIndex("vrs.Users", new[] { "PhoneNumber" });
            DropIndex("vrs.Users", new[] { "Email" });
            DropIndex("vrs.Users", new[] { "LastActivityDate" });
            DropIndex("vrs.Users", new[] { "RegistrationDate" });
            AlterColumn("vrs.Users", "UserName", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("vrs.Users", "Email", c => c.String(maxLength: 256));
            DropColumn("vrs.Users", "BirthDate");
            DropColumn("vrs.Users", "LastActivityDate");
            DropColumn("vrs.Users", "RegistrationDate");
            DropColumn("vrs.Users", "City");
            DropColumn("vrs.Users", "Language");
            DropColumn("vrs.Users", "Country");
            DropColumn("vrs.Users", "Gender");
            DropColumn("vrs.Users", "LastName");
            DropColumn("vrs.Users", "MiddleName");
            DropColumn("vrs.Users", "FirstName");
            CreateIndex("vrs.Users", "UserName", unique: true, name: "UserNameIndex");
        }
    }
}
