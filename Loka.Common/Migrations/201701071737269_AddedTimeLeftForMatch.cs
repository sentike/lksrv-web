namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTimeLeftForMatch : DbMigration
    {
        public override void Up()
        {
            AddColumn("matches.MatchEntity", "TimeLeftInSeconds", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("matches.MatchEntity", "TimeLeftInSeconds");
        }
    }
}
