namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedhoverboardid : DbMigration
    {
        public override void Up()
        {
            AddColumn("players.PlayerEntity", "CurrentHoverBoardItemId", c => c.Guid());
            CreateIndex("players.PlayerEntity", "CurrentHoverBoardItemId");
            AddForeignKey("players.PlayerEntity", "CurrentHoverBoardItemId", "players.InstanceOfPlayerItem", "ItemId");
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerEntity", "CurrentHoverBoardItemId", "players.InstanceOfPlayerItem");
            DropIndex("players.PlayerEntity", new[] { "CurrentHoverBoardItemId" });
            DropColumn("players.PlayerEntity", "CurrentHoverBoardItemId");
        }
    }
}
