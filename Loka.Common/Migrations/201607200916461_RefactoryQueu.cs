namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoryQueu : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "public.SessionQueueEntity", newName: "QueueEntity");
            RenameTable(name: "public.SessionQueueMember", newName: "QueueMember");
            DropForeignKey("players.PlayerEntity", "QueueMemberId", "public.SessionQueueMember");
            DropForeignKey("players.PlayerEntity", "MatchMemberId", "matches.MatchMember");
            DropIndex("players.PlayerEntity", new[] { "MatchMemberId" });
            DropIndex("players.PlayerEntity", new[] { "QueueMemberId" });
            DropColumn("public.QueueEntity", "QueueId");
            RenameColumn(table: "public.QueueEntity", name: "PlayerId", newName: "QueueId");
            RenameIndex(table: "public.QueueEntity", name: "IX_PlayerId", newName: "IX_QueueId");
            //DropPrimaryKey("public.QueueMember");
            AddColumn("public.QueueEntity", "LastActivityDate", c => c.DateTime(nullable: false));
            AddColumn("public.QueueMember", "MatchMemberId", c => c.Guid());
            AddPrimaryKey("public.QueueMember", "PlayerId");
            CreateIndex("public.QueueMember", "MatchMemberId");
            AddForeignKey("public.QueueMember", "MatchMemberId", "matches.MatchMember", "MemberId");
            DropColumn("players.PlayerEntity", "MatchMemberId");
            DropColumn("players.PlayerEntity", "QueueMemberId");
            DropColumn("public.QueueMember", "QueueMemberId");
        }
        
        public override void Down()
        {
            AddColumn("public.QueueMember", "QueueMemberId", c => c.Guid(nullable: false));
            AddColumn("players.PlayerEntity", "QueueMemberId", c => c.Guid());
            AddColumn("players.PlayerEntity", "MatchMemberId", c => c.Guid());
            DropForeignKey("public.QueueMember", "MatchMemberId", "matches.MatchMember");
            DropIndex("public.QueueMember", new[] { "MatchMemberId" });
            DropPrimaryKey("public.QueueMember");
            DropColumn("public.QueueMember", "MatchMemberId");
            DropColumn("public.QueueEntity", "LastActivityDate");
            AddPrimaryKey("public.QueueMember", "QueueMemberId");
            RenameIndex(table: "public.QueueEntity", name: "IX_QueueId", newName: "IX_PlayerId");
            RenameColumn(table: "public.QueueEntity", name: "QueueId", newName: "PlayerId");
            AddColumn("public.QueueEntity", "QueueId", c => c.Guid(nullable: false));
            CreateIndex("players.PlayerEntity", "QueueMemberId");
            CreateIndex("players.PlayerEntity", "MatchMemberId");
            AddForeignKey("players.PlayerEntity", "MatchMemberId", "matches.MatchMember", "MemberId");
            AddForeignKey("players.PlayerEntity", "QueueMemberId", "public.SessionQueueMember", "QueueMemberId", cascadeDelete: true);
            RenameTable(name: "public.QueueMember", newName: "SessionQueueMember");
            RenameTable(name: "public.QueueEntity", newName: "SessionQueueEntity");
        }
    }
}
