namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedOldChat : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.PrivateChatMessageData", "ReceiverId", "players.PlayerEntity");
            DropForeignKey("public.PrivateChatMessageData", "SenderId", "players.PlayerEntity");
            DropForeignKey("public.PrivateChatMessage", "MessageDataId", "public.PrivateChatMessageData");
            DropForeignKey("public.PrivateChatMessage", "PlayerId", "players.PlayerEntity");
            DropForeignKey("public.GlobalChatMessage", "EditorId", "players.PlayerEntity");
            DropForeignKey("public.GlobalChatMessage", "SenderId", "players.PlayerEntity");
            DropIndex("public.PrivateChatMessage", new[] { "MessageDataId" });
            DropIndex("public.PrivateChatMessage", new[] { "PlayerId" });
            DropIndex("public.PrivateChatMessageData", new[] { "SenderId" });
            DropIndex("public.PrivateChatMessageData", new[] { "ReceiverId" });
            DropIndex("public.GlobalChatMessage", new[] { "SenderId" });
            DropIndex("public.GlobalChatMessage", new[] { "EditorId" });
            DropTable("public.PrivateChatMessage");
            DropTable("public.PrivateChatMessageData");
            DropTable("public.GlobalChatMessage");
        }
        
        public override void Down()
        {
            CreateTable(
                "public.GlobalChatMessage",
                c => new
                    {
                        MessageId = c.Guid(nullable: false),
                        SenderId = c.Guid(nullable: false),
                        EditorId = c.Guid(),
                        Status = c.Int(nullable: false),
                        LanguageId = c.Short(nullable: false),
                        Message = c.String(),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.MessageId);
            
            CreateTable(
                "public.PrivateChatMessageData",
                c => new
                    {
                        MessageId = c.Guid(nullable: false),
                        Message = c.String(),
                        Date = c.DateTime(nullable: false),
                        IsReaded = c.Boolean(nullable: false),
                        SenderId = c.Guid(nullable: false),
                        ReceiverId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.MessageId);
            
            CreateTable(
                "public.PrivateChatMessage",
                c => new
                    {
                        MessageId = c.Guid(nullable: false),
                        MessageDataId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.MessageId);
            
            CreateIndex("public.GlobalChatMessage", "EditorId");
            CreateIndex("public.GlobalChatMessage", "SenderId");
            CreateIndex("public.PrivateChatMessageData", "ReceiverId");
            CreateIndex("public.PrivateChatMessageData", "SenderId");
            CreateIndex("public.PrivateChatMessage", "PlayerId");
            CreateIndex("public.PrivateChatMessage", "MessageDataId");
            AddForeignKey("public.GlobalChatMessage", "SenderId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
            AddForeignKey("public.GlobalChatMessage", "EditorId", "players.PlayerEntity", "PlayerId");
            AddForeignKey("public.PrivateChatMessage", "PlayerId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
            AddForeignKey("public.PrivateChatMessage", "MessageDataId", "public.PrivateChatMessageData", "MessageId", cascadeDelete: true);
            AddForeignKey("public.PrivateChatMessageData", "SenderId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
            AddForeignKey("public.PrivateChatMessageData", "ReceiverId", "players.PlayerEntity", "PlayerId", cascadeDelete: true);
        }
    }
}
