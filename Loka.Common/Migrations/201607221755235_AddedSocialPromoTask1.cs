namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSocialPromoTask1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "players.PlayerSocialTaskEntity",
                c => new
                    {
                        PlayerTaskId = c.Guid(nullable: false, defaultValueSql: "gen_random_uuid()"),
                        PlayerId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        SocialTaskId = c.Guid(nullable: false, defaultValueSql: string.Empty),
                        RepostDate = c.DateTime(nullable: false, defaultValueSql: "Now()"),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PlayerTaskId)
                .ForeignKey("promo.SocialTaskEntity", t => t.SocialTaskId, cascadeDelete: true)
                .ForeignKey("players.PlayerEntity", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.SocialTaskId);
        }
        
        public override void Down()
        {
            DropForeignKey("players.PlayerSocialTaskEntity", "PlayerId", "players.PlayerEntity");
            DropForeignKey("players.PlayerSocialTaskEntity", "SocialTaskId", "promo.SocialTaskEntity");
            DropIndex("players.PlayerSocialTaskEntity", new[] { "SocialTaskId" });
            DropIndex("players.PlayerSocialTaskEntity", new[] { "PlayerId" });
            DropTable("players.PlayerSocialTaskEntity");
        }
    }
}
