namespace Loka.Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNewTransactions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "transaction.MatchTransactionEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        AccountId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Amount = c.Long(nullable: false),
                        LastAmount = c.Long(nullable: false),
                        SessionId = c.Guid(nullable: false),
                        MatchId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .Index(t => t.AccountId)
                .Index(t => t.CreatedDate)
                .Index(t => t.ProductId)
                .Index(t => t.SessionId)
                .Index(t => t.MatchId);
            
            CreateTable(
                "transaction.OrderTransactionEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        AccountId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Amount = c.Long(nullable: false),
                        LastAmount = c.Long(nullable: false),
                        OrderId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .Index(t => t.AccountId)
                .Index(t => t.CreatedDate)
                .Index(t => t.ProductId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "transaction.StoreTransactionEntity",
                c => new
                    {
                        EntityId = c.Guid(nullable: false),
                        AccountId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Amount = c.Long(nullable: false),
                        LastAmount = c.Long(nullable: false),
                        SessionId = c.Guid(nullable: false),
                        RecipientId = c.Guid(nullable: false),
                        Cost = c.Long(nullable: false),
                        Balanace = c.Long(nullable: false),
                        Currency = c.Short(nullable: false),
                        Notified = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId)
                .Index(t => t.AccountId)
                .Index(t => t.CreatedDate)
                .Index(t => t.ProductId)
                .Index(t => t.RecipientId);
            
            AddColumn("store.AbstractItemInstance", "ProductId", c => c.Int(nullable: false));
            AddColumn("players.InstanceOfPlayerItem", "ProductId", c => c.Int(nullable: false));
            DropColumn("store.AbstractItemInstance", "ItemId");
        }
        
        public override void Down()
        {
            AddColumn("store.AbstractItemInstance", "ItemId", c => c.Int(nullable: false));
            DropIndex("transaction.StoreTransactionEntity", new[] { "RecipientId" });
            DropIndex("transaction.StoreTransactionEntity", new[] { "ProductId" });
            DropIndex("transaction.StoreTransactionEntity", new[] { "CreatedDate" });
            DropIndex("transaction.StoreTransactionEntity", new[] { "AccountId" });
            DropIndex("transaction.OrderTransactionEntity", new[] { "OrderId" });
            DropIndex("transaction.OrderTransactionEntity", new[] { "ProductId" });
            DropIndex("transaction.OrderTransactionEntity", new[] { "CreatedDate" });
            DropIndex("transaction.OrderTransactionEntity", new[] { "AccountId" });
            DropIndex("transaction.MatchTransactionEntity", new[] { "MatchId" });
            DropIndex("transaction.MatchTransactionEntity", new[] { "SessionId" });
            DropIndex("transaction.MatchTransactionEntity", new[] { "ProductId" });
            DropIndex("transaction.MatchTransactionEntity", new[] { "CreatedDate" });
            DropIndex("transaction.MatchTransactionEntity", new[] { "AccountId" });
            DropColumn("players.InstanceOfPlayerItem", "ProductId");
            DropColumn("store.AbstractItemInstance", "ProductId");
            DropTable("transaction.StoreTransactionEntity");
            DropTable("transaction.OrderTransactionEntity");
            DropTable("transaction.MatchTransactionEntity");
        }
    }
}
