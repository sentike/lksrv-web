--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2016-06-23 00:25:06

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = "Store", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 225 (class 1259 OID 46781)
-- Name: AbstractItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "AbstractItemInstance" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 1 NOT NULL,
    "IsAvalible" boolean DEFAULT true NOT NULL,
    "DefaultSkinId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "AbstractItemInstance" OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 46804)
-- Name: ArmourItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ArmourItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "ArmourItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 1))
);


ALTER TABLE "ArmourItemInstance" OWNER TO postgres;

--
-- TOC entry 2214 (class 0 OID 46781)
-- Dependencies: 225
-- Data for Name: AbstractItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "Bonus_Money", "Bonus_Experience", "Bonus_Reputation", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 1, 1800, false, 0, 0, 0, 2, 1, true, 0);


--
-- TOC entry 2215 (class 0 OID 46804)
-- Dependencies: 229
-- Data for Name: ArmourItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 1);


--
-- TOC entry 2101 (class 2606 OID 46830)
-- Name: AbstractItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AbstractItemInstance"
    ADD CONSTRAINT "AbstractItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2103 (class 2606 OID 46844)
-- Name: ArmourItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2104 (class 2606 OID 46865)
-- Name: ArmourItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


-- Completed on 2016-06-23 00:25:06

--
-- PostgreSQL database dump complete
--

