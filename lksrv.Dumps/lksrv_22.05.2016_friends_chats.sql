--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-05-22 15:13:53

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 16 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2300 (class 0 OID 0)
-- Dependencies: 16
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 234 (class 1259 OID 66321)
-- Name: GlobalChatMessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "GlobalChatMessage" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "EditorId" uuid,
    "LanguageId" smallint NOT NULL
);


ALTER TABLE "GlobalChatMessage" OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 66326)
-- Name: PrivateChatMessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "PrivateChatMessage" (
    "MessageId" uuid NOT NULL,
    "MessageDataId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL
);


ALTER TABLE "PrivateChatMessage" OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 66331)
-- Name: PrivateChatMessageData; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "PrivateChatMessageData" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "ReceiverId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "IsReaded" boolean NOT NULL
);


ALTER TABLE "PrivateChatMessageData" OWNER TO postgres;

--
-- TOC entry 2173 (class 2606 OID 66325)
-- Name: GlobalChatMessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_pkey" PRIMARY KEY ("MessageId");


--
-- TOC entry 2177 (class 2606 OID 66335)
-- Name: PrivateChatMessageData_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_pkey" PRIMARY KEY ("MessageId");


--
-- TOC entry 2175 (class 2606 OID 66330)
-- Name: PrivateChatMessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_pkey" PRIMARY KEY ("MessageId");


--
-- TOC entry 2181 (class 2606 OID 66356)
-- Name: PrivateChatMessageData_ReceiverId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_ReceiverId_fkey" FOREIGN KEY ("ReceiverId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2180 (class 2606 OID 66351)
-- Name: PrivateChatMessageData_SenderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_SenderId_fkey" FOREIGN KEY ("SenderId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2179 (class 2606 OID 66346)
-- Name: PrivateChatMessage_MessageDataId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_MessageDataId_fkey" FOREIGN KEY ("MessageDataId") REFERENCES "PrivateChatMessageData"("MessageId") ON DELETE CASCADE;


--
-- TOC entry 2178 (class 2606 OID 66336)
-- Name: PrivateChatMessage_PlayerId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2301 (class 0 OID 0)
-- Dependencies: 16
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-05-22 15:13:54

--
-- PostgreSQL database dump complete
--

