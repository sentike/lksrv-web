--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-07-01 14:20:59

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 18 (class 2615 OID 16619)
-- Name: Achievements; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Achievements";


ALTER SCHEMA "Achievements" OWNER TO postgres;

--
-- TOC entry 12 (class 2615 OID 16620)
-- Name: League; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "League";


ALTER SCHEMA "League" OWNER TO postgres;

--
-- TOC entry 17 (class 2615 OID 49807)
-- Name: Matches; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Matches";


ALTER SCHEMA "Matches" OWNER TO postgres;

--
-- TOC entry 13 (class 2615 OID 16621)
-- Name: Players; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Players";


ALTER SCHEMA "Players" OWNER TO postgres;

--
-- TOC entry 16 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2544 (class 0 OID 0)
-- Dependencies: 16
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 15 (class 2615 OID 16623)
-- Name: vrs; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA vrs;


ALTER SCHEMA vrs OWNER TO postgres;

SET search_path = "Achievements", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 237 (class 1259 OID 91111)
-- Name: AchievementContainer; Type: TABLE; Schema: Achievements; Owner: postgres
--

CREATE TABLE "AchievementContainer" (
    "AchievementTypeId" smallint NOT NULL
);


ALTER TABLE "AchievementContainer" OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 91118)
-- Name: AchievementInstance; Type: TABLE; Schema: Achievements; Owner: postgres
--

CREATE TABLE "AchievementInstance" (
    "AchievementBonusId" integer NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "Amount" integer DEFAULT 0 NOT NULL,
    "BonusPackId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "AchievementInstance" OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 91116)
-- Name: AchievementInstance_AchievementBonusId_seq; Type: SEQUENCE; Schema: Achievements; Owner: postgres
--

CREATE SEQUENCE "AchievementInstance_AchievementBonusId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "AchievementInstance_AchievementBonusId_seq" OWNER TO postgres;

--
-- TOC entry 2546 (class 0 OID 0)
-- Dependencies: 238
-- Name: AchievementInstance_AchievementBonusId_seq; Type: SEQUENCE OWNED BY; Schema: Achievements; Owner: postgres
--

ALTER SEQUENCE "AchievementInstance_AchievementBonusId_seq" OWNED BY "AchievementInstance"."AchievementBonusId";


SET search_path = "League", pg_catalog;

--
-- TOC entry 208 (class 1259 OID 16647)
-- Name: LeagueEntity; Type: TABLE; Schema: League; Owner: postgres
--

CREATE TABLE "LeagueEntity" (
    "LeagueId" uuid NOT NULL,
    "Cash_Money" smallint NOT NULL,
    "Cash_Donate" smallint NOT NULL,
    "Info_Name" character varying(20) NOT NULL,
    "Info_Abbr" character varying(5) NOT NULL,
    "Info_FoundedDate" timestamp without time zone NOT NULL,
    "Info_AccessType" smallint NOT NULL,
    "Info_JoinPrice" smallint NOT NULL
);


ALTER TABLE "LeagueEntity" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16650)
-- Name: LeagueMemberEntity; Type: TABLE; Schema: League; Owner: postgres
--

CREATE TABLE "LeagueMemberEntity" (
    "LeagueId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Access" smallint NOT NULL,
    "JoinDate" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "LeagueMemberEntity" OWNER TO postgres;

SET search_path = "Matches", pg_catalog;

--
-- TOC entry 221 (class 1259 OID 49808)
-- Name: MatchEntity; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchEntity" (
    "MatchId" uuid NOT NULL,
    "NodeId" uuid NOT NULL,
    "GameModeTypeId" smallint NOT NULL,
    "GameMapTypeId" smallint NOT NULL,
    "Created" timestamp without time zone NOT NULL,
    "Started" timestamp without time zone NOT NULL,
    "Finished" timestamp without time zone NOT NULL,
    "WinnerTeamId" uuid,
    "NumberOfBots" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "MatchEntity" OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 49819)
-- Name: MatchMember; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchMember" (
    "MemberId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "TeamId" uuid NOT NULL,
    "Level" smallint DEFAULT 1 NOT NULL,
    "MatchId" uuid NOT NULL,
    "Award_Money" smallint DEFAULT 0 NOT NULL,
    "Award_Experience" smallint DEFAULT 0 NOT NULL,
    "Award_Reputation" smallint DEFAULT 0 NOT NULL,
    "Award_FractionId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "MatchMember" OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 49813)
-- Name: MatchTeam; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MatchTeam" (
    "TeamId" uuid NOT NULL,
    "MatchId" uuid NOT NULL
);


ALTER TABLE "MatchTeam" OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 49825)
-- Name: MemberAchievements; Type: TABLE; Schema: Matches; Owner: postgres
--

CREATE TABLE "MemberAchievements" (
    "AchievementId" uuid NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "AchievementCount" smallint NOT NULL,
    "MemberId" uuid NOT NULL
);


ALTER TABLE "MemberAchievements" OWNER TO postgres;

SET search_path = "Players", pg_catalog;

--
-- TOC entry 211 (class 1259 OID 16658)
-- Name: FriendEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "FriendEntity" (
    "MemberId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FriendId" uuid NOT NULL,
    "State" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "FriendEntity" OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 115889)
-- Name: InstanceOfInstalledAddon; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfInstalledAddon" (
    "AddonId" uuid NOT NULL,
    "ItemId" uuid,
    "AddonSlot" smallint NOT NULL,
    "ContainerId" uuid NOT NULL
);


ALTER TABLE "InstanceOfInstalledAddon" OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 107659)
-- Name: InstanceOfItemAddon; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfItemAddon" (
    "ItemId" uuid NOT NULL,
    modification_1_type smallint DEFAULT 0 NOT NULL,
    modification_2_type smallint DEFAULT 0 NOT NULL,
    modification_3_type smallint DEFAULT 0 NOT NULL,
    modification_4_type smallint DEFAULT 0 NOT NULL,
    modification_5_type smallint DEFAULT 0 NOT NULL,
    modification_1_value smallint DEFAULT 0,
    modification_2_value smallint DEFAULT 0,
    modification_3_value smallint DEFAULT 0,
    modification_4_value smallint DEFAULT 0,
    modification_5_value smallint DEFAULT 0
);


ALTER TABLE "InstanceOfItemAddon" OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 107654)
-- Name: InstanceOfItemSkin; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfItemSkin" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfItemSkin" OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 91015)
-- Name: InstanceOfPlayerArmour; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerArmour" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfPlayerArmour" OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16654)
-- Name: InstanceOfPlayerCharacter; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerCharacter" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL
);


ALTER TABLE "InstanceOfPlayerCharacter" OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 90987)
-- Name: InstanceOfPlayerItem; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerItem" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "ModelId" smallint NOT NULL,
    modification_1_type smallint DEFAULT 0 NOT NULL,
    modification_2_type smallint DEFAULT 0 NOT NULL,
    modification_3_type smallint DEFAULT 0 NOT NULL,
    modification_4_type smallint DEFAULT 0 NOT NULL,
    modification_5_type smallint DEFAULT 0 NOT NULL,
    modification_1_value real DEFAULT 0 NOT NULL,
    modification_2_value real DEFAULT 0 NOT NULL,
    modification_3_value real DEFAULT 0 NOT NULL,
    modification_4_value real DEFAULT 0 NOT NULL,
    modification_5_value real DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "InstanceOfPlayerItem" OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 91032)
-- Name: InstanceOfPlayerWeapon; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerWeapon" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfPlayerWeapon" OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 91170)
-- Name: InstanceOfTargetItem; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfTargetItem" (
    "ItemId" uuid NOT NULL,
    "TargetPlayerItemId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "ModelId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "InstanceOfTargetItem" OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 91126)
-- Name: PlayerAchievement; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerAchievement" (
    "PlayerAchievementId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "LastAchievementInstanceId" smallint,
    "Amount" smallint NOT NULL
);


ALTER TABLE "PlayerAchievement" OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16662)
-- Name: PlayerEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerEntity" (
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerName" character varying(24) NOT NULL,
    "Experience_Level" smallint DEFAULT 0 NOT NULL,
    "Experience_Experience" bigint DEFAULT 0 NOT NULL,
    "Experience_WeekExperience" bigint DEFAULT 0 NOT NULL,
    "Cash_Money" bigint DEFAULT 0 NOT NULL,
    "Cash_Donate" bigint DEFAULT 0 NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL,
    "PremiumEndDate" timestamp without time zone,
    "LastActivitiDate" timestamp without time zone DEFAULT now() NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT now() NOT NULL,
    "CurrentFractionId" uuid,
    "Experience_LastLevel" smallint DEFAULT 0 NOT NULL,
    "WeekBonus_JoinDate" timestamp without time zone DEFAULT now() NOT NULL,
    "WeekBonus_JoinCount" smallint DEFAULT 0 NOT NULL,
    "WeekBonus_NotifyDate" timestamp without time zone DEFAULT now() NOT NULL,
    CONSTRAINT "PlayerEntity_Experience_Level_check" CHECK (("Experience_Level" < 250)),
    CONSTRAINT "PlayerEntity_Experience_WeekJoinCount_check" CHECK (("WeekBonus_JoinCount" < 8))
);


ALTER TABLE "PlayerEntity" OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16632)
-- Name: PlayerProfileEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerProfileEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "CharacterId" uuid NOT NULL,
    "HelmetId" uuid,
    "MaskId" uuid,
    "ArmourId" uuid,
    "BackpackId" uuid,
    "GlovesId" uuid,
    "PantsId" uuid,
    "BootsId" uuid,
    "PrimaryWeaponId" uuid,
    "SecondaryWeaponId" uuid,
    "PrimaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "PrimaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "PlayerId" uuid NOT NULL,
    "IsEnabled" boolean DEFAULT true NOT NULL,
    "Name" character varying(24),
    "IsUnlocked" boolean DEFAULT false NOT NULL,
    CONSTRAINT "PrimaryWeaponAmmo" CHECK ((("PrimaryWeaponAmmo_Primary" + "PrimaryWeaponAmmo_Secondary") > 0))
);


ALTER TABLE "PlayerProfileEntity" OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 149147)
-- Name: PlayerReputationEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerReputationEntity" (
    "ReputationId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FractionId" smallint NOT NULL,
    "CurrentRank" smallint NOT NULL,
    "LastRank" smallint NOT NULL,
    "Reputation" bigint NOT NULL,
    CONSTRAINT "PlayerReputationEntity_CurrentRank_check" CHECK (("CurrentRank" <= 10)),
    CONSTRAINT "PlayerReputationEntity_FractionId_check" CHECK (("FractionId" < 5)),
    CONSTRAINT "PlayerReputationEntity_LastRank_check" CHECK (("LastRank" <= 10))
);


ALTER TABLE "PlayerReputationEntity" OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- TOC entry 225 (class 1259 OID 66321)
-- Name: GlobalChatMessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "GlobalChatMessage" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "EditorId" uuid,
    "LanguageId" smallint NOT NULL
);


ALTER TABLE "GlobalChatMessage" OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 66326)
-- Name: PrivateChatMessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "PrivateChatMessage" (
    "MessageId" uuid NOT NULL,
    "MessageDataId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL
);


ALTER TABLE "PrivateChatMessage" OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 66331)
-- Name: PrivateChatMessageData; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "PrivateChatMessageData" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "ReceiverId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "IsReaded" boolean NOT NULL
);


ALTER TABLE "PrivateChatMessageData" OWNER TO postgres;

SET search_path = vrs, pg_catalog;

--
-- TOC entry 214 (class 1259 OID 16734)
-- Name: Deposits; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Deposits" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserId" uuid NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "Value" bigint DEFAULT 0 NOT NULL,
    "Currency" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "Deposits" OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16740)
-- Name: Roles; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Roles" (
    "Id" uuid NOT NULL,
    "Name" character varying(256) NOT NULL
);


ALTER TABLE "Roles" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16743)
-- Name: UserClaims; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserClaims" (
    "Id" integer NOT NULL,
    "ClaimType" character varying(256),
    "ClaimValue" character varying(256),
    "UserId" uuid NOT NULL
);


ALTER TABLE "UserClaims" OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16749)
-- Name: UserClaims_Id_seq; Type: SEQUENCE; Schema: vrs; Owner: postgres
--

CREATE SEQUENCE "UserClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "UserClaims_Id_seq" OWNER TO postgres;

--
-- TOC entry 2547 (class 0 OID 0)
-- Dependencies: 217
-- Name: UserClaims_Id_seq; Type: SEQUENCE OWNED BY; Schema: vrs; Owner: postgres
--

ALTER SEQUENCE "UserClaims_Id_seq" OWNED BY "UserClaims"."Id";


--
-- TOC entry 218 (class 1259 OID 16751)
-- Name: UserLogins; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserLogins" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "LoginProvider" character varying(128) NOT NULL,
    "ProviderKey" character varying(128) NOT NULL
);


ALTER TABLE "UserLogins" OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16755)
-- Name: UserRoles; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "UserRoles" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RoleId" uuid NOT NULL
);


ALTER TABLE "UserRoles" OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16759)
-- Name: Users; Type: TABLE; Schema: vrs; Owner: postgres
--

CREATE TABLE "Users" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserName" character varying(256) NOT NULL,
    "PasswordHash" character varying(256),
    "SecurityStamp" uuid,
    "Email" character varying(256) DEFAULT NULL::character varying,
    "EmailConfirmed" boolean DEFAULT false NOT NULL,
    "PhoneNumber" character varying(32),
    "PhoneNumberConfirmed" boolean DEFAULT false NOT NULL,
    "TwoFactorEnabled" boolean DEFAULT false NOT NULL,
    "LockoutEndDateUtc" timestamp without time zone,
    "LockoutEnabled" boolean DEFAULT false NOT NULL,
    "AccessFailedCount" smallint DEFAULT 0 NOT NULL,
    "PlayerEntityId" bigint,
    "Balance" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "Users" OWNER TO postgres;

SET search_path = "Achievements", pg_catalog;

--
-- TOC entry 2286 (class 2604 OID 91121)
-- Name: AchievementBonusId; Type: DEFAULT; Schema: Achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementInstance" ALTER COLUMN "AchievementBonusId" SET DEFAULT nextval('"AchievementInstance_AchievementBonusId_seq"'::regclass);


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2257 (class 2604 OID 16773)
-- Name: Id; Type: DEFAULT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims" ALTER COLUMN "Id" SET DEFAULT nextval('"UserClaims_Id_seq"'::regclass);


SET search_path = "Achievements", pg_catalog;

--
-- TOC entry 2363 (class 2606 OID 91115)
-- Name: AchievementContainer_pkey; Type: CONSTRAINT; Schema: Achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementContainer"
    ADD CONSTRAINT "AchievementContainer_pkey" PRIMARY KEY ("AchievementTypeId");


--
-- TOC entry 2365 (class 2606 OID 91125)
-- Name: AchievementInstance_pkey; Type: CONSTRAINT; Schema: Achievements; Owner: postgres
--

ALTER TABLE ONLY "AchievementInstance"
    ADD CONSTRAINT "AchievementInstance_pkey" PRIMARY KEY ("AchievementBonusId");


SET search_path = "League", pg_catalog;

--
-- TOC entry 2307 (class 2606 OID 16785)
-- Name: LeagueEntity_Info_Abbr_key; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Abbr_key" UNIQUE ("Info_Abbr");


--
-- TOC entry 2309 (class 2606 OID 16787)
-- Name: LeagueEntity_Info_Name_key; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Name_key" UNIQUE ("Info_Name");


--
-- TOC entry 2311 (class 2606 OID 16789)
-- Name: LeagueEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_pkey" PRIMARY KEY ("LeagueId");


--
-- TOC entry 2313 (class 2606 OID 16791)
-- Name: LeagueMemberEntity_pkey; Type: CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_pkey" PRIMARY KEY ("PlayerId");


SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2339 (class 2606 OID 49892)
-- Name: MatchEntity_WinnerTeamId_key; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_key" UNIQUE ("WinnerTeamId");


--
-- TOC entry 2341 (class 2606 OID 49812)
-- Name: MatchEntity_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_pkey" PRIMARY KEY ("MatchId");


--
-- TOC entry 2345 (class 2606 OID 49846)
-- Name: MatchMember_PlayerId_TeamId_key; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_TeamId_key" UNIQUE ("PlayerId", "TeamId");


--
-- TOC entry 2347 (class 2606 OID 49824)
-- Name: MatchMember_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_pkey" PRIMARY KEY ("MemberId");


--
-- TOC entry 2343 (class 2606 OID 49818)
-- Name: MatchTeam_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_pkey" PRIMARY KEY ("TeamId");


--
-- TOC entry 2349 (class 2606 OID 49829)
-- Name: MemberAchievements_pkey; Type: CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_pkey" PRIMARY KEY ("AchievementId");


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2357 (class 2606 OID 91004)
-- Name: AbstractPlayerItemInstance_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "AbstractPlayerItemInstance_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2317 (class 2606 OID 16797)
-- Name: FriendEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_pkey" PRIMARY KEY ("MemberId");


--
-- TOC entry 2381 (class 2606 OID 115905)
-- Name: InstanceOfInstalledAddon_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_pkey" PRIMARY KEY ("ContainerId");


--
-- TOC entry 2379 (class 2606 OID 107673)
-- Name: InstanceOfItemAddon_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemAddon"
    ADD CONSTRAINT "InstanceOfItemAddon_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2375 (class 2606 OID 91174)
-- Name: InstanceOfItemSkin_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfItemSkin_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2377 (class 2606 OID 107658)
-- Name: InstanceOfItemSkin_pkey1; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemSkin"
    ADD CONSTRAINT "InstanceOfItemSkin_pkey1" PRIMARY KEY ("ItemId");


--
-- TOC entry 2359 (class 2606 OID 91095)
-- Name: InstanceOfPlayerArmour_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerArmour"
    ADD CONSTRAINT "InstanceOfPlayerArmour_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2315 (class 2606 OID 91081)
-- Name: InstanceOfPlayerCharacter_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerCharacter"
    ADD CONSTRAINT "InstanceOfPlayerCharacter_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2361 (class 2606 OID 91088)
-- Name: InstanceOfPlayerWeapon_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerWeapon"
    ADD CONSTRAINT "InstanceOfPlayerWeapon_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2367 (class 2606 OID 91134)
-- Name: PlayerAchievement_AchievementTypeId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_AchievementTypeId_key" UNIQUE ("AchievementTypeId");


--
-- TOC entry 2369 (class 2606 OID 91136)
-- Name: PlayerAchievement_PlayerId_LastAchievementInstanceId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_PlayerId_LastAchievementInstanceId_key" UNIQUE ("PlayerId", "LastAchievementInstanceId");


--
-- TOC entry 2371 (class 2606 OID 91132)
-- Name: PlayerAchievement_PlayerId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_PlayerId_key" UNIQUE ("PlayerId");


--
-- TOC entry 2373 (class 2606 OID 91130)
-- Name: PlayerAchievement_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_pkey" PRIMARY KEY ("PlayerAchievementId");


--
-- TOC entry 2305 (class 2606 OID 16783)
-- Name: PlayerAssetEnity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PlayerAssetEnity_pkey" PRIMARY KEY ("AssetId");


--
-- TOC entry 2319 (class 2606 OID 16799)
-- Name: PlayerEntity_PlayerName_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerName_key" UNIQUE ("PlayerName");


--
-- TOC entry 2321 (class 2606 OID 16801)
-- Name: PlayerEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_pkey" PRIMARY KEY ("PlayerId");


--
-- TOC entry 2383 (class 2606 OID 149173)
-- Name: PlayerReputationEntity_PlayerId_FractionId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_PlayerId_FractionId_key" UNIQUE ("PlayerId", "FractionId");


--
-- TOC entry 2385 (class 2606 OID 149151)
-- Name: PlayerReputationEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_pkey" PRIMARY KEY ("ReputationId");


SET search_path = public, pg_catalog;

--
-- TOC entry 2351 (class 2606 OID 66325)
-- Name: GlobalChatMessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_pkey" PRIMARY KEY ("MessageId");


--
-- TOC entry 2355 (class 2606 OID 66335)
-- Name: PrivateChatMessageData_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_pkey" PRIMARY KEY ("MessageId");


--
-- TOC entry 2353 (class 2606 OID 66330)
-- Name: PrivateChatMessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_pkey" PRIMARY KEY ("MessageId");


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2323 (class 2606 OID 16825)
-- Name: Deposits_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Deposits"
    ADD CONSTRAINT "Deposits_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2325 (class 2606 OID 16827)
-- Name: Roles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Roles"
    ADD CONSTRAINT "Roles_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2328 (class 2606 OID 16829)
-- Name: UserClaims_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "UserClaims_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2331 (class 2606 OID 16831)
-- Name: UserLogins_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "UserLogins_pkey" PRIMARY KEY ("UserId", "LoginProvider", "ProviderKey");


--
-- TOC entry 2335 (class 2606 OID 16833)
-- Name: UserRoles_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "UserRoles_pkey" PRIMARY KEY ("UserId", "RoleId");


--
-- TOC entry 2337 (class 2606 OID 16835)
-- Name: Users_pkey; Type: CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 2326 (class 1259 OID 16836)
-- Name: IX_UserClaims_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserClaims_UserId" ON "UserClaims" USING btree ("UserId");


--
-- TOC entry 2329 (class 1259 OID 16837)
-- Name: IX_UserLogins_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserLogins_UserId" ON "UserLogins" USING btree ("UserId");


--
-- TOC entry 2332 (class 1259 OID 16838)
-- Name: IX_UserRoles_RoleId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserRoles_RoleId" ON "UserRoles" USING btree ("RoleId");


--
-- TOC entry 2333 (class 1259 OID 16839)
-- Name: IX_UserRoles_UserId; Type: INDEX; Schema: vrs; Owner: postgres
--

CREATE INDEX "IX_UserRoles_UserId" ON "UserRoles" USING btree ("UserId");


SET search_path = "League", pg_catalog;

--
-- TOC entry 2387 (class 2606 OID 16875)
-- Name: LeagueMemberEntity_LeagueId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_LeagueId_fkey" FOREIGN KEY ("LeagueId") REFERENCES "LeagueEntity"("LeagueId") ON DELETE CASCADE;


--
-- TOC entry 2388 (class 2606 OID 16880)
-- Name: LeagueMemberEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: League; Owner: postgres
--

ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = "Matches", pg_catalog;

--
-- TOC entry 2398 (class 2606 OID 49886)
-- Name: MatchEntity_WinnerTeamId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_fkey" FOREIGN KEY ("WinnerTeamId") REFERENCES "MatchTeam"("TeamId");


--
-- TOC entry 2402 (class 2606 OID 49870)
-- Name: MatchMember_MatchId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;


--
-- TOC entry 2400 (class 2606 OID 49835)
-- Name: MatchMember_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2401 (class 2606 OID 49840)
-- Name: MatchMember_TeamId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_TeamId_fkey" FOREIGN KEY ("TeamId") REFERENCES "MatchTeam"("TeamId") ON DELETE CASCADE;


--
-- TOC entry 2399 (class 2606 OID 49830)
-- Name: MatchTeam_MatchId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;


--
-- TOC entry 2403 (class 2606 OID 49847)
-- Name: MemberAchievements_MemberId_fkey; Type: FK CONSTRAINT; Schema: Matches; Owner: postgres
--

ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_MemberId_fkey" FOREIGN KEY ("MemberId") REFERENCES "MatchMember"("MemberId") ON DELETE CASCADE;


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2390 (class 2606 OID 16895)
-- Name: FriendEntity_FriendId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_FriendId_fkey" FOREIGN KEY ("FriendId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2391 (class 2606 OID 16900)
-- Name: FriendEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2422 (class 2606 OID 115894)
-- Name: InstanceOfInstalledAddon_AddonId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_AddonId_fkey" FOREIGN KEY ("AddonId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2423 (class 2606 OID 115899)
-- Name: InstanceOfInstalledAddon_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfInstalledAddon"
    ADD CONSTRAINT "InstanceOfInstalledAddon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2421 (class 2606 OID 107674)
-- Name: InstanceOfItemAddon_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemAddon"
    ADD CONSTRAINT "InstanceOfItemAddon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2420 (class 2606 OID 107679)
-- Name: InstanceOfItemSkin_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfItemSkin"
    ADD CONSTRAINT "InstanceOfItemSkin_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfTargetItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2419 (class 2606 OID 91180)
-- Name: InstanceOfItemSkin_TargetPlayerItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfItemSkin_TargetPlayerItemId_fkey" FOREIGN KEY ("TargetPlayerItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2412 (class 2606 OID 91096)
-- Name: InstanceOfPlayerArmour_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerArmour"
    ADD CONSTRAINT "InstanceOfPlayerArmour_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2389 (class 2606 OID 91082)
-- Name: InstanceOfPlayerCharacter_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerCharacter"
    ADD CONSTRAINT "InstanceOfPlayerCharacter_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2410 (class 2606 OID 91198)
-- Name: InstanceOfPlayerItem_ModelId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "InstanceOfPlayerItem_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "Store"."AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2411 (class 2606 OID 91101)
-- Name: InstanceOfPlayerItem_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "InstanceOfPlayerItem_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2413 (class 2606 OID 91089)
-- Name: InstanceOfPlayerWeapon_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerWeapon"
    ADD CONSTRAINT "InstanceOfPlayerWeapon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2418 (class 2606 OID 107689)
-- Name: InstanceOfTargetItem_ModelId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfTargetItem_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "Store"."AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2417 (class 2606 OID 107684)
-- Name: InstanceOfTargetItem_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfTargetItem"
    ADD CONSTRAINT "InstanceOfTargetItem_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2415 (class 2606 OID 91142)
-- Name: PlayerAchievement_AchievementTypeId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_AchievementTypeId_fkey" FOREIGN KEY ("AchievementTypeId") REFERENCES "Achievements"."AchievementContainer"("AchievementTypeId") ON DELETE CASCADE;


--
-- TOC entry 2416 (class 2606 OID 91152)
-- Name: PlayerAchievement_LastAchievementInstanceId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_LastAchievementInstanceId_fkey" FOREIGN KEY ("LastAchievementInstanceId") REFERENCES "Achievements"."AchievementInstance"("AchievementBonusId");


--
-- TOC entry 2414 (class 2606 OID 91137)
-- Name: PlayerAchievement_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAchievement"
    ADD CONSTRAINT "PlayerAchievement_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2393 (class 2606 OID 149167)
-- Name: PlayerEntity_CurrentFractionId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_CurrentFractionId_fkey" FOREIGN KEY ("CurrentFractionId") REFERENCES "PlayerReputationEntity"("ReputationId");


--
-- TOC entry 2392 (class 2606 OID 16905)
-- Name: PlayerEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES vrs."Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2425 (class 2606 OID 149152)
-- Name: PlayerReputationEntity_FractionId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "Store"."FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2424 (class 2606 OID 149157)
-- Name: PlayerReputationEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2386 (class 2606 OID 16870)
-- Name: PreSetEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PreSetEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = public, pg_catalog;

--
-- TOC entry 2405 (class 2606 OID 124086)
-- Name: GlobalChatMessage_EditorId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_EditorId_fkey" FOREIGN KEY ("EditorId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2404 (class 2606 OID 124081)
-- Name: GlobalChatMessage_SenderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_SenderId_fkey" FOREIGN KEY ("SenderId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2409 (class 2606 OID 66356)
-- Name: PrivateChatMessageData_ReceiverId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_ReceiverId_fkey" FOREIGN KEY ("ReceiverId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2408 (class 2606 OID 66351)
-- Name: PrivateChatMessageData_SenderId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_SenderId_fkey" FOREIGN KEY ("SenderId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2407 (class 2606 OID 66346)
-- Name: PrivateChatMessage_MessageDataId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_MessageDataId_fkey" FOREIGN KEY ("MessageDataId") REFERENCES "PrivateChatMessageData"("MessageId") ON DELETE CASCADE;


--
-- TOC entry 2406 (class 2606 OID 66336)
-- Name: PrivateChatMessage_PlayerId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;


SET search_path = vrs, pg_catalog;

--
-- TOC entry 2394 (class 2606 OID 16950)
-- Name: FK_UserClaims_Users_User_Id; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "FK_UserClaims_Users_User_Id" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2395 (class 2606 OID 16955)
-- Name: FK_UserLogins_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "FK_UserLogins_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2396 (class 2606 OID 16960)
-- Name: FK_UserRoles_Roles_RoleId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Roles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "Roles"("Id") ON DELETE CASCADE;


--
-- TOC entry 2397 (class 2606 OID 16965)
-- Name: FK_UserRoles_Users_UserId; Type: FK CONSTRAINT; Schema: vrs; Owner: postgres
--

ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;


--
-- TOC entry 2545 (class 0 OID 0)
-- Dependencies: 16
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-07-01 14:20:59

--
-- PostgreSQL database dump complete
--

