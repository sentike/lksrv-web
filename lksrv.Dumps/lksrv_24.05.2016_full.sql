PGDMP     +    0                t            postgres    9.5.2    9.5.2 �    �	           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �	           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                        2615    16619    Achievements    SCHEMA        CREATE SCHEMA "Achievements";
    DROP SCHEMA "Achievements";
             postgres    false                        2615    16620    League    SCHEMA        CREATE SCHEMA "League";
    DROP SCHEMA "League";
             postgres    false                        2615    49807    Matches    SCHEMA        CREATE SCHEMA "Matches";
    DROP SCHEMA "Matches";
             postgres    false                        2615    16621    Players    SCHEMA        CREATE SCHEMA "Players";
    DROP SCHEMA "Players";
             postgres    false                        2615    16622    Store    SCHEMA        CREATE SCHEMA "Store";
    DROP SCHEMA "Store";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �	           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    16            �	           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    16                        2615    16623    vrs    SCHEMA        CREATE SCHEMA vrs;
    DROP SCHEMA vrs;
             postgres    false            �            1259    16647    LeagueEntity    TABLE     t  CREATE TABLE "LeagueEntity" (
    "LeagueId" uuid NOT NULL,
    "Cash_Money" smallint NOT NULL,
    "Cash_Donate" smallint NOT NULL,
    "Info_Name" character varying(20) NOT NULL,
    "Info_Abbr" character varying(5) NOT NULL,
    "Info_FoundedDate" timestamp without time zone NOT NULL,
    "Info_AccessType" smallint NOT NULL,
    "Info_JoinPrice" smallint NOT NULL
);
 $   DROP TABLE "League"."LeagueEntity";
       League         postgres    false    12            �            1259    16650    LeagueMemberEntity    TABLE     �   CREATE TABLE "LeagueMemberEntity" (
    "LeagueId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Access" smallint NOT NULL,
    "JoinDate" timestamp without time zone DEFAULT now() NOT NULL
);
 *   DROP TABLE "League"."LeagueMemberEntity";
       League         postgres    false    12            �            1259    49808    MatchEntity    TABLE     ]  CREATE TABLE "MatchEntity" (
    "MatchId" uuid NOT NULL,
    "NodeId" uuid NOT NULL,
    "GameModeTypeId" smallint NOT NULL,
    "GameMapTypeId" smallint NOT NULL,
    "Created" timestamp without time zone NOT NULL,
    "Started" timestamp without time zone NOT NULL,
    "Finished" timestamp without time zone NOT NULL,
    "WinnerTeamId" uuid
);
 $   DROP TABLE "Matches"."MatchEntity";
       Matches         postgres    false    17            �            1259    49819    MatchMember    TABLE     T  CREATE TABLE "MatchMember" (
    "MemberId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "TeamId" uuid NOT NULL,
    "Level" smallint DEFAULT 1 NOT NULL,
    "MatchId" uuid NOT NULL,
    "Award_Money" smallint DEFAULT 0 NOT NULL,
    "Award_Experience" smallint DEFAULT 0 NOT NULL,
    "Award_Reputation" smallint DEFAULT 0 NOT NULL
);
 $   DROP TABLE "Matches"."MatchMember";
       Matches         postgres    false    17            �            1259    49813 	   MatchTeam    TABLE     V   CREATE TABLE "MatchTeam" (
    "TeamId" uuid NOT NULL,
    "MatchId" uuid NOT NULL
);
 "   DROP TABLE "Matches"."MatchTeam";
       Matches         postgres    false    17            �            1259    49825    MemberAchievements    TABLE     �   CREATE TABLE "MemberAchievements" (
    "AchievementId" uuid NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "AchievementCount" smallint NOT NULL,
    "MemberId" uuid NOT NULL
);
 +   DROP TABLE "Matches"."MemberAchievements";
       Matches         postgres    false    17            �            1259    16658    FriendEntity    TABLE     �   CREATE TABLE "FriendEntity" (
    "MemberId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FriendId" uuid NOT NULL,
    "IsLocked" boolean DEFAULT false NOT NULL
);
 %   DROP TABLE "Players"."FriendEntity";
       Players         postgres    false    16    16    13            �            1259    91015    InstanceOfPlayerArmour    TABLE     F   CREATE TABLE "InstanceOfPlayerArmour" (
    "ItemId" uuid NOT NULL
);
 /   DROP TABLE "Players"."InstanceOfPlayerArmour";
       Players         postgres    false    13            �            1259    16654    InstanceOfPlayerCharacter    TABLE     j   CREATE TABLE "InstanceOfPlayerCharacter" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL
);
 2   DROP TABLE "Players"."InstanceOfPlayerCharacter";
       Players         postgres    false    16    16    13            �            1259    90987    InstanceOfPlayerItem    TABLE     R  CREATE TABLE "InstanceOfPlayerItem" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Skin_MaterialId" smallint DEFAULT 0 NOT NULL,
    "Skin_TextureId" smallint DEFAULT 0 NOT NULL,
    "ModelId" smallint NOT NULL,
    modification_1_type smallint DEFAULT 0 NOT NULL,
    modification_2_type smallint DEFAULT 0 NOT NULL,
    modification_3_type smallint DEFAULT 0 NOT NULL,
    modification_4_type smallint DEFAULT 0 NOT NULL,
    modification_5_type smallint DEFAULT 0 NOT NULL,
    modification_1_value smallint DEFAULT 0 NOT NULL,
    modification_2_value smallint DEFAULT 0 NOT NULL,
    modification_3_value smallint DEFAULT 0 NOT NULL,
    modification_4_value smallint DEFAULT 0 NOT NULL,
    modification_5_value smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 0 NOT NULL
);
 -   DROP TABLE "Players"."InstanceOfPlayerItem";
       Players         postgres    false    16    16    13            �            1259    91032    InstanceOfPlayerWeapon    TABLE     F   CREATE TABLE "InstanceOfPlayerWeapon" (
    "ItemId" uuid NOT NULL
);
 /   DROP TABLE "Players"."InstanceOfPlayerWeapon";
       Players         postgres    false    13            �            1259    16662    PlayerEntity    TABLE     �  CREATE TABLE "PlayerEntity" (
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerName" character varying(24) NOT NULL,
    "Experience_Level" smallint DEFAULT 0 NOT NULL,
    "Experience_Experience" bigint DEFAULT 0 NOT NULL,
    "Experience_WeekExperience" bigint DEFAULT 0 NOT NULL,
    "Cash_Money" bigint DEFAULT 0 NOT NULL,
    "Cash_Donate" bigint DEFAULT 0 NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL,
    "PremiumEndDate" timestamp without time zone,
    "LastActivitiDate" timestamp without time zone DEFAULT now() NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT now() NOT NULL,
    "CurrentFractionTypeId" smallint DEFAULT 1 NOT NULL,
    "Reputation_Keepers" bigint DEFAULT 0 NOT NULL,
    "Reputation_Keepers_Friend" bigint DEFAULT 0 NOT NULL,
    "Reputation_RIFT" bigint DEFAULT 0 NOT NULL,
    "Reputation_RIFT_Friend" bigint DEFAULT 0 NOT NULL
);
 %   DROP TABLE "Players"."PlayerEntity";
       Players         postgres    false    16    16    13            �            1259    16632    PlayerProfileEntity    TABLE     ]  CREATE TABLE "PlayerProfileEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "CharacterId" uuid NOT NULL,
    "HelmetId" uuid,
    "MaskId" uuid,
    "ArmourId" uuid,
    "BackpackId" uuid,
    "GlovesId" uuid,
    "PantsId" uuid,
    "BootsId" uuid,
    "PrimaryWeaponId" uuid,
    "SecondaryWeaponId" uuid,
    "PrimaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "PrimaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "PlayerId" uuid NOT NULL,
    "IsEnabled" boolean DEFAULT true NOT NULL,
    "Name" character varying(24),
    "IsUnlocked" boolean DEFAULT false NOT NULL,
    CONSTRAINT "PrimaryWeaponAmmo" CHECK ((("PrimaryWeaponAmmo_Primary" + "PrimaryWeaponAmmo_Secondary") > 0))
);
 ,   DROP TABLE "Players"."PlayerProfileEntity";
       Players         postgres    false    16    16    13            �            1259    90937    AbstractItemInstance    TABLE     �  CREATE TABLE "AbstractItemInstance" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 1 NOT NULL
);
 +   DROP TABLE "Store"."AbstractItemInstance";
       Store         postgres    false    14            �            1259    91071    AmmoItemInstance    TABLE     m   CREATE TABLE "AmmoItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);
 '   DROP TABLE "Store"."AmmoItemInstance";
       Store         postgres    false    14            �            1259    90955    ArmourItemInstance    TABLE     �   CREATE TABLE "ArmourItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "ArmourItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 1))
);
 )   DROP TABLE "Store"."ArmourItemInstance";
       Store         postgres    false    14            �            1259    90980    CharacterItemInstance    TABLE     �   CREATE TABLE "CharacterItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "CharacterItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 5))
);
 ,   DROP TABLE "Store"."CharacterItemInstance";
       Store         postgres    false    14            �            1259    16682    FractionEntity    TABLE     @  CREATE TABLE "FractionEntity" (
    "FractionId" smallint NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Discount_Armour" smallint DEFAULT 0 NOT NULL,
    "Discount_Ammo" smallint DEFAULT 0 NOT NULL,
    "Discount_Weapon" smallint DEFAULT 0 NOT NULL
);
 %   DROP TABLE "Store"."FractionEntity";
       Store         postgres    false    14            �            1259    16727    ModificationEntity    TABLE       CREATE TABLE "ModificationEntity" (
    "ModificationId" uuid NOT NULL,
    "ModificationTypeId" smallint DEFAULT 0 NOT NULL,
    "Range_Minimum" smallint DEFAULT 2 NOT NULL,
    "Range_Maximum" smallint DEFAULT 10 NOT NULL,
    "Range_Chance" smallint DEFAULT 50 NOT NULL
);
 )   DROP TABLE "Store"."ModificationEntity";
       Store         postgres    false    14            �            1259    90974    SkinItemInstance    TABLE     �   CREATE TABLE "SkinItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "ArmourItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 1))
);
 '   DROP TABLE "Store"."SkinItemInstance";
       Store         postgres    false    14            �            1259    90967    WeaponItemInstance    TABLE     �   CREATE TABLE "WeaponItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "WeaponItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 0))
);
 )   DROP TABLE "Store"."WeaponItemInstance";
       Store         postgres    false    14            �            1259    66321    GlobalChatMessage    TABLE       CREATE TABLE "GlobalChatMessage" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "EditorId" uuid,
    "LanguageId" smallint NOT NULL
);
 '   DROP TABLE public."GlobalChatMessage";
       public         postgres    false    16            �            1259    66326    PrivateChatMessage    TABLE     �   CREATE TABLE "PrivateChatMessage" (
    "MessageId" uuid NOT NULL,
    "MessageDataId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL
);
 (   DROP TABLE public."PrivateChatMessage";
       public         postgres    false    16            �            1259    66331    PrivateChatMessageData    TABLE       CREATE TABLE "PrivateChatMessageData" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "ReceiverId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "IsReaded" boolean NOT NULL
);
 ,   DROP TABLE public."PrivateChatMessageData";
       public         postgres    false    16            �            1259    16734    Deposits    TABLE       CREATE TABLE "Deposits" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserId" uuid NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "Value" bigint DEFAULT 0 NOT NULL,
    "Currency" smallint DEFAULT 0 NOT NULL
);
    DROP TABLE vrs."Deposits";
       vrs         postgres    false    16    16    15            �            1259    16740    Roles    TABLE     ]   CREATE TABLE "Roles" (
    "Id" uuid NOT NULL,
    "Name" character varying(256) NOT NULL
);
    DROP TABLE vrs."Roles";
       vrs         postgres    false    15            �            1259    16743 
   UserClaims    TABLE     �   CREATE TABLE "UserClaims" (
    "Id" integer NOT NULL,
    "ClaimType" character varying(256),
    "ClaimValue" character varying(256),
    "UserId" uuid NOT NULL
);
    DROP TABLE vrs."UserClaims";
       vrs         postgres    false    15            �            1259    16749    UserClaims_Id_seq    SEQUENCE     u   CREATE SEQUENCE "UserClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE vrs."UserClaims_Id_seq";
       vrs       postgres    false    217    15            �	           0    0    UserClaims_Id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE "UserClaims_Id_seq" OWNED BY "UserClaims"."Id";
            vrs       postgres    false    218            �            1259    16751 
   UserLogins    TABLE     �   CREATE TABLE "UserLogins" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "LoginProvider" character varying(128) NOT NULL,
    "ProviderKey" character varying(128) NOT NULL
);
    DROP TABLE vrs."UserLogins";
       vrs         postgres    false    16    16    15            �            1259    16755 	   UserRoles    TABLE     v   CREATE TABLE "UserRoles" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RoleId" uuid NOT NULL
);
    DROP TABLE vrs."UserRoles";
       vrs         postgres    false    16    16    15            �            1259    16759    Users    TABLE     �  CREATE TABLE "Users" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserName" character varying(256) NOT NULL,
    "PasswordHash" character varying(256),
    "SecurityStamp" uuid,
    "Email" character varying(256) DEFAULT NULL::character varying,
    "EmailConfirmed" boolean DEFAULT false NOT NULL,
    "PhoneNumber" character varying(32),
    "PhoneNumberConfirmed" boolean DEFAULT false NOT NULL,
    "TwoFactorEnabled" boolean DEFAULT false NOT NULL,
    "LockoutEndDateUtc" timestamp without time zone,
    "LockoutEnabled" boolean DEFAULT false NOT NULL,
    "AccessFailedCount" smallint DEFAULT 0 NOT NULL,
    "PlayerEntityId" bigint,
    "Balance" bigint DEFAULT 0 NOT NULL
);
    DROP TABLE vrs."Users";
       vrs         postgres    false    16    16    15            �           2604    16773    Id    DEFAULT     f   ALTER TABLE ONLY "UserClaims" ALTER COLUMN "Id" SET DEFAULT nextval('"UserClaims_Id_seq"'::regclass);
 =   ALTER TABLE vrs."UserClaims" ALTER COLUMN "Id" DROP DEFAULT;
       vrs       postgres    false    218    217            �	          0    16647    LeagueEntity 
   TABLE DATA                     League       postgres    false    208   �       �	          0    16650    LeagueMemberEntity 
   TABLE DATA                     League       postgres    false    209   �       �	          0    49808    MatchEntity 
   TABLE DATA                     Matches       postgres    false    222   
�       �	          0    49819    MatchMember 
   TABLE DATA                     Matches       postgres    false    224   $�       �	          0    49813 	   MatchTeam 
   TABLE DATA                     Matches       postgres    false    223   >�       �	          0    49825    MemberAchievements 
   TABLE DATA                     Matches       postgres    false    225   X�       �	          0    16658    FriendEntity 
   TABLE DATA                     Players       postgres    false    211   r�       �	          0    91015    InstanceOfPlayerArmour 
   TABLE DATA                     Players       postgres    false    235   ��       �	          0    16654    InstanceOfPlayerCharacter 
   TABLE DATA                     Players       postgres    false    210   [�       �	          0    90987    InstanceOfPlayerItem 
   TABLE DATA                     Players       postgres    false    234   ٿ       �	          0    91032    InstanceOfPlayerWeapon 
   TABLE DATA                     Players       postgres    false    236   K�       �	          0    16662    PlayerEntity 
   TABLE DATA                     Players       postgres    false    212   x�       �	          0    16632    PlayerProfileEntity 
   TABLE DATA                     Players       postgres    false    207   ��       �	          0    90937    AbstractItemInstance 
   TABLE DATA                     Store       postgres    false    229   ��       �	          0    91071    AmmoItemInstance 
   TABLE DATA                     Store       postgres    false    237   .�       �	          0    90955    ArmourItemInstance 
   TABLE DATA                     Store       postgres    false    230   H�       �	          0    90980    CharacterItemInstance 
   TABLE DATA                     Store       postgres    false    233   ��       �	          0    16682    FractionEntity 
   TABLE DATA                     Store       postgres    false    213   ;�       �	          0    16727    ModificationEntity 
   TABLE DATA                     Store       postgres    false    214   ��       �	          0    90974    SkinItemInstance 
   TABLE DATA                     Store       postgres    false    232   ��       �	          0    90967    WeaponItemInstance 
   TABLE DATA                     Store       postgres    false    231   
�       �	          0    66321    GlobalChatMessage 
   TABLE DATA                     public       postgres    false    226   ��       �	          0    66326    PrivateChatMessage 
   TABLE DATA                     public       postgres    false    227   c�       �	          0    66331    PrivateChatMessageData 
   TABLE DATA                     public       postgres    false    228   }�       �	          0    16734    Deposits 
   TABLE DATA                     vrs       postgres    false    215   ��       �	          0    16740    Roles 
   TABLE DATA                     vrs       postgres    false    216   ��       �	          0    16743 
   UserClaims 
   TABLE DATA                     vrs       postgres    false    217   ��       �	           0    0    UserClaims_Id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('"UserClaims_Id_seq"', 2, true);
            vrs       postgres    false    218            �	          0    16751 
   UserLogins 
   TABLE DATA                     vrs       postgres    false    219   ��       �	          0    16755 	   UserRoles 
   TABLE DATA                     vrs       postgres    false    220   ��       �	          0    16759    Users 
   TABLE DATA                     vrs       postgres    false    221   ��       �           2606    16785    LeagueEntity_Info_Abbr_key 
   CONSTRAINT     f   ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Abbr_key" UNIQUE ("Info_Abbr");
 W   ALTER TABLE ONLY "League"."LeagueEntity" DROP CONSTRAINT "LeagueEntity_Info_Abbr_key";
       League         postgres    false    208    208            �           2606    16787    LeagueEntity_Info_Name_key 
   CONSTRAINT     f   ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Name_key" UNIQUE ("Info_Name");
 W   ALTER TABLE ONLY "League"."LeagueEntity" DROP CONSTRAINT "LeagueEntity_Info_Name_key";
       League         postgres    false    208    208            �           2606    16789    LeagueEntity_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_pkey" PRIMARY KEY ("LeagueId");
 N   ALTER TABLE ONLY "League"."LeagueEntity" DROP CONSTRAINT "LeagueEntity_pkey";
       League         postgres    false    208    208            �           2606    16791    LeagueMemberEntity_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_pkey" PRIMARY KEY ("PlayerId");
 Z   ALTER TABLE ONLY "League"."LeagueMemberEntity" DROP CONSTRAINT "LeagueMemberEntity_pkey";
       League         postgres    false    209    209            	           2606    49892    MatchEntity_WinnerTeamId_key 
   CONSTRAINT     j   ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_key" UNIQUE ("WinnerTeamId");
 Y   ALTER TABLE ONLY "Matches"."MatchEntity" DROP CONSTRAINT "MatchEntity_WinnerTeamId_key";
       Matches         postgres    false    222    222            	           2606    49812    MatchEntity_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_pkey" PRIMARY KEY ("MatchId");
 M   ALTER TABLE ONLY "Matches"."MatchEntity" DROP CONSTRAINT "MatchEntity_pkey";
       Matches         postgres    false    222    222            	           2606    49846    MatchMember_PlayerId_TeamId_key 
   CONSTRAINT     s   ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_TeamId_key" UNIQUE ("PlayerId", "TeamId");
 \   ALTER TABLE ONLY "Matches"."MatchMember" DROP CONSTRAINT "MatchMember_PlayerId_TeamId_key";
       Matches         postgres    false    224    224    224            	           2606    49824    MatchMember_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_pkey" PRIMARY KEY ("MemberId");
 M   ALTER TABLE ONLY "Matches"."MatchMember" DROP CONSTRAINT "MatchMember_pkey";
       Matches         postgres    false    224    224            	           2606    49818    MatchTeam_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_pkey" PRIMARY KEY ("TeamId");
 I   ALTER TABLE ONLY "Matches"."MatchTeam" DROP CONSTRAINT "MatchTeam_pkey";
       Matches         postgres    false    223    223            	           2606    49829    MemberAchievements_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_pkey" PRIMARY KEY ("AchievementId");
 [   ALTER TABLE ONLY "Matches"."MemberAchievements" DROP CONSTRAINT "MemberAchievements_pkey";
       Matches         postgres    false    225    225            '	           2606    91021 <   AbstractPlayerItemInstance_ItemId_ModelId_CategoryTypeId_key 
   CONSTRAINT     �   ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "AbstractPlayerItemInstance_ItemId_ModelId_CategoryTypeId_key" UNIQUE ("ItemId", "ModelId", "CategoryTypeId");
 �   ALTER TABLE ONLY "Players"."InstanceOfPlayerItem" DROP CONSTRAINT "AbstractPlayerItemInstance_ItemId_ModelId_CategoryTypeId_key";
       Players         postgres    false    234    234    234    234            )	           2606    91014 >   AbstractPlayerItemInstance_ModelId_CategoryTypeId_PlayerId_key 
   CONSTRAINT     �   ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "AbstractPlayerItemInstance_ModelId_CategoryTypeId_PlayerId_key" UNIQUE ("ModelId", "CategoryTypeId", "PlayerId");
 �   ALTER TABLE ONLY "Players"."InstanceOfPlayerItem" DROP CONSTRAINT "AbstractPlayerItemInstance_ModelId_CategoryTypeId_PlayerId_key";
       Players         postgres    false    234    234    234    234            +	           2606    91004    AbstractPlayerItemInstance_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "AbstractPlayerItemInstance_pkey" PRIMARY KEY ("ItemId");
 e   ALTER TABLE ONLY "Players"."InstanceOfPlayerItem" DROP CONSTRAINT "AbstractPlayerItemInstance_pkey";
       Players         postgres    false    234    234            �           2606    16797    FriendEntity_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_pkey" PRIMARY KEY ("MemberId");
 O   ALTER TABLE ONLY "Players"."FriendEntity" DROP CONSTRAINT "FriendEntity_pkey";
       Players         postgres    false    211    211            -	           2606    91095    InstanceOfPlayerArmour_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY "InstanceOfPlayerArmour"
    ADD CONSTRAINT "InstanceOfPlayerArmour_pkey" PRIMARY KEY ("ItemId");
 c   ALTER TABLE ONLY "Players"."InstanceOfPlayerArmour" DROP CONSTRAINT "InstanceOfPlayerArmour_pkey";
       Players         postgres    false    235    235            �           2606    91081    InstanceOfPlayerCharacter_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY "InstanceOfPlayerCharacter"
    ADD CONSTRAINT "InstanceOfPlayerCharacter_pkey" PRIMARY KEY ("ItemId");
 i   ALTER TABLE ONLY "Players"."InstanceOfPlayerCharacter" DROP CONSTRAINT "InstanceOfPlayerCharacter_pkey";
       Players         postgres    false    210    210            /	           2606    91088    InstanceOfPlayerWeapon_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY "InstanceOfPlayerWeapon"
    ADD CONSTRAINT "InstanceOfPlayerWeapon_pkey" PRIMARY KEY ("ItemId");
 c   ALTER TABLE ONLY "Players"."InstanceOfPlayerWeapon" DROP CONSTRAINT "InstanceOfPlayerWeapon_pkey";
       Players         postgres    false    236    236            �           2606    16783    PlayerAssetEnity_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PlayerAssetEnity_pkey" PRIMARY KEY ("AssetId");
 Z   ALTER TABLE ONLY "Players"."PlayerProfileEntity" DROP CONSTRAINT "PlayerAssetEnity_pkey";
       Players         postgres    false    207    207            �           2606    16799    PlayerEntity_PlayerName_key 
   CONSTRAINT     h   ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerName_key" UNIQUE ("PlayerName");
 Y   ALTER TABLE ONLY "Players"."PlayerEntity" DROP CONSTRAINT "PlayerEntity_PlayerName_key";
       Players         postgres    false    212    212            �           2606    16801    PlayerEntity_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_pkey" PRIMARY KEY ("PlayerId");
 O   ALTER TABLE ONLY "Players"."PlayerEntity" DROP CONSTRAINT "PlayerEntity_pkey";
       Players         postgres    false    212    212            	           2606    90954    AbstractItemInstance_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY "AbstractItemInstance"
    ADD CONSTRAINT "AbstractItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");
 ]   ALTER TABLE ONLY "Store"."AbstractItemInstance" DROP CONSTRAINT "AbstractItemInstance_pkey";
       Store         postgres    false    229    229    229            1	           2606    91076    AmmoItemInstance_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY "AmmoItemInstance"
    ADD CONSTRAINT "AmmoItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");
 U   ALTER TABLE ONLY "Store"."AmmoItemInstance" DROP CONSTRAINT "AmmoItemInstance_pkey";
       Store         postgres    false    237    237    237            	           2606    90959    ArmourItemInstance_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");
 Y   ALTER TABLE ONLY "Store"."ArmourItemInstance" DROP CONSTRAINT "ArmourItemInstance_pkey";
       Store         postgres    false    230    230    230            %	           2606    90985    CharacterItemInstance_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY "CharacterItemInstance"
    ADD CONSTRAINT "CharacterItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");
 _   ALTER TABLE ONLY "Store"."CharacterItemInstance" DROP CONSTRAINT "CharacterItemInstance_pkey";
       Store         postgres    false    233    233    233            �           2606    16813    FractionEntity_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY "FractionEntity"
    ADD CONSTRAINT "FractionEntity_pkey" PRIMARY KEY ("FractionId");
 Q   ALTER TABLE ONLY "Store"."FractionEntity" DROP CONSTRAINT "FractionEntity_pkey";
       Store         postgres    false    213    213            �           2606    16821 ,   ModificatioBaseEntity_ModificationTypeId_key 
   CONSTRAINT     �   ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_ModificationTypeId_key" UNIQUE ("ModificationTypeId");
 n   ALTER TABLE ONLY "Store"."ModificationEntity" DROP CONSTRAINT "ModificatioBaseEntity_ModificationTypeId_key";
       Store         postgres    false    214    214            �           2606    16823    ModificatioBaseEntity_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_pkey" PRIMARY KEY ("ModificationId");
 \   ALTER TABLE ONLY "Store"."ModificationEntity" DROP CONSTRAINT "ModificatioBaseEntity_pkey";
       Store         postgres    false    214    214            #	           2606    90979    SkinItemInstance_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY "SkinItemInstance"
    ADD CONSTRAINT "SkinItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");
 U   ALTER TABLE ONLY "Store"."SkinItemInstance" DROP CONSTRAINT "SkinItemInstance_pkey";
       Store         postgres    false    232    232    232            !	           2606    90972    WeaponItemInstance_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY "WeaponItemInstance"
    ADD CONSTRAINT "WeaponItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");
 Y   ALTER TABLE ONLY "Store"."WeaponItemInstance" DROP CONSTRAINT "WeaponItemInstance_pkey";
       Store         postgres    false    231    231    231            	           2606    66325    GlobalChatMessage_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_pkey" PRIMARY KEY ("MessageId");
 V   ALTER TABLE ONLY public."GlobalChatMessage" DROP CONSTRAINT "GlobalChatMessage_pkey";
       public         postgres    false    226    226            	           2606    66335    PrivateChatMessageData_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_pkey" PRIMARY KEY ("MessageId");
 `   ALTER TABLE ONLY public."PrivateChatMessageData" DROP CONSTRAINT "PrivateChatMessageData_pkey";
       public         postgres    false    228    228            	           2606    66330    PrivateChatMessage_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_pkey" PRIMARY KEY ("MessageId");
 X   ALTER TABLE ONLY public."PrivateChatMessage" DROP CONSTRAINT "PrivateChatMessage_pkey";
       public         postgres    false    227    227            �           2606    16825    Deposits_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY "Deposits"
    ADD CONSTRAINT "Deposits_pkey" PRIMARY KEY ("Id");
 A   ALTER TABLE ONLY vrs."Deposits" DROP CONSTRAINT "Deposits_pkey";
       vrs         postgres    false    215    215            �           2606    16827 
   Roles_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "Roles"
    ADD CONSTRAINT "Roles_pkey" PRIMARY KEY ("Id");
 ;   ALTER TABLE ONLY vrs."Roles" DROP CONSTRAINT "Roles_pkey";
       vrs         postgres    false    216    216             	           2606    16829    UserClaims_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "UserClaims_pkey" PRIMARY KEY ("Id");
 E   ALTER TABLE ONLY vrs."UserClaims" DROP CONSTRAINT "UserClaims_pkey";
       vrs         postgres    false    217    217            	           2606    16831    UserLogins_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "UserLogins_pkey" PRIMARY KEY ("UserId", "LoginProvider", "ProviderKey");
 E   ALTER TABLE ONLY vrs."UserLogins" DROP CONSTRAINT "UserLogins_pkey";
       vrs         postgres    false    219    219    219    219            	           2606    16833    UserRoles_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "UserRoles_pkey" PRIMARY KEY ("UserId", "RoleId");
 C   ALTER TABLE ONLY vrs."UserRoles" DROP CONSTRAINT "UserRoles_pkey";
       vrs         postgres    false    220    220    220            		           2606    16835 
   Users_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY ("Id");
 ;   ALTER TABLE ONLY vrs."Users" DROP CONSTRAINT "Users_pkey";
       vrs         postgres    false    221    221            �           1259    16836    IX_UserClaims_UserId    INDEX     L   CREATE INDEX "IX_UserClaims_UserId" ON "UserClaims" USING btree ("UserId");
 '   DROP INDEX vrs."IX_UserClaims_UserId";
       vrs         postgres    false    217            	           1259    16837    IX_UserLogins_UserId    INDEX     L   CREATE INDEX "IX_UserLogins_UserId" ON "UserLogins" USING btree ("UserId");
 '   DROP INDEX vrs."IX_UserLogins_UserId";
       vrs         postgres    false    219            	           1259    16838    IX_UserRoles_RoleId    INDEX     J   CREATE INDEX "IX_UserRoles_RoleId" ON "UserRoles" USING btree ("RoleId");
 &   DROP INDEX vrs."IX_UserRoles_RoleId";
       vrs         postgres    false    220            	           1259    16839    IX_UserRoles_UserId    INDEX     J   CREATE INDEX "IX_UserRoles_UserId" ON "UserRoles" USING btree ("UserId");
 &   DROP INDEX vrs."IX_UserRoles_UserId";
       vrs         postgres    false    220            3	           2606    16875     LeagueMemberEntity_LeagueId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_LeagueId_fkey" FOREIGN KEY ("LeagueId") REFERENCES "LeagueEntity"("LeagueId") ON DELETE CASCADE;
 c   ALTER TABLE ONLY "League"."LeagueMemberEntity" DROP CONSTRAINT "LeagueMemberEntity_LeagueId_fkey";
       League       postgres    false    2281    209    208            4	           2606    16880     LeagueMemberEntity_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;
 c   ALTER TABLE ONLY "League"."LeagueMemberEntity" DROP CONSTRAINT "LeagueMemberEntity_PlayerId_fkey";
       League       postgres    false    209    212    2291            >	           2606    49886    MatchEntity_WinnerTeamId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_fkey" FOREIGN KEY ("WinnerTeamId") REFERENCES "MatchTeam"("TeamId");
 Z   ALTER TABLE ONLY "Matches"."MatchEntity" DROP CONSTRAINT "MatchEntity_WinnerTeamId_fkey";
       Matches       postgres    false    2319    223    222            B	           2606    49870    MatchMember_MatchId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;
 U   ALTER TABLE ONLY "Matches"."MatchMember" DROP CONSTRAINT "MatchMember_MatchId_fkey";
       Matches       postgres    false    224    222    2317            @	           2606    49835    MatchMember_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;
 V   ALTER TABLE ONLY "Matches"."MatchMember" DROP CONSTRAINT "MatchMember_PlayerId_fkey";
       Matches       postgres    false    212    224    2291            A	           2606    49840    MatchMember_TeamId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_TeamId_fkey" FOREIGN KEY ("TeamId") REFERENCES "MatchTeam"("TeamId") ON DELETE CASCADE;
 T   ALTER TABLE ONLY "Matches"."MatchMember" DROP CONSTRAINT "MatchMember_TeamId_fkey";
       Matches       postgres    false    2319    223    224            ?	           2606    49830    MatchTeam_MatchId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;
 Q   ALTER TABLE ONLY "Matches"."MatchTeam" DROP CONSTRAINT "MatchTeam_MatchId_fkey";
       Matches       postgres    false    222    2317    223            C	           2606    49847     MemberAchievements_MemberId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_MemberId_fkey" FOREIGN KEY ("MemberId") REFERENCES "MatchMember"("MemberId") ON DELETE CASCADE;
 d   ALTER TABLE ONLY "Matches"."MemberAchievements" DROP CONSTRAINT "MemberAchievements_MemberId_fkey";
       Matches       postgres    false    2323    224    225            6	           2606    16895    FriendEntity_FriendId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_FriendId_fkey" FOREIGN KEY ("FriendId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;
 X   ALTER TABLE ONLY "Players"."FriendEntity" DROP CONSTRAINT "FriendEntity_FriendId_fkey";
       Players       postgres    false    212    211    2291            7	           2606    16900    FriendEntity_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;
 X   ALTER TABLE ONLY "Players"."FriendEntity" DROP CONSTRAINT "FriendEntity_PlayerId_fkey";
       Players       postgres    false    212    211    2291            K	           2606    91096 "   InstanceOfPlayerArmour_ItemId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "InstanceOfPlayerArmour"
    ADD CONSTRAINT "InstanceOfPlayerArmour_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;
 j   ALTER TABLE ONLY "Players"."InstanceOfPlayerArmour" DROP CONSTRAINT "InstanceOfPlayerArmour_ItemId_fkey";
       Players       postgres    false    234    235    2347            5	           2606    91082 %   InstanceOfPlayerCharacter_ItemId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "InstanceOfPlayerCharacter"
    ADD CONSTRAINT "InstanceOfPlayerCharacter_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;
 p   ALTER TABLE ONLY "Players"."InstanceOfPlayerCharacter" DROP CONSTRAINT "InstanceOfPlayerCharacter_ItemId_fkey";
       Players       postgres    false    2347    234    210            J	           2606    91106 !   InstanceOfPlayerItem_ModelId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "InstanceOfPlayerItem_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "Store"."AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;
 g   ALTER TABLE ONLY "Players"."InstanceOfPlayerItem" DROP CONSTRAINT "InstanceOfPlayerItem_ModelId_fkey";
       Players       postgres    false    229    229    234    2333    234            I	           2606    91101 "   InstanceOfPlayerItem_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "InstanceOfPlayerItem"
    ADD CONSTRAINT "InstanceOfPlayerItem_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;
 h   ALTER TABLE ONLY "Players"."InstanceOfPlayerItem" DROP CONSTRAINT "InstanceOfPlayerItem_PlayerId_fkey";
       Players       postgres    false    234    2291    212            L	           2606    91089 "   InstanceOfPlayerWeapon_ItemId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "InstanceOfPlayerWeapon"
    ADD CONSTRAINT "InstanceOfPlayerWeapon_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;
 j   ALTER TABLE ONLY "Players"."InstanceOfPlayerWeapon" DROP CONSTRAINT "InstanceOfPlayerWeapon_ItemId_fkey";
       Players       postgres    false    234    2347    236            9	           2606    66289     PlayerEntity_FractionTypeId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_FractionTypeId_fkey" FOREIGN KEY ("CurrentFractionTypeId") REFERENCES "Store"."FractionEntity"("FractionId");
 ^   ALTER TABLE ONLY "Players"."PlayerEntity" DROP CONSTRAINT "PlayerEntity_FractionTypeId_fkey";
       Players       postgres    false    212    213    2293            8	           2606    16905    PlayerEntity_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES vrs."Users"("Id") ON DELETE CASCADE;
 X   ALTER TABLE ONLY "Players"."PlayerEntity" DROP CONSTRAINT "PlayerEntity_PlayerId_fkey";
       Players       postgres    false    221    212    2313            2	           2606    16870    PreSetEntity_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PreSetEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;
 _   ALTER TABLE ONLY "Players"."PlayerProfileEntity" DROP CONSTRAINT "PreSetEntity_PlayerId_fkey";
       Players       postgres    false    207    2291    212            H	           2606    90960    ArmourItemInstance_ModelId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;
 a   ALTER TABLE ONLY "Store"."ArmourItemInstance" DROP CONSTRAINT "ArmourItemInstance_ModelId_fkey";
       Store       postgres    false    230    230    229    2333    229            G	           2606    66356 &   PrivateChatMessageData_ReceiverId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_ReceiverId_fkey" FOREIGN KEY ("ReceiverId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;
 k   ALTER TABLE ONLY public."PrivateChatMessageData" DROP CONSTRAINT "PrivateChatMessageData_ReceiverId_fkey";
       public       postgres    false    212    228    2291            F	           2606    66351 $   PrivateChatMessageData_SenderId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_SenderId_fkey" FOREIGN KEY ("SenderId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;
 i   ALTER TABLE ONLY public."PrivateChatMessageData" DROP CONSTRAINT "PrivateChatMessageData_SenderId_fkey";
       public       postgres    false    2291    212    228            E	           2606    66346 %   PrivateChatMessage_MessageDataId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_MessageDataId_fkey" FOREIGN KEY ("MessageDataId") REFERENCES "PrivateChatMessageData"("MessageId") ON DELETE CASCADE;
 f   ALTER TABLE ONLY public."PrivateChatMessage" DROP CONSTRAINT "PrivateChatMessage_MessageDataId_fkey";
       public       postgres    false    227    228    2331            D	           2606    66336     PrivateChatMessage_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;
 a   ALTER TABLE ONLY public."PrivateChatMessage" DROP CONSTRAINT "PrivateChatMessage_PlayerId_fkey";
       public       postgres    false    2291    227    212            :	           2606    16950    FK_UserClaims_Users_User_Id    FK CONSTRAINT     �   ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "FK_UserClaims_Users_User_Id" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;
 Q   ALTER TABLE ONLY vrs."UserClaims" DROP CONSTRAINT "FK_UserClaims_Users_User_Id";
       vrs       postgres    false    217    2313    221            ;	           2606    16955    FK_UserLogins_Users_UserId    FK CONSTRAINT     �   ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "FK_UserLogins_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;
 P   ALTER TABLE ONLY vrs."UserLogins" DROP CONSTRAINT "FK_UserLogins_Users_UserId";
       vrs       postgres    false    219    2313    221            <	           2606    16960    FK_UserRoles_Roles_RoleId    FK CONSTRAINT     �   ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Roles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "Roles"("Id") ON DELETE CASCADE;
 N   ALTER TABLE ONLY vrs."UserRoles" DROP CONSTRAINT "FK_UserRoles_Roles_RoleId";
       vrs       postgres    false    216    220    2301            =	           2606    16965    FK_UserRoles_Users_UserId    FK CONSTRAINT     �   ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;
 N   ALTER TABLE ONLY vrs."UserRoles" DROP CONSTRAINT "FK_UserRoles_Users_UserId";
       vrs       postgres    false    221    2313    220            �	   �   x�M˹�@����l�&����(&Ehk�T
w�`��+j�b�o2�I^B��[@k�Nw���jZ��S���>7֨���5�QS��1c�b�o_,��H%�߱�����;�lev�J(ԇC��'�\��c`6	"H��D�^�(
�; J�z6���˷�{�Gh���)ϟ��������+D�3�q9�I�      �	   
   x���          �	   
   x���          �	   
   x���          �	   
   x���          �	   
   x���          �	   
   x���          �	   �   x��ͱn1 ���"�,��䜨C�
�B�;�=*]�п/���{eܽ�\ٟn,���\���K�t�.���et�\�\��q��׏�ѭW>��Y2�$B�=#�vV���<dJ�y��s�bAA(����=���9�6�B�fP9#p�6UnT}}*0f#@��I4Eљ0�0ރa���n�      �	   n   x���v
Q���WP��+.I�KN�O�I�L-r�H,JL.I-RR�P�,I��LQ�Ts�	uV�P77�43�02�M1M3�5�04ҵH57�MJMKI52H4MM5S״��� C��      �	   b  x���Mk�@�{~��%	d`�fgDO��`h�Ҹ�����b���uJ��N(�J��"X��jAH;iv}{�q>�]�o&����VVYo�;y��l�����tw����d�;��~]�W���R�����s�����j]�����,�2�v�^-�b��M�
]_����b_h?����+���7�)���w�	|Yo����F�'�߾�ty;9;5�K؉n3!$�H�R�TنӋ�)�Л���)BW���u6X٭3OþL�;�ߜ�F�ér􆤃�%C�\��9_�P1T�`U���U�j���1֔ $�!�`����-�t���cT=�*a�B�� �u�J�֢������zP�γ�Ʒ�Y x��ci��Ր��U�
|`U��X(�ch���2[���`U+�qU���,+˻nI,�x�����v��U��*�ASl{5V`F����C�3�UyT=�j����5�PZN�
5rd¢y�*���U-lb�P�W�+$�k���O&��/��-X5� %G�ٴ�U"t�mò�ެ,v��!ƽzXUE��d�'-�T1`�f�;%�2X5���U�D�8g��K�42��ѻP���{�]>9��V��      �	     x��ҽNC1�Oqu���%;�GL��ZD��'�� ��ۓ�w��O>>��|xz����i����U/�N��]��[ӯ��<����>�:������y�m1��YdL�%(j��+!�.�����_���d�{)
z(>  UO��k��ܔ) A�Y 0)d3SebB�V�GJ$*�A�JP,"��FH�rZ�D& .q\;����|�,W�� �䓸
���PGNE[�n�%J��lP��!�'c���E��(�U@c��r*W?jJ`����������l� ��ۢ      �	     x�}��o�0���+^��.-�d2���T�mʹ�R����/�3l�=�}_�zm��8AA�<!c��(_j���u���E|���*A	��!�`s�~��
��[�x��>�K5/$�Gû���Zr~fK[Qo}��$䕞fZ��i٬V
�^(�&M	���P��xI� �߮����mK,����i��P���ڱI�m�ۘ1p��F^�w]�2ʙ9D�j�?2�g��SbZ��0q�ŐE&6��y��F�u��W��P��e/��:��^�ݥk      �	   H  x��P]k�0}�W��T�@���eOu���2u{��ab�H�����
�{hzι�\RV��m��j���������mE7nǃ��^>0�ʛ#���\��;�M{��a�P޷���Xp���ϱ�����pͻ�Ɯa�o[�>��M�Hө�ʗ���<)����;���{㫼{&�G9������M�������M�BS_�@�N	q)��Xh�U$���ϑ�h��4�XQm}i�X�h!� ��j�\���I�	�q�S��͒a�%ŔA(b&tėƀ������ZD$���l(�4!v��3�q��Y͛�d�0�L~�k��      �	      x�ݗ�k� ���+�S=���NY���v�f�׾�B�!�����d4��w�;<�x��|�/v%�����S�F�7p�R!O��o�*~�����[��6���Zin!ׯJ
.�d��%��ⷆ���-���5�\�t�7��]��l�o�teW����!_{4���v���>��9�>�%w����Qɒd��:�d����i~�� �"K��1��O�4X����@F���#	��� l���F����pQ��2l0/�$m9��f!M�N����uѵ      �	   
   x���          �	   e   x���v
Q���WPr,��/-�,I���+.I�KNUR�P��OI��LQ�QPrN,IM�/��,H�h*�9���+h�(jZsyR�4C��fDUӌ!�qq 7�^[      �	   n   x���v
Q���WPr�H,JL.I-�,I���+.I�KNUR�P��OI��LQ��I,IM�/��,H�h*�9���+h�(�jZsyR�@CjhDm��m�	�4��� Ti�&      �	   �   x���v
Q���WPr+JL.���s�+�,�TRЀ�x�(�((9��ǻV�e��%�"�|��R+A\�����Ҽ�xǢ���"T���|���Ă�<%M�0G�P�`Cd�i��9�h4�h<�h2�h��@.. �.�      �	   
   x���          �	   
   x���          �	   s   x���v
Q���WP
OM,���,I���+.I�KNUR�P��OI��LQ�QPrN,IM�/��,H�h*�9���+h�(hZsyR�4C��fDUӌ�j�)UM3��i�T5�b ���M      �	   �	  x��\Mo]��ϯp��=�"ER�h�6@�M�{}N&�$���%�U�*T�a_??�O�<�P�_�}���^�}���g�����{�����S�a>{��ٯ��ǳO��͟���������?��_���\?��i_�?~����ԟ~������o�{���wO�>o�W-�:��sh�RX�Zr�K�<��|Q��i��Q�B�ރqm6��8x?�O�Ç�O������}��!�@�D�R�ˤ�,��i|����7o^<�w�������K�M8��5�*-��te�&⒏b��P&+r��95�� �gh9ΐ�شn�C�b��J��2{�Tb�C��BY\k:��q<�D�`��UZ���8yh�#tk��b~��Fr�/���&�A*�P��������i8g�{ �L��N�q˃h-��$�s�֪�q�߃A�'FELu� �,x=�&#'���irJ1]T�.��{����5�H��VI�i4y��Z�0��0�Q���`�����q
� ݃Ai�k���DN�a��l�t�ln�``]-�5B]51�Jb�洒Um��8 �ċ$R˃2K��V��,��*�%��1p%���hqH��
1�-�(��e� ���.��1ͅ*�V�9�F��K����h3�}Dr�	�.����yl׈rXk׀���MS��IDQ�"^�kԘKF%�vҠ�l�j�G�5s#0p�rQP3ш4؂��Gpa(��3A4c�v�d��Eܨ ���I
0P8F%�mNH���mTX&ǣ�`�u�e�HA=�	���L�(Kt��@D J����V!�w�5�zh�Š�#D8�q��DwuMY�C�P���>!�iX�H{;�Q@�n�H�$ɊbQ`����V�l��ُ�	�r�w�Dj�/�<�Fr� ����Q�q}�I9^��yu���}�8�����B#��i
���<��ZF90�a����r�XB1m�g�� f��V�$�)hMȅ,ȅ�$�Z�����(��^č���8,xD�D�d�^B�$UzY�ͅ��'�=dIԣ尴�;�E��"a͞'{�g�300U��r�������(��ʨ��`���|><å]�[_�N�0���R�}P[@u��%�R��F`PX��^��VGf�F���X�N����}z��M[��H��Pq�e�Bla�^��u��A�x�N���o#��6/t��FiH�
��:��{mw�q#%KNp�c*�@��N!h�LI��٭w` ��vS.��t�Z&0B�L��-�9c�����M��c�=���F��R)�%"It��q �#�č�2�b�;��F��$��\f�4$���L� ���8H��hA؀o4��āFN�z�Q�xM����6R�dsy
�PɥN�l�-�qv� (D�\6�$�k6i��ڔ��f����[m� �S��
8 N��2��C"��[ku�i��`�͛Ƒ�fs�#DI��`g���9+����B���tcM�5��n�����qO��H��8(��/���J%am�{ӭ^��k�8��LL�h������ѹ���������n�هw=���\čL����+���[�_�HMA�I%��鑒d��ZT���H{��Y$��b)9�v>�/��U��D	삖|PBM�9��S��cP��E[m=Q�Ek��-����qͶ���ʇۉ�"�t�Nt�hq��M¦�T��2����qr�kL��w��@���BW�tg����k�J,|�Nlsu�ӈ��P�*����<d���Pz/�`݉�R�E��B#m��ԡ���rzx�lqc���%,�FM%x��8�����������%K9��{<��*��m@;��~�;�G1��F�RdR�7C.8dql�����1�i{ai��̃Վ8 M�j����F_f�4���E)�d,��E�DC�/Gy�>�&����D�_@�����[�3�}�;�/+�e�~r�v��+0`���%h�E���n�/
�ބ��&gG�6��&�d�&���0�CI����
ʖ�n�t�~��M�y�,S��� k�[5;�`��sJ���#[�|�yk9�����y�6�&��N�[<{�(�&^�h)�����"��8FXN����8;��#_TS��=���/d��g£���p�_��.���f�E���yF`Д!r���L~\'⫶�Q�Fl�,_K}��c�Yk�':��(�t�~ ���~i�FIR�[����N�Gt���S��f����T�D�%1춲�H� uՋ����G+_p$���v=h3RGMX�� )�E�`6Ӻ����`�&65p�T�Ύ�x�r˳i�=̴�#L�
#��'Wi'�3�d�Ľ�{Q.n-z�NHDѱ�BG�ܩ��*Ń���%�ɺ*�Ұ�RXH|�	�ܪ@0[=q�?gz7߾��󿣐�R��9���7�ю��      �	   
   x���          �	   
   x���          �	   
   x���          �	   
   x���          �	   
   x���          �	   �   x�5���0  w���RH�M��tr` 4�e��&��@br�ݥy�<J����j6Sf_�0C��jx p��~;m�m��j��%�����!3�K�(8n��u�5VF�HP��Ơ@�3u��۹�Dȼuc��3tn9�F�h�N���T�P�4b\(�=��B�6+      �	   
   x���          �	   �  x��T�N�@}�+���
�x��U!��J�Cӷ�[n�l�4��ڄR�E��ky�9͜9gv��}3�����Z=�M��k�������V�z!Ϸi�{�Ϫxd�&��Q�u�W0���4��le�gib��J��� m�M;��4k' �=6H�2��D_Ba�B��i�R&�;ec�[�&)�İ3e�����	1$����n�A�Վ;�d�"C�(��P
y�g׎&��Z��b����,��U����v�?��Ns��S���Y�=7��@�#?-���W�Yoywwn2q���uF���Uk��t����(5p����`_d`!��i-����%��;�>��s��&��R^R[�͐oI���|L1�_j��_E|�]��M�H�{�.{���z���/'����.�6��o�q�kG.Ľn�.8Μ�i�7�
0��3T���ː��2�zX`�2�7�_��]f0���C�j��p�|#�VX���e�e���M�^L�!�tߋ�p��B�I�6խph,���վv���Jo��A	���%DD�R|_$�ĕ1���>Q�~ {#7Buoo�]7�'�+C&��Ikb�����82��p=�fu��]��Ө���Oǚ��--��YJK*�\&�{H-%�V>�;�Wm���NaZ�     