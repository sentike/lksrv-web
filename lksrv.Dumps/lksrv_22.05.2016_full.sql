PGDMP         $                t            postgres    9.5.2    9.5.2 �    
           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            
           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                        2615    16619    Achievements    SCHEMA        CREATE SCHEMA "Achievements";
    DROP SCHEMA "Achievements";
             postgres    false                        2615    16620    League    SCHEMA        CREATE SCHEMA "League";
    DROP SCHEMA "League";
             postgres    false                        2615    49807    Matches    SCHEMA        CREATE SCHEMA "Matches";
    DROP SCHEMA "Matches";
             postgres    false                        2615    16621    Players    SCHEMA        CREATE SCHEMA "Players";
    DROP SCHEMA "Players";
             postgres    false                        2615    16622    Store    SCHEMA        CREATE SCHEMA "Store";
    DROP SCHEMA "Store";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            
           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    16            
           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    16                        2615    16623    vrs    SCHEMA        CREATE SCHEMA vrs;
    DROP SCHEMA vrs;
             postgres    false            �            1259    16647    LeagueEntity    TABLE     t  CREATE TABLE "LeagueEntity" (
    "LeagueId" uuid NOT NULL,
    "Cash_Money" smallint NOT NULL,
    "Cash_Donate" smallint NOT NULL,
    "Info_Name" character varying(20) NOT NULL,
    "Info_Abbr" character varying(5) NOT NULL,
    "Info_FoundedDate" timestamp without time zone NOT NULL,
    "Info_AccessType" smallint NOT NULL,
    "Info_JoinPrice" smallint NOT NULL
);
 $   DROP TABLE "League"."LeagueEntity";
       League         postgres    false    12            �            1259    16650    LeagueMemberEntity    TABLE     �   CREATE TABLE "LeagueMemberEntity" (
    "LeagueId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Access" smallint NOT NULL,
    "JoinDate" timestamp without time zone DEFAULT now() NOT NULL
);
 *   DROP TABLE "League"."LeagueMemberEntity";
       League         postgres    false    12            �            1259    49808    MatchEntity    TABLE     ]  CREATE TABLE "MatchEntity" (
    "MatchId" uuid NOT NULL,
    "NodeId" uuid NOT NULL,
    "GameModeTypeId" smallint NOT NULL,
    "GameMapTypeId" smallint NOT NULL,
    "Created" timestamp without time zone NOT NULL,
    "Started" timestamp without time zone NOT NULL,
    "Finished" timestamp without time zone NOT NULL,
    "WinnerTeamId" uuid
);
 $   DROP TABLE "Matches"."MatchEntity";
       Matches         postgres    false    17            �            1259    49819    MatchMember    TABLE     T  CREATE TABLE "MatchMember" (
    "MemberId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "TeamId" uuid NOT NULL,
    "Level" smallint DEFAULT 1 NOT NULL,
    "MatchId" uuid NOT NULL,
    "Award_Money" smallint DEFAULT 0 NOT NULL,
    "Award_Experience" smallint DEFAULT 0 NOT NULL,
    "Award_Reputation" smallint DEFAULT 0 NOT NULL
);
 $   DROP TABLE "Matches"."MatchMember";
       Matches         postgres    false    17            �            1259    49813 	   MatchTeam    TABLE     V   CREATE TABLE "MatchTeam" (
    "TeamId" uuid NOT NULL,
    "MatchId" uuid NOT NULL
);
 "   DROP TABLE "Matches"."MatchTeam";
       Matches         postgres    false    17            �            1259    49825    MemberAchievements    TABLE     �   CREATE TABLE "MemberAchievements" (
    "AchievementId" uuid NOT NULL,
    "AchievementTypeId" smallint NOT NULL,
    "AchievementCount" smallint NOT NULL,
    "MemberId" uuid NOT NULL
);
 +   DROP TABLE "Matches"."MemberAchievements";
       Matches         postgres    false    17            �            1259    16658    FriendEntity    TABLE     �   CREATE TABLE "FriendEntity" (
    "MemberId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FriendId" uuid NOT NULL,
    "IsLocked" boolean DEFAULT false NOT NULL
);
 %   DROP TABLE "Players"."FriendEntity";
       Players         postgres    false    16    16    13            �            1259    16624    PlayerArmourEntity    TABLE     �  CREATE TABLE "PlayerArmourEntity" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Skin_MaterialId" smallint DEFAULT 0 NOT NULL,
    "Skin_TextureId" smallint DEFAULT 0 NOT NULL,
    "PrimaryAmmoAmount" smallint DEFAULT 0 NOT NULL,
    "SecondaryAmmoAmount" smallint DEFAULT 0 NOT NULL,
    "ItemModelId" smallint NOT NULL,
    modification_1_type smallint DEFAULT 0 NOT NULL,
    modification_2_type smallint DEFAULT 0 NOT NULL,
    modification_3_type smallint DEFAULT 0 NOT NULL,
    modification_4_type smallint DEFAULT 0 NOT NULL,
    modification_5_type smallint DEFAULT 0 NOT NULL,
    modification_1_value smallint DEFAULT 0 NOT NULL,
    modification_2_value smallint DEFAULT 0 NOT NULL,
    modification_3_value smallint DEFAULT 0 NOT NULL,
    modification_4_value smallint DEFAULT 0 NOT NULL,
    modification_5_value smallint DEFAULT 0 NOT NULL
);
 +   DROP TABLE "Players"."PlayerArmourEntity";
       Players         postgres    false    16    16    13            �            1259    16654    PlayerCharacterEntity    TABLE     �   CREATE TABLE "PlayerCharacterEntity" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "ItemModelId" smallint NOT NULL
);
 .   DROP TABLE "Players"."PlayerCharacterEntity";
       Players         postgres    false    16    16    13            �            1259    16662    PlayerEntity    TABLE     �  CREATE TABLE "PlayerEntity" (
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerName" character varying(24) NOT NULL,
    "Experience_Level" smallint DEFAULT 0 NOT NULL,
    "Experience_Experience" bigint DEFAULT 0 NOT NULL,
    "Experience_WeekExperience" bigint DEFAULT 0 NOT NULL,
    "Cash_Money" bigint DEFAULT 0 NOT NULL,
    "Cash_Donate" bigint DEFAULT 0 NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL,
    "PremiumEndDate" timestamp without time zone,
    "LastActivitiDate" timestamp without time zone DEFAULT now() NOT NULL,
    "LastActivityDate" timestamp without time zone DEFAULT now() NOT NULL,
    "CurrentFractionTypeId" smallint DEFAULT 1 NOT NULL,
    "Reputation_Keepers" bigint DEFAULT 0 NOT NULL,
    "Reputation_Keepers_Friend" bigint DEFAULT 0 NOT NULL,
    "Reputation_RIFT" bigint DEFAULT 0 NOT NULL,
    "Reputation_RIFT_Friend" bigint DEFAULT 0 NOT NULL
);
 %   DROP TABLE "Players"."PlayerEntity";
       Players         postgres    false    16    16    13            �            1259    16632    PlayerProfileEntity    TABLE     ]  CREATE TABLE "PlayerProfileEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "CharacterId" uuid NOT NULL,
    "HelmetId" uuid,
    "MaskId" uuid,
    "ArmourId" uuid,
    "BackpackId" uuid,
    "GlovesId" uuid,
    "PantsId" uuid,
    "BootsId" uuid,
    "PrimaryWeaponId" uuid,
    "SecondaryWeaponId" uuid,
    "PrimaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "PrimaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Primary" smallint DEFAULT 0 NOT NULL,
    "SecondaryWeaponAmmo_Secondary" smallint DEFAULT 0 NOT NULL,
    "PlayerId" uuid NOT NULL,
    "IsEnabled" boolean DEFAULT true NOT NULL,
    "Name" character varying(24),
    "IsUnlocked" boolean DEFAULT false NOT NULL,
    CONSTRAINT "PrimaryWeaponAmmo" CHECK ((("PrimaryWeaponAmmo_Primary" + "PrimaryWeaponAmmo_Secondary") > 0))
);
 ,   DROP TABLE "Players"."PlayerProfileEntity";
       Players         postgres    false    16    16    13            �            1259    16641    PlayerWeaponEntity    TABLE     "  CREATE TABLE "PlayerWeaponEntity" (
    "ItemId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "Skin_MaterialId" smallint DEFAULT 0 NOT NULL,
    "Skin_TextureId" smallint DEFAULT 0 NOT NULL,
    "ItemModelId" smallint NOT NULL,
    modification_1_type smallint DEFAULT 0 NOT NULL,
    modification_2_type smallint DEFAULT 0 NOT NULL,
    modification_3_type smallint DEFAULT 0 NOT NULL,
    modification_4_type smallint DEFAULT 0 NOT NULL,
    modification_5_type smallint DEFAULT 0 NOT NULL,
    modification_1_value smallint DEFAULT 0 NOT NULL,
    modification_2_value smallint DEFAULT 0 NOT NULL,
    modification_3_value smallint DEFAULT 0 NOT NULL,
    modification_4_value smallint DEFAULT 0 NOT NULL,
    modification_5_value smallint DEFAULT 0 NOT NULL
);
 +   DROP TABLE "Players"."PlayerWeaponEntity";
       Players         postgres    false    16    16    13            �            1259    16674    CharacterArmourAssetEntity    TABLE     �   CREATE TABLE "CharacterArmourAssetEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ItemId" smallint NOT NULL,
    "CharacterId" smallint NOT NULL
);
 1   DROP TABLE "Store"."CharacterArmourAssetEntity";
       Store         postgres    false    16    16    14            �            1259    16678    CharacterWeaponAssetEntity    TABLE     �   CREATE TABLE "CharacterWeaponAssetEntity" (
    "AssetId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "ItemId" smallint NOT NULL,
    "CharacterId" smallint NOT NULL
);
 1   DROP TABLE "Store"."CharacterWeaponAssetEntity";
       Store         postgres    false    16    16    14            �            1259    16682    FractionEntity    TABLE     @  CREATE TABLE "FractionEntity" (
    "FractionId" smallint NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Discount_Armour" smallint DEFAULT 0 NOT NULL,
    "Discount_Ammo" smallint DEFAULT 0 NOT NULL,
    "Discount_Weapon" smallint DEFAULT 0 NOT NULL
);
 %   DROP TABLE "Store"."FractionEntity";
       Store         postgres    false    14            �            1259    16690    ItemAmmoEntity    TABLE     �  CREATE TABLE "ItemAmmoEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" smallint NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);
 %   DROP TABLE "Store"."ItemAmmoEntity";
       Store         postgres    false    14            �            1259    16699    ItemArmourEntity    TABLE     �  CREATE TABLE "ItemArmourEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);
 '   DROP TABLE "Store"."ItemArmourEntity";
       Store         postgres    false    14            �            1259    74521    ItemArmourModification    TABLE     �   CREATE TABLE "ItemArmourModification" (
    "ModificationTypeId" smallint NOT NULL,
    "Range_Min" smallint NOT NULL,
    "Range_Max" smallint NOT NULL
);
 -   DROP TABLE "Store"."ItemArmourModification";
       Store         postgres    false    14            �            1259    16708    ItemCharacterEntity    TABLE     �  CREATE TABLE "ItemCharacterEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer DEFAULT 5000 NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);
 *   DROP TABLE "Store"."ItemCharacterEntity";
       Store         postgres    false    14            �            1259    16718    ItemWeaponEntity    TABLE     �  CREATE TABLE "ItemWeaponEntity" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Reputation" smallint DEFAULT 0 NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL
);
 '   DROP TABLE "Store"."ItemWeaponEntity";
       Store         postgres    false    14            �            1259    74526    ItemWeaponModification    TABLE     �   CREATE TABLE "ItemWeaponModification" (
    "ModificationTypeId" smallint NOT NULL,
    "Range_Min" smallint NOT NULL,
    "Range_Max" smallint NOT NULL
);
 -   DROP TABLE "Store"."ItemWeaponModification";
       Store         postgres    false    14            �            1259    16727    ModificationEntity    TABLE       CREATE TABLE "ModificationEntity" (
    "ModificationId" uuid NOT NULL,
    "ModificationTypeId" smallint DEFAULT 0 NOT NULL,
    "Range_Minimum" smallint DEFAULT 2 NOT NULL,
    "Range_Maximum" smallint DEFAULT 10 NOT NULL,
    "Range_Chance" smallint DEFAULT 50 NOT NULL
);
 )   DROP TABLE "Store"."ModificationEntity";
       Store         postgres    false    14            �            1259    66321    GlobalChatMessage    TABLE       CREATE TABLE "GlobalChatMessage" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "EditorId" uuid,
    "LanguageId" smallint NOT NULL
);
 '   DROP TABLE public."GlobalChatMessage";
       public         postgres    false    16            �            1259    66326    PrivateChatMessage    TABLE     �   CREATE TABLE "PrivateChatMessage" (
    "MessageId" uuid NOT NULL,
    "MessageDataId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL
);
 (   DROP TABLE public."PrivateChatMessage";
       public         postgres    false    16            �            1259    66331    PrivateChatMessageData    TABLE       CREATE TABLE "PrivateChatMessageData" (
    "MessageId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "ReceiverId" uuid NOT NULL,
    "Message" character varying(256) NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "IsReaded" boolean NOT NULL
);
 ,   DROP TABLE public."PrivateChatMessageData";
       public         postgres    false    16            �            1259    16734    Deposits    TABLE       CREATE TABLE "Deposits" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserId" uuid NOT NULL,
    "Date" timestamp without time zone NOT NULL,
    "Status" smallint NOT NULL,
    "Value" bigint DEFAULT 0 NOT NULL,
    "Currency" smallint DEFAULT 0 NOT NULL
);
    DROP TABLE vrs."Deposits";
       vrs         postgres    false    16    16    15            �            1259    16740    Roles    TABLE     ]   CREATE TABLE "Roles" (
    "Id" uuid NOT NULL,
    "Name" character varying(256) NOT NULL
);
    DROP TABLE vrs."Roles";
       vrs         postgres    false    15            �            1259    16743 
   UserClaims    TABLE     �   CREATE TABLE "UserClaims" (
    "Id" integer NOT NULL,
    "ClaimType" character varying(256),
    "ClaimValue" character varying(256),
    "UserId" uuid NOT NULL
);
    DROP TABLE vrs."UserClaims";
       vrs         postgres    false    15            �            1259    16749    UserClaims_Id_seq    SEQUENCE     u   CREATE SEQUENCE "UserClaims_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE vrs."UserClaims_Id_seq";
       vrs       postgres    false    15    225            
           0    0    UserClaims_Id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE "UserClaims_Id_seq" OWNED BY "UserClaims"."Id";
            vrs       postgres    false    226            �            1259    16751 
   UserLogins    TABLE     �   CREATE TABLE "UserLogins" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "LoginProvider" character varying(128) NOT NULL,
    "ProviderKey" character varying(128) NOT NULL
);
    DROP TABLE vrs."UserLogins";
       vrs         postgres    false    16    16    15            �            1259    16755 	   UserRoles    TABLE     v   CREATE TABLE "UserRoles" (
    "UserId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "RoleId" uuid NOT NULL
);
    DROP TABLE vrs."UserRoles";
       vrs         postgres    false    16    16    15            �            1259    16759    Users    TABLE     �  CREATE TABLE "Users" (
    "Id" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "UserName" character varying(256) NOT NULL,
    "PasswordHash" character varying(256),
    "SecurityStamp" uuid,
    "Email" character varying(256) DEFAULT NULL::character varying,
    "EmailConfirmed" boolean DEFAULT false NOT NULL,
    "PhoneNumber" character varying(32),
    "PhoneNumberConfirmed" boolean DEFAULT false NOT NULL,
    "TwoFactorEnabled" boolean DEFAULT false NOT NULL,
    "LockoutEndDateUtc" timestamp without time zone,
    "LockoutEnabled" boolean DEFAULT false NOT NULL,
    "AccessFailedCount" smallint DEFAULT 0 NOT NULL,
    "PlayerEntityId" bigint,
    "Balance" bigint DEFAULT 0 NOT NULL
);
    DROP TABLE vrs."Users";
       vrs         postgres    false    16    16    15            �           2604    16773    Id    DEFAULT     f   ALTER TABLE ONLY "UserClaims" ALTER COLUMN "Id" SET DEFAULT nextval('"UserClaims_Id_seq"'::regclass);
 =   ALTER TABLE vrs."UserClaims" ALTER COLUMN "Id" DROP DEFAULT;
       vrs       postgres    false    226    225            �	          0    16647    LeagueEntity 
   TABLE DATA                     League       postgres    false    210   O�       �	          0    16650    LeagueMemberEntity 
   TABLE DATA                     League       postgres    false    211   %�       

          0    49808    MatchEntity 
   TABLE DATA                     Matches       postgres    false    230   ?�       
          0    49819    MatchMember 
   TABLE DATA                     Matches       postgres    false    232   Y�       
          0    49813 	   MatchTeam 
   TABLE DATA                     Matches       postgres    false    231   s�       
          0    49825    MemberAchievements 
   TABLE DATA                     Matches       postgres    false    233   ��       �	          0    16658    FriendEntity 
   TABLE DATA                     Players       postgres    false    213   ��       �	          0    16624    PlayerArmourEntity 
   TABLE DATA                     Players       postgres    false    207   ��       �	          0    16654    PlayerCharacterEntity 
   TABLE DATA                     Players       postgres    false    212   %�       �	          0    16662    PlayerEntity 
   TABLE DATA                     Players       postgres    false    214   ��       �	          0    16632    PlayerProfileEntity 
   TABLE DATA                     Players       postgres    false    208   ��       �	          0    16641    PlayerWeaponEntity 
   TABLE DATA                     Players       postgres    false    209   U�       �	          0    16674    CharacterArmourAssetEntity 
   TABLE DATA                     Store       postgres    false    215   ��       �	          0    16678    CharacterWeaponAssetEntity 
   TABLE DATA                     Store       postgres    false    216   �       �	          0    16682    FractionEntity 
   TABLE DATA                     Store       postgres    false    217    �       �	          0    16690    ItemAmmoEntity 
   TABLE DATA                     Store       postgres    false    218   ��       �	          0    16699    ItemArmourEntity 
   TABLE DATA                     Store       postgres    false    219   q�       
          0    74521    ItemArmourModification 
   TABLE DATA                     Store       postgres    false    237   8�        
          0    16708    ItemCharacterEntity 
   TABLE DATA                     Store       postgres    false    220   R�       
          0    16718    ItemWeaponEntity 
   TABLE DATA                     Store       postgres    false    221   �       
          0    74526    ItemWeaponModification 
   TABLE DATA                     Store       postgres    false    238   �       
          0    16727    ModificationEntity 
   TABLE DATA                     Store       postgres    false    222   %�       
          0    66321    GlobalChatMessage 
   TABLE DATA                     public       postgres    false    234   ?�       
          0    66326    PrivateChatMessage 
   TABLE DATA                     public       postgres    false    235   �       
          0    66331    PrivateChatMessageData 
   TABLE DATA                     public       postgres    false    236   /�       
          0    16734    Deposits 
   TABLE DATA                     vrs       postgres    false    223   I�       
          0    16740    Roles 
   TABLE DATA                     vrs       postgres    false    224   c�       
          0    16743 
   UserClaims 
   TABLE DATA                     vrs       postgres    false    225   }�       
           0    0    UserClaims_Id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('"UserClaims_Id_seq"', 2, true);
            vrs       postgres    false    226            
          0    16751 
   UserLogins 
   TABLE DATA                     vrs       postgres    false    227   ��       
          0    16755 	   UserRoles 
   TABLE DATA                     vrs       postgres    false    228   I�       	
          0    16759    Users 
   TABLE DATA                     vrs       postgres    false    229   c�       	           2606    16785    LeagueEntity_Info_Abbr_key 
   CONSTRAINT     f   ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Abbr_key" UNIQUE ("Info_Abbr");
 W   ALTER TABLE ONLY "League"."LeagueEntity" DROP CONSTRAINT "LeagueEntity_Info_Abbr_key";
       League         postgres    false    210    210            	           2606    16787    LeagueEntity_Info_Name_key 
   CONSTRAINT     f   ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_Info_Name_key" UNIQUE ("Info_Name");
 W   ALTER TABLE ONLY "League"."LeagueEntity" DROP CONSTRAINT "LeagueEntity_Info_Name_key";
       League         postgres    false    210    210            	           2606    16789    LeagueEntity_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY "LeagueEntity"
    ADD CONSTRAINT "LeagueEntity_pkey" PRIMARY KEY ("LeagueId");
 N   ALTER TABLE ONLY "League"."LeagueEntity" DROP CONSTRAINT "LeagueEntity_pkey";
       League         postgres    false    210    210            	           2606    16791    LeagueMemberEntity_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_pkey" PRIMARY KEY ("PlayerId");
 Z   ALTER TABLE ONLY "League"."LeagueMemberEntity" DROP CONSTRAINT "LeagueMemberEntity_pkey";
       League         postgres    false    211    211            G	           2606    49892    MatchEntity_WinnerTeamId_key 
   CONSTRAINT     j   ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_key" UNIQUE ("WinnerTeamId");
 Y   ALTER TABLE ONLY "Matches"."MatchEntity" DROP CONSTRAINT "MatchEntity_WinnerTeamId_key";
       Matches         postgres    false    230    230            I	           2606    49812    MatchEntity_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_pkey" PRIMARY KEY ("MatchId");
 M   ALTER TABLE ONLY "Matches"."MatchEntity" DROP CONSTRAINT "MatchEntity_pkey";
       Matches         postgres    false    230    230            M	           2606    49846    MatchMember_PlayerId_TeamId_key 
   CONSTRAINT     s   ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_TeamId_key" UNIQUE ("PlayerId", "TeamId");
 \   ALTER TABLE ONLY "Matches"."MatchMember" DROP CONSTRAINT "MatchMember_PlayerId_TeamId_key";
       Matches         postgres    false    232    232    232            O	           2606    49824    MatchMember_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_pkey" PRIMARY KEY ("MemberId");
 M   ALTER TABLE ONLY "Matches"."MatchMember" DROP CONSTRAINT "MatchMember_pkey";
       Matches         postgres    false    232    232            K	           2606    49818    MatchTeam_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_pkey" PRIMARY KEY ("TeamId");
 I   ALTER TABLE ONLY "Matches"."MatchTeam" DROP CONSTRAINT "MatchTeam_pkey";
       Matches         postgres    false    231    231            Q	           2606    49829    MemberAchievements_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_pkey" PRIMARY KEY ("AchievementId");
 [   ALTER TABLE ONLY "Matches"."MemberAchievements" DROP CONSTRAINT "MemberAchievements_pkey";
       Matches         postgres    false    233    233            	           2606    16775 %   ArmourEntity_PlayerId_ItemModelId_key 
   CONSTRAINT     �   ALTER TABLE ONLY "PlayerArmourEntity"
    ADD CONSTRAINT "ArmourEntity_PlayerId_ItemModelId_key" UNIQUE ("PlayerId", "ItemModelId");
 i   ALTER TABLE ONLY "Players"."PlayerArmourEntity" DROP CONSTRAINT "ArmourEntity_PlayerId_ItemModelId_key";
       Players         postgres    false    207    207    207            	           2606    16777    ArmourEntity_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY "PlayerArmourEntity"
    ADD CONSTRAINT "ArmourEntity_pkey" PRIMARY KEY ("ItemId");
 U   ALTER TABLE ONLY "Players"."PlayerArmourEntity" DROP CONSTRAINT "ArmourEntity_pkey";
       Players         postgres    false    207    207            	           2606    16793 $   CharacterEntity_PlayerId_ModelId_key 
   CONSTRAINT     �   ALTER TABLE ONLY "PlayerCharacterEntity"
    ADD CONSTRAINT "CharacterEntity_PlayerId_ModelId_key" UNIQUE ("PlayerId", "ItemModelId");
 k   ALTER TABLE ONLY "Players"."PlayerCharacterEntity" DROP CONSTRAINT "CharacterEntity_PlayerId_ModelId_key";
       Players         postgres    false    212    212    212            	           2606    16795    CharacterItemEntity_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY "PlayerCharacterEntity"
    ADD CONSTRAINT "CharacterItemEntity_pkey" PRIMARY KEY ("ItemId");
 _   ALTER TABLE ONLY "Players"."PlayerCharacterEntity" DROP CONSTRAINT "CharacterItemEntity_pkey";
       Players         postgres    false    212    212            	           2606    16797    FriendEntity_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_pkey" PRIMARY KEY ("MemberId");
 O   ALTER TABLE ONLY "Players"."FriendEntity" DROP CONSTRAINT "FriendEntity_pkey";
       Players         postgres    false    213    213            	           2606    16779 )   ItemWeaponEntity_PlayerId_ItemModelId_key 
   CONSTRAINT     �   ALTER TABLE ONLY "PlayerWeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_PlayerId_ItemModelId_key" UNIQUE ("PlayerId", "ItemModelId");
 m   ALTER TABLE ONLY "Players"."PlayerWeaponEntity" DROP CONSTRAINT "ItemWeaponEntity_PlayerId_ItemModelId_key";
       Players         postgres    false    209    209    209            	           2606    16781    ItemWeaponEntity_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY "PlayerWeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_pkey" PRIMARY KEY ("ItemId");
 Y   ALTER TABLE ONLY "Players"."PlayerWeaponEntity" DROP CONSTRAINT "ItemWeaponEntity_pkey";
       Players         postgres    false    209    209            		           2606    16783    PlayerAssetEnity_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PlayerAssetEnity_pkey" PRIMARY KEY ("AssetId");
 Z   ALTER TABLE ONLY "Players"."PlayerProfileEntity" DROP CONSTRAINT "PlayerAssetEnity_pkey";
       Players         postgres    false    208    208            	           2606    16799    PlayerEntity_PlayerName_key 
   CONSTRAINT     h   ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerName_key" UNIQUE ("PlayerName");
 Y   ALTER TABLE ONLY "Players"."PlayerEntity" DROP CONSTRAINT "PlayerEntity_PlayerName_key";
       Players         postgres    false    214    214            	           2606    16801    PlayerEntity_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_pkey" PRIMARY KEY ("PlayerId");
 O   ALTER TABLE ONLY "Players"."PlayerEntity" DROP CONSTRAINT "PlayerEntity_pkey";
       Players         postgres    false    214    214            !	           2606    16803 1   CharacterArmourAssetEntity_ItemId_CharacterId_key 
   CONSTRAINT     �   ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_ItemId_CharacterId_key" UNIQUE ("ItemId", "CharacterId");
 {   ALTER TABLE ONLY "Store"."CharacterArmourAssetEntity" DROP CONSTRAINT "CharacterArmourAssetEntity_ItemId_CharacterId_key";
       Store         postgres    false    215    215    215            #	           2606    16805    CharacterArmourAssetEntity_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_pkey" PRIMARY KEY ("AssetId");
 i   ALTER TABLE ONLY "Store"."CharacterArmourAssetEntity" DROP CONSTRAINT "CharacterArmourAssetEntity_pkey";
       Store         postgres    false    215    215            /	           2606    16807    CharacterEntity_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY "ItemCharacterEntity"
    ADD CONSTRAINT "CharacterEntity_pkey" PRIMARY KEY ("ModelId");
 W   ALTER TABLE ONLY "Store"."ItemCharacterEntity" DROP CONSTRAINT "CharacterEntity_pkey";
       Store         postgres    false    220    220            %	           2606    16809 1   CharacterWeaponAssetEntity_ItemId_CharacterId_key 
   CONSTRAINT     �   ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_ItemId_CharacterId_key" UNIQUE ("ItemId", "CharacterId");
 {   ALTER TABLE ONLY "Store"."CharacterWeaponAssetEntity" DROP CONSTRAINT "CharacterWeaponAssetEntity_ItemId_CharacterId_key";
       Store         postgres    false    216    216    216            '	           2606    16811    CharacterWeaponAssetEntity_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_pkey" PRIMARY KEY ("AssetId");
 i   ALTER TABLE ONLY "Store"."CharacterWeaponAssetEntity" DROP CONSTRAINT "CharacterWeaponAssetEntity_pkey";
       Store         postgres    false    216    216            )	           2606    16813    FractionEntity_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY "FractionEntity"
    ADD CONSTRAINT "FractionEntity_pkey" PRIMARY KEY ("FractionId");
 Q   ALTER TABLE ONLY "Store"."FractionEntity" DROP CONSTRAINT "FractionEntity_pkey";
       Store         postgres    false    217    217            +	           2606    16815    ItemAmmoEntity_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY "ItemAmmoEntity"
    ADD CONSTRAINT "ItemAmmoEntity_pkey" PRIMARY KEY ("ModelId");
 Q   ALTER TABLE ONLY "Store"."ItemAmmoEntity" DROP CONSTRAINT "ItemAmmoEntity_pkey";
       Store         postgres    false    218    218            -	           2606    16817    ItemArmourBaseEntity_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY "ItemArmourEntity"
    ADD CONSTRAINT "ItemArmourBaseEntity_pkey" PRIMARY KEY ("ModelId");
 Y   ALTER TABLE ONLY "Store"."ItemArmourEntity" DROP CONSTRAINT "ItemArmourBaseEntity_pkey";
       Store         postgres    false    219    219            Y	           2606    74525    ItemArmourModification_pkey 
   CONSTRAINT        ALTER TABLE ONLY "ItemArmourModification"
    ADD CONSTRAINT "ItemArmourModification_pkey" PRIMARY KEY ("ModificationTypeId");
 a   ALTER TABLE ONLY "Store"."ItemArmourModification" DROP CONSTRAINT "ItemArmourModification_pkey";
       Store         postgres    false    237    237            1	           2606    16819    ItemWeaponBaseEntity_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY "ItemWeaponEntity"
    ADD CONSTRAINT "ItemWeaponBaseEntity_pkey" PRIMARY KEY ("ModelId");
 Y   ALTER TABLE ONLY "Store"."ItemWeaponEntity" DROP CONSTRAINT "ItemWeaponBaseEntity_pkey";
       Store         postgres    false    221    221            [	           2606    74530    ItemWeaponModification_pkey 
   CONSTRAINT        ALTER TABLE ONLY "ItemWeaponModification"
    ADD CONSTRAINT "ItemWeaponModification_pkey" PRIMARY KEY ("ModificationTypeId");
 a   ALTER TABLE ONLY "Store"."ItemWeaponModification" DROP CONSTRAINT "ItemWeaponModification_pkey";
       Store         postgres    false    238    238            3	           2606    16821 ,   ModificatioBaseEntity_ModificationTypeId_key 
   CONSTRAINT     �   ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_ModificationTypeId_key" UNIQUE ("ModificationTypeId");
 n   ALTER TABLE ONLY "Store"."ModificationEntity" DROP CONSTRAINT "ModificatioBaseEntity_ModificationTypeId_key";
       Store         postgres    false    222    222            5	           2606    16823    ModificatioBaseEntity_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY "ModificationEntity"
    ADD CONSTRAINT "ModificatioBaseEntity_pkey" PRIMARY KEY ("ModificationId");
 \   ALTER TABLE ONLY "Store"."ModificationEntity" DROP CONSTRAINT "ModificatioBaseEntity_pkey";
       Store         postgres    false    222    222            S	           2606    66325    GlobalChatMessage_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY "GlobalChatMessage"
    ADD CONSTRAINT "GlobalChatMessage_pkey" PRIMARY KEY ("MessageId");
 V   ALTER TABLE ONLY public."GlobalChatMessage" DROP CONSTRAINT "GlobalChatMessage_pkey";
       public         postgres    false    234    234            W	           2606    66335    PrivateChatMessageData_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_pkey" PRIMARY KEY ("MessageId");
 `   ALTER TABLE ONLY public."PrivateChatMessageData" DROP CONSTRAINT "PrivateChatMessageData_pkey";
       public         postgres    false    236    236            U	           2606    66330    PrivateChatMessage_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_pkey" PRIMARY KEY ("MessageId");
 X   ALTER TABLE ONLY public."PrivateChatMessage" DROP CONSTRAINT "PrivateChatMessage_pkey";
       public         postgres    false    235    235            7	           2606    16825    Deposits_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY "Deposits"
    ADD CONSTRAINT "Deposits_pkey" PRIMARY KEY ("Id");
 A   ALTER TABLE ONLY vrs."Deposits" DROP CONSTRAINT "Deposits_pkey";
       vrs         postgres    false    223    223            9	           2606    16827 
   Roles_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "Roles"
    ADD CONSTRAINT "Roles_pkey" PRIMARY KEY ("Id");
 ;   ALTER TABLE ONLY vrs."Roles" DROP CONSTRAINT "Roles_pkey";
       vrs         postgres    false    224    224            <	           2606    16829    UserClaims_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "UserClaims_pkey" PRIMARY KEY ("Id");
 E   ALTER TABLE ONLY vrs."UserClaims" DROP CONSTRAINT "UserClaims_pkey";
       vrs         postgres    false    225    225            ?	           2606    16831    UserLogins_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "UserLogins_pkey" PRIMARY KEY ("UserId", "LoginProvider", "ProviderKey");
 E   ALTER TABLE ONLY vrs."UserLogins" DROP CONSTRAINT "UserLogins_pkey";
       vrs         postgres    false    227    227    227    227            C	           2606    16833    UserRoles_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "UserRoles_pkey" PRIMARY KEY ("UserId", "RoleId");
 C   ALTER TABLE ONLY vrs."UserRoles" DROP CONSTRAINT "UserRoles_pkey";
       vrs         postgres    false    228    228    228            E	           2606    16835 
   Users_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY ("Id");
 ;   ALTER TABLE ONLY vrs."Users" DROP CONSTRAINT "Users_pkey";
       vrs         postgres    false    229    229            :	           1259    16836    IX_UserClaims_UserId    INDEX     L   CREATE INDEX "IX_UserClaims_UserId" ON "UserClaims" USING btree ("UserId");
 '   DROP INDEX vrs."IX_UserClaims_UserId";
       vrs         postgres    false    225            =	           1259    16837    IX_UserLogins_UserId    INDEX     L   CREATE INDEX "IX_UserLogins_UserId" ON "UserLogins" USING btree ("UserId");
 '   DROP INDEX vrs."IX_UserLogins_UserId";
       vrs         postgres    false    227            @	           1259    16838    IX_UserRoles_RoleId    INDEX     J   CREATE INDEX "IX_UserRoles_RoleId" ON "UserRoles" USING btree ("RoleId");
 &   DROP INDEX vrs."IX_UserRoles_RoleId";
       vrs         postgres    false    228            A	           1259    16839    IX_UserRoles_UserId    INDEX     J   CREATE INDEX "IX_UserRoles_UserId" ON "UserRoles" USING btree ("UserId");
 &   DROP INDEX vrs."IX_UserRoles_UserId";
       vrs         postgres    false    228            c	           2606    16875     LeagueMemberEntity_LeagueId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_LeagueId_fkey" FOREIGN KEY ("LeagueId") REFERENCES "LeagueEntity"("LeagueId") ON DELETE CASCADE;
 c   ALTER TABLE ONLY "League"."LeagueMemberEntity" DROP CONSTRAINT "LeagueMemberEntity_LeagueId_fkey";
       League       postgres    false    210    2323    211            d	           2606    16880     LeagueMemberEntity_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "LeagueMemberEntity"
    ADD CONSTRAINT "LeagueMemberEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;
 c   ALTER TABLE ONLY "League"."LeagueMemberEntity" DROP CONSTRAINT "LeagueMemberEntity_PlayerId_fkey";
       League       postgres    false    214    2335    211            w	           2606    49886    MatchEntity_WinnerTeamId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MatchEntity"
    ADD CONSTRAINT "MatchEntity_WinnerTeamId_fkey" FOREIGN KEY ("WinnerTeamId") REFERENCES "MatchTeam"("TeamId");
 Z   ALTER TABLE ONLY "Matches"."MatchEntity" DROP CONSTRAINT "MatchEntity_WinnerTeamId_fkey";
       Matches       postgres    false    2379    230    231            {	           2606    49870    MatchMember_MatchId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;
 U   ALTER TABLE ONLY "Matches"."MatchMember" DROP CONSTRAINT "MatchMember_MatchId_fkey";
       Matches       postgres    false    2377    232    230            y	           2606    49835    MatchMember_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;
 V   ALTER TABLE ONLY "Matches"."MatchMember" DROP CONSTRAINT "MatchMember_PlayerId_fkey";
       Matches       postgres    false    232    214    2335            z	           2606    49840    MatchMember_TeamId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MatchMember"
    ADD CONSTRAINT "MatchMember_TeamId_fkey" FOREIGN KEY ("TeamId") REFERENCES "MatchTeam"("TeamId") ON DELETE CASCADE;
 T   ALTER TABLE ONLY "Matches"."MatchMember" DROP CONSTRAINT "MatchMember_TeamId_fkey";
       Matches       postgres    false    232    2379    231            x	           2606    49830    MatchTeam_MatchId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MatchTeam"
    ADD CONSTRAINT "MatchTeam_MatchId_fkey" FOREIGN KEY ("MatchId") REFERENCES "MatchEntity"("MatchId") ON DELETE CASCADE;
 Q   ALTER TABLE ONLY "Matches"."MatchTeam" DROP CONSTRAINT "MatchTeam_MatchId_fkey";
       Matches       postgres    false    231    230    2377            |	           2606    49847     MemberAchievements_MemberId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "MemberAchievements"
    ADD CONSTRAINT "MemberAchievements_MemberId_fkey" FOREIGN KEY ("MemberId") REFERENCES "MatchMember"("MemberId") ON DELETE CASCADE;
 d   ALTER TABLE ONLY "Matches"."MemberAchievements" DROP CONSTRAINT "MemberAchievements_MemberId_fkey";
       Matches       postgres    false    233    232    2383            ]	           2606    16840    ArmourEntity_ItemModelId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerArmourEntity"
    ADD CONSTRAINT "ArmourEntity_ItemModelId_fkey" FOREIGN KEY ("ItemModelId") REFERENCES "Store"."ItemArmourEntity"("ModelId") ON DELETE CASCADE;
 a   ALTER TABLE ONLY "Players"."PlayerArmourEntity" DROP CONSTRAINT "ArmourEntity_ItemModelId_fkey";
       Players       postgres    false    207    219    2349            \	           2606    16845    ArmourEntity_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerArmourEntity"
    ADD CONSTRAINT "ArmourEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;
 ^   ALTER TABLE ONLY "Players"."PlayerArmourEntity" DROP CONSTRAINT "ArmourEntity_PlayerId_fkey";
       Players       postgres    false    207    2335    214            e	           2606    16885    CharacterEntity_ModelId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerCharacterEntity"
    ADD CONSTRAINT "CharacterEntity_ModelId_fkey" FOREIGN KEY ("ItemModelId") REFERENCES "Store"."ItemCharacterEntity"("ModelId") ON DELETE CASCADE;
 c   ALTER TABLE ONLY "Players"."PlayerCharacterEntity" DROP CONSTRAINT "CharacterEntity_ModelId_fkey";
       Players       postgres    false    212    220    2351            f	           2606    16890    CharacterEntity_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerCharacterEntity"
    ADD CONSTRAINT "CharacterEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;
 d   ALTER TABLE ONLY "Players"."PlayerCharacterEntity" DROP CONSTRAINT "CharacterEntity_PlayerId_fkey";
       Players       postgres    false    212    214    2335            g	           2606    16895    FriendEntity_FriendId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_FriendId_fkey" FOREIGN KEY ("FriendId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;
 X   ALTER TABLE ONLY "Players"."FriendEntity" DROP CONSTRAINT "FriendEntity_FriendId_fkey";
       Players       postgres    false    213    214    2335            h	           2606    16900    FriendEntity_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;
 X   ALTER TABLE ONLY "Players"."FriendEntity" DROP CONSTRAINT "FriendEntity_PlayerId_fkey";
       Players       postgres    false    213    214    2335            b	           2606    16850 %   InvertoryItemBaseEntity_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerWeaponEntity"
    ADD CONSTRAINT "InvertoryItemBaseEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;
 i   ALTER TABLE ONLY "Players"."PlayerWeaponEntity" DROP CONSTRAINT "InvertoryItemBaseEntity_PlayerId_fkey";
       Players       postgres    false    209    2335    214            a	           2606    16855 !   ItemWeaponEntity_ItemModelId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerWeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_ItemModelId_fkey" FOREIGN KEY ("ItemModelId") REFERENCES "Store"."ItemWeaponEntity"("ModelId") ON DELETE CASCADE;
 e   ALTER TABLE ONLY "Players"."PlayerWeaponEntity" DROP CONSTRAINT "ItemWeaponEntity_ItemModelId_fkey";
       Players       postgres    false    209    2353    221            `	           2606    16860 !   PlayerAssetEnity_CharacterId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PlayerAssetEnity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "PlayerCharacterEntity"("ItemId") ON DELETE CASCADE;
 f   ALTER TABLE ONLY "Players"."PlayerProfileEntity" DROP CONSTRAINT "PlayerAssetEnity_CharacterId_fkey";
       Players       postgres    false    208    2329    212            j	           2606    66289     PlayerEntity_FractionTypeId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_FractionTypeId_fkey" FOREIGN KEY ("CurrentFractionTypeId") REFERENCES "Store"."FractionEntity"("FractionId");
 ^   ALTER TABLE ONLY "Players"."PlayerEntity" DROP CONSTRAINT "PlayerEntity_FractionTypeId_fkey";
       Players       postgres    false    217    214    2345            i	           2606    16905    PlayerEntity_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES vrs."Users"("Id") ON DELETE CASCADE;
 X   ALTER TABLE ONLY "Players"."PlayerEntity" DROP CONSTRAINT "PlayerEntity_PlayerId_fkey";
       Players       postgres    false    214    229    2373            _	           2606    16865    PreSetEntity_HelmetId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PreSetEntity_HelmetId_fkey" FOREIGN KEY ("HelmetId") REFERENCES "PlayerArmourEntity"("ItemId") ON DELETE CASCADE;
 _   ALTER TABLE ONLY "Players"."PlayerProfileEntity" DROP CONSTRAINT "PreSetEntity_HelmetId_fkey";
       Players       postgres    false    208    207    2311            ^	           2606    16870    PreSetEntity_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PlayerProfileEntity"
    ADD CONSTRAINT "PreSetEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;
 _   ALTER TABLE ONLY "Players"."PlayerProfileEntity" DROP CONSTRAINT "PreSetEntity_PlayerId_fkey";
       Players       postgres    false    208    214    2335            k	           2606    16910 +   CharacterArmourAssetEntity_CharacterId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "ItemCharacterEntity"("ModelId") ON DELETE CASCADE;
 u   ALTER TABLE ONLY "Store"."CharacterArmourAssetEntity" DROP CONSTRAINT "CharacterArmourAssetEntity_CharacterId_fkey";
       Store       postgres    false    215    220    2351            l	           2606    16915 &   CharacterArmourAssetEntity_ItemId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "CharacterArmourAssetEntity"
    ADD CONSTRAINT "CharacterArmourAssetEntity_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "ItemArmourEntity"("ModelId") ON DELETE CASCADE;
 p   ALTER TABLE ONLY "Store"."CharacterArmourAssetEntity" DROP CONSTRAINT "CharacterArmourAssetEntity_ItemId_fkey";
       Store       postgres    false    215    2349    219            m	           2606    16920 +   CharacterWeaponAssetEntity_CharacterId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_CharacterId_fkey" FOREIGN KEY ("CharacterId") REFERENCES "ItemCharacterEntity"("ModelId") ON DELETE CASCADE;
 u   ALTER TABLE ONLY "Store"."CharacterWeaponAssetEntity" DROP CONSTRAINT "CharacterWeaponAssetEntity_CharacterId_fkey";
       Store       postgres    false    216    220    2351            n	           2606    16925 &   CharacterWeaponAssetEntity_ItemId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "CharacterWeaponAssetEntity"
    ADD CONSTRAINT "CharacterWeaponAssetEntity_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "ItemWeaponEntity"("ModelId") ON DELETE CASCADE;
 p   ALTER TABLE ONLY "Store"."CharacterWeaponAssetEntity" DROP CONSTRAINT "CharacterWeaponAssetEntity_ItemId_fkey";
       Store       postgres    false    216    221    2353            o	           2606    16930    ItemAmmoEntity_FractionId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "ItemAmmoEntity"
    ADD CONSTRAINT "ItemAmmoEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;
 \   ALTER TABLE ONLY "Store"."ItemAmmoEntity" DROP CONSTRAINT "ItemAmmoEntity_FractionId_fkey";
       Store       postgres    false    217    218    2345            p	           2606    16935     ItemArmourEntity_FractionId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "ItemArmourEntity"
    ADD CONSTRAINT "ItemArmourEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;
 `   ALTER TABLE ONLY "Store"."ItemArmourEntity" DROP CONSTRAINT "ItemArmourEntity_FractionId_fkey";
       Store       postgres    false    219    217    2345            q	           2606    16940 #   ItemCharacterEntity_FractionId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "ItemCharacterEntity"
    ADD CONSTRAINT "ItemCharacterEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;
 f   ALTER TABLE ONLY "Store"."ItemCharacterEntity" DROP CONSTRAINT "ItemCharacterEntity_FractionId_fkey";
       Store       postgres    false    220    217    2345            r	           2606    16945     ItemWeaponEntity_FractionId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "ItemWeaponEntity"
    ADD CONSTRAINT "ItemWeaponEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "FractionEntity"("FractionId") ON DELETE CASCADE;
 `   ALTER TABLE ONLY "Store"."ItemWeaponEntity" DROP CONSTRAINT "ItemWeaponEntity_FractionId_fkey";
       Store       postgres    false    2345    221    217            �	           2606    66356 &   PrivateChatMessageData_ReceiverId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_ReceiverId_fkey" FOREIGN KEY ("ReceiverId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;
 k   ALTER TABLE ONLY public."PrivateChatMessageData" DROP CONSTRAINT "PrivateChatMessageData_ReceiverId_fkey";
       public       postgres    false    214    236    2335            	           2606    66351 $   PrivateChatMessageData_SenderId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PrivateChatMessageData"
    ADD CONSTRAINT "PrivateChatMessageData_SenderId_fkey" FOREIGN KEY ("SenderId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;
 i   ALTER TABLE ONLY public."PrivateChatMessageData" DROP CONSTRAINT "PrivateChatMessageData_SenderId_fkey";
       public       postgres    false    2335    214    236            ~	           2606    66346 %   PrivateChatMessage_MessageDataId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_MessageDataId_fkey" FOREIGN KEY ("MessageDataId") REFERENCES "PrivateChatMessageData"("MessageId") ON DELETE CASCADE;
 f   ALTER TABLE ONLY public."PrivateChatMessage" DROP CONSTRAINT "PrivateChatMessage_MessageDataId_fkey";
       public       postgres    false    236    2391    235            }	           2606    66336     PrivateChatMessage_PlayerId_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "PrivateChatMessage"
    ADD CONSTRAINT "PrivateChatMessage_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "Players"."PlayerEntity"("PlayerId") ON DELETE CASCADE;
 a   ALTER TABLE ONLY public."PrivateChatMessage" DROP CONSTRAINT "PrivateChatMessage_PlayerId_fkey";
       public       postgres    false    235    214    2335            s	           2606    16950    FK_UserClaims_Users_User_Id    FK CONSTRAINT     �   ALTER TABLE ONLY "UserClaims"
    ADD CONSTRAINT "FK_UserClaims_Users_User_Id" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;
 Q   ALTER TABLE ONLY vrs."UserClaims" DROP CONSTRAINT "FK_UserClaims_Users_User_Id";
       vrs       postgres    false    225    2373    229            t	           2606    16955    FK_UserLogins_Users_UserId    FK CONSTRAINT     �   ALTER TABLE ONLY "UserLogins"
    ADD CONSTRAINT "FK_UserLogins_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;
 P   ALTER TABLE ONLY vrs."UserLogins" DROP CONSTRAINT "FK_UserLogins_Users_UserId";
       vrs       postgres    false    227    2373    229            u	           2606    16960    FK_UserRoles_Roles_RoleId    FK CONSTRAINT     �   ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Roles_RoleId" FOREIGN KEY ("RoleId") REFERENCES "Roles"("Id") ON DELETE CASCADE;
 N   ALTER TABLE ONLY vrs."UserRoles" DROP CONSTRAINT "FK_UserRoles_Roles_RoleId";
       vrs       postgres    false    224    2361    228            v	           2606    16965    FK_UserRoles_Users_UserId    FK CONSTRAINT     �   ALTER TABLE ONLY "UserRoles"
    ADD CONSTRAINT "FK_UserRoles_Users_UserId" FOREIGN KEY ("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE;
 N   ALTER TABLE ONLY vrs."UserRoles" DROP CONSTRAINT "FK_UserRoles_Users_UserId";
       vrs       postgres    false    229    2373    228            �	   �   x�M˹�@����l�&����(&Ehk�T
w�`��+j�b�o2�I^B��[@k�Nw���jZ��S���>7֨���5�QS��1c�b�o_,��H%�߱�����;�lev�J(ԇC��'�\��c`6	"H��D�^�(
�; J�z6���˷�{�Gh���)ϟ��������+D�3�q9�I�      �	   
   x���          

   
   x���          
   
   x���          
   
   x���          
   
   x���          �	   
   x���          �	   T  x����k�0��E�����a'
Ӎ�v��$�2ێR���W�c6�̃B����.W�u�.�w��~���̚��5�-��0�6��N�����sQm܆�����o��	����f?+�z�=Z�ǂ�+��,j����/��q[��Fl��K�e_ľH}Q�E�y���������z�:�I�4�}����H�4>��<�@�9،��
�i�ȏ��(�)�`�����1d��+H0u�⿏�� ���:'�\K@�t�`I:`��bM�Y<]\�/�Zi4�&
�I��\2��B�8����;L�|Ė�)�Λ<���X���3����;���C      �	   �   x�5��
�0F�>E��z!5�?�I$C@���~��(T���ooE��w�c�^_f����y�Ň����~�G\8˸��i/�ɏ���v~^g�n��U�,Ke��)�����jA��@+�i'gӂ���c%�C
'�W�&!`Ě�F�X��ȷI�| np-      �	     x�}�]o�0����7h"��Lwe2��vI*X3��T3����9C����<OO{�W^�`-H{��*Oj��6�\� m�j7��:�{�J�L0��7�7>�į�r����u+��uӭ��,C�E��b+�W�TX�}�ɴ&3^�q��Ah����M�J�Ծ���¨���o!����H��xܮl����q$��Շ��}<{�V�c<��ڱ!1mHm�1t�a`�ـ��2ʙ�#F�gi��E��:0�1-J���5�i���ƽe��۬��b��n���j�~ �ݥ=      �	   H  x��P�j�0��+�/N �Z�AON1�!uC��ǲ��4Ķ����uHȡI3�����|��mX�o^����Hݪ3ծ��v��aS'�{2�̙����u#}��9�^�ߏ(��}r,����Sm~��
��Ɯ��5�?��5���7�/i�yR�/wo���o����Ţ�?�cC��mkS�:c��r������'��c>�K	T<�R�%�0p�̕�:��Pp�� Jn*�#�J�֗o���ݮ TUy�S:��G�/�1�a��%�s&.�iY(i�K�% �p%/ �0�=�lo�uO�?2{�L&����      �	   �  x���Mk�@�����	d`V;�EO9� h�R�����΂�c����W�j(����^���a�yܬ?m��q��Z~��YN_�_����k��Y6�<7yywy?�o����;9���-m�{�z���}�p�2,x>涴���xة]w~�?b=�X��hƢ�}�����z��h5C���ܿ���T7�Z0����$�#9H�(�X�u\�U+�u4h���@(�ɲsA�b����Q����-���kn�-���9r�)�PC��C����YkE�%���fǫ;��^ЃJE�R��9Y@m��s�<�Qώ�vL�(Z�P��@�`�#d��~$�2�Ɏvv����<A]����s�p�>OZ���hf�k;"�dJQ�Lv�~�0��$���L?W���a�h<      �	   
   x���          �	   
   x���          �	   �   x���v
Q���WPr+JL.���s�+�,�TRЀ�x�(�((9��ǻV�e��%�"�|��R+A\�����Ҽ�xǢ���"T���|���Ă�<%M�0G�P�`Cd�i��9�h4�h<�h2�h��@.. �.�      �	   �   x�͎A�@����eO
� :Ym��j]e�	wG�1���JT�������<�����̫3�@'Z�0��̙�3l��-O���T''Co���F�b�f�u���� c��:)`�HQ��u�Q5./�vMҋ(��lQE��Toa嗂�'�{�f�����'�s�      �	   �   x��Q�
�@���=)x�B
:Ym��j]e��+���iJu鞏w���E	�S`Qz�4�~[ˮ�B��'`�PX���@�c5��T:���o��Q
�q�Rt*���C���D�Ybl:�u)Ũ�Z��x�f��.4ӵ���s����+�/a^kg�O��l�`z�����z�`���xְ�      
   
   x���           
   �   x�ݏO�@��~�eO
쏧Nf,��Z�X�E�������խs�cfx0�xV��$<+�r�.��AT���D�KSUC�k������J�9��(���))�b��ϩ�0}"��04 ����6J��~�6ެy�%GV7��Q�^D��_�6���KV0�-`08�1Rg[      
   �   x�Ւ�
�@��}�0�
=t��'�
.�z,��P�3�ME��i���!	��ϗ�d��ܮ�q��E�d")��-T�Ϙl�,�b�jJGg�Hz���*)��XɦNJ���&������ˆ�J��Y%�m���~4�%�]tD���I5v�#zC��;@�OGl0B�!�A�x?\� tR���o�@�_�:c�&��.uR�2����@��B�u�|��      
   
   x���          
   
   x���          
   �	  x��\Mo]��ϯp��=�"ER�h�6@�M�{}N&�$���%�U�*T�a_??�O�<�P�_�}���^�}���g�����{�����S�a>{��ٯ��ǳO��͟���������?��_���\?��i_�?~����ԟ~������o�{���wO�>o�W-�:��sh�RX�Zr�K�<��|Q��i��Q�B�ރqm6��8x?�O�Ç�O������}��!�@�D�R�ˤ�,��i|����7o^<�w�������K�M8��5�*-��te�&⒏b��P&+r��95�� �gh9ΐ�شn�C�b��J��2{�Tb�C��BY\k:��q<�D�`��UZ���8yh�#tk��b~��Fr�/���&�A*�P��������i8g�{ �L��N�q˃h-��$�s�֪�q�߃A�'FELu� �,x=�&#'���irJ1]T�.��{����5�H��VI�i4y��Z�0��0�Q���`�����q
� ݃Ai�k���DN�a��l�t�ln�``]-�5B]51�Jb�洒Um��8 �ċ$R˃2K��V��,��*�%��1p%���hqH��
1�-�(��e� ���.��1ͅ*�V�9�F��K����h3�}Dr�	�.����yl׈rXk׀���MS��IDQ�"^�kԘKF%�vҠ�l�j�G�5s#0p�rQP3ш4؂��Gpa(��3A4c�v�d��Eܨ ���I
0P8F%�mNH���mTX&ǣ�`�u�e�HA=�	���L�(Kt��@D J����V!�w�5�zh�Š�#D8�q��DwuMY�C�P���>!�iX�H{;�Q@�n�H�$ɊbQ`����V�l��ُ�	�r�w�Dj�/�<�Fr� ����Q�q}�I9^��yu���}�8�����B#��i
���<��ZF90�a����r�XB1m�g�� f��V�$�)hMȅ,ȅ�$�Z�����(��^č���8,xD�D�d�^B�$UzY�ͅ��'�=dIԣ尴�;�E��"a͞'{�g�300U��r�������(��ʨ��`���|><å]�[_�N�0���R�}P[@u��%�R��F`PX��^��VGf�F���X�N����}z��M[��H��Pq�e�Bla�^��u��A�x�N���o#��6/t��FiH�
��:��{mw�q#%KNp�c*�@��N!h�LI��٭w` ��vS.��t�Z&0B�L��-�9c�����M��c�=���F��R)�%"It��q �#�č�2�b�;��F��$��\f�4$���L� ���8H��hA؀o4��āFN�z�Q�xM����6R�dsy
�PɥN�l�-�qv� (D�\6�$�k6i��ڔ��f����[m� �S��
8 N��2��C"��[ku�i��`�͛Ƒ�fs�#DI��`g���9+����B���tcM�5��n�����qO��H��8(��/���J%am�{ӭ^��k�8��LL�h������ѹ���������n�هw=���\čL����+���[�_�HMA�I%��鑒d��ZT���H{��Y$��b)9�v>�/��U��D	삖|PBM�9��S��cP��E[m=Q�Ek��-����qͶ���ʇۉ�"�t�Nt�hq��M¦�T��2����qr�kL��w��@���BW�tg����k�J,|�Nlsu�ӈ��P�*����<d���Pz/�`݉�R�E��B#m��ԡ���rzx�lqc���%,�FM%x��8�����������%K9��{<��*��m@;��~�;�G1��F�RdR�7C.8dql�����1�i{ai��̃Վ8 M�j����F_f�4���E)�d,��E�DC�/Gy�>�&����D�_@�����[�3�}�;�/+�e�~r�v��+0`���%h�E���n�/
�ބ��&gG�6��&�d�&���0�CI����
ʖ�n�t�~��M�y�,S��� k�[5;�`��sJ���#[�|�yk9�����y�6�&��N�[<{�(�&^�h)�����"��8FXN����8;��#_TS��=���/d��g£���p�_��.���f�E���yF`Д!r���L~\'⫶�Q�Fl�,_K}��c�Yk�':��(�t�~ ���~i�FIR�[����N�Gt���S��f����T�D�%1춲�H� uՋ����G+_p$���v=h3RGMX�� )�E�`6Ӻ����`�&65p�T�Ύ�x�r˳i�=̴�#L�
#��'Wi'�3�d�Ľ�{Q.n-z�NHDѱ�BG�ܩ��*Ń���%�ɺ*�Ұ�RXH|�	�ܪ@0[=q�?gz7߾��󿣐�R��9���7�ю��      
   
   x���          
   
   x���          
   
   x���          
   
   x���          
   
   x���          
   �   x�5���0  w���RH�M��tr` 4�e��&��@br�ݥy�<J����j6Sf_�0C��jx p��~;m�m��j��%�����!3�K�(8n��u�5VF�HP��Ơ@�3u��۹�Dȼuc��3tn9�F�h�N���T�P�4b\(�=��B�6+      
   
   x���          	
   �  x��T�N�@}�+���
�x��U!��J�Cӷ�[n�l�4��ڄR�E��ky�9͜9gv��}3�����Z=�M��k�������V�z!Ϸi�{�Ϫxd�&��Q�u�W0���4��le�gib��J��� m�M;��4k' �=6H�2��D_Ba�B��i�R&�;ec�[�&)�İ3e�����	1$����n�A�Վ;�d�"C�(��P
y�g׎&��Z��b����,��U����v�?��Ns��S���Y�=7��@�#?-���W�Yoywwn2q���uF���Uk��t����(5p����`_d`!��i-����%��;�>��s��&��R^R[�͐oI���|L1�_j��_E|�]��M�H�{�.{���z���/'����.�6��o�q�kG.Ľn�.8Μ�i�7�
0��3T���ː��2�zX`�2�7�_��]f0���C�j��p�|#�VX���e�e���M�^L�!�tߋ�p��B�I�6խph,���վv���Jo��A	���%DD�R|_$�ĕ1���>Q�~ {#7Buoo�]7�'�+C&��Ikb�����82��p=�fu��]��Ө���Oǚ��--��YJK*�\&�{H-%�V>�;�Wm���NaZ�     