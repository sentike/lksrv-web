--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-07-08 19:41:06

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE "LokaWebSite";
--
-- TOC entry 2203 (class 1262 OID 148700)
-- Name: LokaWebSite; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "LokaWebSite" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';


ALTER DATABASE "LokaWebSite" OWNER TO postgres;

\connect "LokaWebSite"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 9 (class 2615 OID 148701)
-- Name: Blog; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Blog";


ALTER SCHEMA "Blog" OWNER TO postgres;

--
-- TOC entry 10 (class 2615 OID 148962)
-- Name: Forum; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Forum";


ALTER SCHEMA "Forum" OWNER TO postgres;

--
-- TOC entry 11 (class 2615 OID 148964)
-- Name: Players; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Players";


ALTER SCHEMA "Players" OWNER TO postgres;

--
-- TOC entry 7 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2204 (class 0 OID 0)
-- Dependencies: 7
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2206 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 2 (class 3079 OID 148925)
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- TOC entry 2207 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET search_path = "Blog", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 188 (class 1259 OID 148745)
-- Name: ArticleCommentEntity; Type: TABLE; Schema: Blog; Owner: postgres
--

CREATE TABLE "ArticleCommentEntity" (
    "CommentId" uuid NOT NULL,
    "PostId" uuid NOT NULL,
    "SenderId" uuid NOT NULL,
    "Message" character varying(512) NOT NULL,
    "PostedDate" timestamp without time zone NOT NULL,
    "Likes" bigint NOT NULL,
    "TargetCommentId" uuid
);


ALTER TABLE "ArticleCommentEntity" OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 148702)
-- Name: ArticlePostEntity; Type: TABLE; Schema: Blog; Owner: postgres
--

CREATE TABLE "ArticlePostEntity" (
    "PostId" uuid NOT NULL,
    "PostWriterId" uuid NOT NULL,
    "PostDateTime" timestamp without time zone NOT NULL,
    "Views" bigint NOT NULL,
    "AllowComments" boolean DEFAULT true NOT NULL,
    "Category" uuid NOT NULL
);


ALTER TABLE "ArticlePostEntity" OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 148708)
-- Name: ArticleTranslateEntity; Type: TABLE; Schema: Blog; Owner: postgres
--

CREATE TABLE "ArticleTranslateEntity" (
    "TranslateId" uuid NOT NULL,
    "PostId" uuid NOT NULL,
    "LanguageId" smallint NOT NULL,
    "Title" character varying(128) NOT NULL,
    "ContentFull" text NOT NULL,
    "Description" character varying(196) NOT NULL,
    "KeyWords" character varying(128) NOT NULL,
    "ContentPreview" text DEFAULT 'ts'::text NOT NULL,
    "BannerImageUrl" character varying(256),
    "Likes" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "ArticleTranslateEntity" OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 148716)
-- Name: TagsEntity; Type: TABLE; Schema: Blog; Owner: postgres
--

CREATE TABLE "TagsEntity" (
    "TagId" uuid NOT NULL
);


ALTER TABLE "TagsEntity" OWNER TO postgres;

SET search_path = "Players", pg_catalog;

--
-- TOC entry 189 (class 1259 OID 149079)
-- Name: FriendEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "FriendEntity" (
    "MemberId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FriendId" uuid NOT NULL,
    "State" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "FriendEntity" OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 149084)
-- Name: PlayerAccountEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerAccountEntity" (
    "PlayerId" uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "PlayerName" character varying(24) NOT NULL,
    "Experience_Level" smallint DEFAULT 0 NOT NULL,
    "Experience_Experience" bigint DEFAULT 0 NOT NULL,
    "Experience_WeekExperience" bigint DEFAULT 0 NOT NULL,
    "Cash_Money" bigint DEFAULT 0 NOT NULL,
    "Cash_Donate" bigint DEFAULT 0 NOT NULL,
    "RegistrationDate" timestamp without time zone DEFAULT now() NOT NULL,
    "PremiumEndDate" timestamp without time zone,
    "LastActivityDate" timestamp without time zone DEFAULT now() NOT NULL,
    "CurrentFractionTypeId" smallint DEFAULT 0,
    "Reputation_Keepers" bigint DEFAULT 0 NOT NULL,
    "Reputation_Keepers_Friend" bigint DEFAULT 0 NOT NULL,
    "Reputation_RIFT" bigint DEFAULT 0 NOT NULL,
    "Reputation_RIFT_Friend" bigint DEFAULT 0 NOT NULL
);


ALTER TABLE "PlayerAccountEntity" OWNER TO postgres;

SET search_path = "Blog", pg_catalog;

--
-- TOC entry 2196 (class 0 OID 148745)
-- Dependencies: 188
-- Data for Name: ArticleCommentEntity; Type: TABLE DATA; Schema: Blog; Owner: postgres
--

INSERT INTO "ArticleCommentEntity" ("CommentId", "PostId", "SenderId", "Message", "PostedDate", "Likes", "TargetCommentId") VALUES ('01dc73e5-c05e-4bb2-a726-6f145df720bb', '4aefbf49-b803-4052-98bd-00d909080bda', '4aefbf49-b803-4052-98bd-00d909080bda', 'fdsfs', '2016-06-23 02:00:10.260209', 0, NULL);


--
-- TOC entry 2193 (class 0 OID 148702)
-- Dependencies: 185
-- Data for Name: ArticlePostEntity; Type: TABLE DATA; Schema: Blog; Owner: postgres
--

INSERT INTO "ArticlePostEntity" ("PostId", "PostWriterId", "PostDateTime", "Views", "AllowComments", "Category") VALUES ('4aefbf49-b803-4052-98bd-00d909080bda', '4aefbf49-b803-4052-98bd-00d909080bda', '2016-06-20 20:43:02.103151', 10, true, '4aefbf49-b803-4052-98bd-00d909080bda');


--
-- TOC entry 2194 (class 0 OID 148708)
-- Dependencies: 186
-- Data for Name: ArticleTranslateEntity; Type: TABLE DATA; Schema: Blog; Owner: postgres
--

INSERT INTO "ArticleTranslateEntity" ("TranslateId", "PostId", "LanguageId", "Title", "ContentFull", "Description", "KeyWords", "ContentPreview", "BannerImageUrl", "Likes") VALUES ('4aefbf49-b803-4052-98bd-00d909080bd2', '4aefbf49-b803-4052-98bd-00d909080bda', 1, 'Hello World!', 'Hello World!', 'Hello World!', 'Hello World!', 'ts', NULL, 5);
INSERT INTO "ArticleTranslateEntity" ("TranslateId", "PostId", "LanguageId", "Title", "ContentFull", "Description", "KeyWords", "ContentPreview", "BannerImageUrl", "Likes") VALUES ('4aefbf49-b803-4052-98bd-00d909080bd1', '4aefbf49-b803-4052-98bd-00d909080bda', 0, 'Привет мир', 'Привет мир', 'Привет мир', 'Привет мир', 'ts', 'http://lokagame.com/Content/img/main.jpg', 10);


--
-- TOC entry 2195 (class 0 OID 148716)
-- Dependencies: 187
-- Data for Name: TagsEntity; Type: TABLE DATA; Schema: Blog; Owner: postgres
--



SET search_path = "Players", pg_catalog;

--
-- TOC entry 2197 (class 0 OID 149079)
-- Dependencies: 189
-- Data for Name: FriendEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--



--
-- TOC entry 2198 (class 0 OID 149084)
-- Dependencies: 190
-- Data for Name: PlayerAccountEntity; Type: TABLE DATA; Schema: Players; Owner: postgres
--

INSERT INTO "PlayerAccountEntity" ("PlayerId", "PlayerName", "Experience_Level", "Experience_Experience", "Experience_WeekExperience", "Cash_Money", "Cash_Donate", "RegistrationDate", "PremiumEndDate", "LastActivityDate", "CurrentFractionTypeId", "Reputation_Keepers", "Reputation_Keepers_Friend", "Reputation_RIFT", "Reputation_RIFT_Friend") VALUES ('4aefbf49-b803-4052-98bd-00d909080bda', 'SeNTike', 0, 0, 0, 0, 0, '2016-06-22 23:56:12.899698', NULL, '2016-06-22 23:56:12.899698', 0, 0, 0, 0, 0);


SET search_path = "Blog", pg_catalog;

--
-- TOC entry 2066 (class 2606 OID 148752)
-- Name: ArticleCommentEntity_pkey; Type: CONSTRAINT; Schema: Blog; Owner: postgres
--

ALTER TABLE ONLY "ArticleCommentEntity"
    ADD CONSTRAINT "ArticleCommentEntity_pkey" PRIMARY KEY ("CommentId");


--
-- TOC entry 2062 (class 2606 OID 148715)
-- Name: ArticleTranslateEntity_pkey; Type: CONSTRAINT; Schema: Blog; Owner: postgres
--

ALTER TABLE ONLY "ArticleTranslateEntity"
    ADD CONSTRAINT "ArticleTranslateEntity_pkey" PRIMARY KEY ("TranslateId");


--
-- TOC entry 2060 (class 2606 OID 148707)
-- Name: PostEntity_pkey; Type: CONSTRAINT; Schema: Blog; Owner: postgres
--

ALTER TABLE ONLY "ArticlePostEntity"
    ADD CONSTRAINT "PostEntity_pkey" PRIMARY KEY ("PostId");


--
-- TOC entry 2064 (class 2606 OID 148720)
-- Name: TagsEntity_pkey; Type: CONSTRAINT; Schema: Blog; Owner: postgres
--

ALTER TABLE ONLY "TagsEntity"
    ADD CONSTRAINT "TagsEntity_pkey" PRIMARY KEY ("TagId");


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2068 (class 2606 OID 149102)
-- Name: FriendEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_pkey" PRIMARY KEY ("MemberId");


--
-- TOC entry 2070 (class 2606 OID 149104)
-- Name: PlayerEntity_PlayerName_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAccountEntity"
    ADD CONSTRAINT "PlayerEntity_PlayerName_key" UNIQUE ("PlayerName");


--
-- TOC entry 2072 (class 2606 OID 149106)
-- Name: PlayerEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerAccountEntity"
    ADD CONSTRAINT "PlayerEntity_pkey" PRIMARY KEY ("PlayerId");


SET search_path = "Blog", pg_catalog;

--
-- TOC entry 2075 (class 2606 OID 149122)
-- Name: ArticleCommentEntity_SenderId_fkey; Type: FK CONSTRAINT; Schema: Blog; Owner: postgres
--

ALTER TABLE ONLY "ArticleCommentEntity"
    ADD CONSTRAINT "ArticleCommentEntity_SenderId_fkey" FOREIGN KEY ("SenderId") REFERENCES "Players"."PlayerAccountEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2076 (class 2606 OID 149130)
-- Name: ArticleCommentEntity_SenderId_fkey1; Type: FK CONSTRAINT; Schema: Blog; Owner: postgres
--

ALTER TABLE ONLY "ArticleCommentEntity"
    ADD CONSTRAINT "ArticleCommentEntity_SenderId_fkey1" FOREIGN KEY ("SenderId") REFERENCES "Players"."PlayerAccountEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2073 (class 2606 OID 149117)
-- Name: ArticlePostEntity_PostWriterId_fkey; Type: FK CONSTRAINT; Schema: Blog; Owner: postgres
--

ALTER TABLE ONLY "ArticlePostEntity"
    ADD CONSTRAINT "ArticlePostEntity_PostWriterId_fkey" FOREIGN KEY ("PostWriterId") REFERENCES "Players"."PlayerAccountEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2074 (class 2606 OID 148721)
-- Name: ArticleTranslateEntity_PostId_fkey; Type: FK CONSTRAINT; Schema: Blog; Owner: postgres
--

ALTER TABLE ONLY "ArticleTranslateEntity"
    ADD CONSTRAINT "ArticleTranslateEntity_PostId_fkey" FOREIGN KEY ("PostId") REFERENCES "ArticlePostEntity"("PostId") ON DELETE CASCADE;


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2077 (class 2606 OID 149107)
-- Name: FriendEntity_FriendId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_FriendId_fkey" FOREIGN KEY ("FriendId") REFERENCES "PlayerAccountEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2078 (class 2606 OID 149112)
-- Name: FriendEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "FriendEntity"
    ADD CONSTRAINT "FriendEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerAccountEntity"("PlayerId") ON DELETE CASCADE;


--
-- TOC entry 2205 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-07-08 19:41:06

--
-- PostgreSQL database dump complete
--

