--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-07-04 04:36:02

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = "Players", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 237 (class 1259 OID 214748)
-- Name: InstanceOfPlayerKit; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "InstanceOfPlayerKit" (
    "ItemId" uuid NOT NULL
);


ALTER TABLE "InstanceOfPlayerKit" OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 214753)
-- Name: ProfileItemEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "ProfileItemEntity" (
    "ProfileItemId" uuid NOT NULL,
    "ProfileId" uuid NOT NULL,
    "ItemId" uuid NOT NULL,
    "SlotId" smallint NOT NULL
);


ALTER TABLE "ProfileItemEntity" OWNER TO postgres;

SET search_path = "Store", pg_catalog;

--
-- TOC entry 239 (class 1259 OID 214787)
-- Name: KitItemInstance; Type: TABLE; Schema: Store; Owner: postgres
--

CREATE TABLE "KitItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);


ALTER TABLE "KitItemInstance" OWNER TO postgres;

SET search_path = "Players", pg_catalog;

--
-- TOC entry 2166 (class 2606 OID 214752)
-- Name: InstanceOfPlayerKit_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "InstanceOfPlayerKit"
    ADD CONSTRAINT "InstanceOfPlayerKit_pkey" PRIMARY KEY ("ItemId");


--
-- TOC entry 2168 (class 2606 OID 214757)
-- Name: ProfileItemEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "ProfileItemEntity"
    ADD CONSTRAINT "ProfileItemEntity_pkey" PRIMARY KEY ("ProfileItemId");


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2170 (class 2606 OID 214791)
-- Name: KitItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "KitItemInstance"
    ADD CONSTRAINT "KitItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


SET search_path = "Players", pg_catalog;

--
-- TOC entry 2172 (class 2606 OID 214758)
-- Name: ProfileItemEntity_ItemId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "ProfileItemEntity"
    ADD CONSTRAINT "ProfileItemEntity_ItemId_fkey" FOREIGN KEY ("ItemId") REFERENCES "InstanceOfPlayerItem"("ItemId") ON DELETE CASCADE;


--
-- TOC entry 2171 (class 2606 OID 214763)
-- Name: ProfileItemEntity_ProfileId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "ProfileItemEntity"
    ADD CONSTRAINT "ProfileItemEntity_ProfileId_fkey" FOREIGN KEY ("ProfileId") REFERENCES "PlayerProfileEntity"("AssetId") ON DELETE CASCADE;


SET search_path = "Store", pg_catalog;

--
-- TOC entry 2173 (class 2606 OID 214792)
-- Name: KitItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "KitItemInstance"
    ADD CONSTRAINT "KitItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


-- Completed on 2016-07-04 04:36:02

--
-- PostgreSQL database dump complete
--

