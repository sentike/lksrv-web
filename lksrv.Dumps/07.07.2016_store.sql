--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2016-07-07 19:07:59

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 11 (class 2615 OID 47483)
-- Name: Store; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "Store";


ALTER SCHEMA "Store" OWNER TO postgres;

SET search_path = "Store", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 206 (class 1259 OID 47664)
-- Name: AbstractItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "AbstractItemInstance" (
    "ModelId" smallint NOT NULL,
    "Level" smallint DEFAULT 0 NOT NULL,
    "Cost_Amount" integer NOT NULL,
    "Cost_IsDonate" boolean DEFAULT false NOT NULL,
    "FractionId" smallint DEFAULT 0 NOT NULL,
    "CategoryTypeId" smallint DEFAULT 1 NOT NULL,
    "IsAvalible" boolean DEFAULT true NOT NULL,
    "DefaultSkinId" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "AbstractItemInstance" OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 47673)
-- Name: AddonItemContainer; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "AddonItemContainer" (
    "ContainerId" integer NOT NULL,
    "ModelId" smallint NOT NULL,
    "ItemId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    "ModelCategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AddonItemContainer" OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 47676)
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE; Schema: Store; Owner: postgres
--

CREATE SEQUENCE "AddonItemContainer_ContainerId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "AddonItemContainer_ContainerId_seq" OWNER TO postgres;

--
-- TOC entry 2242 (class 0 OID 0)
-- Dependencies: 208
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE OWNED BY; Schema: Store; Owner: postgres
--

ALTER SEQUENCE "AddonItemContainer_ContainerId_seq" OWNED BY "AddonItemContainer"."ContainerId";


--
-- TOC entry 209 (class 1259 OID 47678)
-- Name: AddonItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "AddonItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AddonItemInstance" OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 47681)
-- Name: AmmoItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "AmmoItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);


ALTER TABLE "AmmoItemInstance" OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 47684)
-- Name: ArmourItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "ArmourItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "ArmourItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 1))
);


ALTER TABLE "ArmourItemInstance" OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 47688)
-- Name: CharacterItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "CharacterItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "CharacterItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 5))
);


ALTER TABLE "CharacterItemInstance" OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 47692)
-- Name: FractionEntity; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "FractionEntity" (
    "FractionId" smallint NOT NULL,
    "Bonus_Experience" smallint DEFAULT 0 NOT NULL,
    "Bonus_Money" smallint DEFAULT 0 NOT NULL,
    "Discount_Armour" smallint DEFAULT 0 NOT NULL,
    "Discount_Ammo" smallint DEFAULT 0 NOT NULL,
    "Discount_Weapon" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE "FractionEntity" OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 47700)
-- Name: KitItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "KitItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL
);


ALTER TABLE "KitItemInstance" OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 47703)
-- Name: SkinItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "SkinItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "SkinItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 7))
);


ALTER TABLE "SkinItemInstance" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 47707)
-- Name: WeaponItemInstance; Type: TABLE; Schema: Store; Owner: postgres; Tablespace: 
--

CREATE TABLE "WeaponItemInstance" (
    "ModelId" smallint NOT NULL,
    "CategoryTypeId" smallint NOT NULL,
    CONSTRAINT "WeaponItemInstance_CategoryTypeId_check" CHECK (("CategoryTypeId" = 0))
);


ALTER TABLE "WeaponItemInstance" OWNER TO postgres;

--
-- TOC entry 2077 (class 2604 OID 47755)
-- Name: ContainerId; Type: DEFAULT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer" ALTER COLUMN "ContainerId" SET DEFAULT nextval('"AddonItemContainer_ContainerId_seq"'::regclass);


--
-- TOC entry 2227 (class 0 OID 47664)
-- Dependencies: 206
-- Data for Name: AbstractItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (9, 1, 1000, false, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (10, 1, 1000, false, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (11, 1, 1000, false, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 0, 5000, false, 1, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 0, 3500, false, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 0, 4500, false, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 0, 7000, false, 2, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 0, 175000, false, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 0, 23543, false, 0, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 0, 14477, true, 1, 0, false, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 1, 1000, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (18, 3, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (20, 4, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (23, 6, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (22, 7, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (19, 3, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (21, 9, 1000, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 2, 50700, false, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 1, 58700, false, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 3, 50000, false, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 4, 15000, false, 0, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 5000, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (24, 0, 1000, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (26, 1, 2600, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (27, 2, 8600, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (25, 2, 11200, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (12, 0, 6800, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (15, 0, 2600, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (13, 1, 9800, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (14, 1, 4200, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (16, 2, 18600, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (17, 2, 22100, false, 0, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (28, 1, 5400, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 1, 2600, false, 2, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 1, 4200, false, 2, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 1, 3200, false, 2, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 1, 1100, false, 2, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 2, 1800, false, 2, 1, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 0, false, 0, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 1, 100, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 2, 500, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 1, 400, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 2, 850, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 1, 250, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 0, 100, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 1, 400, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 2, 450, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (9, 3, 600, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (10, 3, 800, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (11, 3, 900, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (12, 2, 200, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (13, 2, 0, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (14, 2, 400, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (15, 3, 600, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (16, 3, 800, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (17, 0, 0, false, 0, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (18, 0, 0, false, 0, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (19, 0, 0, false, 0, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 0, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 1, 500, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 1, 1500, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 1, 1600, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 1, 2000, false, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (8, 1, 100000, true, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (29, 3, 11200, false, 2, 0, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 1, 10000, true, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (7, 1, 100000, true, 0, 7, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 0, 0, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 10, 50000, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (2, 20, 80000, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (3, 20, 80000, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (4, 40, 150000, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (5, 30, 110000, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (6, 38, 140000, false, 0, 5, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (0, 1, 800, false, 2, 9, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (1, 1, 900, false, 0, 9, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (20, 2, 800, false, 2, 4, true, 0);
INSERT INTO "AbstractItemInstance" ("ModelId", "Level", "Cost_Amount", "Cost_IsDonate", "FractionId", "CategoryTypeId", "IsAvalible", "DefaultSkinId") VALUES (21, 2, 600, false, 2, 4, true, 0);


--
-- TOC entry 2228 (class 0 OID 47673)
-- Dependencies: 207
-- Data for Name: AddonItemContainer; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2243 (class 0 OID 0)
-- Dependencies: 208
-- Name: AddonItemContainer_ContainerId_seq; Type: SEQUENCE SET; Schema: Store; Owner: postgres
--

SELECT pg_catalog.setval('"AddonItemContainer_ContainerId_seq"', 1, false);


--
-- TOC entry 2230 (class 0 OID 47678)
-- Dependencies: 209
-- Data for Name: AddonItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (9, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (10, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (11, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (12, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (13, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (14, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (15, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (16, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (17, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (18, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (19, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (20, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (21, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (22, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (23, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (24, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (25, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (26, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (27, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (28, 4);
INSERT INTO "AddonItemInstance" ("ModelId", "CategoryTypeId") VALUES (29, 4);


--
-- TOC entry 2231 (class 0 OID 47681)
-- Dependencies: 210
-- Data for Name: AmmoItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--



--
-- TOC entry 2232 (class 0 OID 47684)
-- Dependencies: 211
-- Data for Name: ArmourItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 1);
INSERT INTO "ArmourItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 1);


--
-- TOC entry 2233 (class 0 OID 47688)
-- Dependencies: 212
-- Data for Name: CharacterItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 5);
INSERT INTO "CharacterItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 5);


--
-- TOC entry 2234 (class 0 OID 47692)
-- Dependencies: 213
-- Data for Name: FractionEntity; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (1, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (2, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (3, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (4, 0, 0, 0, 0, 0);
INSERT INTO "FractionEntity" ("FractionId", "Bonus_Experience", "Bonus_Money", "Discount_Armour", "Discount_Ammo", "Discount_Weapon") VALUES (0, 0, 0, 0, 0, 0);


--
-- TOC entry 2235 (class 0 OID 47700)
-- Dependencies: 214
-- Data for Name: KitItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "KitItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 9);
INSERT INTO "KitItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 9);


--
-- TOC entry 2236 (class 0 OID 47703)
-- Dependencies: 215
-- Data for Name: SkinItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (4, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 7);
INSERT INTO "SkinItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 7);


--
-- TOC entry 2237 (class 0 OID 47707)
-- Dependencies: 216
-- Data for Name: WeaponItemInstance; Type: TABLE DATA; Schema: Store; Owner: postgres
--

INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (0, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (1, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (2, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (3, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (5, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (6, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (7, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (8, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (9, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (10, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (11, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (12, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (13, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (14, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (15, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (16, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (17, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (18, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (19, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (20, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (21, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (22, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (23, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (24, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (25, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (26, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (27, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (28, 0);
INSERT INTO "WeaponItemInstance" ("ModelId", "CategoryTypeId") VALUES (29, 0);


--
-- TOC entry 2088 (class 2606 OID 47829)
-- Name: AbstractItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AbstractItemInstance"
    ADD CONSTRAINT "AbstractItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2090 (class 2606 OID 47831)
-- Name: AddonItemContainer_AddonId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_AddonId_key" UNIQUE ("ModelId");


--
-- TOC entry 2092 (class 2606 OID 47833)
-- Name: AddonItemContainer_CategoryTypeId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_CategoryTypeId_key" UNIQUE ("CategoryTypeId");


--
-- TOC entry 2094 (class 2606 OID 47835)
-- Name: AddonItemContainer_ItemId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_ItemId_key" UNIQUE ("ItemId");


--
-- TOC entry 2096 (class 2606 OID 47837)
-- Name: AddonItemContainer_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_pkey" PRIMARY KEY ("ContainerId");


--
-- TOC entry 2098 (class 2606 OID 47839)
-- Name: AddonItemInstance_ModelId_key; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "AddonItemInstance_ModelId_key" UNIQUE ("ModelId");


--
-- TOC entry 2102 (class 2606 OID 47841)
-- Name: AmmoItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AmmoItemInstance"
    ADD CONSTRAINT "AmmoItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2104 (class 2606 OID 47843)
-- Name: ArmourItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2106 (class 2606 OID 47845)
-- Name: CharacterItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "CharacterItemInstance"
    ADD CONSTRAINT "CharacterItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2108 (class 2606 OID 47847)
-- Name: FractionEntity_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "FractionEntity"
    ADD CONSTRAINT "FractionEntity_pkey" PRIMARY KEY ("FractionId");


--
-- TOC entry 2100 (class 2606 OID 47849)
-- Name: ItemAddonInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "AddonItemInstance"
    ADD CONSTRAINT "ItemAddonInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2110 (class 2606 OID 47851)
-- Name: KitItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "KitItemInstance"
    ADD CONSTRAINT "KitItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2112 (class 2606 OID 47853)
-- Name: SkinItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "SkinItemInstance"
    ADD CONSTRAINT "SkinItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2114 (class 2606 OID 47855)
-- Name: WeaponItemInstance_pkey; Type: CONSTRAINT; Schema: Store; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "WeaponItemInstance"
    ADD CONSTRAINT "WeaponItemInstance_pkey" PRIMARY KEY ("ModelId", "CategoryTypeId");


--
-- TOC entry 2115 (class 2606 OID 48028)
-- Name: AddonItemContainer_ItemId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "AddonItemContainer"
    ADD CONSTRAINT "AddonItemContainer_ItemId_fkey" FOREIGN KEY ("ItemId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2116 (class 2606 OID 48033)
-- Name: ArmourItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "ArmourItemInstance"
    ADD CONSTRAINT "ArmourItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


--
-- TOC entry 2117 (class 2606 OID 48038)
-- Name: KitItemInstance_ModelId_fkey; Type: FK CONSTRAINT; Schema: Store; Owner: postgres
--

ALTER TABLE ONLY "KitItemInstance"
    ADD CONSTRAINT "KitItemInstance_ModelId_fkey" FOREIGN KEY ("ModelId", "CategoryTypeId") REFERENCES "AbstractItemInstance"("ModelId", "CategoryTypeId") ON DELETE CASCADE;


-- Completed on 2016-07-07 19:07:59

--
-- PostgreSQL database dump complete
--

