--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-06-27 01:20:55

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = "Players", pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 248 (class 1259 OID 149147)
-- Name: PlayerReputationEntity; Type: TABLE; Schema: Players; Owner: postgres
--

CREATE TABLE "PlayerReputationEntity" (
    "ReputationId" uuid NOT NULL,
    "PlayerId" uuid NOT NULL,
    "FractionId" smallint NOT NULL,
    "CurrentRank" smallint NOT NULL,
    "LastRank" smallint NOT NULL,
    "Reputation" bigint NOT NULL
);


ALTER TABLE "PlayerReputationEntity" OWNER TO postgres;

--
-- TOC entry 2198 (class 2606 OID 149173)
-- Name: PlayerReputationEntity_PlayerId_FractionId_key; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_PlayerId_FractionId_key" UNIQUE ("PlayerId", "FractionId");


--
-- TOC entry 2200 (class 2606 OID 149151)
-- Name: PlayerReputationEntity_pkey; Type: CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_pkey" PRIMARY KEY ("ReputationId");


--
-- TOC entry 2202 (class 2606 OID 149152)
-- Name: PlayerReputationEntity_FractionId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_FractionId_fkey" FOREIGN KEY ("FractionId") REFERENCES "Store"."FractionEntity"("FractionId") ON DELETE CASCADE;


--
-- TOC entry 2201 (class 2606 OID 149157)
-- Name: PlayerReputationEntity_PlayerId_fkey; Type: FK CONSTRAINT; Schema: Players; Owner: postgres
--

ALTER TABLE ONLY "PlayerReputationEntity"
    ADD CONSTRAINT "PlayerReputationEntity_PlayerId_fkey" FOREIGN KEY ("PlayerId") REFERENCES "PlayerEntity"("PlayerId") ON DELETE CASCADE;


-- Completed on 2016-06-27 01:20:56

--
-- PostgreSQL database dump complete
--

