﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Instance;
using Loka.Player;
using Loka.Server.Player.Models;

namespace Loka.Areas.Admin.View.PlayerController
{
    public class PlayerEditView : IAbstractPlayerView
    {
        public Guid PlayerId { get; set; }
        public string PlayerName { get; set; }
        public DateTime? PremiumEndDate { get; set; }
        public PlayerExperience Experience { get; set; }
        public PlayerWeekBonus WeekBonus { get; set; }
        public DateTime RegistrationDate { get; set; }
        public AccountMigrationAction MigrationAction { get; set; }
    }
}
