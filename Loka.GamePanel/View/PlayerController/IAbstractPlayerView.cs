﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Player.Instance;
using Loka.Player;

namespace Loka.Areas.Admin.View.PlayerController
{
    interface IAbstractPlayerView
    {
        Guid PlayerId { get; set; }
        string PlayerName { get; set; }
        PlayerExperience Experience { get; set; }
        DateTime RegistrationDate { get; set; }
    }
}
