﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Cluster;
using Loka.Common.Match.GameMode;
using Loka.Common.Player.Instance;
using Loka.Infrastructure;
using Loka.Player;
using Loka.Server.Models.Queue;

namespace Loka.Areas.Admin.View.PlayerController
{
    public class PlayerSearchView : IAbstractPlayerView
    {
        public Guid PlayerId { get; set; }
        public string PlayerName { get; set; }
        public PlayerExperience Experience { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime JoinDate { get; set; }
        public GameModeTypeId GameModeTypeId { get; set; }
        public ClusterRegionId RegionTypeId { get; set; }
        public QueueRange Level { get; set; }
        public bool Ready { get; set; }

    }
}