﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Fraction;
using Loka.Common.Store.Item.Addon;
using Loka.Common.Store.Item.Character;
using Loka.Common.Store.Item.Skin;

namespace Loka.GamePanel.View.Store
{
    public class ItemCreateView
    {
        //[Required]
        [Display(Name = "Модель")]
        public short ModelId { get; set; }

        public string ModelIdName { get; set; }
        //[Required]

        [Display(Name = "Категория")]
        public CategoryTypeId CategoryTypeId { get; set; } = CategoryTypeId.End;

        public CharacterModelFlagId CharacterModelsFlag { get; set; }

        [Display(Name = "Флаги")]
        public ItemInstanceFlags[] Flags { get; set; }
        public ItemInstanceFlags InstanceFlags => Flags.Aggregate(ItemInstanceFlags.None, (current, f) => current | f);

        //[Required]
        [Display(Name = "Уровень")]
        public int Level { get; set; } = 1;
        public ItemSkinTypeId DefaultSkinId { get; set; }

        //[Required]
        [Display(Name = "Фракция")]
        public FractionTypeId FractionId { get; set; }

        //[Required]
        public short DefaultAmount { get; set; } = 1;

        //[Required]
        [Display(Name = "Стомость")]
        public StoreItemCost Cost { get; set; }

        [Display(Name = "Стомость подписки")]
        public StoreItemCost SubscribeCost { get; set; }
        public PlayerProfileTypeId TargetProfileId { get; set; } = PlayerProfileTypeId.End;
        public ItemMigrationAction MigrationAction { get; set; }

        [Display(Name = "Производительность")]
        public long Performance { get; set; }

        [Display(Name = "Вместимость")]
        public long Storage { get; set; }
        public long Duration { get; set; }
    }

    public static class ViewHelper
    {
        public static ItemCreateView AsView(this ItemInstanceEntity entity)
        {
            Mapper.Initialize
            (
                c => c.CreateMap<ItemInstanceEntity, ItemCreateView>()
                .ForMember(p => p.Flags, p=>p.MapFrom(m => m.FlagsArray()))
            );
            return Mapper.Map<ItemCreateView>(entity);
        }

        public static ItemInstanceEntity AsEntity(this ItemCreateView view)
        {

            return new ItemInstanceEntity
            {
                ModelId = view.ModelId,
                CategoryTypeId = view.CategoryTypeId,
                CharacterModelsFlag = view.CharacterModelsFlag,

                Flags = view.InstanceFlags,
                Level = view.Level,

                DefaultSkinId = view.DefaultSkinId,
                FractionId = view.FractionId,

                Cost = view.Cost,
                SubscribeCost = view.Cost,

                TargetProfileId = view.TargetProfileId,
                MigrationAction = view.MigrationAction,

                Duration = view.Duration,
            };
        }
    }

}