﻿using Loka.Common.Store;
using Loka.Common.Store.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Loka.Common.Store.Abstract;

namespace Loka.GamePanel.View.Store
{
    public class ItemOptionView
    {
        public Guid EntityId { get; set; }

        public short ModelId { get; set; }
        public string ModelName { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }

        public ItemInstancePropertyName Key { get; set; }
        public DateTime EditDate { get; set; }
        public string Last { get; set; }
        public string Value { get; set; }
        public string Default { get; set; }

        public static SelectList PropertyNameSelectList(ItemInstancePropertyName current)
        {
            return new SelectList(ItemInstancePropertyHelper.Values, current);
        }


        public ItemOptionView() { }

        

        public ItemOptionView(ItemInstanceEntity entity)
        {
            ModelId = entity.ModelId;
            ModelName = entity.ModelIdName;
            CategoryTypeId = entity.CategoryTypeId;
        }

        public ItemOptionView(ItemInstancePropertyEntity entity)
        {
            EntityId = entity.EntityId;
            ModelId = entity.ModelId;
            ModelName = entity.ModelEntity.ModelIdName;
            CategoryTypeId = entity.CategoryTypeId;
            Key = entity.Key;
            Last = entity.Last;
            Value = entity.Value;
            Default = entity.Default;
        }

    }
}