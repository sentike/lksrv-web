﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loka.Areas.Admin.View
{
    public class AccountLoginView
    {
        public string ProviderKey { get; set; }
        public string LoginProvider { get; set; }
    }
}