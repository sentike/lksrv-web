﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Areas.Admin.View
{
    public class AccountUserView
    {
        public Guid UserId { get; set; }
        public long Balance { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public string SteamLogin { get; set; } 
    }
}
