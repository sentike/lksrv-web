﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Store;
using Loka.Common.Store.Fraction;
using Loka.Common.Store.Item.Character;

namespace Loka.GamePanel.View.CharacterProfile
{
    public class CharacterProfileView
    {
        [Display(Name = "#")]
        public int TemplateId { get; set; }

        [Display(Name = "Фракция")]
        public FractionTypeId Fraction { get; set; }

        //======================================================

        [Display(Name = "Персонаж")]
        public short CharacterModelId { get; set; }
        public CategoryTypeId CharacterCategoryId { get; set; } = CategoryTypeId.Character;

        public CharacterModelId CharacterId
        {
            get { return (CharacterModelId) CharacterModelId; }
            set { CharacterModelId = (short) value; }
        }

        //======================================================

        [Display(Name = "Предмет")]
        public short ItemModelId { get; set; }
        public string ItemModelName { get; set; }

        [Display(Name = "Категория")]
        public CategoryTypeId ItemCategoryId { get; set; }

        //======================================================

        [Display(Name = "Слот")]
        public PlayerProfileInstanceSlotId ProfileSlot { get; set; }

        //======================================================
        public static readonly List<SelectListItem> ItemCategories = new List<SelectListItem>
        {
            new SelectListItem {Value = CategoryTypeId.Weapon.ToString(), Text = CategoryTypeId.Weapon.ToString()},
            new SelectListItem {Value = CategoryTypeId.Armour.ToString(), Text = CategoryTypeId.Armour.ToString()},
            new SelectListItem {Value = CategoryTypeId.Grenade.ToString(), Text = CategoryTypeId.Grenade.ToString()},
            new SelectListItem {Value = CategoryTypeId.Kit.ToString(), Text = CategoryTypeId.Kit.ToString()},
        };

        public override string ToString()
        {
            return $"[{TemplateId}] | {CharacterId}] | {ProfileSlot}: [{ItemCategoryId} - {ItemModelId}]";
        }
    }

    public static partial class ViewHelper
    {
        private static readonly IMapper Mapper = new MapperConfiguration(c =>
        {
            c.CreateMap<CharacterProfileTemplate, CharacterProfileView>()
                .ForMember(p => p.Fraction, p => p.MapFrom(m => m.ItemModelEntity.FractionId))
                .ForMember(p => p.ItemModelName, p => p.MapFrom(m => m.ItemModelEntity.ModelIdName));
            c.CreateMap<CharacterProfileView, CharacterProfileTemplate>();
        }
        ).CreateMapper();

        public static List<CharacterProfileView> AsView(this ICollection<CharacterProfileTemplate> entity)
        {
            return entity.Select(AsView).ToList();
        }

        public static CharacterProfileView AsView(this CharacterProfileTemplate entity)
        {
            return Mapper.Map<CharacterProfileView>(entity);
        }


        public static CharacterProfileTemplate AsEntity(this CharacterProfileView entity)
        {
            return Mapper.Map<CharacterProfileTemplate>(entity);
        }
    }

}