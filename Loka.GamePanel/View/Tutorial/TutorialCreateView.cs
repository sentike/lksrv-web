﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Loka.Common.Store;
using Loka.Common.Tutorial;

namespace Loka.GamePanel.View.Tutorial
{
    public class TutorialCreateView
    {
        [Display(Name = "#")]
        public long TutorialId { get; set; }

        [Display(Name = "Название этапа в Blueprint")]
        public string TutorialName { get; set; }

        [Display(Name = "Коментарий")]
        public string AdminComment { get; set; }

        public List<TutorialCreateStepView> Steps { get; set; } = new List<TutorialCreateStepView>();
    }

    public class TutorialCreateStepView
    {
        [Display(Name = "Ид этапа")]
        public long StepId { get; set; }
        public long StepKey { get; set; }

        [Display(Name = "Ид урока")]
        public long TutorialId { get; set; }
        public List<SelectListItem> TutorialSelectListItems { get; set; } = new List<SelectListItem>();

        [Display(Name = "Вознагражение. Категория предмета")]
        public CategoryTypeId? RewardCategoryId { get; set; }

        [Display(Name = "Вознагражение. Модель предмета")]
        public short? RewardModelId { get; set; }
        public string RewardModelName { get; set; }


        [Display(Name = "Вознагражение. Сколько выдать")]
        public long RewardGive { get; set; }

        [Display(Name = "Вознагражение. Сколько использовать при выдаче")]
        public long RewardActivate { get; set; }

        [Display(Name = "Коментарий")]
        public string AdminComment { get; set; }
    }

    public static class ViewHelper
    {
        private static readonly IMapper Mapper = new MapperConfiguration(c =>
            {
                c.CreateMap<TutorialStepEntity, TutorialCreateStepView>()
                    .ForMember(p => p.RewardModelName, p => p.MapFrom(m => m.RewardEntity.ModelIdName));

                c.CreateMap<TutorialInstanceEntity, TutorialCreateView>()
                    .ForSourceMember(p => p.StepEntities, p => p.Ignore())
                    .ForMember(p => p.Steps, p => p.MapFrom(m => m.StepEntities.OrderBy(q => q.StepId).Select(q => q.AsCreateView())));

            }
        ).CreateMapper();

        public static TutorialCreateStepView AsCreateView(this TutorialStepEntity entity)
        {
            return Mapper.Map<TutorialCreateStepView>(entity);
        }


        public static TutorialCreateView AsCreateView(this TutorialInstanceEntity entity)
        {
            return Mapper.Map<TutorialCreateView>(entity);
        }
    }

}