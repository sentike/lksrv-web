﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loka.Areas.Admin.Models
{
    public class GameModeOnlineStatistic
    {
    }

    public class HomeStatistic
    {
        public long Players { get; set; }
        public long NewPlayers { get; set; }
        public long OnlinePlayers { get; set; }
    }
}