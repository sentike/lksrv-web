﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;


namespace Loka.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        //[Display(ResourceType = typeof(UserSharedFields), Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        //[Display(Name = "Код")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        //[Display(Name = "Запомнить браузер?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        //[Display(ResourceType = typeof(UserSharedFields), Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        //[Display(ResourceType = typeof(UserSharedFields), Name  = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        //[Display(ResourceType = typeof(UserSharedFields), Name = "Password")]
        public string Password { get; set; }

        //[Display(ResourceType = typeof(UserSharedFields), Name = "RememberMe")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        //[Display(ResourceType = typeof(UserSharedFields), Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        //[Display(ResourceType = typeof(UserSharedFields), Name = "Password")]
        public string Password { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        //[Display(ResourceType = typeof(UserSharedFields), Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        //[Display(ResourceType = typeof(UserSharedFields), Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        //[Display(ResourceType = typeof(UserSharedFields), Name = "PasswordConfirm")]
        [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        //[Display(ResourceType = typeof(UserSharedFields), Name = "Email")]
        public string Email { get; set; }
    }
}
