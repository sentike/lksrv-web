﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Areas.Admin.Models
{
    class OnlineStatistic
    {
        public long Online { get; set; }
        public long Queue { get; set; }
        public long Game { get; set; }
    }
}
