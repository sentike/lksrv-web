﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Loka.Areas.Admin.Infrastructures
{
    /// <summary>
    /// Base controller for all Admin area
    /// </summary>
    public abstract class AbstractAdminController : Controller
    {

    }
}