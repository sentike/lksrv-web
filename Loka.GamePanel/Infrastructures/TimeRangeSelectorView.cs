﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;
using Loka.Common.Store;

namespace Loka.Areas.Admin.Infrastructures
{
    public class TimeRangeSelectorView
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public RouteValueDictionary Route { get; set; } = new RouteValueDictionary();

        public RouteValueDictionary WithCustomRoute(KeyValuePair<string, object> pair)
        {
            return WithCustomRoute(new[] {pair});
        }

        public RouteValueDictionary WithCustomRoute(IEnumerable<KeyValuePair<string, object>> pairs)
        {
            var result = new RouteValueDictionary(Route);
            foreach (var pair in pairs)
            {
                result.Add(pair.Key, pair.Value);
            }
            return result;
        }

		public static SelectList CurrencyDropDownList(GameCurrency currency) => new SelectList(Enum.GetValues(typeof(GameCurrency)).Cast<GameCurrency>().ToArray(), currency);
    }
}