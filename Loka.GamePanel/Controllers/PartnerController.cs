﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Loka.Areas.Admin.Infrastructures;

namespace Loka.Areas.Admin.Controllers
{
    public class PartnerController : AbstractAdminController
    {
        // GET: Admin/Partner
        public ActionResult Index()
        {
            return View();
        }

        // GET: Admin/Partner/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Partner/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Partner/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Partner/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/Partner/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Partner/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Partner/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
