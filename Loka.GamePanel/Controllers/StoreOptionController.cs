﻿using Loka.Common.Store;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Loka.Common.Infrastructure;
using Loka.Common.Store.Property;
using Loka.GamePanel.View.Store;
using Loka.Server.Infrastructure.DataRepository;

namespace Loka.GamePanel.Controllers
{
    public class StoreOptionController : Controller
    {
        // GET: StoreOption
        public ActionResult Index(short modelId, CategoryTypeId categoryTypeId)
        {
            ViewBag.Title = "Список параметров";

            using (var db = new DataRepository())
            {
                var item = db.StoreItemInstance.Include(p => p.PropertyEntities)
                    .SingleOrDefault(p => p.ModelId == modelId && p.CategoryTypeId == categoryTypeId);

                if(item == null)
                {
                    return HttpNotFound();
                }



                ViewBag.modelId = modelId;
                ViewBag.modelName = item.ModelIdName;
                ViewBag.categoryTypeId = categoryTypeId;


                return View(item.PropertyEntities.Select(p => new ItemOptionView(p)).ToArray());
            }
        }

        // GET: StoreOption/Create
        public ActionResult Create(short modelId, CategoryTypeId categoryTypeId)
        {
            ViewBag.Title = "Добавление параметра";

            using (var db = new DataRepository())
            {
                var item = db.StoreItemInstance.Include(p => p.PropertyEntities)
                    .SingleOrDefault(p => p.ModelId == modelId && p.CategoryTypeId == categoryTypeId);

                if (item == null)
                {
                    return HttpNotFound();
                }

                return View(new ItemOptionView(item));
            }
        }

        // POST: StoreOption/Create
        [HttpPost]
        public ActionResult Create(short modelId, CategoryTypeId categoryTypeId, ItemOptionView model)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    var item = db.StoreItemInstance.Include(p => p.PropertyEntities)
                        .SingleOrDefault(p => p.ModelId == model.ModelId && p.CategoryTypeId == model.CategoryTypeId);

                    if (item == null)
                    {
                        return HttpNotFound();
                    }

                    if (item.PropertyEntities.Any(p => p.Key == model.Key))
                    {
                        ModelState.AddModelError(nameof(model.Key), "Параметр уже есть");
                        return View(model);
                    }

                    item.PropertyEntities.Add(new ItemInstancePropertyEntity
                    {
                        EntityId = Guid.NewGuid(),
                        CategoryTypeId = model.CategoryTypeId,
                        ModelId = model.ModelId,
                        Key = model.Key,
                        Value = model.Value,
                        Hash = model.Value.Crc64Hash(),
                        EditDate = DateTime.UtcNow,
                        ModelEntity = item,
                        Default = model.Default,
                        Last = model.Value
                    });

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateException e)
                    {
                        ModelState.AddModelError(string.Empty, e);
                        return View(model);
                    }

                    return RedirectToAction("Index", "StoreOption", new {modelId = model.ModelId, categoryTypeId = model.CategoryTypeId});
                }
            }
            catch(Exception e)
            {
                ModelState.AddModelError(string.Empty, e);
                return View(model);
            }
        }

        // GET: StoreOption/Edit/5
        public ActionResult Edit(Guid? id)
        {
            ViewBag.Title = "Редактирование параметра";

            using (var db = new DataRepository())
            {
                if (id == null || id == Guid.Empty)
                {
                    return HttpNotFound();
                }

                var param = db.StoreItemProperties.Include(p => p.ModelEntity).SingleOrDefault(p => p.EntityId == id);
                if (param == null)
                {
                    return HttpNotFound();
                }

                var item = param.ModelEntity;
                if (item == null)
                {
                    return HttpNotFound();
                }

                return View(new ItemOptionView(param));
            }
        }

        // POST: StoreOption/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid? id, ItemOptionView model)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    if (id == null || id == Guid.Empty)
                    {
                        return HttpNotFound();
                    }

                    var param = db.StoreItemProperties.Include(p => p.ModelEntity).SingleOrDefault(p => p.EntityId == id);
                    if (param == null)
                    {
                        return HttpNotFound();
                    }

                    var item = param.ModelEntity;
                    if (item == null)
                    {
                        return HttpNotFound();
                    }

                    if (item.PropertyEntities.Any(p => p.Key == model.Key && p.EntityId != model.EntityId))
                    {
                        ModelState.AddModelError(nameof(model.Key), "Параметр уже есть");
                        return View(model);
                    }


                    param.Last = param.Value;
                    param.Value = model.Value;
                    param.Hash = model.Value.Crc64Hash();
                    param.EditDate = DateTime.UtcNow;
                    param.Default = model.Default;

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateException e)
                    {
                        ModelState.AddModelError(string.Empty, e);
                        return View(model);
                    }

                    return RedirectToAction("Index", "StoreOption", new { modelId = item.ModelId, categoryTypeId = item.CategoryTypeId });
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError(string.Empty, e);
                return View(model);
            }
        }
    }
}
