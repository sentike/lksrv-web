﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Loka.Areas.Admin.Infrastructures;

namespace Loka.Areas.Admin.Controllers
{
    public class SessionController : AbstractAdminController
    {
        // GET: Admin/Session
        public ActionResult Index()
        {
            return View();
        }

        // GET: Admin/Session/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Session/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Session/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Session/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/Session/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Session/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Session/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
