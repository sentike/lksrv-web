﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Loka.Common.Store;
using Loka.Common.Store.Item.Character;
using Loka.GamePanel.View.CharacterProfile;
using Loka.Server.Infrastructure.DataRepository;

namespace Loka.GamePanel.Controllers
{
    [SuppressMessage("ReSharper", "LocalizableElement")]
    public class CharacterProfileController : Controller
    {
        // GET: CharacterProfile
        public ActionResult Index()
        {
            using (var db = new DataRepository())
            {
                var view = db.CharacterProfileTemplates.ToList().AsView();
                return View(view);
            }
        }

        // GET: CharacterProfile/Create
        public ActionResult Create(CharacterModelId? id)
        {
            byte model = 0;
            if (id != null) model = (byte) id.Value;
            return View(new CharacterProfileView
            {
                CharacterModelId = model,
                CharacterCategoryId = CategoryTypeId.Character,
            });
        }

        // POST: CharacterProfile/Create
        [HttpPost]
        public ActionResult Create(CharacterProfileView view)
        {
            using (var db = new DataRepository())
            {
                //=====================================================================================
                if (db.StoreItemInstance.Any(p => p.CategoryTypeId == view.CharacterCategoryId &&
                                                  p.ModelId == view.CharacterModelId) == false)
                {
                    ModelState.AddModelError(nameof(view.CharacterModelId), "Персонаж не найден");
                    return View(view);
                }

                //=====================================================================================
                if (db.StoreItemInstance.Any(p => p.CategoryTypeId == view.ItemCategoryId &&
                                                  p.ModelId == view.ItemModelId) == false)
                {
                    ModelState.AddModelError(nameof(view.ItemModelId), "Предмет не найден");
                    return View(view);
                }

                //=====================================================================================
                if (view.ItemCategoryId == CategoryTypeId.Character)
                {
                    ModelState.AddModelError(nameof(view.ItemCategoryId), "Не может быть персонажем");
                    return View(view);
                }

                //=====================================================================================
                if (view.ItemCategoryId != CategoryTypeId.Weapon 
                    && view.ItemCategoryId != CategoryTypeId.Armour
                    && view.ItemCategoryId != CategoryTypeId.Kit
                    && view.ItemCategoryId != CategoryTypeId.Grenade
                    )
                {
                    ModelState.AddModelError(nameof(view.ItemCategoryId), "Может быть только оружием, броней или спец. средством");
                    return View(view);
                }

                //=====================================================================================
                //if (db.CharacterProfileTemplates.Any(p => p.CharacterModelId == view.CharacterModelId &&
                //                                          p.ItemModelId == view.ItemModelId &&
                //                                          p.ItemCategoryId == view.ItemCategoryId))
                //{
                //    ModelState.AddModelError(nameof(view.ProfileSlot), $"{view.ProfileSlot} уже занят у данного персонажа");
                //    return View(view);
                //}

                db.CharacterProfileTemplates.Add(view.AsEntity());
                db.SaveChanges();
                return RedirectToAction("Index");

            }
        }

        // GET: CharacterProfile/Edit/5
        public ActionResult Edit(int? id)
        {
            using (var db = new DataRepository())
            {
                if (id == null || id <= 0)
                {
                    return HttpNotFound($"{id} was null or not found");
                }

                var entity = db.CharacterProfileTemplates.SingleOrDefault(p => p.TemplateId == id.Value);
                if (entity == null)
                {
                    return HttpNotFound($"item with {id} was null or not found");
                }

                var view = entity.AsView();
                return View(view);
            }
        }

        // POST: CharacterProfile/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CharacterProfileView view)
        {
            using (var db = new DataRepository())
            {
                var entity = db.CharacterProfileTemplates.SingleOrDefault(p => p.TemplateId == id);
                if (entity == null)
                {
                    return HttpNotFound($"item with {id} was null or not found");
                }

                if (db.StoreItemInstance.Any(p => p.CategoryTypeId == view.ItemCategoryId &&
                                                  p.ModelId == view.ItemModelId) == false)
                {
                    ModelState.AddModelError(nameof(view.ItemModelId), "Предмет не найден");
                    return View(view);
                }

                //=====================================================================================
                //if (db.CharacterProfileTemplates.Any(p => p.CharacterModelId == view.CharacterModelId &&
                //                                          p.ItemModelId == view.ItemModelId &&
                //                                          p.ItemCategoryId == view.ItemCategoryId &&
                //                                          p.TemplateId != id))
                //{
                //    ModelState.AddModelError(nameof(view.ProfileSlot), $"{view.ProfileSlot} уже занят у данного персонажа");
                //    return View(view);
                //}

                entity.CharacterModelId = view.CharacterModelId;
                entity.CharacterCategoryId = view.CharacterCategoryId;

                entity.ItemModelId = view.ItemModelId;
                entity.ItemCategoryId = view.ItemCategoryId;

                entity.ProfileSlot = view.ProfileSlot;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        // GET: CharacterProfile/Delete/5
        public ActionResult Delete(int? id)
        {
            using (var db = new DataRepository())
            {
                if (id == null || id <= 0)
                {
                    return HttpNotFound($"{id} was null or not found");
                }

                var entity = db.CharacterProfileTemplates.SingleOrDefault(p => p.TemplateId == id.Value);
                if (entity == null)
                {
                    return HttpNotFound($"item with {id} was null or not found");
                }

                var view = entity.AsView();
                return View(view);
            }
        }

        // POST: CharacterProfile/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, CharacterProfileView view)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    var entity = db.CharacterProfileTemplates.SingleOrDefault(p => p.TemplateId == id);
                    if (entity == null)
                    {
                        return HttpNotFound($"item with {id} was null or not found");
                    }
                    db.CharacterProfileTemplates.Remove(entity);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View(view);
            }
        }
    }
}
