﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Loka.Areas.Admin.Infrastructures;
using Loka.Common.Achievements;
using Loka.Common.Building.Player;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Fraction;
using Loka.Common.Store.Item.Addon;
using Loka.Common.Store.Item.Armour;
using Loka.Common.Store.Item.Character;
using Loka.Common.Store.Item.Kit;
using Loka.Common.Store.Item.Weapon;
using Loka.GamePanel.View.Store;
using Loka.Server.Infrastructure.DataRepository;

namespace Loka.Areas.Admin.Controllers
{
    public class StoreController : AbstractAdminController
    {
        // GET: Admin/Store
        public ActionResult Index()
        {
            ViewBag.Title = "Список товаров";

            using (var db = new DataRepository())
            {
                return View(db.StoreItemInstance.OrderByDescending(i => i.Flags.HasFlag(ItemInstanceFlags.Hidded)).ToList());
            }
        }

        // GET: Admin/Store/Create
        public ActionResult Create(CategoryTypeId? categoryTypeId)
        {
            ViewBag.Title = "Добавление нового товара";
            ViewBag.CategoryTypeId = categoryTypeId;

            return View(new ItemCreateView
            {
                CategoryTypeId = CategoryTypeId.End
            });
        }

        // POST: Admin/Store/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ItemCreateView view)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    if(view.CategoryTypeId == CategoryTypeId.End) return View(view);
                    if (db.StoreItemInstance.Any(i => i.ModelId == view.ModelId && i.CategoryTypeId == view.CategoryTypeId) == false)
                    {
                        if (view.CategoryTypeId == CategoryTypeId.Addon) db.StoreItemInstance.Add(new AbstractItemAddonInstance(view.AsEntity()));
                        else if (view.CategoryTypeId == CategoryTypeId.Material) db.StoreItemInstance.Add(new AbstractItemAddonInstance(view.AsEntity()));
                        else db.StoreItemInstance.Add(view.AsEntity());
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch(Exception e)
            {
                ModelState.AddModelError(nameof(view.ModelId), e);
                return View(view);
            }
            return View(view);
        }

        // GET: Admin/Store/Edit/5
        public ActionResult Edit(short modelId, CategoryTypeId categoryTypeId)
        {
            using (var db = new DataRepository())
            {
                var itemInstance = db.StoreItemInstance.FirstOrDefault(i => i.ModelId == modelId && i.CategoryTypeId == categoryTypeId);
                if (itemInstance == null)
                {
                    return View("NotFoundPage");
                }

                ViewBag.Title = $"Редактирование {itemInstance.ModelIdName} [{itemInstance.CategoryTypeId}]";
                return View(itemInstance.AsView());
            }
        }

        // POST: Admin/Store/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(short modelId, CategoryTypeId categoryTypeId, ItemCreateView collection)
        {
            using (var db = new DataRepository())
            {
                var itemInstance = db.StoreItemInstance.FirstOrDefault(i => i.ModelId == modelId && i.CategoryTypeId == categoryTypeId);
                if (itemInstance == null)
                {
                    return View();
                }

                itemInstance.FractionId = collection.FractionId;
                itemInstance.Cost.Currency = collection.Cost.Currency;
                itemInstance.Cost.Amount = collection.Cost.Amount;

                itemInstance.SubscribeCost.Currency = collection.SubscribeCost.Currency;
                itemInstance.SubscribeCost.Amount = collection.SubscribeCost.Amount;


                itemInstance.Flags = collection.InstanceFlags;
                itemInstance.Level = collection.Level;
                itemInstance.TargetProfileId = collection.TargetProfileId;


                db.SaveChanges();

                return RedirectToAction("Index");
            }
        }

        // GET: Admin/Store/Delete/5
        public ActionResult Delete(short modelId, CategoryTypeId categoryTypeId)
        {
            using (var db = new DataRepository())
            {
                var itemInstance = db.StoreItemInstance.FirstOrDefault(i => i.ModelId == modelId && i.CategoryTypeId == categoryTypeId);
                if (itemInstance == null)
                {
                    ViewBag.Title = $"Удаление {modelId} [{categoryTypeId}]";
                    return View("NotFoundPage");
                }

                ViewBag.Title = $"Удаление {itemInstance.ModelIdName} [{itemInstance.CategoryTypeId}]";
                return View(itemInstance);
            }
        }

        // POST: Admin/Store/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(short modelId, CategoryTypeId categoryTypeId, FormCollection collection)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    var itemInstance = db.StoreItemInstance.FirstOrDefault(i => i.ModelId == modelId && i.CategoryTypeId == categoryTypeId);
                    if (itemInstance == null)
                    {
                        ViewBag.Title = $"Удаление {modelId} [{categoryTypeId}]";
                        return View("NotFoundPage");
                    }
                    db.StoreItemInstance.Remove(itemInstance);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View();
            }
        }
    }
}
