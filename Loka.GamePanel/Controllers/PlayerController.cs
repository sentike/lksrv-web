﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Loka.Areas.Admin.Infrastructures;
using Loka.Areas.Admin.View.PlayerController;
using Loka.Common.Match.GameMode;
using Loka.Common.Player.Instance;
using Loka.Common.Player.Statistic.PvE;
using Loka.Infrastructure;
using Loka.Player;
using Loka.Server.Infrastructure.DataRepository;


namespace Loka.Areas.Admin.Controllers
{
    public class PlayerEntityContainer : IAbstractPlayerView
    {
        public Guid PlayerId { get; set; }
        public string PlayerName { get; set; }
	    public ICollection<PlayerCashEntity> CashEntities { get; set; }
		public PlayerExperience Experience { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public double Difficulty { get; set; }
    }

    public class PlayerController : AbstractAdminController
    {
        // GET: Admin/Player
        public ActionResult Index()
        {
            ViewBag.Title = "Список игроков";

            using (var db = new DataRepository())
            {
                var q = db.AccountPlayerEntity.Select(p => new PlayerEntityContainer
                {
                    PlayerId = p.PlayerId,
                    PlayerName = p.PlayerName,
					CashEntities =  p.CashEntities,
                    Experience = p.Experience,
                    LastActivityDate = p.LastActivityDate,
                    RegistrationDate = p.RegistrationDate,
                }).ToList();
                return View(q);
            }
        }

        public ActionResult Online(long intervalInMinuts = 5)
        {
            ViewBag.Title = "Список игроков в сети ";

            var currentTime = DateTime.UtcNow.AddMinutes(-intervalInMinuts);
            using (var db = new DataRepository())
            {
                var q = db.AccountPlayerEntity.Where(p => p.LastActivityDate >= currentTime).Select(p => new PlayerEntityContainer
                {
                    PlayerId = p.PlayerId,
                    PlayerName = p.PlayerName,
                    CashEntities = p.CashEntities,
                    Experience = p.Experience,
                    LastActivityDate = p.LastActivityDate,
                    RegistrationDate = p.RegistrationDate,
                }).ToList();
                return View(q);
            }
        }

        //public ActionResult Search(long intervalInMinuts = 5, GameModeTypeId gameModeFlag = GameModeTypeId.None)
        //{
        //    ViewBag.Title = "Список игроков в ожидании матча";
        //    ViewBag.IntervalInMinuts = intervalInMinuts;
        //    ViewBag.GameModeFlag = gameModeFlag;
		//
        //    using (var db = new DataRepository())
        //    {
        //        var currentTime = DateTime.UtcNow.AddMinutes(-intervalInMinuts);
        //        var query = db.QueueEntity
        //            .Include(p => p.PlayerEntity)
        //            .Where(q => q.LastActivityDate >= currentTime && (q.GameModeTypeId == gameModeFlag || q.GameModeTypeId.HasFlag(gameModeFlag)))
        //            .Select(p => new PlayerSearchView
        //        {
        //            PlayerId = p.PlayerEntity.PlayerId,
        //            PlayerName = p.PlayerEntity.PlayerName,
        //            Experience = p.PlayerEntity.Experience,
        //            RegistrationDate = p.PlayerEntity.RegistrationDate,
        //            GameModeTypeId = p.GameModeTypeId,
        //            RegionTypeId = p.RegionTypeId,
        //            JoinDate = p.JoinDate,
        //            Level = p.Level,
        //            Ready = p.Ready
        //        }).ToList();
        //        return View(query);
        //    }
        //}

        // GET: Admin/Player/Details/5
        public ActionResult Details(Guid id)
        {
            using (var db = new DataRepository())
            {
                var player = db.AccountPlayerEntity
                    .Include(p => p.InventoryItemList)
                    .Include(p => p.InventoryProfileList)
                    .Include(p => p.InventoryItemList.Select(i => i.ItemModelEntity))
                    .Include(p => p.InventoryProfileList.Select(i => i.Items))
                    .Include(p => p.AchievementList.Select(i => i.AchievementEntity))
                    .Include(p => p.AchievementList.Select(i => i.AchievementEntity.Achievement))
                    .Include(p => p.QueueEntity)
                    .Include(p => p.FriendList)
                    .Include(p => p.FriendList.Select(f => f.FriendEntity))
                    .Include(p => p.FriendList.Select(f => f.FriendEntity.QueueEntity))
                    .Include(p => p.MatchMembers)
                    .Include(p => p.MatchMembers.Select(m => m.MatchEntity))
                    .Include(p => p.MatchMembers.Select(m => m.MatchEntity.MemberList))
                    .Include(p => p.CurrentFractionEntity)
                    .Include(p => p.PvEMatchStatisticEntities)
                    //.Include(p => p.User)
                    //.Include(p => p.User.Logins)
                    //.Include(p => p.User.Claims)
                    .SingleOrDefault(p => p.PlayerId == id);

                if (player?.QueueEntity != null)
                {
                    db.Entry(player.QueueEntity).Reference(p => p.PartyEntity).Load();
                    if (player?.QueueEntity?.PartyEntity != null)
                    {
                        db.Entry(player.QueueEntity.PartyEntity).Collection(p => p.Members).Load();
                    }
                }

                return View(player);
            }
        }

        // GET: Admin/Player/Create
        public ActionResult CreatePvEStatitic(Guid? id)
        {
            using (var db = new DataRepository())
            {
                if (db.AccountPlayerEntity.Any(p => p.PlayerId == id) == false)
                {
                    // Todo error
                }
                return View();
            }
        }

        // POST: Admin/Player/Create
        [HttpPost]
        public ActionResult CreatePvEStatitic(PlayerPvEMatchStatisticView collection)
        {
            using (var db = new DataRepository())
            {
                var player = db.AccountPlayerEntity.SingleOrDefault(p => p.PlayerId == collection.PlayerId);
                if (player == null)
                {
                    ModelState.AddModelError(nameof(collection.PlayerId), "Player Not Found");
                    return View();
                }
                else
                {
                    if (player.PvEMatchStatisticEntities.Any(p => p.Level == collection.Level && p.GameModeTypeId == collection.GameModeTypeId))
                    {
                        ModelState.AddModelError(nameof(collection.Level), "PvE Statistic witch selected GameMode & Level was avalible");
                        ModelState.AddModelError(nameof(collection.GameModeTypeId), "PvE Statistic witch selected GameMode & Level was avalible");
                        return View();
                    }
                    else
                    {
                        player.PvEMatchStatisticEntities.Add(PlayerPvEMatchStatisticEntity.Factory(collection));
                        db.SaveChanges();
                        return RedirectToAction("Details", new { id = player.PlayerId.ToString() });
                    }
                }
            }
        }

        // GET: Admin/Player/Edit/5
        public ActionResult EditPvEStatitic(Guid id)
        {
            using (var db = new DataRepository())
            {
                var statistic = db.PvEMatchStatisticEntities.FirstOrDefault(p => p.StatisticEntityId == id);
                if (statistic== null)
                {
                    return View("NotFoundPage");
                }
                return View(new PlayerPvEMatchStatisticView(statistic));
            }
        }

        // POST: Admin/Player/Edit/5
        [HttpPost]
        public ActionResult EditPvEStatitic(Guid id, PlayerPvEMatchStatisticView collection)
        {
            using (var db = new DataRepository())
            {
                var player = db.AccountPlayerEntity.SingleOrDefault(p => p.PlayerId == collection.PlayerId);
                if (player == null)
                {
                    ModelState.AddModelError(nameof(collection.PlayerId), "Player Not Found");
                    return View();
                }
                else
                {
                    var statistic = player.PvEMatchStatisticEntities.SingleOrDefault(p => p.Level == collection.Level && p.GameModeTypeId == collection.GameModeTypeId);
                    if (statistic == null)
                    {
                        ModelState.AddModelError(nameof(collection.PlayerId), "Instance Not Found");
                        return View();
                    }
                    else
                    {
                        statistic.Score = collection.Score;
                        db.SaveChanges();
                        return RedirectToAction("Details", new { id = player.PlayerId.ToString() });
                    }
                }
            }
        }

        // GET: Admin/Player/Delete/5
        public ActionResult Delete(Guid id)
        {
            return View();
        }

        // POST: Admin/Player/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
