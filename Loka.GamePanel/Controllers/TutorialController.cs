﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Loka.Areas.Admin.Infrastructures;
using Loka.Common.Store;
using Loka.Common.Tutorial;
using Loka.GamePanel.View.Tutorial;
using Loka.Server.Infrastructure.DataRepository;

namespace Loka.GamePanel.Controllers
{
    public class TutorialController : AbstractAdminController
    {
        // GET: Tutorial
        public ActionResult Index()
        {
            using (var db = new DataRepository())
            {
                return View(db.TutorialInstanceEntities.ToArray().Select(p => p.AsCreateView()).ToList());
            }
        }

        #region Tutorial


        public ActionResult Create()
        {
            return View();
        }

        // POST: Tutorial/Create
        [HttpPost]
        public ActionResult Create(TutorialCreateView view)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    db.TutorialInstanceEntities.Add(new TutorialInstanceEntity()
                    {
                        TutorialName = view.TutorialName,
                        AdminComment = view.AdminComment,
                        CreatedDate = DateTime.UtcNow,
                    });

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View(view);
            }
        }

        // GET: Tutorial/Edit/5
        public ActionResult Edit(long id)
        {
            using (var db = new DataRepository())
            {
                var entity = db.TutorialInstanceEntities.SingleOrDefault(p => p.TutorialId == id);
                var view = entity.AsCreateView();
                return View(view);
            }
        }

        // POST: Tutorial/Edit/5
        [HttpPost]
        public ActionResult Edit(long id, TutorialCreateView view)
        {
            using (var db = new DataRepository())
            {
                var entity = db.TutorialInstanceEntities.SingleOrDefault(p => p.TutorialId == id);
                if (entity == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound, "Tutorial not found!");
                }

                entity.TutorialName = view.TutorialName;
                entity.AdminComment = view.AdminComment;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        public ActionResult Delete(long id)
        {
            using (var db = new DataRepository())
            {
                var entity = db.TutorialInstanceEntities.SingleOrDefault(p => p.TutorialId == id);
                var view = entity.AsCreateView();
                return View(view);
            }
        }

        // POST: Tutorial/Delete/5
        [HttpPost]
        public ActionResult Delete(long id, TutorialCreateView view)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    var entity = db.TutorialInstanceEntities.SingleOrDefault(p => p.TutorialId == id);
                    if (entity == null)
                    {
                        ModelState.AddModelError(nameof(view.TutorialId), "Not found!");
                        return View(view);
                    }

                    //var requeries = db.TutorialInstanceEntities.Where(p => p.NextTutorialId == id).Select(p => new { p.TutorialId, p.TutorialName })
                    //    .ToArray().Select(p => $"[Ид: {p.TutorialId}] {p.TutorialName}").ToArray();
                    //if (requeries.Any())
                    //{
                    //    ModelState.AddModelError(nameof(view.NextTutorialId), "От этого этапа обучения зависят другие этапы: " + string.Join(",", requeries));
                    //    return View(view);
                    //}

                    db.TutorialInstanceEntities.Remove(entity);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError(String.Empty, e);
                return View(view);
            }
        }


        #endregion

        public List<SelectListItem> GetTutorialSelectList(DataRepository db, long? id)
        {
            return db.TutorialInstanceEntities.Select(p => new {p.TutorialId, p.TutorialName})
                .ToArray()
                .Select(p => new SelectListItem
                {
                    Value = p.TutorialId.ToString(),
                    Text = p.TutorialName,
                    Selected = p.TutorialId == id
                })
                .ToList();
        }

        // GET: Tutorial/Create
        public ActionResult CreateStep(long? id)
        {
            using (var db = new DataRepository())
            {
                return View(new TutorialCreateStepView
                {
                    TutorialId = id ?? 0,
                    TutorialSelectListItems = GetTutorialSelectList(db, id)
                });
            }
        }

        // POST: Tutorial/Create
        [HttpPost]
        public ActionResult CreateStep(TutorialCreateStepView view)
        {
            using (var db = new DataRepository())
            {
                try
                {
                    if (view.RewardCategoryId == null || view.RewardModelId == null || view.RewardCategoryId >= CategoryTypeId.End || view.RewardCategoryId < 0)
                    {
                        view.RewardCategoryId = null;
                        view.RewardModelId = null;
                    }
                    else
                    {
                        if (db.StoreItemInstance.Any(p => p.ModelId == view.RewardModelId &&
                                                          p.CategoryTypeId == view.RewardCategoryId) == false)
                        {
                            ModelState.AddModelError(nameof(view.RewardModelId), "Reward item not found!");
                            ModelState.AddModelError(nameof(view.RewardCategoryId), "Reward item not found!");
                            view.TutorialSelectListItems = GetTutorialSelectList(db, view.TutorialId);
                            return View(view);
                        }
                    }

                    db.TutorialStepEntities.Add(new TutorialStepEntity()
                    {
                        CreatedDate = DateTime.UtcNow,
                        TutorialId = view.TutorialId,
                        RewardCategoryId = view.RewardCategoryId,
                        RewardModelId = view.RewardModelId,
                        RewardActivate = view.RewardActivate,
                        RewardGive = view.RewardGive,
                        AdminComment = view.AdminComment,
                        StepId = view.StepId
                });

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch(Exception e)
                {
                    ModelState.AddModelError(String.Empty, e);
                    view.TutorialSelectListItems = GetTutorialSelectList(db, view.TutorialId);
                    return View(view);
                }
            }
        }

        

        // GET: Tutorial/Edit/5
        [HttpGet]
        public ActionResult EditStep(long id)
        {
            using (var db = new DataRepository())
            {
                var entity = db.TutorialStepEntities.SingleOrDefault(p => p.StepKey == id);
                var view = entity.AsCreateView();
                view.TutorialSelectListItems = GetTutorialSelectList(db, view.TutorialId);
                return View(view);
            }
        }
        
        // POST: Tutorial/Edit/5
        [HttpPost]
        public ActionResult EditStep(long id, TutorialCreateStepView view)
        {
            using (var db = new DataRepository())
            {
                var entity = db.TutorialStepEntities.SingleOrDefault(p => p.StepKey == id);
                if (entity == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound, "Tutorial not found!");
                }

                if (view.RewardCategoryId == null || view.RewardModelId == null || view.RewardCategoryId >= CategoryTypeId.End || view.RewardCategoryId < 0)
                {
                    view.RewardCategoryId = null;
                    view.RewardModelId = null;
                }
                else
                {
                    if (db.StoreItemInstance.Any(p => p.ModelId == view.RewardModelId &&
                                                      p.CategoryTypeId == view.RewardCategoryId) == false)
                    {
                        ModelState.AddModelError(nameof(view.RewardModelId), "Reward item not found!");
                        ModelState.AddModelError(nameof(view.RewardCategoryId), "Reward item not found!");
                        view.TutorialSelectListItems = GetTutorialSelectList(db, view.TutorialId);
                        return View(view);
                    }
                }

                entity.RewardCategoryId = view.RewardCategoryId;
                entity.RewardModelId = view.RewardModelId;
                entity.AdminComment = view.AdminComment;
                entity.TutorialId = view.TutorialId;
                entity.StepId = view.StepId;
                entity.RewardGive = view.RewardGive;
                entity.RewardActivate = view.RewardActivate;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }


       

        // GET: Tutorial/Delete/5
        public ActionResult DeleteStep(long id)
        {
            using (var db = new DataRepository())
            {
                var entity = db.TutorialStepEntities.SingleOrDefault(p => p.StepId == id);
                var view = entity.AsView();
                return View(view);
            }
        }
        
        // POST: Tutorial/Delete/5
        [HttpPost]
        public ActionResult DeleteStep(long id, TutorialCreateStepView view)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    var entity = db.TutorialStepEntities.SingleOrDefault(p => p.StepId == id);
                    if (entity == null)
                    {
                        ModelState.AddModelError(nameof(view.StepId), "Not found!");
                        return View(view);
                    }
        
                    //var requeries = db.TutorialInstanceEntities.Where(p => p.NextTutorialId == id).Select(p => new { p.TutorialId, p.TutorialName })
                    //    .ToArray().Select(p => $"[Ид: {p.TutorialId}] {p.TutorialName}").ToArray();
                    //if (requeries.Any())
                    //{
                    //    ModelState.AddModelError(nameof(view.NextTutorialId), "От этого этапа обучения зависят другие этапы: " + string.Join(",", requeries));
                    //    return View(view);
                    //}
        
                    db.TutorialStepEntities.Remove(entity);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError(String.Empty, e);
                return View(view);
            }
        }
    }
}
