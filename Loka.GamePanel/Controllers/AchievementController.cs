﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Loka.Areas.Admin.Infrastructures;
using Loka.Common.Achievements;
using Loka.Common.Achievements.Instance;
using Loka.Server.Infrastructure.DataRepository;

namespace Loka.Areas.Admin.Controllers
{
    public class AchievementController : AbstractAdminController
    {
        // GET: Admin/Achievement
        public ActionResult Index()
        {
            using (var db = new DataRepository())
            {
                return View(db.Achievements.OrderByDescending(a => a.AchievementTypeId).Include(a => a.Achievement).ToArray());
            }
        }

        // GET: Admin/Achievement/Details/5
        public ActionResult Details(AchievementTypeId id)
        {
            using (var db = new DataRepository())
            {
                var instance = db.Achievements.Include(a => a.Achievement).FirstOrDefault(a => a.AchievementTypeId == id);
                return instance == null ? View("NotFoundPage") : View(instance);
            }
        }

        // GET: Admin/Achievement/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Achievement/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Achievement/Edit/5
        public ActionResult Edit(int id)
        {
            using (var db = new DataRepository())
            {
                var instance = db.AchievementInstances.FirstOrDefault(a => a.AchievementBonusId == id);
                return instance == null ? View("NotFoundPage") : View(instance);
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, AchievementInstanceEntity collection)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    var instance = db.AchievementInstances.FirstOrDefault(a => a.AchievementBonusId == id);
                    if (instance == null)
                    {
                        return View();
                    }

                    instance.BonusPackId = collection.BonusPackId;
                    instance.Amount = collection.Amount;

                    db.SaveChanges();
                    return View(instance);
                }
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Admin/Achievement/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Achievement/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
