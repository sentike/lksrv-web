﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Loka.Areas.Admin.Infrastructures;
using Loka.Common.Match.GameMode;
using Loka.Server.Infrastructure.DataRepository;

namespace Loka.Areas.Admin.Controllers
{
    public class GameController : AbstractAdminController
    {
        // GET: Admin/Game
        //public ActionResult Index(long intervalInMinuts = 0, GameModeTypeId gameModeFlag = GameModeTypeId.None)
        //{
        //    ViewBag.IntervalInMinuts = intervalInMinuts;
        //    ViewBag.GameModeFlag = gameModeFlag;
		//
        //    using (var db = new DataRepository())
        //    {
        //        if (intervalInMinuts < 5)
        //        {
        //            ViewBag.Title = $"Список матчей за все время, режим: {gameModeFlag}";
        //            return View("Index", db.MatchEntity.Where(m => m.GameModeTypeId.HasFlag(gameModeFlag)).OrderByDescending(m => m.Created)/*.Take(500)*/.Include(m => m.MemberList).ToArray());
        //        }
        //        else
        //        {
        //            ViewBag.Title = $"Список матчей за {intervalInMinuts} минут, режим: {gameModeFlag}";
        //            var currentTime = DateTime.UtcNow.AddMinutes(-intervalInMinuts);
        //            return View("Index", db.MatchEntity.Where(m => m.Created >= currentTime && m.GameModeTypeId.HasFlag(gameModeFlag)).OrderByDescending(m => m.Created)/*.Take(500)*/.Include(m => m.MemberList).ToArray());
        //        }
        //    }
        //}

        //public ActionResult Online(long intervalInMinuts = 5, GameModeTypeId gameModeFlag = GameModeTypeId.None)
        //{
        //    return Index(intervalInMinuts, gameModeFlag);
        //}

        // GET: Admin/Game/Details/5
        public ActionResult Details(Guid matchId)
        {
            using (var db = new DataRepository())
            {
                var itemInstance = db.MatchEntity.Include(m => m.TeamList)
                    .Include(m => m.MemberList)
                    .Include(m => m.MemberList.Select(p=> p.PlayerEntity))
                    .FirstOrDefault(i => i.MatchId == matchId);

                if (itemInstance == null)
                {
                    return View("NotFoundPage");
                }

                return View(itemInstance);
            }
        }

        public ActionResult PlayerDetails(Guid matchid)
        {
            throw new NotImplementedException();
        }

        // GET: Admin/Game/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Game/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Game/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/Game/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Game/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Game/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
