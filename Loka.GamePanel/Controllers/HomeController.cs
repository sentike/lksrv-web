﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Loka.Areas.Admin.Infrastructures;
using Loka.Areas.Admin.Models;
using Loka.Server.Infrastructure.DataRepository;

namespace Loka.Areas.Admin.Controllers
{
    public class HomeController : AbstractAdminController
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            using (var db = new DataRepository())
            {
                var lastDayTime = DateTime.UtcNow.AddHours(-12);
                var seconds = DateTime.UtcNow.AddSeconds(-30);

                return View(new HomeStatistic
                {
                    Players = db.AccountPlayerEntity.LongCount(),
                    NewPlayers = db.AccountPlayerEntity.LongCount(m => m.RegistrationDate >= lastDayTime),
                    OnlinePlayers = db.GameSessionEntity.LongCount(m => m.LastActivityDate >= seconds),
                });
            }
        }
    }
}