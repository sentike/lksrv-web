﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Loka.GamePanel.Startup))]
namespace Loka.GamePanel
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
