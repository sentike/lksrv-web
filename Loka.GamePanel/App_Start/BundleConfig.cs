﻿using System.Web;
using System.Web.Optimization;

namespace Loka.GamePanel
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/slide").Include(
                "~/Content/Scripts/simple-slide.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                   "~/Content/bootstrap.css",
                   "~/Content/site.css"));


            bundles.Add(new ScriptBundle("~/bundles/Admin/Flot/Scripts").Include(
                "~/Content/Admin/vendors/Flot/jquery.flot.js",
                "~/Content/Admin/vendors/Flot/jquery.flot.pie.js",
                "~/Content/Admin/vendors/Flot/jquery.flot.time.js",
                "~/Content/Admin/vendors/Flot/jquery.flot.stack.js",
                "~/Content/Admin/vendors/Flot/jquery.flot.resize.js",
                "~/Content/Admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js",
                "~/Content/Admin/vendors/flot-spline/js/jquery.flot.spline.min.js",
                "~/Content/Admin/vendors/flot.curvedlines/curvedLines.js"
                ));

            //========================================================================================================





            bundles.Add(new ScriptBundle("~/bundles/Admin/DataBase/Scripts").Include(
                 "~/Content/Admin/vendors/datatables.net/js/jquery.dataTables.min.js",
                 "~/Content/Admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js",
                 "~/Content/Admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js",
                 "~/Content/Admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js",
                 "~/Content/Admin/vendors/datatables.net-buttons/js/buttons.flash.min.js",
                 "~/Content/Admin/vendors/datatables.net-buttons/js/buttons.html5.min.js",
                 "~/Content/Admin/vendors/datatables.net-buttons/js/buttons.print.min.js",
                 "~/Content/Admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js",
                 "~/Content/Admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js",
                 "~/Content/Admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js",
                 "~/Content/Admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js",
                 "~/Content/Admin/vendors/datatables.net-scroller/js/datatables.scroller.min.js",
                 "~/Content/Admin/vendors/jszip/dist/jszip.min.js",
                 "~/Content/Admin/vendors/pdfmake/build/pdfmake.min.js",
                 "~/Content/Admin/vendors/pdfmake/build/vfs_fonts.js"
             ));

            bundles.Add(new StyleBundle("~/bundles/Admin/DataBase/Styles").Include(
            "~/Content/Admin/vendors/datatables.net-bs/css/dataTables.bootstrap.css",
            "~/Content/Admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.css",
            "~/Content/Admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.css",
            "~/Content/Admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.css",
            "~/Content/Admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.css"));
        }
    }
}
