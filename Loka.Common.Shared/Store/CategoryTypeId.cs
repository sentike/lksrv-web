﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store
{
    public enum CategoryTypeId
    {
        Weapon,
        Armour,
        Ammo,
        Service,
        Addon,
        Character,
        Profile,

        Material,

        Kit,

        Grenade,
        Building,

        Currency,

        Vehicles,
        Worlds,

        End
    }
}
 