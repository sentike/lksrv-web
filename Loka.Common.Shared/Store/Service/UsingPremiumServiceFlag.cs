﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store.Service
{

    [Flags]
    public enum UsingPremiumServiceFlag
    {
        None = 0,
        PremiumAccount = 1,
        BoosterOfReputation = 2,
        BoosterOfExperience = 4,
        BoosterOfMoney = 8
    }
}
