﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Store.Service
{
    public enum ServiceTypeId
    {
        None,
        PremiumAccount,
        BoosterOfReputation,
        BoosterOfExperience,
        BoosterOfMoney
    }
}
