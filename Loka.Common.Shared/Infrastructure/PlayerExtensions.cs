﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Infrastructure
{
    public static class PlayerExtensions
    {
        public static bool IsEmailAddress(this string str)
        {
            return str.Contains('@') && str.Contains('.');
        }
    }
}
