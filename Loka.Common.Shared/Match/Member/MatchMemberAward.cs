using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Common.Store.Fraction;

namespace Loka.Common.Match.Member
{
    [ComplexType]
    public class MatchMemberAward
    {
        public long Money { get; set; }
        public long Donate { get; set; }
	    public long Bitcoin { get; set; }

		public int Experience { get; set; }

        public int Reputation { get; set; }
        public FractionTypeId FractionId { get; set; }
    }
}