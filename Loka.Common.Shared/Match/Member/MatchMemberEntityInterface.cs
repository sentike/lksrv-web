﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Match.Member
{
    public interface IMatchMemberEntityInterface
    {
        Guid MemberId { get; set; }
        Guid MatchId { get; set; }
        Guid PlayerId { get; set; }
        byte Level { get; set; }
        MatchMemberAward Award { get; set; }
        MatchMemberStatisticContainer Statistic { get; set; }
        double Difficulty { get; set; }
    }
}
