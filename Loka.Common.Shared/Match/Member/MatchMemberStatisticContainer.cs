﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Store.Service;

namespace Loka.Common.Match.Member
{
    [ComplexType]
    public class MatchMemberStatisticContainer
    {
        public UsingPremiumServiceFlag Service { get; set; }

        [DisplayName("Очки")]
        public long Score { get; set; }

        [DisplayName("Убийства")]
        public long Kills { get; set; }

        [DisplayName("Смерти")]
        public long Deads { get; set; }

        [DisplayName("К/Д")]
        [DisplayFormat(DataFormatString = "{0:F}", ApplyFormatInEditMode = true)]
        public double KillDeadAvg => Math.Max(Kills, 1.0) / Math.Max(Deads, 1.0);

        [DisplayName("Серии")]
        public long SeriesKills => SmallSeriesKill + ShortSeriesKill + LargeSeriesKill + EpicSeriesKill;

        /// <summary>
        /// Series of 3 Kill
        /// </summary>
        [Display(Name = "Серия из 3 убийств")]
        public long SmallSeriesKill { get; set; }

        /// <summary>
        /// Series of 5 Kill
        /// </summary>
        [Display(Name = "Серия из 5 убийств")]
        public long ShortSeriesKill { get; set; }

        /// <summary>
        /// Series of 7 Kill
        /// </summary>
        [Display(Name = "Серия из 7 убийств")]
        public long LargeSeriesKill { get; set; }

        /// <summary>
        /// Series more 7 Kill
        /// </summary>
        [Display(Name = "Серия из больше 7 убийств")]
        public long EpicSeriesKill { get; set; }

        [DisplayName("Поддержка")]
        public long Assists { get; set; }
        public long TeamKills { get; set; }
        public long Shots { get; set; }
        public long Hits { get; set; }
    }
}
