﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Common.Match.GameMap
{
    public enum GameMapTypeId
    {
        None,
        PvP,
        Outpost,
        Hangar,
        MilitaryBase,
        Tropic,
        DesertTown,
        Lobby,
        End

    };

    public enum GameMapFlags
    {
        None = 0,
        PvP = 1 << GameMapTypeId.PvP,
        Outpost = 1 << GameMapTypeId.Outpost,
        Hangar= 1 << GameMapTypeId.Hangar,
        MilitaryBase= 1 << GameMapTypeId.MilitaryBase,
        Tropic = 1 << GameMapTypeId.Tropic,
        DesertTown= 1 << GameMapTypeId.DesertTown,
        Lobby = 1 << GameMapTypeId.Lobby,
        End = 1 << GameMapTypeId.End,
    }
}