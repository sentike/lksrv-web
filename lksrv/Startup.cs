﻿using System;
using System.Web.Http;
using CronExpressionDescriptor;
using Loka.Common.Improvements;
using Loka.Server.Infrastructure.DataRepository;


using Microsoft.Owin;
using Owin;
using VRS.Infrastructure;
using WebApi.OutputCache.Core.Cache;
using WebApi.OutputCache.V2;
using GlobalConfiguration = System.Web.Http.GlobalConfiguration;


[assembly: OwinStartup(typeof(Loka.Server.Startup))]
namespace Loka.Server
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "{controller}/{action}/{id}",
               defaults: new { id = RouteParameter.Optional }
           );

            config.EnableCors();
            config.CacheOutputConfiguration().RegisterCacheOutputProvider(() => new MemoryCacheDefault());
        }



    }
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            NinjectWebCommon.Start();
            HttpConfiguration config = new HttpConfiguration();

            GlobalConfiguration.Configure(WebApiConfig.Register);

            NinjectWebCommon.RegisterNinject(GlobalConfiguration.Configuration);

            app.CreatePerOwinContext(DataRepository.Create);

            //app.UseWebApi(config);
        }

    }
}
