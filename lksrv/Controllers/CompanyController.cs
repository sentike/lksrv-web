﻿using System;
using System.Web.Http;
using Loka.Infrastructure;
using Ninject;
using System.Linq;
using System.Web;
using Loka.Common.Player.Company;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Player.Infrastructure;
using Loka.Server.Player.Models;

namespace Loka.Server.Controllers
{
    public class CompanyController : AbstractApiController
    {
        [Authorize, HttpPost]
        public IHttpActionResult StartMission(FReqestOneParam<long> missionId)
        {
            var playerMission = PlayerAccountEntity.MissionEntities.SingleOrDefault(e => e.MissionTaskId == missionId);
            if (playerMission == null)
            {
                var mission = User.Db.ScenarioTaskEntities.SingleOrDefault(e => e.TaskId == missionId);
                if (mission == null)
                {
                    return Conflict();
                }
                else
                {
                    try
                    {
                        PlayerAccountEntity.MissionEntities.Add(new PlayerMissionEntity
                        {
                            PlayerMissionId = Guid.NewGuid(),
                            StartDate = DateTime.UtcNow,
                            MissionTaskId = missionId,
                        });
                        User.Db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e);
                        return BadRequest();
                    }
                    return Json(missionId);
                }
            }
            else if (playerMission.MissionState == PlayerMissionState.Idle)
            {
                playerMission.MissionState = PlayerMissionState.Process;
                return Json(missionId);
            }
            return BadRequest();
        }

        [Authorize, HttpPost]
        public IHttpActionResult CancelMission(FReqestOneParam<long> missionId)
        {
            var playerMission = PlayerAccountEntity.MissionEntities.SingleOrDefault(e => e.MissionTaskId == missionId);
            if (playerMission == null)
            {
                return BadRequest();
            }
            else if (playerMission.MissionState == PlayerMissionState.Process)
            {
                playerMission.MissionState = PlayerMissionState.Idle;
                return Ok();
            }
            return Conflict();
        }
    }
}