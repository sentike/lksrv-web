﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Player.Statistic;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Player.Infrastructure;
using Loka.Server.Player.Models;



using Loka.Infrastructure;
using Loka.Server.Controllers.IdentityController.Operation;
using Loka.Server.Infrastructure;
using Loka.Common.Player.Inventory;
using Loka.Common.Store.Item.Addon;
using Loka.Common.Store.Item.Skin;
using Ninject;

namespace Loka.Server.Controllers.HangarController
{
    public class PreSetEquipRequest
    {
        public Guid ItemId;

        public CategoryTypeId CategoryId;

        public PlayerProfileInstanceSlotId SetSlotId;

        public Guid PreSetId;

        public bool IsEquip;
    }

    public class PreSetEquipResponse : PreSetEquipRequest
    {
        public long Count { get; private set; }
        public PreSetEquipResponse(PreSetEquipRequest request, long count)
        {
            Count = count;
            ItemId = request.ItemId;
            CategoryId = request.CategoryId;
            SetSlotId = request.SetSlotId;
            PreSetId = request.PreSetId;
            IsEquip = request.IsEquip;
        }
    }



    public struct ItemSellRequest
    {
        public Guid ItemId;

        public CategoryTypeId CategoryId;
    }

    public struct ChangeSetPropertyRequest
    {
        public Guid PreSetId;
        public bool IsEnabled;
        public string Name;
    }

    public struct RequestAllowedModifications
    {
        public Guid ItemId;
        public CategoryTypeId CategoryId;
        public bool IsDonate { get; set; }
    }

    public struct ConfirmSelectModification
    {
        public Guid ItemId;
        public CategoryTypeId CategoryId;
        public byte ModificationId;
    }

    public class HangarController : AbstractApiController
    {
        [Authorize, HttpPost]
        public IHttpActionResult ChangeSetProperty(ChangeSetPropertyRequest request)
        {
            //var preSetInstance = PlayerInstance.InventoryProfileList.SingleOrDefault(s => s.AssetId == request.PreSetId);
            //if (preSetInstance == null)
            //{
            //    return BadRequest();
            //}
            //
            //
            //preSetInstance.Name = request.Name;
            //preSetInstance.IsEnabled = request.IsEnabled;
            return Json(request);
        }

        [Authorize, HttpPost]
        public IHttpActionResult ItemEquip(PreSetEquipRequest request)
        {

            var preSetInstance = PlayerAccountEntity.InventoryProfileList.SingleOrDefault(s => s.AssetId == request.PreSetId);
            if (preSetInstance == null)
            {
                return BadRequest();
            }


            if (request.CategoryId == CategoryTypeId.Character)
            {
                if (request.IsEquip && PlayerAccountEntity.CharacterList.Any(i => i.ItemId == request.ItemId))
                {
                    preSetInstance.CharacterId = request.ItemId;
                }
            }
            else if (request.CategoryId == CategoryTypeId.Service)
            {
                var service = PlayerAccountEntity.ServiceList.FirstOrDefault(i => i.ItemId == request.ItemId);
                if (service != null)
                {
                    service.Activate(1);
                    return Json(new PreSetEquipResponse(request, service.Amount));
                }
            }
            else
            {
                var item = PlayerAccountEntity.InventoryItemList.SingleOrDefault(i => i.ItemId == request.ItemId && i.CategoryTypeId == request.CategoryId);
                if (request.IsEquip && item != null)
                {
                    User.Db.PlayerProfileItem.AddOrUpdate(preSetInstance.Equip(item, request.SetSlotId));
                }
                else
                {
                    var slotEntity = preSetInstance.Equip(null, request.SetSlotId);
                    if (slotEntity != null)
                    {
                        User.Db.PlayerProfileItem.Remove(slotEntity);
                    }
                }
            }
            return Json(new PreSetEquipResponse(request, 1));
        }


        [Authorize, HttpPost]
        public IHttpActionResult ItemUpgrade(RequestAllowedModifications request)
        {
            var itemInstance = PlayerAccountEntity.InventoryItemList.SingleOrDefault(s => s.ItemId == request.ItemId && s.CategoryTypeId == request.CategoryId);
            if (itemInstance == null)
            {
                return BadRequest();
            }
            else
            {
                var cost = new StoreItemCost
                {
                    Currency = request.IsDonate ? GameCurrency.Donate : GameCurrency.Money,
                    Amount = request.IsDonate ? 10 : 5000
                };

                if (PlayerAccountEntity.Pay(cost))
                {
                    PlayerSessionItemModification modification;
                    if (itemInstance.RandomizeModificationList(request.IsDonate, out modification))
                    {
                        User.PlayerGameSessionEntity.Modification = modification;
                        //PlayerAccountEntity.TransactionEntities.Add(PaymentTransactionEntity.Factory(itemInstance.ItemModelEntity, TransactionPaymentType.Upgrade, cost));
                        return Json(modification.Modifications);
                    }
                    return Conflict();
                }
                return new StatusCodeResult(HttpStatusCode.PaymentRequired, this);
            }
        }

        //=========================================================================================================================

        [Authorize, HttpPost]
        public IHttpActionResult ConfirmItemUpgrade(ConfirmSelectModification request)
        {
            var itemInstance = PlayerAccountEntity.InventoryItemList.SingleOrDefault(s => s.ItemId == request.ItemId && s.CategoryTypeId == request.CategoryId);
            if (itemInstance == null)
            {
                return BadRequest();
            }
            else
            {
                if (User.PlayerGameSessionEntity.Modification.ItemId == itemInstance.ItemId)
                {
                    if (itemInstance.ApplyModification(request.ModificationId, User.PlayerGameSessionEntity.Modification))
                    {
                        return Json(new RequestLootData.FInvertoryItemWrapper(itemInstance));
                    }
                }
            }
            return Conflict();
        }



        public class EquipAddonRequest
        {
            public Guid ItemId;
            public byte AddonModelId;
            public EquipAddonSlot AddonSlot;
            public CategoryTypeId CategoryTypeId;

            public override string ToString()
            {
                return $"Addon {ItemId} / {AddonModelId} [{CategoryTypeId}] / slot: {AddonSlot}";
            }
        }

        public class EquipAddonResponse : EquipAddonRequest
        {
            public Guid AddonItemId;

            public EquipAddonResponse(EquipAddonRequest property, Guid addonId)
            {
                ItemId = property.ItemId;
                AddonModelId = property.AddonModelId;
                AddonSlot = property.AddonSlot;
                AddonItemId = addonId;
            }
        }
        

        [Authorize, HttpPost]
        public IHttpActionResult AddonEquip(EquipAddonRequest request)
        {
            StoreItemCost cost = null;
            try
            {
                var itemInstance = PlayerAccountEntity.InventoryItemList.Single(i => i.ItemId == request.ItemId);
                if (itemInstance == null)
                {
                    Logger.Info($"AddonEquip: {request} not found");
                    return Conflict();
                }
                else
                {
                    Logger.Info($"AddonEquip: {itemInstance} found");
                }

                request.CategoryTypeId = request.AddonSlot == EquipAddonSlot.Skin ? CategoryTypeId.Material : CategoryTypeId.Addon;
                var addonEntity = itemInstance.AvalibleAddonList.FirstOrDefault(s => s.ModelId == request.AddonModelId && s.CategoryTypeId == request.CategoryTypeId);
                if (addonEntity == null)
                {
                    ItemInstanceEntity addonInstance = User.Db.StoreItemInstance.FirstOrDefault(s => s.ModelId == request.AddonModelId && s.CategoryTypeId == request.CategoryTypeId);
                    if (addonInstance == null)
                    {
                        Logger.Info($"AddonEquip: Unistall all addons in slot {request.AddonSlot}");

                        var addons = itemInstance.InstalledAddons.Where(a => a.AddonSlot == request.AddonSlot);
                        itemInstance.InstalledAddons.RemoveAll(a => a.AddonSlot == request.AddonSlot);
                        User.Db.PlayerInstalledAddon.RemoveRange(addons);
                        return Json(new EquipAddonResponse(request, Guid.Empty));
                    }
                    else if (PlayerAccountEntity.Pay(addonInstance.Cost))
                    {
                        cost = addonInstance.Cost;
                        if (request.AddonSlot == EquipAddonSlot.Skin)
                        {
                            PlayerInventoryItemEntity.Factory(PlayerAccountEntity.PlayerId, addonInstance);
                            addonEntity = new AbstractTargetPlayerItem(PlayerAccountEntity.PlayerId, request.AddonModelId, CategoryTypeId.Material);
                            Logger.Info($"AddonEquip: Buy skin {addonInstance} for {PlayerAccountEntity.PlayerName} [{PlayerAccountEntity.PlayerId}], addonId: {addonEntity.ItemId}");
                        }
                        else
                        {
                            addonEntity = new AbstractTargetPlayerItem(PlayerAccountEntity.PlayerId, request.AddonModelId, CategoryTypeId.Addon);
                            Logger.Info($"AddonEquip: Buy addon {addonInstance} for {PlayerAccountEntity.PlayerName} [{PlayerAccountEntity.PlayerId}], addonId: {addonEntity.ItemId}");
                        }

                        //PlayerAccountEntity.TransactionEntities.Add(PaymentTransactionEntity.Factory(addonInstance));
                        itemInstance.AvalibleAddonList.Add(addonEntity);
                    }
                    else
                    {
                        Logger.Info($"AddonEquip: Buy addon {addonInstance} for {PlayerAccountEntity.PlayerName} [{PlayerAccountEntity.PlayerId}], not enouth money");
                        return BadRequest("Not enouth money");
                    }
                }

                var slot = itemInstance.InstalledAddons.SingleOrDefault(a => a.AddonSlot == request.AddonSlot);
                if (slot == null)
                {
                    slot = new PlayerAddonAttachedEntity
                    {
                        ItemId = itemInstance.ItemId,
                        AddonId = addonEntity.ItemId,
                        AddonSlot = request.AddonSlot,
                        ContainerId = Guid.NewGuid()
                    };
                    itemInstance.InstalledAddons.Add(slot);
                }
                else
                {
                    slot.AddonId = addonEntity.ItemId;
                }

                return Json(new EquipAddonResponse(request, slot.AddonId));

            }
            catch (Exception exception)
            {
                if (cost != null)
                {
                    PlayerAccountEntity.ForcePay(cost);
                }
                Logger.Error(exception);
                throw;
            }
        }
    }
}
