﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using Loka.Common.Building;
using Loka.Common.Building.Player;
using Loka.Common.Chat.Chanel;
using Loka.Common.Cluster;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Player.Fraction;
using Loka.Common.Player.Instance;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Fraction;
using Loka.Infrastructure;
using Loka.Player;
using Loka.Server.Controllers.IdentityController.Operation;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Migration;
using Loka.Server.Player;
using Loka.Server.Player.Infrastructure;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;
using Loka.Common.Player.Statistic;
using Loka.Common.Store;
using Loka.Common.Tutorial;
using Loka.Server.Models;
using Loka.Server.Models.Account;
using Loka.Server.Service.Player;
using Loka.Server.Service.Session;
using Loka.Server.Session;
using Microsoft.AspNet.Identity.Owin;
using Ninject;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.DataProtection;
using Newtonsoft.Json;
using SteamSharp;
using SteamSharp.Authenticators;
using VRS.Infrastructure;
using VRS.Infrastructure.Environment;


namespace Loka.Server.Controllers.IdentityController
{
    public partial class IdentityController : AbstractApiController
    {
        [Authorize, HttpGet]
        public IHttpActionResult RequestLootData()
        {
            //========================================================================
            var global = User.Db.ChatChanelEntities.SingleOrDefault(p => p.ChanelGroup == ChatChanelGroup.Global)
                         ?? User.Db.ChatChanelEntities.Add(new ChatChanelEntity
                         {
                             ChanelId = Guid.NewGuid(),
                             ChanelGroup = ChatChanelGroup.Global,
                         });

            var member = User.Db.ChatChanelMemberEntities.FirstOrDefault(p => p.PlayerId == UserId && p.ChanelId == global.ChanelId)
                         ?? User.Db.ChatChanelMemberEntities.Add(new ChatChanelMemberEntity
                         {
                             MemberId = Guid.NewGuid(),
                             PlayerId = UserId,
                             ChanelId = global.ChanelId,
                         });

            member.LastActivityDate = DateTime.UtcNow.AddHours(-6);
            User.Db.SaveChanges();

            

            //========================================================================
            try
            {
                PlayerAccountEntity.FillEmptyProfilesByWeapons();
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}] [FillEmptyProfilesByWeapons]", e);
            }

            //========================================================================
            try
            {                
                var response = PlayerAccountEntity.InventoryItemList
                    .Select(p => new RequestLootData.FInvertoryItemWrapper(p))
                    .ToArray();
                return Json(response);
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestLootData][{UserId}] [Response]", e);
                throw;
            }
        }

        [Authorize, HttpGet]
        public IHttpActionResult OnRequestClientData()
        {
            //==================================================
            //  Обновляет баланс на сайте
            UserBitcoinRepository.Update(PlayerAccountEntity);

            //==================================================
            //  Обновляет список игроков онлайн
            PlayerNotifyService.UpdatePlayerStatistic(PlayerAccountEntity);

            //==================================================
            //  Обновляет уровень игрока
            PlayerAccountEntity.Experience.Calculate();

            //==================================================
            //  Выдача ещедневного бонуса
            if (PlayerAccountEntity.WeekBonus.Calculate())
            {
                PlayerAccountEntity.Give(GameCurrency.Money, PlayerWeekBonus.BonusAmount * PlayerAccountEntity.WeekBonus.JoinCount);
            }

            //==================================================
            //  Собираем статистику по входам в игру
            if (User.PlayerGameSessionEntity.SessionAttach == PlayerSessionAttach.Client)
            {
                PlayerAccountEntity.LastSessionId = User.PlayerGameSessionEntity.SessionId;
                SessionNotifyService.AddPlayerSession(User.PlayerGameSessionEntity);
            }

            //==================================================
            //  Создаем лобби, если его нет
            try
            {
                if (PlayerAccountEntity.WorldBuildingEntities.Any(p => p.BuildingModelId == BuildingModelId.LobbyMainBuilding) == false)
                {
                    PlayerAccountEntity.WorldBuildingEntities.Add(WorldBuildingItemEntity.Factory(BuildingModelId.LobbyMainBuilding));
                }
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[OnRequestClientData:{PlayerAccountEntity?.PlayerName}][4][LobbyMainBuilding]", e);
            }

            //==================================================
            var storages = PlayerAccountEntity.UpdatePlayerStorage();

            //==================================================
            return Json(new RequestClientData.Response(PlayerAccountEntity));

            /*
                             while (PlayerAccountEntity.MigrationAction < AccountMigrationTool.LastMigrationVersion)
                {
                    var migration = AccountMigrationTool.Migration.SingleOrDefault(m => m.MigrationId == PlayerAccountEntity.NextMigrationAction);
                    if (migration == null)
                    {
                        throw new AccountMigrationException(new NullableAccountMigrationAction(), PlayerAccountEntity, new Exception
                        (
                            $"Migration {PlayerAccountEntity.NextMigrationAction} was nullptr, player: {PlayerAccountEntity.PlayerName}"
                        ));
                    }
                    else
                    {
                        Exception exception = null;
                        if (migration.Migrate(User.Db, PlayerAccountEntity, out exception) == false)
                        {
                            LoggerContainer.AccountLogger.WriteExceptionMessage($"[OnRequestClientData:{PlayerAccountEntity?.PlayerName}][1][Migration:{PlayerAccountEntity.NextMigrationAction}]", exception);
                        }
                    }
                }
                */
        }

        //=====================================================================================================

        [Authorize, HttpGet]
        public IHttpActionResult RequestPreSetData()
        {
            try
            {
                PlayerAccountEntity.UpdateProfileStatus();
                return Json(Operation.RequestPreSetData.Response(PlayerAccountEntity));
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[RequestPreSetData:{PlayerAccountEntity?.PlayerName}]", e);
                throw;
            }
        }



        //=====================================================================================================

        public class ChangeRegionAccountView
        {
            public ClusterRegionId Region { get; set; }

            [MaxLength(3)]
            public string Language { get; set; }
        }

        [Authorize, HttpPost]
        public IHttpActionResult ChangeRegionAccount(ChangeRegionAccountView view)
        {
            LanguageTypeId language = LanguageTypeId.None;
            if (Enum.TryParse(view.Language.ToLower(), out language))
            {
                User.PlayerInstance.Language = language;
            }
            return Ok();
        }

        public static readonly Regex EmailRegex = new Regex("^([A-Za-z0-9\\-\\._]{3,20})@([A-Za-z0-9]{3,10})\\.([A-Za-z]{2,3})$", RegexOptions.IgnoreCase | RegexOptions.Compiled);


        //[Authorize, HttpPost]
        //public async Task<IHttpActionResult> OnConfirmEmailByCode(FReqestOneParam<string> code)
        //{
        //    try
        //    {
        //        if (string.IsNullOrWhiteSpace(code))
        //        {
        //            return BadRequest();
        //        }
        //        else
        //        {
        //            var token = code.Value.Trim();
        //            if (token.Length < 4 || token.Length > 8)
        //            {
        //                return BadRequest();
        //            }

        //            var result = await UserManager.ConfirmEmaiCode(UserId, token);
        //            Logger.Error($"OnConfirmEmailByCode #1 [{PlayerAccountEntity?.PlayerName}] | Email: {AccountUserEntity.Email} | Succeeded: {result.Succeeded} | {string.Join(",", result.Errors)}");
        //            return Json(new FReqestOneParam<bool>(result.Succeeded));
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.Error($"OnConfirmEmailByCode #2 [{PlayerAccountEntity?.PlayerName}] | Email: {AccountUserEntity.Email} | {exception}");
        //        throw;
        //    }
        //}

        //[Authorize, HttpGet]
        //public IHttpActionResult OnCheckEmailConfirmStatus()
        //{
        //    return Json(new FReqestOneParam<bool>(AccountUserEntity.EmailConfirmed));
        //}

        //[Authorize, HttpPost]
        //public async Task<IHttpActionResult> OnChangeAccountEmail(FReqestOneParam<string> email)
        //{
        //    try
        //    {
        //        email = email.Value.ToLower();
        //        Logger.Error($"OnChangeAccountEmail #1 [{PlayerAccountEntity?.PlayerName}] | Email: {email.Value} | input");
        //        //======================================================================================
        //        if (EmailRegex.IsMatch(email) == false)
        //        {
        //            Logger.Error($"OnChangeAccountEmail #2 [{PlayerAccountEntity?.PlayerName}] | Email: {email.Value} | bad email");
        //            return BadRequest();
        //        }

        //        //======================================================================================
        //        if (string.IsNullOrWhiteSpace(AccountUserEntity.Email))
        //        {
        //            return await ChangeAccountEmail(email);
        //        }
        //        else
        //        {
        //            long emailAsUserId = 0;
        //            if (long.TryParse(email, out emailAsUserId))
        //            {
        //                return await ChangeAccountEmail(email);
        //            }
        //            else if (AccountUserEntity.Email.Equals(email.Value, StringComparison.OrdinalIgnoreCase))
        //            {
        //                if (AccountUserEntity.EmailConfirmed)
        //                {
        //                    //  Email already belongs to you and is confirmed
        //                    Logger.Error($"OnChangeAccountEmail #3 [{PlayerAccountEntity?.PlayerName}] | Email: {email.Value} |  Email already belongs to you and is confirmed");
        //                    return new HttpActionResult((HttpStatusCode) 700, email);
        //                }
        //                else
        //                {
        //                    //  Email belongs to you, sent a confirmation code
        //                    Logger.Error($"OnChangeAccountEmail #4 [{PlayerAccountEntity?.PlayerName}] | Email: {email.Value} |  Email belongs to you, sent a confirmation code");
        //                    UserManager.SendEmailCode(UserId);
        //                    return new HttpActionResult((HttpStatusCode) 701, email);
        //                }
        //            }
        //            else
        //            {
        //                return await ChangeAccountEmail(email);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.Error($"OnChangeAccountEmail #8 [{PlayerAccountEntity?.PlayerName}] | Email: {email.Value} | Exception {e}");
        //        throw;
        //    }
        //}

        //private async Task<IHttpActionResult> ChangeAccountEmail(FReqestOneParam<string> email)
        //{
        //    var userEmail = User.ApplicationDbContext.Users.FirstOrDefault(e => e.Email == email);
        //    if (userEmail == null)
        //    {
        //        AccountUserEntity.EmailChangeDate = DateTime.UtcNow;
        //        AccountUserEntity.EmailSafeValue = AccountUserEntity.Email;
        //        if (string.IsNullOrWhiteSpace(AccountUserEntity.Email) == false && AccountUserEntity.Email.Contains("@") && AccountUserEntity.Email.Contains("."))
        //        {
        //            await UserManager.SafeSendEmailAsync(UserId, "Служба безопасности", $"Для вашей учетной записи был изменен Email-адресс на новый: {email}.");
        //        }

        //        await UserManager.SetEmailAsync(UserId, email);
        //        UserManager.SendEmailCode(UserId);

        //        Logger.Error($"OnChangeAccountEmail #5 [{PlayerAccountEntity?.PlayerName}] | Email: {email.Value} | changed");
        //        return Json(email);
        //    }
        //    else if (userEmail.EmailConfirmed)
        //    {
        //        // email use by another user and address confirmed
        //        Logger.Error($"OnChangeAccountEmail #6 [{PlayerAccountEntity?.PlayerName}] | Email: {email.Value} | email use by another user and address confirmed");
        //        return new HttpActionResult((HttpStatusCode)702, email);
        //    }
        //    else
        //    {
        //        // email use by another user, but address not confirmed
        //        Logger.Error($"OnChangeAccountEmail #7 [{PlayerAccountEntity?.PlayerName}] | Email: {email.Value} | email use by another user, but address not confirmed");
        //        return  new HttpActionResult((HttpStatusCode)703, email);
        //    }
        //}

        [Inject]
        public PlayerRepository PlayerRepository { get; set; }
    }
}
