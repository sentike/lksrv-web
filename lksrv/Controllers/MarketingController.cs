﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using Loka.Common.Improvements;
using Loka.Common.Improvements.Category;
using Loka.Common.Improvements.Instance;
using Loka.Common.Improvements.Sentence;
using Loka.Common.Improvements.Vote;
using Loka.Common.Store;
using Loka.Infrastructure;



using Loka.Server.Controllers.StoreController.Operation;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using VRS.Infrastructure.Environment;
using VRS.Infrastructure.IpAddress;
using WebApi.OutputCache.V2;

namespace Loka.Server.Controllers
{
    public class MarketingController : AbstractApiController
    {
        [AllowAnonymous, HttpGet]
        public IHttpActionResult FillImprovementsList()
        {
            using (var db = new DataRepository())
            {
                db.ImprovementEntities.Add(new ImprovementEntity
                {
                    ImprovementId = Guid.NewGuid(),
                    CategoryId = ImprovementCategoryId.GamePlay,
                    CreatedDate = DateTime.UtcNow,
                    LocalizationEntities = new List<ImprovementLocalizationEntity>()
                    {
                        new ImprovementLocalizationEntity() {LocalizationId = Guid.NewGuid(), Title = "Патронная коробка", Message = "Коробка которая восполняет боеприпасы", LanguageId = LanguageTypeId.ru},
                        new ImprovementLocalizationEntity() {LocalizationId = Guid.NewGuid(), Title = "Ammo box", Message = "Ammo box for refil armours", LanguageId = LanguageTypeId.en},
                    }
                });

                return Ok(db.SaveChanges());
            }
        }

        [AllowAnonymous, HttpGet]
        public IHttpActionResult RequestImprovementsList_a(LanguageTypeId language)
        {
            using (var db = new DataRepository())
            {
                return Json
                (
                    new ImprovementViewModel
                    {
                        DateOfEndVotes = DateTime.UtcNow.StartOfWeek(DayOfWeek.Monday).AddDays(7).ToUnixTime(),
                        //--------------------------------------------------------
                        ImprovementViews = db.ImprovementEntities./*Where(e => e.ProgressState == ImprovementProgressState.Voting).Take(5)
                                            .Concat(db.ImprovementEntities.Where(e => e.ProgressState == ImprovementProgressState.Development).Take(5))
                                            .Concat(db.ImprovementEntities.Where(e => e.ProgressState == ImprovementProgressState.Release).Take(10))
                                            .*/ToList().AsView(language, Guid.Empty).ToList()
                        //--------------------------------------------------------
                    }
                );
            }
        }

        [Authorize, HttpGet]
        public IHttpActionResult RequestImprovementsList()
        {
            Logger.Error($"RequestImprovementsList: {PlayerAccountEntity.PlayerName} | {PlayerAccountEntity.Language}");
            var res = new ImprovementViewModel
            {
                DateOfEndVotes = DateTime.UtcNow.StartOfWeek(DayOfWeek.Monday).AddDays(7).ToUnixTime(),
                //--------------------------------------------------------
                ImprovementViews = User.Db.ImprovementEntities.Where(e => e.ProgressState == ImprovementProgressState.Voting) /*.Take(5)*/
                    .Concat(User.Db.ImprovementEntities.Where(e => e.ProgressState == ImprovementProgressState.Development).Take(5))
                    .Concat(User.Db.ImprovementEntities.Where(e => e.ProgressState == ImprovementProgressState.Release).Take(10))
                    .ToList().AsView(UserLanguage, UserId),
                //--------------------------------------------------------
                SentenceViews = User.Db.SentenceEntities.ToList().AsView(UserId)
            };

            return Json(res);
        }

        [Authorize, HttpPost]
        public IHttpActionResult VoteImprovement(ImprovementVoteCreateView view)
        {
            var cost = new StoreItemCost {Currency = GameCurrency.Donate, Amount = OperationListOfInstance.Response.ConstCostOfImprovementVote };
            //===============================
            if (PlayerAccountEntity.IsAllowPay(cost) == false)
            {
                return StatusCode(HttpStatusCode.PaymentRequired);
            }

            //===============================
            var instance = User.Db.ImprovementEntities.SingleOrDefault(e => e.ImprovementId == view.ImprovementId);
            if (instance == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }

            //===============================
            if (instance.ProgressState != ImprovementProgressState.Voting)
            {
                return StatusCode(HttpStatusCode.MethodNotAllowed);
            }

            //===============================
            if (PlayerAccountEntity.IsAllowVoteImprovement == false)
            {
                return StatusCode(HttpStatusCode.NotAcceptable);
            }

            //===============================
            ImprovementVoteEntity entity = ImprovementVoteEntity.Factory(view, instance);
            try
            {
                PlayerAccountEntity.VoteEntities.Add(entity);
                User.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                Logger.Error($"VoteImprovement: {entity} | exception: {exception}");
            }
            finally
            {
                PlayerAccountEntity.ForcePay(cost);
            }
            //===============================
            return Ok(view);
        }

        //------------------------------------------------------------------------

        [Authorize, HttpPost]
        public IHttpActionResult VoteSentence(ImprovementVoteCreateView view)
        {
            //===============================
            if (PlayerAccountEntity.SentenceVoteEntities.Any(e => e.SentenceId == view.ImprovementId))
            {
                return Conflict();
            }

            //===============================
            var instance = User.Db.SentenceEntities.SingleOrDefault(e => e.SentenceId == view.ImprovementId);
            if (instance == null)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }

            //===============================
            if (instance.ProgressState != SentenceProgressState.Voting)
            {
                return StatusCode(HttpStatusCode.MethodNotAllowed);
            }

            //===============================
            PlayerAccountEntity.SentenceVoteEntities.Add(SentenceVoteEntity.Factory(view));
            return Ok(view);
        }

        [Authorize, HttpPost]
        public IHttpActionResult PushSentence(ImprovementCreateView view)
        {
            if (PlayerAccountEntity.IsAllowPushProposal == false)
            {
                return StatusCode(HttpStatusCode.RequestTimeout);
            }

            view.CategoryId = ImprovementCategoryId.GamePlay;
            var cost = new StoreItemCost
            {
                Currency = GameCurrency.Donate,
                Amount = PlayerAccountEntity.IsDonatePushSentence 
                    ? OperationListOfInstance.Response.ConstCostOfSentencePush 
                    : 0
            };
            //===============================
            if (PlayerAccountEntity.IsAllowPay(cost) == false)
            {
                return StatusCode(HttpStatusCode.PaymentRequired);
            }
 
            //===============================
            SentenceEntity entity = SentenceEntity.Factory(view);
            try
            {
                PlayerAccountEntity.SentenceEntities.Add(entity);
                User.Db.SaveChanges();
            }
            catch (Exception exception)
            {
                Logger.Error($"VoteImprovement: {entity} | exception: {exception}");
            }
            finally
            {
                PlayerAccountEntity.ForcePay(cost);
            }
            //===============================
            return Ok();
        }


    }
}