﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Loka.Common.Match.GameMap;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Common.Player.Statistic.PvE;
using Loka.Infrastructure;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Models.Session;
using Ninject;
using VRS.Infrastructure;
using WebApi.OutputCache.V2;

namespace Loka.Server.Session
{
    public class SessionController : AbstractApiController
    {
        [Inject]
        public SessionRepository SessionRepository { get; set; }

        [Authorize, HttpPost]
        public IHttpActionResult OnAttackTo(FReqestOneParam<Guid> player)
        {      
            //======================================================================
            if (UserId == player)
            {
                return Conflict();
            }

            //======================================================================
            var defender = User.Db.AccountPlayerEntity.FirstOrDefault(p => p.PlayerId == player);
            if (defender == null)
            {
                return NotFound();
            }

            //======================================================================
            if (DateTime.UtcNow < defender.LastAttackedDate)
            {
                return BadRequest();
            }

            //======================================================================
            var gm = User.Db.GameModeEntity.Single(p => p.GameModeId == GameModeTypeId.Attack);
            if (gm.AttackTo(User.Db, UserId, player.Value))
            {
                return Ok();
            }
            return InternalServerError();
        }


        [Authorize, HttpPost]
        public IHttpActionResult GetMatchResult(FReqestOneParam<Guid> mathId )
        {
            return Ok();
        }

        public struct LobbyMatchResultDetailed
        {
            public LobbyMatchResultDetailed(MatchEntity matchEntity)
            {
                MatchId = matchEntity.MatchId;
                var universal = matchEntity.TakePlayer(matchEntity.Options.UniversalId)?.PlayerEntity;
                UniversalId = universal?.PlayerId;
                UniversalName = universal?.PlayerName;
                GameModeTypeId = matchEntity.Options.GameMode;
                GameMapTypeId = matchEntity.Options.GameMap;
                Award = new MatchMemberAward();
                IsWin = false;
            }

            public LobbyMatchResultDetailed(MatchMemberEntity m) : this(m.MatchEntity)
            {
                IsWin = m.TeamId == m.MatchEntity.WinnerTeamId;
                Award = m.Award;
            }

            public Guid MatchId { get; set; }
            public GameModeFlags GameModeTypeId { get; set; }
            public GameMapFlags GameMapTypeId { get; set; }
            public bool IsWin { get; set; }

            public Guid? UniversalId { get; set; }
            public string UniversalName { get; set; }

            public MatchMemberAward Award { get; set; }

        };

        [Authorize, HttpGet]
        public IHttpActionResult GetMatchResults()
        {
            try
            {
                var lastMatchMembers = User.Db.MatchMember.Where(m => m.PlayerId == PlayerAccountEntity.PlayerId && m.State == MatchMemberState.Finished).OrderByDescending(m => m.MatchEntity.Finished).Take(10).ToArray();
                foreach (var member in lastMatchMembers) member.State = MatchMemberState.Notified;
                return Ok(lastMatchMembers.Select(m => new LobbyMatchResultDetailed(m)).ToArray());
            }
            catch (Exception e)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage($"[GetMatchResults][1]: {PlayerAccountEntity.PlayerName}", e);
                throw;
            }
        }

        [Authorize, HttpGet]
        public IHttpActionResult GameSessionBreak()
        {
            SessionRepository.LeaveFromQueue(PlayerAccountEntity);
            throw new NotImplementedException();
           // return Json(SessionRepository.QueueStatus(PlayerAccountEntity));
        }


        [Authorize, HttpGet]
        public IHttpActionResult GameSessionContinue()
        {
            throw new NotImplementedException();
           // return Json(SessionRepository.QueueStatus(PlayerAccountEntity));
        }

        //[AllowAnonymous, HttpGet]
        //[CacheOutput(ClientTimeSpan = 10, ServerTimeSpan = 10)]
        //public IHttpActionResult GetQueuenOnline()
        //{
        //    try
        //    {
        //        using (var db = new DataRepository())
        //        {
        //            var lastTenMinuts = DateTime.UtcNow.AddMinutes(-10);
        //            var online = db.QueueEntity.Where(p => p.Ready && p.LastActivityDate >= lastTenMinuts).ToArray().Where(p => p.IsOnline).ToArray();
        //            return Json(Enum.GetValues(typeof(GameModeTypeId)).Cast<GameModeTypeId>().Select(g => new QueuenOnlineContainer(g, online.LongCount(q => q.Options.GameMode == g))));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        LoggerContainer.SessionLogger.WriteExceptionMessage($"[GetQueuenOnline]: {PlayerAccountEntity.PlayerName}", e);
        //        throw;
        //    }
        //}

        public struct PlayerRatingTopView
        {
            public IReadOnlyCollection<PlayerPvEMatchStatisticView> PvERating { get; set; }
            public IReadOnlyCollection<PlayerRatingTopContainer> PlayerRating { get; set; }
        }

        [AllowAnonymous, HttpGet]
        [CacheOutput(ClientTimeSpan = 120, ServerTimeSpan = 120)]
        public IHttpActionResult GetPlayerRatingTop()
        {
            try
            {
                return Json(new PlayerRatingTopView
                {
                    PlayerRating = PlayerRatingBuffer.Instance.CkeckUpdate(),
                    PvERating = PvERatingBuffer.Instance.CkeckUpdate()
                });
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return Ok();
            }
            // return Json(DataRepository.AccountPlayerEntity.AsNoTracking().Where(p => p.Experience.WeekExperience >= 0 && p.Experience.Experience >= 0).OrderByDescending(p => p.Experience.WeekExperience).Take(60).ToArray().Select(p => new PlayerRatingTopContainer(p)).ToArray());
        }
    }
}
