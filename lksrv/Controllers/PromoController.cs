﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Loka.Common.Player.Instance;
using Loka.Infrastructure;
using Loka.Player;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Promo;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;
using Loka.Common.Store.Item.Armour;
using Loka.Common.Store.Item.Weapon;
using WebGrease.Css.Extensions;
using Loka.Server.Infrastructure.Promo;
using Loka.Server.Models.Account.Promo;
using Loka.Common.Store;

namespace Loka.Server.Controllers
{
    public class PromoController : AbstractApiController
    {

        //=====================================================================================================

        //[Authorize, HttpPost]
        //public IHttpActionResult ActivatePromo(FReqestOneParam<string> code)
        //{
        //    var codeEntity = User.Db.PartnerCode.SingleOrDefault(p => p.Name.ToLower().Equals(code.Value.ToLower()));
        //    Logger.Warn($"Attemp activate code: {code.Value} | exist: {codeEntity != null}");
        //    if (codeEntity == null)
        //        return BadRequest();
        //
        //    if (PlayerAccountEntity.PromoCodes.Any(c => c.CodeId == codeEntity.PromoId))
        //        return Conflict();
        //
        //    PlayerAccountEntity.PromoCodes.Add(new PlayerPromoCodeEntity(codeEntity.PromoId));
        //
        //
        //    if (PlayerAccountEntity.FirstPartnerId == null)
        //    {
        //        PlayerAccountEntity.FirstPartnerId = codeEntity.PartnerId;
        //        var promoItem = User.Db.PromoItem.ToArray().PickRandom();
        //        if (promoItem != null)
        //        {
        //            Logger.Warn($"promoItem: {promoItem.ModelId} [{promoItem.CategoryTypeId}] | items: {User.Db.PromoItem.Count()}");
        //            if (PlayerAccountEntity.InventoryItemList.Any(i => i.ModelId == promoItem.ModelId && i.CategoryTypeId == promoItem.CategoryTypeId) == false)
        //            {
        //                var itemEntity = promoItem.ModelInstance.Factory(PlayerAccountEntity);
        //                PlayerAccountEntity.InventoryItemList.Add(itemEntity);
        //                return Json(new ActivatePromoResponse(itemEntity.ItemId, promoItem.ModelId, promoItem.CategoryTypeId));
        //            }
        //        }
        //        else
        //        {
        //            Logger.Warn("promoItem not found!");
        //        }
        //    }
        //
        //    
        //    var rnd = new Random();
        //    var cash = new PlayerCashBalance
        //    {
        //        Money = rnd.Next(500, 1500),
        //        Donate = 0//rnd.Next(50, 100)
        //    };
        //
        //    Logger.Warn($"Activated third partner code, {cash}");
        //
        //    PlayerAccountEntity.Cash.Money += cash.Money;
        //    PlayerAccountEntity.Cash.Donate += cash.Donate;
        //
        //    return Json(new ActivatePromoResponse(cash));
        //}

        //=====================================================================================================

        [Authorize, HttpGet]
        public IHttpActionResult ListOfSocialTask()
        {
            List<SocialTaskEntity> avalibleTasks = new List<SocialTaskEntity>(8);
            try
            {

                var playerTasks = PlayerAccountEntity.SocialTaskList.OrderByDescending(t => t.RepostDate);
                foreach (var social in User.Db.SocialGroupEntity.ToArray())
                {
                    var lastPlayerTask = playerTasks.FirstOrDefault(t => t.SocialTaskEntity.SocialGroupId == social.SocialGroupId);
                    if (lastPlayerTask == null || lastPlayerTask.RepostDate.ToElapsedSeconds() >= social.PostIntevalSeconds)
                    {
                        var task = social.TaskList.FirstOrDefault(t => playerTasks.Select(q => q.SocialTaskId).Contains(t.TaskId) == false);
                        if (task != null)
                            avalibleTasks.Add(task);
                    }
                }

            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            return Json(avalibleTasks.Select(t => new SocialTaskWrapper(t)));
        }

        [Authorize, HttpPost]
        public IHttpActionResult TakeSocialTask(FReqestOneParam<Guid> taskId)
        {
            try
            {
                var task = User.Db.SocialTaskEntity.FirstOrDefault(t => t.TaskId == taskId);
                if (task == null)
                    return BadRequest();

                if (PlayerAccountEntity.SocialTaskList.Any(t => t.SocialTaskId == taskId))
                    return Conflict();

                PlayerAccountEntity.SocialTaskList.Add(new PlayerSocialTaskEntity(PlayerAccountEntity.PlayerId, task.TaskId));

                return Json(new FReqestOneParam<string>(task.UrlAddress));
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return BadRequest();
            }
        }

        //=====================================================================================================

        public struct SocialTaskStatusContainer
        {
            public SocialGroupId SocialGroupId;
            public StoreItemCost Award;

            public SocialTaskStatusContainer(PlayerSocialTaskEntity playerSocialTaskEntity)
            {
                playerSocialTaskEntity.State = SocialRepostState.Done;
                SocialGroupId = playerSocialTaskEntity.SocialTaskEntity.SocialGroupId;
                Award = playerSocialTaskEntity.SocialTaskEntity.Bonus;
            }
        }

        public struct SocialTaskStatusResponse
        {
            public int ActiveTaskCount;
            public IEnumerable<SocialTaskStatusContainer> CompletedTasks;

            public SocialTaskStatusResponse(int activeTaskCount, IEnumerable<SocialTaskStatusContainer> completedTasks)
            {
                ActiveTaskCount = activeTaskCount;
                CompletedTasks = completedTasks;
            }
        }

        [Authorize, HttpGet]
        public IHttpActionResult StatusOfSocialTask()
        {
            var activeTasks = PlayerAccountEntity.SocialTaskList.Where(t => t.State == SocialRepostState.None).ToArray();
            var completedTasks = activeTasks.Where(t => DateTime.UtcNow >= t.ConfirmDate).ToArray();

            return Json(new SocialTaskStatusResponse(activeTasks.Count(), completedTasks.Select(t => new SocialTaskStatusContainer(t)).ToArray()));
        }

    }
}