﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Loka.Common.Building;
using Loka.Common.Player.Fraction;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Fraction;
using Loka.Infrastructure;
using Loka.Server.Infrastructure;

namespace Loka.Server.Controllers.IdentityController
{
    public partial class IdentityController : AbstractApiController
    {

        [Authorize, HttpPost]
        public IHttpActionResult OnSelectFraction(FReqestOneParam<FractionTypeId> request)
        {
            Logger.Error($"OnSelectFraction[{request.Value}] / 1: {PlayerAccountEntity.PlayerName} [{PlayerAccountEntity.PlayerId}]");

            //=========================================================================
            try
            {
                if (PlayerAccountEntity.WorldBuildingEntities.Any() == false)
                {
                    Dictionary<BuildingModelId, long> depends;
                    PlayerAccountEntity.ApplyTemplate(User.Db, Guid.Parse("df780577-cf4e-4e77-adc3-fc186fe47b49"), true, out depends);
                }
            }
            catch (Exception e)
            {
                Logger.Error($"OnSelectFraction[0]: {PlayerAccountEntity.PlayerName} [{PlayerAccountEntity.PlayerId}]: {e}");
            }

            //=========================================================================
            try
            {
                if (request >= FractionTypeId.End || (int)request.Value < 0)
                {
                    return BadRequest("Out of range");
                }

                //=========================================================================
                var playerFraction = PlayerAccountEntity.ReputationList.SingleOrDefault(f => f.FractionId == request.Value);
                if (playerFraction == null)
                {
                    PlayerAccountEntity.ReputationList.Add(playerFraction = PlayerFractionReputationEntity.Factory(PlayerAccountEntity.PlayerId, request.Value, 1));
                }
                else
                {
                    playerFraction.LastRank = playerFraction.CurrentRank;
                    playerFraction.CurrentRank++;
                }

                //=========================================================================
                if (PlayerAccountEntity.CurrentFractionId == null || PlayerAccountEntity.CurrentFractionId == Guid.Empty)
                {
                    PlayerAccountEntity.CurrentFractionId = playerFraction.ReputationId;
                    PlayerAccountEntity.CurrentFractionEntity = playerFraction;
                }

                //=========================================================================
                var ids = PlayerAccountEntity.InventoryItemList.Select(p => new { p.ModelId, p.CategoryTypeId }).ToArray();
                var prototypes = User.Db.StoreItemInstance.Where(i => i.Level == 0 && i.FractionId == PlayerAccountEntity.CurrentFractionEntity.FractionId && i.Flags.HasFlag(ItemInstanceFlags.Hidded) == false).ToArray();
                var items = prototypes.Where(p => ids.Contains(new { p.ModelId, p.CategoryTypeId }) == false).ToArray();
                foreach (var instance in items)
                {
                    PlayerAccountEntity.InventoryItemList.Add(instance.Factory(PlayerAccountEntity));
                }

                //=========================================================================
                //  Add Weapons to Profiles
                try
                {
                    PlayerAccountEntity.FillEmptyProfilesByWeapons();
                }
                catch (Exception exception)
                {
                    Logger.Error($"OnSelectFraction[1]: {PlayerAccountEntity.PlayerName} [{PlayerAccountEntity.PlayerId}]: {exception}");
                }

                //=========================================================================
                User.Db.SaveChanges();

                //=========================================================================
                return Json(new PlayerFractionReputationView(playerFraction));
            }
            catch (Exception exception)
            {
                Logger.Error($"OnSelectFraction[2]: {PlayerAccountEntity.PlayerName} [{PlayerAccountEntity.PlayerId}]: {exception}");
                throw;
            }
        }

        //=====================================================================================================

        [Authorize, HttpPost]
        public IHttpActionResult OnSwitchFraction(FReqestOneParam<Guid> request)
        {
            var fraction = PlayerAccountEntity.ReputationList.SingleOrDefault(f => f.ReputationId == request.Value);
            if (fraction == null)
            {
                return NotFound();
            }

            PlayerAccountEntity.CurrentFractionId = fraction.ReputationId;
            return Json(request);
        }
    }
}