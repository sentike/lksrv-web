﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading;
using System.Web.Http;
using Loka.Common.Building;
using Loka.Common.Building.Handlers;
using Loka.Common.Building.Player;
using Loka.Common.Player.Inventory;
using Loka.Common.Store;
using Loka.Common.Store.Property;
using Loka.Infrastructure;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models;
using Loka.Server.Player.Models;
using Newtonsoft.Json;
using VRS.Infrastructure;

/**
 *  Решение проблемы с миграцией: https://developercommunity.visualstudio.com/content/problem/25424/error-running-entity-framework-6-code-first-migrat.html
    gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\Microsoft.Build.Framework.dll"
    gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\Microsoft.Build.dll"
    gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\Microsoft.Build.Engine.dll"
    gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\Microsoft.Build.Conversion.Core.dll"
    gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\Microsoft.Build.Tasks.Core.dll"
    gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\Microsoft.Build.Utilities.Core.dll"
 */

namespace Loka.Server.Controllers
{
    public class BuildingController : AbstractApiController
    {
        [AllowAnonymous, HttpGet]
        public IHttpActionResult ApplyTemplateGlobal(/*Guid id*/)
        {
            using (var db = new DataRepository())
            {
                var m = (byte)BuildingModelId.LobbyMainBuilding;
                foreach (var p in db.AccountPlayerEntity.Where(a => a.WorldBuildingEntities.Any(q => q.ModelId == m) == false).ToArray())
                {
                    p.WorldBuildingEntities.Add(WorldBuildingItemEntity.Factory(BuildingModelId.LobbyMainBuilding));
                    //ApplyTemplate(id, p, true);
                }
                db.SaveChanges();
            }
            return Ok();
        }

        [AllowAnonymous, HttpGet]
        public IHttpActionResult SaveTemplate(Guid id)
        {
            using (var db = new DataRepository())
            {
                db.Configuration.AutoDetectChangesEnabled = true;
                db.Configuration.ProxyCreationEnabled = true;
                db.Configuration.LazyLoadingEnabled = true;

                var player = db.AccountPlayerEntity
                    .Include(p => p.WorldBuildingEntities)
                    .Include(p => p.InventoryItemList)
                    .FirstOrDefault(p => p.PlayerId == id);
                new SaveTemplateView
                {
                    Cost = new StoreItemCost()
                    {
                        Amount = 100500,
                        Currency = GameCurrency.Money
                    }, Name = "DmSky", AllowSell = true
                }.SaveTemplate(player, db);
            }
            return Ok();
        }

        [AllowAnonymous, HttpGet]
        public IHttpActionResult ApplyTemplate(Guid id, Guid player, bool force)
        {
            using (var db = new DataRepository())
            {
                var pl = db.AccountPlayerEntity.SingleOrDefault(p => p.PlayerId == player);
                Dictionary<BuildingModelId, long> deps = new Dictionary<BuildingModelId, long>();
                pl.ApplyTemplate(db, id, force, out deps);
                return Json(deps);
            }
        }

        public class HarvestResponse
        {
            public WorldBuildingStorageView Storage { get; set; }
            public long Harvested { get; set; }
        }

        [Authorize, HttpPost]
        public IHttpActionResult OnHarvest(FReqestOneParam<Guid> id)
        {
            try
            {
                LoggerContainer.BuildingLogger.Trace($"[OnHarvest][{PlayerAccountEntity.PlayerName}][{id.Value}]");
                //===============================================================================================
                var instance = PlayerAccountEntity.WorldBuildingEntities.SingleOrDefault(i => i.ItemId == id);
                if (instance == null)
                {
                    return DataResponse(404, id);
                }
            
                //===============================================================================================
                var avalible = PlayerAccountEntity.AvalibleCash();
            
                //===============================================================================================
                LoggerContainer.BuildingLogger.Trace($"OnHarvest[0]: {PlayerAccountEntity.PlayerName}  | avalible: {avalible}");
            
                //===============================================================================================
                if (avalible <= 0)
                {
                    return DataResponse(400, id);
                }
            
                //===============================================================================================
                var storage = instance.Harvest(avalible);
                if (storage > 0)
                {
                    //PlayerAccountEntity.TransactionEntities.Add(DepositTransactionEntity.Factory(storage, DepositTransactionSource.Miner));
                    PlayerAccountEntity.Give(GameCurrency.Money, storage);
                    User.Db.SaveChanges();
                }
            
            
                LoggerContainer.BuildingLogger.Trace($"OnHarvest[1]: {PlayerAccountEntity.PlayerName} | {storage} | avalible: {avalible}");
                //===============================================================================================
                var response = new HarvestResponse
                {
                    Storage = instance.AsStorageView(),
                    Harvested = storage
                };
                
                //===============================================================================================
                return Json(response);
            }
            catch (Exception e)
            {
                LoggerContainer.BuildingLogger.WriteExceptionMessage($"[OnHarvest][{PlayerAccountEntity.PlayerName}][{id.Value}]", e);
                throw;
            }
        }

	    [AllowAnonymous, HttpGet]
	    public IHttpActionResult GetListOfBuildingsT()
	    {
		    try
		    {
			    using (var db = new DataRepository())
			    {
				    var id = db.BuildingItemEntities.First().PlayerId;
					db.Configuration.ProxyCreationEnabled = true;
				    db.Configuration.AutoDetectChangesEnabled = false;
				    db.Configuration.LazyLoadingEnabled = false;

				    var r = db.BuildingItemEntities.AsNoTracking().Where(p => p.PlayerId == id)
					    .Include(p => p.PropertyEntities).Include(p => p.PropertyEntities.Select(q => q.PropertyEntity)).ToList();

				    var v = r.AsView();
				    return Json(v);
			    }
		    }
		    catch (Exception e)
		    {
			    LoggerContainer.BuildingLogger.WriteExceptionMessage($"[GetListOfBuildings][{PlayerAccountEntity.PlayerName}]", e);
			    throw;
		    }

	    }

        [Authorize, HttpGet]
        public IHttpActionResult GetListOfProperties()
        {

            try
            {
                using (var db = new DataRepository())
                {
                    db.Configuration.ProxyCreationEnabled = true;
                    db.Configuration.AutoDetectChangesEnabled = false;
                    db.Configuration.LazyLoadingEnabled = false;

                    //================================
                    var r = db.BuildingPropertyEntities.AsNoTracking().Include(p => p.PropertyEntity).Where(p => p.PlayerId == UserId).Select(p => new
                    {
                        Key = p.PropertyEntity.Key.ToString(),
                        p.BuildingId,
                        p.Value
                    }).ToArray();

                    //================================
                    return Json(r);
                }
            }
            catch (Exception e)
            {
                LoggerContainer.BuildingLogger.WriteExceptionMessage($"[GetListOfBuildings][{PlayerAccountEntity.PlayerName}]", e);
                throw;
            }
        }

        public class PlayerNameIdPair
        {
            public Guid Id { get; set; }
            public string Name { get; set; }

            public PlayerNameIdPair() { }
            public PlayerNameIdPair(PlayerEntity player)
            {
                Id = player?.PlayerId ?? Guid.Empty;
                Name = player?.PlayerName;
            }

            public override string ToString()
            {
                return $"{Id} - {Name}";
            }
        }

        public class PlayerWorldModel
        {
            public PlayerNameIdPair Current { get; set; }
            public PlayerNameIdPair Preview { get; set; }
            public PlayerNameIdPair Next { get; set; }
            public List<WorldBuildingItemView> Buildings { get; set; } = new List<WorldBuildingItemView>();
        }

        [Authorize, HttpPost]
        public IHttpActionResult GetListOfBuildings2(FReqestOneParam<Guid> id)
        {
            try
            {
                using (var db = new DataRepository())
                {
                    var result = new PlayerWorldModel();
                    db.Configuration.ProxyCreationEnabled = true;
                    db.Configuration.AutoDetectChangesEnabled = false;
                    db.Configuration.LazyLoadingEnabled = false;

                    //=====================================
                    if (id == Guid.Empty)
                    {
                        id = UserId;
                    }

                    //=====================================
                    var current = db.AccountPlayerEntity.SingleOrDefault(p => p.PlayerId == id);
                    if (current == null)
                    {
                        return BadRequest($"Player [{id.Value}] not found");
                    }

                    //=====================================
                    result.Current = new PlayerNameIdPair(current);


                    //=====================================
                    if (UserId != id)
                    {
                        //=====================================
                        var currentId = current.PlayerId;
                        
                        //=====================================
                        {
                            var preview = db.AccountPlayerEntity.OrderByDescending(p => p.PlayerId).FirstOrDefault(p => p.PlayerId.CompareTo(currentId) < 0)
                                          ?? db.AccountPlayerEntity.OrderBy(p => p.PlayerId).FirstOrDefault(p => p.PlayerId.CompareTo(currentId) < 0);

                            if (preview != null)
                            {
                                result.Preview = new PlayerNameIdPair(preview);
                            }
                        }

                        //=====================================
                        {
                            var next = db.AccountPlayerEntity.OrderBy(p => p.PlayerId).FirstOrDefault(p => p.PlayerId.CompareTo(currentId) > 0)
                                       ?? db.AccountPlayerEntity.OrderByDescending(p => p.PlayerId).FirstOrDefault(p => p.PlayerId.CompareTo(currentId) < 0);

                            if (next != null)
                            {
                                result.Next = new PlayerNameIdPair(next);
                            }
                        }
 

                        //=====================================
                        LoggerContainer.BuildingLogger.Warn($"[GetListOfBuildings2][Current: {result.Current}][Next: {result.Next}][Preview: {result.Preview}]");
                    }

                    //=====================================
                    var r = db.BuildingItemEntities.AsNoTracking().Where(p => p.PlayerId == id)
                        .Include(p => p.PropertyEntities).Include(p => p.PropertyEntities.Select(q => q.PropertyEntity)).ToList();

                    result.Buildings = r.AsView();
                    return Json(result);
                }
            }
            catch (Exception e)
            {
                LoggerContainer.BuildingLogger.WriteExceptionMessage($"[GetListOfBuildings][{PlayerAccountEntity.PlayerName}]", e);
                throw;
            }

        }

        [Authorize, HttpGet]
        public IHttpActionResult GetListOfBuildings()
        {
            try
            {
                using (var db = new DataRepository())
                {
                    db.Configuration.ProxyCreationEnabled = true;
                    db.Configuration.AutoDetectChangesEnabled = false;
                    db.Configuration.LazyLoadingEnabled = false;

                    var r = db.BuildingItemEntities.AsNoTracking().Where(p => p.PlayerId == UserId)
						.Include(p => p.PropertyEntities).Include(p => p.PropertyEntities.Select(q => q.PropertyEntity)).ToList();

                    var v = r.AsView();
                    return Json(v);
                }
            }
            catch (Exception e)
            {
                LoggerContainer.BuildingLogger.WriteExceptionMessage($"[GetListOfBuildings][{PlayerAccountEntity.PlayerName}]", e);
                throw;
            }

        }

        [NonAction]
        private IHttpActionResult TryPlaceObject(WorldBuildingItemView view, out WorldBuildingItemEntity entity)
        {
            try
            {
                entity = null;
                using (var db = new DataRepository())
                {
                    var model = (short)view.ModelId;
                    var instance = db.PlayerInventortItem.Include(p => p.ItemModelEntity).Include(p => p.ItemModelEntity.PropertyEntities)
                        .SingleOrDefault(i => i.ModelId == model && i.CategoryTypeId == CategoryTypeId.Building && i.PlayerId == UserId);

	                if (instance == null)
	                {
		                LoggerContainer.BuildingLogger.Error($"[TryPlaceObject][{PlayerAccountEntity.PlayerName}][{view.ModelId}][{model}]  | instance was nullptr");
						return NotFound();
	                }

	                if (instance.IsAny == false)
	                {
		                return Conflict();
	                }

	                //===================================================================
                    instance.Amount--;
                    User.Db.Entry(instance).State = EntityState.Detached;
                    db.Configuration.ValidateOnSaveEnabled = false;

                    //===================================================================
                    var en = view.AsCreate(UserId);
                    entity = en;

                    //===================================================================
                    var props = instance.ItemModelEntity.PropertyEntities.Select(p => WorldBuildingPropertyEntity.Factory(en, p)).ToArray();
                    entity.PropertyEntities.AddRange(props);

                    view.Properties = props.ToDictionary(p => p.Key, p => new FWorldBuildingProperty(p.Value, p.Default));

                    //===================================================================
                    if (db.BuildingItemEntities.Any(p => p.ItemId == view.ItemId))
                    {
                        view.NewItemId = entity.ItemId = Guid.NewGuid();
                    }
                    else
                    {
                        view.NewItemId = entity.ItemId;
                    }

                    //===================================================================
                    db.BuildingItemEntities.Add(entity);
                    db.SaveChanges();

                    LoggerContainer.BuildingLogger.Trace($"TryPlaceObject: {PlayerAccountEntity.PlayerName} | {entity.BuildingModelId} | {entity.CategoryTypeId}");
                    //===================================================================
                    return Json(view);
                }
            }
            catch (Exception e)
            {
                LoggerContainer.BuildingLogger.WriteExceptionMessage($"[TryPlaceObject][{PlayerAccountEntity.PlayerName}]", e);
                throw;
            }
        }

	    [Authorize, HttpPost]
	    public IHttpActionResult OnRepairObject()
	    {
		    //========================================================================
			var buildings = PlayerAccountEntity.WorldBuildingEntities.ToArray();
			var repaired = new List<WorldBuildingRepairView>(PlayerAccountEntity.WorldBuildingEntities.Count);

			//========================================================================
		    foreach (var building in buildings)
		    {
			    long health;
				if (RepairBuildingHandler.Handler(PlayerAccountEntity, building, out health))
			    {
					repaired.Add(new WorldBuildingRepairView(building.ItemId, health));
				}
			}

		    //========================================================================
		    User.Db.SaveChanges();

			//========================================================================
			return Json(repaired);
	    }


		[Authorize, HttpPost]
	    public IHttpActionResult OnUpdateBuildingProperty(UpdateBuildingProperty property)
		{
			if (ProfileBuildingHandler.Handler(PlayerAccountEntity, property))
			{
				return Json(property);
			}
			return BadRequest();
		}

        [Authorize, HttpPost]
        public IHttpActionResult PlaceObject(WorldBuildingItemView view)
        {

            for (int i = 0; i < 10; i++)
            {
                WorldBuildingItemEntity entity = null;
                Thread.Sleep(100);
                try
                {
                    return TryPlaceObject(view, out entity);
                }
                catch (DbUpdateException e)
                {
                    LoggerContainer.BuildingLogger.WriteExceptionMessage($"[PlaceObject][0][{i}][{PlayerAccountEntity.PlayerName}] {view}", e);
                }
                catch (Exception e)
                {
                    LoggerContainer.BuildingLogger.WriteExceptionMessage($"[PlaceObject][1][{i}][{PlayerAccountEntity.PlayerName}] {view}", e);
                    throw;
                }
            }
            return BadRequest(JsonConvert.SerializeObject(new FReqestOneParam<Guid>(view.ItemId)));
        }

        [Authorize, HttpPost]
        public IHttpActionResult RemoveObject(FReqestOneParam<Guid> id)
        {
            WorldBuildingItemView view = null;
            try
            {
#pragma warning disable 162
                for (int i = 0; i < 10; i++)
#pragma warning restore 162
                {
                    using (var db = new DataRepository())
                    {
                        //======================================
                        var item = User.Db.BuildingItemEntities.SingleOrDefault(p => p.PlayerId == UserId && p.ItemId == id);
                        if (item == null)
                        {
                            LoggerContainer.BuildingLogger.Error($"RemoveObject: {UserId} | ItemId: {id.Value} | error: item was null");
                            return NotFound();
                        }
                        else
                        {
                            view = item.AsView();
                        }

                        //======================================
                        var instance = PlayerAccountEntity.InventoryItemList.FirstOrDefault(b =>  b.ModelId == item.ModelId && b.CategoryTypeId == item.CategoryTypeId);
                        if (instance == null)
                        {
                            try
                            {
                                instance = PlayerInventoryItemEntity.Factory(UserId, item.ModelEntity);
                                PlayerAccountEntity.InventoryItemList.Add(instance);
                                User.Db.SaveChanges();
                                LoggerContainer.BuildingLogger.Trace($"RemoveObject: {UserId} | ItemId: {id.Value} | ModelId: {item.BuildingModelId} [{item.ModelId}] | error: instance was null, created new!");
                            }
                            catch (Exception e)
                            {
                                LoggerContainer.BuildingLogger.WriteExceptionMessage($"RemoveObject[0]: {UserId} | ItemId: {id.Value} | ModelId: {item.BuildingModelId} [{item.ModelId}] | error: instance was null, created failed.", e);
                                return BadRequest();
                            }
                        }

                        if (instance != null)
                        {
                            instance.Amount++;
                        }

                        try
                        {
                            User.Db.Entry(item).State = EntityState.Detached;
                            User.Db.Entry(instance).State = EntityState.Detached;

                            db.Entry(item).State = EntityState.Deleted;
                            db.Entry(instance).State = EntityState.Modified;

                            //======================================
                            db.SaveChanges();

                            //User.Db.ObjectContext().DeleteObject(item);
                            //User.Db.BuildingItemEntities.Remove(item);
                            //User.Db.SaveChanges();
                            //======================================
                            return Json(view);
                        }
                        catch (Exception e)
                        {
                            LoggerContainer.BuildingLogger.WriteExceptionMessage($"RemoveObject[1]: {UserId} | ItemId: {id.Value} | ModelId: {item.BuildingModelId} [{item.ModelId}]", e);
                            throw;
                        }
                    }
                }
                throw new Exception("wtf?");
            }
            catch (Exception e)
            {
                LoggerContainer.BuildingLogger.WriteExceptionMessage($"RemoveObject[2]: {UserId} | ItemId: {id.Value}", e);
                return BadRequest(JsonConvert.SerializeObject(id));
            }
        }
    }
}