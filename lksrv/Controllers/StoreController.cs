﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Loka.Common.Achievements;
using Loka.Common.Building.Handlers;
using Loka.Common.Building.Player;
using Loka.Common.Company.Company;
using Loka.Common.Company.Mission;
using Loka.Common.Exceptions;
using Loka.Common.Player.Inventory;
using Loka.Common.Player.Profile.Profile;
using Loka.Common.Player.Profile.Slot;
using Loka.Common.Store;
using Loka.Server.Controllers.StoreController.Operation;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Fraction;
using Loka.Common.Store.Order;
using Loka.Common.Transaction;
using Loka.Infrastructure;
using Loka.Player;
using Loka.Server.Infrastructure;
using Loka.Common.Tutorial;
using Loka.Server.Infrastructure.Store;
using Loka.Server.Models.Store.Request;
using Loka.Server.Models.Store.Response;
using Loka.Server.Models.Tournament;
using Loka.Server.Player.Models;
using StackExchange.Redis;
using VRS.Infrastructure;

namespace Loka.Server.Controllers.StoreController
{

    public class OrderDeliveryNotifyModel
    {
        public Guid OrderId { get; set; }
        public long CreatedDate { get; set; }

        public short ModelId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }

        public long Amount { get; set; }
        public long LastAmount { get; set; }

        public OrderDeliveryNotifyModel()
        {
            
        }

        public OrderDeliveryNotifyModel(OrderTransactionEntity entity)
        {
            OrderId = entity.OrderId;
            CreatedDate = entity.CreatedDate.ToUnixTime();

            ModelId = entity.ModelId;
            CategoryTypeId = entity.CategoryTypeId;

            Amount = entity.Amount;
            LastAmount = entity.LastAmount;
        }
    }

    public class StoreDeliveryNotifyModel
    {
        public Guid SenderId { get; set; }
        public string SernderName { get; set; }

        public long CreatedDate { get; set; }

        public short ModelId { get; set; }
        public CategoryTypeId CategoryTypeId { get; set; }

        public long Amount { get; set; }

        public StoreDeliveryNotifyModel()
        {

        }

        public StoreDeliveryNotifyModel(StoreTransactionEntity entity)
        {
            SenderId = entity.AccountId;
            SernderName = entity.AccountEntity.PlayerName;


            CreatedDate = entity.CreatedDate.ToUnixTime();

            ModelId = entity.ModelId;
            CategoryTypeId = entity.CategoryTypeId;

            Amount = entity.Amount;
        }
    }

    public partial class StoreController : AbstractApiController
    {
	    [AllowAnonymous, HttpGet]
	    public IHttpActionResult ResetTutorials()
	    {
		    using (var db = new DataRepository())
		    {
		        var tutorials = db.PlayerTutorialEntities.Include(p => p.PlayerEntity).ToArray();
		        foreach (var tutorial in tutorials)
		        {
		            tutorial.PlayerEntity.CurrentTutorialId = null;

		        }

		        db.PlayerTutorialEntities.RemoveRange(tutorials);
                return Ok(db.SaveChanges());
		    }
	    }




		[Authorize, HttpGet]
        public IHttpActionResult OnOrderDeliveryNotify()
        {
            //==============================
            var orders = User.Db.OrderTransactionEntities.Where(p => p.Notified == false && p.AccountId == UserId).ToArray();

            //==============================
            foreach (var order in orders)
            {
                order.Notified = true;
            }

            //==============================
            var models = orders.Select(p => new OrderDeliveryNotifyModel(p)).ToArray();

            return Json(models);
        }


        [Authorize, HttpGet]
        public IHttpActionResult OnStoreDeliveryNotify()
        {
            //==============================
            var orders = User.Db.StoreTransactionEntities.Where(p => p.Notified == false && p.RecipientId == UserId && p.RecipientId != p.AccountId).ToArray();

            //==============================
            foreach (var order in orders)
            {
                order.Notified = true;
            }

            //==============================
            var models = orders.Select(p => new StoreDeliveryNotifyModel(p)).ToArray();

            return Json(models);
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult OrderDelivery(OrderDeliveryRequest request)
        {
            var response = new OrderDeliveryResponse(request);
            //------------------------------------------------------
            LoggerContainer.StoreLogger.Info($"[OrderDelivery][{request.OderId}]");

            try
            {
                using (var db = new DataRepository())
                {
                    var delivery = db.OrderDeliveryEntities.SingleOrDefault(p => p.EntityId == request.OderId) ?? db.OrderDeliveryEntities.Add(new OrderDeliveryEntity
                    {
                        EntityId = request.OderId,
                        CreatedDate = DateTime.UtcNow,
                        CreatorId = request.AccountId,
                        Status = OrderDeliveryStatus.None,
                        ProductEntities = request.Items.Select(e => new OrderProductDeliveryEntity(e.EntityId, e.Item.Amount, e.Item.ModelId, e.Item.ModelCategoryId)).ToArray()
                    });

                    //============================================
                    if (delivery.Status == OrderDeliveryStatus.DeliveredSuccessfully)
                    {
                        LoggerContainer.StoreLogger.Info($"[OrderDelivery][{request.OderId}][DeliveredEarlier]");
                        response.Status = OrderDeliveryStatus.DeliveredEarlier;
                        return Json(response);
                    }

                    //============================================
                    var player = db.AccountPlayerEntity.FirstOrDefault(p => p.PlayerId == request.AccountId);
                    if (player == null)
                    {
                        //-------------------------------------------------------
                        delivery.SwitchStatus(OrderDeliveryStatus.PlayerNotFound);
                        response.Status = OrderDeliveryStatus.PlayerNotFound;

                        //-------------------------------------------------------
                        LoggerContainer.StoreLogger.Info($"[OrderDelivery][{request.OderId}][PlayerNotFound]");

                        //-------------------------------------------------------
                        return Json(response);
                    }

                    //============================================
                    if (request.Items.Any() == false)
                    {
                        //-------------------------------------------------------
                        delivery.SwitchStatus(OrderDeliveryStatus.ItemsWasEmpty);
                        response.Status = OrderDeliveryStatus.ItemsWasEmpty;

                        //-------------------------------------------------------
                        LoggerContainer.StoreLogger.Info($"[OrderDelivery][{request.OderId}][ItemsWasEmpty]");
                        
                        //-------------------------------------------------------
                        return Json(response);
                    }

                    //============================================
                    var avalible = delivery.ProductEntities.Where(p => p.Status != OrderItemDeliveryStatus.Delivered).ToArray();
                    LoggerContainer.StoreLogger.Info($"[OrderDelivery][{request.OderId}][Items: {request.Items.Count} / {avalible.Length}]");

                    //============================================
                    foreach (var product in avalible)
                    {
                        var instance = db.StoreItemInstance.FirstOrDefault(p => p.CategoryTypeId == product.CategoryTypeId && p.ModelId == product.ItemModel);
                        if (instance == null)
                        {
                            //-------------------------------------------------------
                            product.SwitchStatus(OrderItemDeliveryStatus.EntityNotFound);

                            //-------------------------------------------------------
                            LoggerContainer.StoreLogger.Info($"[OrderDelivery][{request.OderId}][EntityNotFound][{product.CategoryTypeId} -- {product.ItemModel}]");

                            //-------------------------------------------------------
                            response.AddItem(product.EntityId, product.ItemModel, OrderItemDeliveryStatus.EntityNotFound);
                        }
                        else
                        {
                            try
                            {
                                ICountable playeritem;
                                if (instance.OnDelivery(db, player, product.Amount, out playeritem))
                                {
                                    LoggerContainer.StoreLogger.Info($"[OrderDelivery][qq][{request.OderId}][product: {product.EntityId} | amount: {product.Amount}][Model: {product.ItemModel}] -- Delivered");

                                    //========================================================
                                    db.OrderTransactionEntities.Add(new OrderTransactionEntity(player.PlayerId, instance.ModelId, instance.CategoryTypeId, product.Amount, playeritem.Count));

                                    //========================================================
                                    LoggerContainer.StoreLogger.Info($"[OrderDelivery][aa][{request.OderId}][product: {product.EntityId} | amount: {product.Amount}][Model: {product.ItemModel}] -- Delivered");

                                    //-------------------------------------------------------
                                    response.AddItem(product.EntityId, product.ItemModel, OrderItemDeliveryStatus.Delivered).InvoiceId = playeritem?.EntityId ?? Guid.NewGuid();
                                    product.SwitchStatus(OrderItemDeliveryStatus.Delivered);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    //-------------------------------------------------------
                                    LoggerContainer.StoreLogger.Info($"[OrderDelivery][{request.OderId}][product: {product.EntityId} | amount: {product.Amount}][Model: {product.ItemModel}] -- DeliveryFailed");

                                    //-------------------------------------------------------
                                    response.AddItem(product.EntityId, product.ItemModel, OrderItemDeliveryStatus.DeliveryFailed).InvoiceId = playeritem?.EntityId ?? Guid.NewGuid();
                                    product.SwitchStatus(OrderItemDeliveryStatus.DeliveryFailed);
                                }
                            }
                            catch (Exception e)
                            {
                                LoggerContainer.StoreLogger.WriteExceptionMessage($"[OrderDelivery][{request.OderId}][product][Exception]", e);
                                response.AddItem(product.EntityId, product.ItemModel, OrderItemDeliveryStatus.DeliveryFailed).Message = e.ToString();
                                product.SwitchStatus(OrderItemDeliveryStatus.DeliveryFailed);
                                product.Exception = e.ToString();
                            }
                        }

                        //============================================
                    }

					//============================================
	                UserBitcoinRepository.Update(player);

					//------------------------------------------------------
					if (response.Items.Any(p => p.Status != OrderItemDeliveryStatus.Delivered))
                    {
                        delivery.SwitchStatus(OrderDeliveryStatus.DeliveredConflicted);
                        response.Status = OrderDeliveryStatus.DeliveredConflicted;
                    }
                    else
                    {
                        delivery.SwitchStatus(OrderDeliveryStatus.DeliveredSuccessfully);
                    }
                }
            }
            catch (Exception exception)
            {
                LoggerContainer.StoreLogger.WriteExceptionMessage($"[OrderDelivery][{request.OderId}][Exception]", exception);
                response.Status = OrderDeliveryStatus.DataBaseError;
                response.Message = exception.ToString();
                return Json(response);
            }

            //------------------------------------------------------
            return Json(response);
        }


        //[AllowAnonymous, HttpGet]
        //public IHttpActionResult WipePlayerAccount(string password, int minute)
        //{
        //    if (password != "sentike" || minute != DateTime.UtcNow.Minute)
        //    {
        //        return BadRequest("bad pass");
        //    }
        //
        //    using (var db = new DataRepository())
        //    {
        //        foreach (var player in db.AccountPlayerEntity.ToArray())
        //        {
        //            player.Cash.Money = 5000;
        //            player.Experience.Level = 0;
        //            player.Experience.Experience = 0;
        //            foreach (var fraction in player.ReputationList)
        //            {
        //                fraction.CurrentRank = fraction.LastRank = 1;
        //                fraction.Reputation = 0;
        //            }
        //        }
        //        
        //        db.PlayerInventortItem.RemoveRange(db.PlayerInventortItem.Where(i => i.ItemModelEntity.Level > 1 && i.CategoryTypeId != CategoryTypeId.Character));
        //        return Ok(db.SaveChanges());
        //    }
        //}

        public class SellItemRequest
        {
            public Guid ItemId { get; set; }
            public long Amount { get; set; }
        }

        public class SellItemResponse
        {
            public Guid ItemId { get; set; }
            public StoreItemCost Summ { get; set; }
            public long Amount { get; set; }
            public long Selled { get; set; }
        }


        [Authorize, HttpPost]
        public IHttpActionResult SellItem(SellItemRequest request)
        {
            //=============================================================================
            if (request.Amount <= 0)
            {
                LoggerContainer.StoreLogger.Warn($"Invalid amount: {request.Amount}");
                return BadRequest($"Invalid amount: {request.Amount}");
            }

            //=============================================================================
            var item = PlayerAccountEntity.InventoryItemList.SingleOrDefault(p => p.ItemId == request.ItemId);
            
            //=============================================================================
            if (item == null)
            {
                LoggerContainer.StoreLogger.Warn($"item[{request.ItemId}] was null, amount: {request.Amount}");
                return NotFound();
            }

            //=============================================================================
            //if (item.ItemModelEntity.Flags.HasFlag(ItemInstanceFlags.AllowSelling) == false)
            //{
            //    //return BadRequest("Sell not allowed");
            //    return Conflict();
            //}

            //=============================================================================
            var selling = Math.Min(item.Amount, request.Amount);
            var cost = item.ItemModelEntity.Cost.Amount * 0.5;
            var summ = new StoreItemCost(selling * cost, item.ItemModelEntity.Cost.Currency);

            //=============================================================================
            PlayerAccountEntity.Give(summ);
            item.Amount = Math.Max(0, item.Amount - selling);

            //=============================================================================
            try
            {
                User.Db.SaveChanges();
            }
            catch (Exception e)
            {
                LoggerContainer.StoreLogger.WriteExceptionMessage($"[SellItem][{request.ItemId}]", e);
                throw;
            }

            //=============================================================================
            LoggerContainer.StoreLogger.Warn($"[SellItem][{request.ItemId}] - [{item.ItemId}] | selling: {selling} | Count: {item.Amount}");


            //=============================================================================
            return Json(new SellItemResponse
            {
                ItemId = item.ItemId,
                Amount = item.Amount,
                Selled = selling,
                Summ = summ
            });
        }

        [Authorize, HttpPost]
        public IHttpActionResult BuyItem(OperationUnlockItem.Request request)
        {
            try
            {
                PlayerEntity recipient = null;
                //=============================================================================
                var instance = User.Db.StoreItemInstance.SingleOrDefault(i => i.ModelId == request.ItemId && i.CategoryTypeId == request.ItemType);
                if (instance == null)
                {
                    throw new ObjectNotFoundException(600, "Item Instance not found", new ArgumentNullException(nameof(instance), request.ToString()));
                }

                //=============================================================================
                var buyer = PlayerAccountEntity;
                if (request.Target == null || request.Target == Guid.Empty)
                {
                    recipient = buyer;
                }
                else
                {
                    recipient = User.Db.AccountPlayerEntity.SingleOrDefault(p => p.PlayerId == request.Target) ?? buyer;
                }

                //=============================================================================
                LoggerContainer.StoreLogger.Trace($"[BuyItem][From {buyer.PlayerName} to {recipient.PlayerName}[{request.Target}]] : {request.ItemType} / {request.ItemId} | Amount: {request.Amount}");



                //=============================================================================
                long bought = 0;
                var item = instance.Buy(buyer, recipient, request.Amount, out bought);
                User.Db.StoreTransactionEntities.Add(new StoreTransactionEntity(UserId, instance.ModelId, instance.CategoryTypeId, bought, item.LastAmount)
                {
                    SessionId = PlayerAccountEntity.LastSessionId,
                    RecipientId = recipient.PlayerId,
                    Currency = instance.Cost.Currency,
                    Cost = instance.Cost.Amount,
                    Balanace = PlayerAccountEntity.TakeCash(instance.Cost.Currency).LastAmount,
                });

                //=============================================================================

#if DEBUG
                if (instance.CategoryTypeId != item.CategoryTypeId)
                {
                    throw new ApplicationException($"Instance item category [{instance.CategoryTypeId}] not equeal player cat {item.CategoryTypeId} | {instance.ModelIdName}");
                }
#endif
                //=============================================================================
                return Json(new FUnlockItemResponseContainer(item, request.Amount, bought));
            }
            catch (OperationException exception)
            {
                LoggerContainer.StoreLogger.WriteExceptionMessage($"[BuyItem][{PlayerAccountEntity.PlayerName}] : {request.ItemType} / {request.ItemId}", exception);
                return Content((HttpStatusCode) exception.ResponseCode, exception.ResponseData);
            }
        }

        public class UnlockProfileResponse : PlayerProfileEntityContainer
        {
            public PlayerProfileTypeId ProfileSlotId;

            public UnlockProfileResponse(PlayerProfileTypeId PlayerProfileTypeId, PlayerProfileEntity profileInstance) 
                : base(profileInstance) 
            {
                ProfileSlotId = PlayerProfileTypeId;
                Items = profileInstance.Items.Select(i => new PlayerInventoryItemContainer(i.ItemId, i.SlotId)).ToArray();
            }
        }

        public IHttpActionResult BuyProfile()
        {
            var count = (PlayerProfileTypeId) PlayerAccountEntity.InventoryProfileList.Count();
            if (count < PlayerProfileTypeId.Three)
            {
                var profileInstance = PlayerProfileEntity.Factory(PlayerAccountEntity.PlayerId,
                    PlayerAccountEntity.CharacterList.First().ItemId, count, true);

                PlayerAccountEntity.InventoryProfileList.Add(profileInstance);
                User.Db.AccountPlayerPreSetEntity.Add(profileInstance);

                return Json(new UnlockProfileResponse(count, profileInstance));
            }
            else if (PlayerAccountEntity.PremiumEndDate.HasValue && count < PlayerProfileTypeId.Five &&
                        PlayerAccountEntity.PremiumEndDate.Value.AddSeconds(10) < DateTime.UtcNow)
            {
                var profileInstance = PlayerProfileEntity.Factory(PlayerAccountEntity.PlayerId,
                    PlayerAccountEntity.CharacterList.First().ItemId, count, false);

                PlayerAccountEntity.InventoryProfileList.Add(profileInstance);
                User.Db.AccountPlayerPreSetEntity.Add(profileInstance);
                return Json(new UnlockProfileResponse(count, profileInstance));
            }
            else if (count < PlayerProfileTypeId.End)
            {
                if (PlayerAccountEntity.Pay(new StoreItemCost() {Amount = 50, Currency = GameCurrency.Donate}))
                {
                    var profileInstance = PlayerProfileEntity.Factory(PlayerAccountEntity.PlayerId,
                        PlayerAccountEntity.CharacterList.First().ItemId, count, true);

                    PlayerAccountEntity.InventoryProfileList.Add(profileInstance);
                    User.Db.AccountPlayerPreSetEntity.Add(profileInstance);
                    return Json(new UnlockProfileResponse(count, profileInstance));
                }
                else return new StatusCodeResult(HttpStatusCode.PaymentRequired, this);
            }
            return Conflict();
        }


        [AllowAnonymous, HttpGet]
        //[CacheOutput(ClientTimeSpan = int.MaxValue, ServerTimeSpan = int.MaxValue)]
        public IHttpActionResult ListOfInstance()
        {
            using (var db = new DataRepository())
            {
                var res = new OperationListOfInstance.Response
                {
                    Tutorials = db.TutorialInstanceEntities.ToArray().AsView(),
                    //GameModeProperties = db.GameModeEntity.ToArray().Select(gm => new GameModePropertyContainer(gm)).ToArray(),
                    Achievements = db.Achievements.ToArray().Select(a => new OperationListOfInstance.AchievementContainer(a)).ToArray(),
                    Items = db.StoreItemInstance.ToArray().Select(i => new ItemBaseContainer(i)).ToArray(),
                    Fraction = db.StoreFractionEntities.ToArray().Select(i => new FractionContainer(i)),
                    ScenarioMissions = db.ScenarioMissionEntities.ToList().AsView(),
                    Tournament = new OperationListOfInstance.TournamentContainer
                    {
                        Start = TournamentSettings.TournamentStartDate.ToUnixTime(),
                        End = TournamentSettings.TournamentEndDate.ToUnixTime(),
                    }
                };
                return Json(res);
            }
        }
            
    }
}