﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Loka.Common.Infrastructure;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.Match;
using Loka.Common.Player.Statistic;
using Loka.Infrastructure;



using Loka.Server.Controllers.IdentityController.Operation;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Queue;
using Loka.Server.Player.Models;
using Loka.Server.Service.Player;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using RestSharp;
using SteamSharp;
using VRS.Infrastructure;
using VRS.Infrastructure.Environment;
using VRS.Infrastructure.IpAddress;

namespace Loka.Server.Controllers.IdentityController
{
    public partial class IdentityController : AbstractApiController
    {
        #region Steam
//        [AllowAnonymous, HttpPost]
//        public async Task<IHttpActionResult> OnSteamAuthentication(FReqestOneParam<string> steamId)
//        {
//            return new StatusCodeResult(HttpStatusCode.PaymentRequired, this);
//#pragma warning disable 162
//            try
//            {
//                using (var db = new ApplicationDbContext())
//                {
//                    var steamProviderKey = $"http://steamcommunity.com/openid/id/{steamId.Value}";

//                    SteamSharp.SteamClient c = new SteamSharp.SteamClient
//                    {
//                        Authenticator =
//                            SteamSharp.Authenticators.APIKeyAuthenticator.ForProtectedResource(
//                                "178F53C45855C4341AAA4DD2D4BA4507")
//                    };

//                    var taskOfGetsteamUserLogin = db.UserLogins.AsNoTracking().FirstOrDefaultAsync(
//                        u => u.LoginProvider == "Steam" && u.ProviderKey == steamProviderKey);

//                    var taskOfGetownedGames = SteamSharp.AppOwnership.GetOwnedApplicationsAsync(c, steamId);

//                    var ownedGames = (await taskOfGetownedGames).Apps.Application ?? new List<App>();
//                    var lokaGame = ownedGames.FirstOrDefault(g => g.ApplicationId == 488720);

//                    Logger.Info($"Attemp login by {steamId.Value}, owned: {lokaGame != null}");

//                    if (lokaGame == null)
//                    {
//                        return new StatusCodeResult(HttpStatusCode.PaymentRequired, this);
//                    }

//                    //==================================================================
//                    var steamUserLogin = await taskOfGetsteamUserLogin;
//                    if (steamUserLogin == null)
//                    {
//                        var id = Guid.NewGuid();
//                        var result = UserManager.Create(new ApplicationUser()
//                        {
//                            Id = id,
//                            UserName = steamId,
//                            Country = UserCountry,
//                            Logins =
//                            {
//                                new ApplicationUserLogin
//                                {
//                                    LoginProvider = "Steam",
//                                    ProviderKey = steamProviderKey,
//                                    UserId = id,
//                                }
//                            }
//                        });

//                        if (result.Succeeded)
//                        {
//                            try
//                            {
//                                using (var mark = new MarketingDbContext())
//                                {
//                                    mark.RegistrationEntities.Add(new RegistrationEntity(id, UserCountry));
//                                }
//                            }
//                            catch (Exception exception)
//                            {
//                                Logger.Error($"RegistrationEntities: {id} | {exception}");
//                            }
//                            return OnIdentification(steamId, true, db);
//                        }
//                        else
//                        {
//                            Logger.Error($"Failed identity {steamId.Value}: {string.Join(",", result.Errors)}");

//                        }
//                        return Json(new Authentication.Response(Guid.Empty, result.Errors));
//                    }
//                    else
//                    {
//                        var userAccount = db.Users.AsNoTracking().First(u => u.Id == steamUserLogin.UserId);
//                        return OnIdentification(userAccount.UserName, true, db);
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Logger.Error(e);
//                return Json(new Authentication.Response(Guid.Empty, new List<string> { e.ToString() }));
//            }
//#pragma warning restore 162
//        }
        #endregion

        //=====================================================================================================

        #region CreateGameAccount

        [Authorize, HttpGet]
        public IHttpActionResult IsExistGameAccount()
        {
            if (CheckIsExistGameAccount())
            {
                return Ok();
            }
            return NotFound();
        }

        //=====================================================================================================
        public readonly string[] BadPlayerNames = new[]
        {
            "Админ", "Администратор", "Admin", "Adminstrator", "Аdminstrator", "Аdminstrаtor", "Adminstrаtor", "Аdminstrator",
            "Moder", "Moderator", "Модератор", "Разработчик", "Developer", "Хуй", "Пизда", "xui", "член", "пенис", "вагина", "пизда",
            "сосать", "сосите", "хуйня", "залупа", "хуита", "дрочь", "конченная", "superadmin", "root", "бляд", "бля", "выебать", "ебать", "ебал",
            "говно",  "гандон", "гaндон", "гaндoн", "гандoн", "гoндон", "гoндон", "гoндoн", "гoндoн", "хуесос", "манда", "мандавошка", "сосать"
        };


        [Authorize, HttpPost]
        public IHttpActionResult CreateGameAccount(CreateGameAccount.Request playerName)
        {
            try
            {
                if (CheckIsExistGameAccount())
                {
                    return Json(new CreateGameAccount.Response(true));
                }
                else if (string.IsNullOrWhiteSpace(playerName) || playerName.Value.Length < 4 || playerName.Value.Length > 24)
                {
                    return BadRequest();
                }
                else if (BadPlayerNames.Any(n => n.Equals(playerName.Value, StringComparison.OrdinalIgnoreCase)))
                {
                    return BadRequest();
                }
                else
                {
                    if (PlayerRepository.IsExistLogin(playerName) == false)
                    {
                        if (CheckIsExistGameAccount())
                        {
                            return Json(new CreateGameAccount.Response(true));
                        }
                        else
                        {
                            PlayerRole role = PlayerRole.None;
                            if (PlayerRepository.CreateGameAccount(playerName, UserId, null, out role))
                            {
                                PlayerNotifyService.UpdateUserName(UserId, playerName);
                                return Json(new CreateGameAccount.Response(true));
                            }
                            else return Conflict();
                        }
                    }
                }
                return Json(new CreateGameAccount.Response(false));
            }
            catch (Exception exception)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[CreateGameAccount][{playerName}]", exception);
                throw;
            }
        }

        private bool CheckIsExistGameAccount()
        {
            try
            {
                var exist = PlayerRepository.IsExistIndex(UserId);
                LoggerContainer.AccountLogger.Trace($"[CheckIsExistGameAccount][{UserId}][Exist: {exist}]");
                return exist;
            }
            catch (Exception exception)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[CheckIsExistGameAccount][{UserId}]", exception);
                throw;
            }
        }
        #endregion

        //=====================================================================================================

        #region Authentication


        public enum LoginFromRemoteStatus
        {
            LoginNotFound,
            LoginWasExist,

            InvalidLogin,
            InvalidPassword,

            Successfully,

            //-----------------
            BadRequest,
            IsNotAllowed,
            IsLockedOut,
            RequiresTwoFactor,

            //-----------------
            SessionExist,

            //-----------------
            GameNotPurchased,
            GameNotAvailable,
        }

        public class LoginFromRemoteResponse
        {
            public Guid? AccountId { get; set; }
            public string AccountName { get; set; }
            public string[] Roles { get; set; }

            public LoginFromRemoteStatus Status { get; set; }
            public LoginFromRemoteResponse() { }

            public LoginFromRemoteResponse(Guid token, LoginFromRemoteStatus status, string name = null)
            {
                AccountName = name;
                AccountId = token;
                Status = status;
            }

            public LoginFromRemoteResponse(LoginFromRemoteStatus status)
            {
                Status = status;
            }

        }

        [AllowAnonymous, HttpPost]
        public IHttpActionResult OnAuthentication(Authentication.Request credential)
        {
            LoggerContainer.AccountLogger.Info($"OnAuthentication: {credential}");
            try
            {

                //----------------------------------------------------------
                var client = new RestClient(ServiceUrl("Account/LoginFromRemote"));
                var request = new RestRequest(Method.POST);

                //----------------------------------------------------------
                request.AddJsonBody(credential);
                var rest = client.Execute(request);

                //----------------------------------------------------------
                var response = JsonConvert.DeserializeObject<LoginFromRemoteResponse>(rest.Content);

                //----------------------------------------------------------
                if (response.AccountId == null || response.Status != LoginFromRemoteStatus.Successfully)
                {
                    return Ok(new LoginFromRemoteResponse(response.Status));
                }

                //----------------------------------------------------------
                return Ok(OnIdentification(credential, response));

            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"OnAuthentication: {credential}", e);
                throw;
            }
        }

        [Authorize, HttpPost]
        public IHttpActionResult OnLogout()
        {
            //PlayerInstance.Lastdate = DateTime.UtcNow;
            //_playerRepository.Save();
            return Ok();
        }

        [NonAction]
        private LoginFromRemoteResponse OnIdentification(Authentication.Request credential, LoginFromRemoteResponse response)
        {
            //=======================================
            if (response.AccountId == null)
            {
                throw new ArgumentNullException(nameof(response.AccountId));
            }

            //=======================================
            var accountId = response.AccountId.Value;


            //=======================================
            try
            {
                using (var gdb = new DataRepository())
                {
                    PlayerRole role = PlayerRole.None;
                    //=======================================
                    if (string.IsNullOrWhiteSpace(response.AccountName) == false)
                    {
                        if (PlayerRepository.IsExistIndex(accountId) == false)
                        {
                            if (response.AccountName.IsEmailAddress() == false && PlayerRepository.IsExistLogin(response.AccountName) == false)
                            {
                                var result = PlayerRepository.CreateGameAccount(response.AccountName, accountId, response.Roles, out role);
                                if(result == false)
                                {
                                    LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][AccountName not created]");
                                }
                            }
                            else
                            {
                                LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][AccountName was exist]");
                            }
                        }
                        else
                        {
                            PlayerRepository.UpdateUserRoles(accountId, response.Roles, out role);
                        }
                    }
                    else
                    {
                        LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][AccountName was null]");
                    }

                    //=======================================
                    LoggerContainer.AccountLogger.Warn($"[OnIdentification][{credential}][Role: {role} / {string.Join(",",  response.Roles)} | IsAdministrator: {role.HasFlag(PlayerRole.Administrator)}]");
                    
                    if (role.HasFlag(PlayerRole.Administrator) == false && role.HasFlag(PlayerRole.BetaTester) == false)
                    {
                        return new LoginFromRemoteResponse(LoginFromRemoteStatus.GameNotPurchased);
                    }

                    //=======================================
                    var sessions = gdb.GameSessionEntity.Where(p => p.PlayerId == accountId).ToArray();
                    var session = sessions.Where(p => p.SessionAttach == PlayerSessionAttach.Client).OrderByDescending(p => p.LastActivityDate).FirstOrDefault();

                    //=======================================
                    if (session != null)
                    {
                        var elapsed = session.LastActivityDate.ToSecondsTime();

                        //----------------------------------------------------------
                        //  Сбрасывание статуса готовности, если давно не был в игре
                        if (elapsed > 60)
                        {
                            var q = gdb.QueueEntity.SingleOrDefault(p => p.QueueId == accountId);
                            if (q != null)
                            {
                                q.Ready = QueueState.None;
                            }
                        }

                        //----------------------------------------------------------
                        if (elapsed > 10)
                        {
                            LoggerContainer.AccountLogger.Info($"OnIdentification: {accountId} | SessionExist | elapsed: {elapsed}");
                            return new LoginFromRemoteResponse(LoginFromRemoteStatus.SessionExist);
                        }
                    }

                    //=======================================
                    {
                        var servers = sessions.Where(p => p.SessionAttach == PlayerSessionAttach.Server && p.LastActivityDate.ElapsedMore(TimeSpan.FromHours(2))).ToArray();
                        gdb.GameSessionEntity.RemoveRange(servers);
                    }

                    //=======================================
                    {
                        var clients = sessions.Where(p => p.SessionAttach == PlayerSessionAttach.Client && p.LastActivityDate.ElapsedMore(TimeSpan.FromMinutes(10))).ToArray();
                        gdb.GameSessionEntity.RemoveRange(clients);
                    }

                    //=======================================
                    gdb.SaveChanges();
                }
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"OnIdentification: {accountId}", e);
                throw;
            }

            //========================================================================
            var sessionId = PlayerRepository.SessionBegin(accountId);

            //========================================================================
            return new LoginFromRemoteResponse(sessionId, LoginFromRemoteStatus.Successfully, response.AccountName);
        }
        #endregion


    }
}