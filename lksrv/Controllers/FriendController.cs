﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Loka.Infrastructure;
using Loka.Player;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Session;
using Loka.Server.Player.Infrastructure;
using Loka.Server.Player.Models;
using Loka.Server.Session;

using Ninject;
using VRS.Infrastructure;
using WebGrease.Css.Extensions;

namespace Loka.Server.Friend
{
    public class FriendController : ApiController
    {
        /// <summary>
        /// Search player by name
        /// </summary>
        /// <param name="playerName">name of target player</param>
        /// <returns>The list of Players that are packaged in a container PlayerFriend</returns>
        [Authorize, HttpPost]
        public IHttpActionResult Search(FReqestOneParam<string> playerName )
        {
            if(playerName.Value.Length >= 3 && playerName.Value.Length < 24)
            {
                using (var db = new DataRepository())
                {
                    var searchResult =
                        db.AccountPlayerEntity.AsNoTracking()
                            .Where(
                                p =>
                                    p.PlayerId != PlayerInstance.PlayerId &&
                                    p.PlayerName.ToLower().Contains(playerName.Value.ToLower()))
                            .ToArray()
                            .Select(p => new PlayerFriend(p))
                            .ToArray();
                    return Json(PlayerInstance.FriendList.Select(f => new PlayerFriend(f)).ToArray().Concat(searchResult).ToArray());
                }
            }
            return BadRequest();
        }

        public struct AddFriendRequestContainer
        {
            public Guid PlayerId { get; set; }
            public bool IsBlackList { get; set; }
        }

        public struct AddFriendResponseContainer
        {
            public AddFriendResponseContainer(AddFriendRequestContainer request, string playerName)
            {
                PlayerId = request.PlayerId;
                IsBlackList = request.IsBlackList;
                PlayerName = playerName;
            }

            public Guid PlayerId { get; set; }
            public string PlayerName { get; set; }
            public bool IsBlackList { get; set; }
        }

        /// <summary>
        /// Directs the player with the given name of request as a Friend
        /// </summary>
        /// <param name="request">name of target player</param>
        /// <returns>Conflict - if the player is already in your list of friends
        /// BadRequest - If a player with the same name can not be found
        /// OK - if the request is submitted and is awaiting approval</returns>
        [Authorize, HttpPost]
        public IHttpActionResult Add(FReqestOneParam<Guid> request)
        {
            //----------------------------------------------------------------------------------------------
            if (PlayerInstance.PlayerId == request)
            {
                return Conflict();
            }

            //----------------------------------------------------------------------------------------------
            //  Check exist player in friend list
            if (PlayerInstance.FriendList.Any(f => f.FriendEntity.PlayerId == request))
            {
                return Conflict();
            }

 
            //----------------------------------------------------------------------------------------------
            //  Attemp search player in DB
            var friendInstance  = User.Db.AccountPlayerEntity.SingleOrDefault(p => p.PlayerId == request);
            if (friendInstance == null)
            {
                return BadRequest();
            }

            //----------------------------------------------------------------------------------------------
            User.Db.PlayerFriendEntity.Add(PlayerFriendEntity.Factory(friendInstance, PlayerInstance, ActionPlayerRequestState.ConfirmedWaiting));
            User.Db.PlayerFriendEntity.Add(PlayerFriendEntity.Factory(PlayerInstance, friendInstance, ActionPlayerRequestState.Waiting));

            User.Db.SaveChanges();

            return Json(new FReqestOneParam<string>(friendInstance.PlayerName));         
        }
          
        [Authorize, HttpPost]
        public IHttpActionResult Remove(FReqestOneParam<Guid> friendId)
        {
            var friends = new[]
            {
                User.Db.PlayerFriendEntity.SingleOrDefault(p => p.PlayerId == PlayerInstance.PlayerId && p.FriendId == friendId),
                User.Db.PlayerFriendEntity.Include(p => p.PlayerEntity).SingleOrDefault(p => p.PlayerId == friendId && p.FriendId ==  PlayerInstance.PlayerId),
            };


            var res = new FReqestOneParam<string>(friends[1]?.PlayerEntity?.PlayerName ?? "Unk");
            User.Db.PlayerFriendEntity.RemoveRange(friends);
            User.Db.SaveChanges();

            //  Remove Current Player from Friend 
            return Json(res);
        }

        public struct ConfirmAddRequest
        {
            public Guid PlayerId { get; set; }
            public bool IsAccept { get; set; }
            public override string ToString()
            {
                return $"[ConfirmAddRequest][PlayerId: {PlayerId}][IsAccept: {IsAccept}]";
            }
        }

        public struct AddFriendConfirmResponse
        {
            public AddFriendConfirmResponse(ConfirmAddRequest request, string playerName)
            {
                PlayerId = request.PlayerId;
                IsAccept = request.IsAccept;
                PlayerName = playerName;
            }

            public Guid PlayerId { get; set; }
            public string PlayerName { get; set; }
            public bool IsAccept { get; set; }
        }

        
        //
        [Authorize, HttpPost]
        public IHttpActionResult ConfirmAdd(ConfirmAddRequest request)
        {
            var friendInstance = PlayerInstance.FriendList.FirstOrDefault(f => f.FriendId == request.PlayerId && f.State == ActionPlayerRequestState.ConfirmedWaiting)
                ?? PlayerInstance.FriendList.FirstOrDefault(f => f.FriendId == request.PlayerId && f.State == ActionPlayerRequestState.Waiting);
            
            if (friendInstance == null)
            {
                return BadRequest();
            }

            var friendRequest = friendInstance.FriendEntity.FriendList.SingleOrDefault(f => f.FriendId == PlayerInstance.PlayerId && f.State == ActionPlayerRequestState.Waiting)
                ?? friendInstance.FriendEntity.FriendList.SingleOrDefault(f => f.FriendId == PlayerInstance.PlayerId && f.State == ActionPlayerRequestState.ConfirmedWaiting);
            if (friendRequest == null)
            {
                return BadRequest();
            }

            var response = new AddFriendConfirmResponse(request, friendInstance.PlayerEntity.PlayerName);


            LoggerContainer.StoreLogger.Warn($"[Friend][ConfirmAdd][request: {request}]");

            if (request.IsAccept)
            {
                friendInstance.State = ActionPlayerRequestState.Confirmed;
                friendRequest.State = ActionPlayerRequestState.Confirmed;
            }
            else
            {
                friendRequest.State = ActionPlayerRequestState.Rejected;
                PlayerInstance.FriendList.Remove(friendInstance);
                User.Db.PlayerFriendEntity.Remove(friendInstance);
            }

            var qq = User.Db.SaveChanges();

            return Json(response);
        }


        /// <summary>
        /// The list of all your friends that are packaged in a container PlayerFriend
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet]
        public IHttpActionResult GetList()
        {
            try
            {
                return Json
                (
                    PlayerInstance.FriendList.Where(f => f.FriendEntity != null).Select(p => new PlayerFriend(p)).ToArray()
                    .Concat(OnlinePlayersBuffer.Instance.CkeckUpdate())
                );
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return Ok();
            }
        }

        private static PlayerEntity PlayerInstance => User?.PlayerInstance;
        private new static ClientPrincipal User => HttpContext.Current.User as ClientPrincipal;

        protected static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
    }
}
