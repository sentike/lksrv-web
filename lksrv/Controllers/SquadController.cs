﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Loka.Common.Match.GameMode;
using Loka.Common.Player.Statistic;
using Loka.Infrastructure;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.Queue;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Models;
using Loka.Server.Models.Queue;
using Loka.Server.Service.Session;
using Loka.Server.Session;
using Ninject;
using VRS.Infrastructure;

namespace Loka.Server.Controllers
{
    public class SquadController : AbstractApiController
    {
        [Inject]
        public SessionRepository SessionRepository { get; set; }


        /// <summary>
        /// Начать поиск боя
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        [Authorize, HttpPost]
        public IHttpActionResult JoinToSearchQueue(SearchSessionOptions options)
        {
            try
            {
                LoggerContainer.SessionLogger.Trace($"SearchBegin[0] [{PlayerAccountEntity}][{options}] | {GameModeFlags.ResearchMatch}={(int)GameModeFlags.ResearchMatch}");
                SessionRepository.JoinToQueue(PlayerAccountEntity, options);
                return Ok();
            }
            catch (SessionSearchBeginException e)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage($"[SearchBegin][1]: {PlayerAccountEntity.PlayerName}", e);
                return new StatusCodeResult((HttpStatusCode)e.ResponseCode, this);
            }
            catch (Exception e)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage($"[SearchBegin][2]: {PlayerAccountEntity.PlayerName}", e);
                throw;
            }
        }


        /// <summary>
        /// Отменить поиск боя
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet]
        public IHttpActionResult LeaveFromSearchQueue()
        {
            SessionRepository.LeaveFromQueue(PlayerAccountEntity);
            return Ok();
        }

        /// <summary>
        /// Получить информацию об отряде и статусе поиска боя
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet]
        public IHttpActionResult GetSqaudInformation()
        {
            LoggerContainer.SessionLogger.Info($"[GetSqaudInformation][GameSessionProperty][{GameSessionProperty?.SessionAttach}]");
            SessionNotifyService.UpdatePlayerSession(GameSessionProperty);

            if (GameSessionProperty?.SessionAttach == PlayerSessionAttach.Client)
            {
                if (PlayerAccountEntity.QueueEntity.Ready == QueueState.Searching)
                {
                    SessionNotifyService.AddMatchMakingSession(PlayerAccountEntity,
                        NewMatchMakingSessionModel.MatchMakingSessionState.SearchGame);
                }
                else
                {
                    SessionNotifyService.AddMatchMakingSession(PlayerAccountEntity,
                        NewMatchMakingSessionModel.MatchMakingSessionState.Idle);
                }
            }
            else
            {
                SessionNotifyService.AddMatchMakingSession(PlayerAccountEntity,
                    NewMatchMakingSessionModel.MatchMakingSessionState.PlayGame);
            }

            try
            {
				//DataRepository.Entry(PlayerAccountEntity).Collection(p => p.MatchMembers).Load();
                return Json(SessionRepository.GetSqaudInforamtion(PlayerAccountEntity));
            }
            catch (Exception e)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage($"[GetSqaudInformation]: {PlayerAccountEntity.PlayerName}", e);
                throw;
            }
        }

        /// <summary>
        /// Отправить приглашение в отряд
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpPost]
        public IHttpActionResult SendSquadInvite(FSquadInviteInformation invite )
        {
            try
            {
                //=======================================================
                var sender = PlayerAccountEntity;
                var target = DataRepository.AccountPlayerEntity.SingleOrDefault(p => p.PlayerId == invite.PlayerId);

                //=======================================================
                //  Если игрок не найден
                if (target == null)
                {
                    throw new SendSquadInviteException(SendSquadInviteExceptionCode.PlayerNotFound);
                }

	            if (target.QueueEntity == null)
	            {
		            target.QueueEntity = new QueueEntity(target.PlayerId, target.PlayerId, SearchSessionOptions.Default, QueueState.None);
	            }

				//=======================================================
				//  Если игрок приглашаем сам себя
				if (sender.QueueEntity.QueueId == target.PlayerId)
                {
                    throw new SendSquadInviteException(SendSquadInviteExceptionCode.InviteYourself);
                }

                //=======================================================
                //  Если игрок в отряде и не лидер пытается пригласить другого игрока в отряд
                DataRepository.Entry(sender.QueueEntity).Collection(p => p.Members).Load();
                if (sender.QueueEntity.InSquad && sender.QueueEntity.IsLeader == false)
                {
                    throw new SendSquadInviteException(SendSquadInviteExceptionCode.YouAreNotLeader);
                }

                //=======================================================
                //  Если игрок уже отправил приглашение другому игроку
                var lastFiveSeconds = DateTime.UtcNow.AddSeconds(-6);
                if (DataRepository.SquadInviteEntities.Any(s => s.SquadId == sender.QueueEntity.QueueId && s.SubmittedDate >= lastFiveSeconds))
                {
                    throw new SendSquadInviteException(SendSquadInviteExceptionCode.InviteTimeoutSent);
                }

	            //=======================================================
	            //  Если игроки отправляют друг другу приглашения
				if (DataRepository.SquadInviteEntities.Any(s => s.SquadId == target.PlayerId && s.PlayerId == sender.PlayerId && s.SubmittedDate >= lastFiveSeconds))
	            {
		            throw new SendSquadInviteException(SendSquadInviteExceptionCode.InviteTimeoutSent);
	            }

                //=======================================================
                //  Если другой игрок не в сети
                if (target.LastActivityDate.ToElapsedSeconds() >= TimeSpan.FromMinutes(10).TotalSeconds)
                {
                    throw new SendSquadInviteExceptionWithValue(SendSquadInviteExceptionCode.PlayerNotOnline, target.PlayerName);
                }

                //=======================================================
                //  Если другой игрок уже в отряде
                DataRepository.Entry(target.QueueEntity).Collection(p => p.Members).Load();

                if (invite.Direction == SquadInviteDirection.Invite)
                {
                    if (target.QueueEntity.InSquad && target.QueueEntity.PartyId != sender.QueueEntity.PartyId)
                    {
                        throw new SendSquadInviteExceptionWithValue(SendSquadInviteExceptionCode.PlayerInSquad, target.PlayerName);
                    }
                }


                //=======================================================
                if (invite.Options.GameMode.HasFlag(GameModeFlags.DuelMatch))
                {
                    if (sender.IsAllowPay(invite.Options.Bet) == false)
                    {
                        throw new SendSquadInviteException(SendSquadInviteExceptionCode.YouAreNotEnouthMoney);
                    }

                    if (target.IsAllowPay(invite.Options.Bet) == false)
					{
						throw new SendSquadInviteExceptionWithValue(SendSquadInviteExceptionCode.PlayerNotEnouthMoney, target.PlayerName);
                    }
                }

                //=======================================================
                // Invite already sent in the squad
                if (DataRepository.SquadInviteEntities.Any(s => s.PlayerId == target.PlayerId && s.State == SquadInviteState.Submitted && s.SquadId == sender.QueueEntity.QueueId))
                {
                    throw new SendSquadInviteExceptionWithValue(SendSquadInviteExceptionCode.InviteAlreadySent, target.PlayerName);
                }

                //=======================================================
                DataRepository.SquadInviteEntities.Add(SquadInviteEntity.Factory(target.PlayerId, sender.QueueEntity.QueueId, invite.Direction, invite.Options));
                DataRepository.SaveChanges();

                //=======================================================
                invite.PlayerName = target.PlayerName;

                //=======================================================
                LoggerContainer.PaymentLogger.Info($"SendSquadInvite[0][{PlayerAccountEntity.PlayerName}]: {invite}");
                return Json(invite);
            }
            catch (SendSquadInviteExceptionWithValue e)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage(
                    $"[SendSquadInvite][0]: {PlayerAccountEntity.PlayerName}", e);
                return new NegotiatedContentResult<FReqestOneParam<string>>((HttpStatusCode)e.ResponseCode, e.Value,
                    this);
            }
            catch (SendSquadInviteException e)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage(
                    $"[SendSquadInvite][1]: {PlayerAccountEntity.PlayerName}", e);
                return new StatusCodeResult((HttpStatusCode)e.ResponseCode, this);
            }
            catch (Exception e)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage(
                    $"[SendSquadInvite][2]: {PlayerAccountEntity.PlayerName}", e);
                throw;
            }
        }

        /// <summary>
        /// Выкинуть игрока из отряда или покинуть его самому
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        [Authorize, HttpPost]
        public IHttpActionResult RemoveFromSquad(FReqestOneParam<Guid?> target)
        {
            try
            {
                return Json(SessionRepository.RemoveFromSquad(PlayerAccountEntity, target));
            }
            catch (RemoveFromSquadException exception)
            {
                return new StatusCodeResult((HttpStatusCode)exception.ResponseCode, this);
            }
        }

        /// <summary>
        /// Получить список приглашений
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpGet]
        public FSquadInviteInformation[] GetSquadInvites()
        {
            try
            {
                //  Automatically cancel all the notices that have been sent to more than 20 minutes ago
                foreach (var invite in PlayerAccountEntity.IncomingSquadInvites.Where(i => i.State == SquadInviteState.Submitted && i.SubmittedDate.ToElapsedSeconds() > 1200))
                {
                    invite.State = SquadInviteState.Received | SquadInviteState.Rejected;
                }

                var incomingNotifications = PlayerAccountEntity.IncomingSquadInvites.Where(i => i.State == SquadInviteState.Submitted).ToArray();
                var outgoingNotifications = PlayerAccountEntity.OutgoingSquadInvites.Where(i => i.State.HasFlag(SquadInviteState.Received) && i.State.HasFlag(SquadInviteState.Notified) == false).ToArray();

                //  save as already notified message
                foreach (var notify in outgoingNotifications)
                {
                    notify.State ^= SquadInviteState.Notified;
                }

                return incomingNotifications.Select(i => new FSquadInviteInformation(i))
                    .Concat(outgoingNotifications.Select(i => new FSquadInviteInformation(i, true))).ToArray();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                throw;
            }
        }

        /// <summary>
        /// Ответить на приглашение в отряд
        /// </summary>
        /// <param name="respond">Информация об отряде</param>
        /// <returns></returns>
        [Authorize, HttpPost]
        public IHttpActionResult RespondSqaudInvite(FSquadInviteInformation respond)
        {
            try
            {
                var target = PlayerAccountEntity;
                var invite = target.IncomingSquadInvites.FirstOrDefault(s => s.InviteId == respond.InviteId);
                if (invite == null)
                {
                    throw new RespondSquadInviteException(RespondSquadInviteExceptionCode.InviteNotFound);
                }

                var sender = invite.SenderPlayerEntity;
                if (invite.State != SquadInviteState.Submitted)
                {
                    throw new RespondSquadInviteExceptionWithValue(RespondSquadInviteExceptionCode.InviteAreRecived,
                        invite.PlayerEntity.PlayerName);
                }

                if (respond.State.HasFlag(SquadInviteState.Approved))
                {
                    invite.State = SquadInviteState.Received ^ SquadInviteState.Approved;
                    if (target.QueueEntity == null)
                    {
                        target.QueueEntity = new QueueEntity(target.PlayerId, target.PlayerId, SearchSessionOptions.Default, QueueState.None);
                    }

                    foreach (var inv in target.IncomingSquadInvites.Where(s => s.State == SquadInviteState.Submitted))
                    {
                        inv.State = SquadInviteState.Received ^ SquadInviteState.Rejected;
                    }


					//  Нельзя принять приглашение если игрок уже в отряде
					DataRepository.Entry(target.QueueEntity).Collection(p => p.Members).Load();
                    if (invite.Direction == SquadInviteDirection.Invite)
                    {
                        if (target.QueueEntity.InSquad && target.QueueEntity.PartyId != invite.SquadId)
                        {
                            invite.State = SquadInviteState.Received ^ SquadInviteState.Rejected;
                            throw new RespondSquadInviteExceptionWithValue(RespondSquadInviteExceptionCode.YouAreInSquad, invite.PlayerEntity.PlayerName);
                        }
                    }

					//  Нельзя принять приглашение если отправитель уже в отряде
					DataRepository.Entry(sender.QueueEntity).Collection(p => p.Members).Load();
					if (sender.QueueEntity.InSquad && sender.QueueEntity.IsLeader == false && target.QueueEntity.PartyId != invite.SquadId)
                    {
	                    invite.State = SquadInviteState.Received ^ SquadInviteState.Rejected;
						throw new RespondSquadInviteExceptionWithValue(RespondSquadInviteExceptionCode.SenderInSquad, invite.PlayerEntity.PlayerName);
                    }

                    //============================================================
                    target.QueueEntity.PartyId = invite.SquadId;
                    //DataRepository.QueueEntity.Attach(target.QueueEntity);
                    DataRepository.Entry(target.QueueEntity).Collection(p => p.Members).Load();
                    DataRepository.Entry(invite.SquadEntity).Collection(p => p.Members).Load();

	                //============================================================
	                //  Автоматически запускать поиск боя при принятии приглашения на дуэль или в режим строительства
					if (invite.Options.GameMode.HasFlag(GameModeFlags.DuelMatch))
                    {
                        if (sender.IsAllowPay(invite.Options.Bet) == false)
                        {
	                        invite.State = SquadInviteState.Received ^ SquadInviteState.Rejected;
							throw new RespondSquadInviteException(RespondSquadInviteExceptionCode.SenderNotEnouthMoney);
                        }

                        if (target.IsAllowPay(invite.Options.Bet) == false)
                        {
	                        invite.State = SquadInviteState.Received ^ SquadInviteState.Rejected;
							throw new RespondSquadInviteException(RespondSquadInviteExceptionCode.NotEnouthMoney);
                        }

						//-----------------------------------------------------------
						//	Кикаем других игроков из отряда, если игрок принимает дуэль
	                    var duel = new []
	                    {
		                    invite.SquadId,
							target.PlayerId,
						};

	                    invite.SquadEntity.Members.RemoveAll(p => duel.Contains(p.QueueId) == false);

	                    //-----------------------------------------------------------
						foreach (var member in invite.SquadEntity.Members)
                        {
                            member.Ready = QueueState.Searching;
                            member.Options = invite.Options;
                            member.Options.UniversalId = sender.PlayerId;
                        }
                    }
	                else if (invite.Options.GameMode.HasFlag(GameModeFlags.BuildingMode))
	                {
		                foreach (var member in invite.SquadEntity.Members.Where(p => p.HasCurrentMatchMember == false).ToArray())
		                {
			                member.Ready = QueueState.Searching;
			                member.Options = invite.Options;
			                member.Options.UniversalId = sender.PlayerId;
		                }
	                }
                    else
                    {
                        foreach (var member in invite.SquadEntity.Members)
                        {
                            member.Options = invite.Options;
                            member.Ready = QueueState.None;
                        }
                    }
                }
                else
                {
                    invite.State = SquadInviteState.Received ^ SquadInviteState.Rejected;
                }

                //============================================================
                LoggerContainer.PaymentLogger.Info($"RespondSqaudInvite[0][{PlayerAccountEntity.PlayerName}]: {respond}");
                DataRepository.SaveChanges();

                //============================================================
                respond.PlayerName = invite?.SenderPlayerEntity?.PlayerName;
                respond.Options = invite.Options;

                //============================================================
                LoggerContainer.PaymentLogger.Info($"RespondSqaudInvite[1][{PlayerAccountEntity.PlayerName}]: {respond}");
                return Json(respond);
            }
            catch (RespondSquadInviteExceptionWithValue exception)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage("RespondSqaudInvite[0]", exception);
                return new NegotiatedContentResult<FReqestOneParam<string>>((HttpStatusCode)exception.ResponseCode, exception.Value, this);
            }
            catch (RespondSquadInviteException exception)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage("RespondSqaudInvite[1]", exception);
                return new StatusCodeResult((HttpStatusCode)exception.ResponseCode, this);
            }
            catch (Exception exception)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage("RespondSqaudInvite[2]", exception);
                throw;
            }
        }
    }
}
