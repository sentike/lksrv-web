﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Loka.Common.Store;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;

namespace Loka.Server.Controllers.IdentityController
{
    public partial class IdentityController : AbstractApiController
    {
        [AllowAnonymous, HttpGet]
        public IHttpActionResult GetBitcoinBalance(Guid id)
        {
            using (var db = new DataRepository())
            {
                var satoshi = db.PlayerCashEntities.SingleOrDefault(p => p.PlayerId == id && p.Currency == GameCurrency.Coin)?.Amount ?? 0;
                var dsatoshi = Convert.ToDecimal(satoshi);
                var bitcoin = dsatoshi / 100000000;
                return Ok(bitcoin);
            }
        }

    }
}