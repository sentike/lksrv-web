﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Partner;

namespace Loka.Server.Controllers
{
    public class PartnerController : ApiController
    {
        //public struct IdentityForm
        //{
        //    public string Login { get; set; }
        //    public string Password { get; set; }
        //}

        public struct IdentityResponse
        {
            public Guid PartnerId { get; set; }
            public string Message { get; set; }
            public IdentityResponse(Guid partnerId, string message)
            {
                PartnerId = partnerId;
                Message = message;
            }
        }

        [AllowAnonymous, HttpGet]
        public IHttpActionResult Login(string email/*, string Password*/)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return BadRequest("Incorrect login");
            }

            using (var db = new DataRepository())
            {
                var partner =
                    db.PartnerAccount.FirstOrDefault(
                        p => p.Login.ToLower() == email.ToLower()/* && p.Password == Password*/);

                if (partner == null)
                {
                    return BadRequest("Partner not found");
                }
                return Json(new IdentityResponse(partner.PartnerId, "Ok"));
            }
        }


        [AllowAnonymous, HttpGet]
        public IHttpActionResult RegisterPartner(string email, string link, string chanelName)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return BadRequest("Incorrect login");
            }

            if (string.IsNullOrWhiteSpace(link))
            {
                return BadRequest("Incorrect link");
            }

            if (string.IsNullOrWhiteSpace(chanelName))
            {
                return BadRequest("Incorrect chanel name");
            }

            using (var db = new DataRepository())
            {
                var partner = db.PartnerAccount.SingleOrDefault(p => p.Login.ToLower() == email.ToLower());
                if (partner == null)
                {
                    partner = db.PartnerAccount.Add(new PartnerAccountEntity()
                    {
                        PartnerId = Guid.NewGuid(),
                        Login = email,
                        Password = link,
                        RegistrationDate = DateTime.UtcNow,
                        Name = chanelName,
                        PromoCodeList = new HashSet<PartnerCodeEntity>()
                    {
                        new PartnerCodeEntity()
                        {
                            PromoId = Guid.NewGuid(),
                            Name = chanelName,
                            Validity = DateTime.UtcNow.AddYears(1)
                        }
                    }
                    });
                    db.SaveChanges();
                    return Json(new IdentityResponse(partner.PartnerId, "Ok"));
                }
                else
                {
                    return Json(new IdentityResponse(partner.PartnerId, "Login exist"));
                }
            }
        }

        [AllowAnonymous, HttpGet]
        public IHttpActionResult AddPromoCode(Guid partnerId, string codeName)
        {
            using (var db = new DataRepository())
            {
                var partner = db.PartnerAccount.FirstOrDefault(p => p.PartnerId == partnerId);
                if (partner == null)
                {
                    return BadRequest("Partner not found");
                }

                if (db.PartnerCode.Any(p => p.Name.ToLower() == codeName.ToLower()))
                {
                    return BadRequest("Promo code name exist");
                }

                PartnerCodeEntity code;
                partner.PromoCodeList.Add(code = new PartnerCodeEntity()
                {
                    Name = codeName,
                    PromoId = Guid.NewGuid(),
                    Validity = DateTime.UtcNow.AddYears(1)
                });
                db.SaveChanges();
                return Json(code.PartnerId);
            }
        }

        public struct PromoCodesContainer
        {
            public PromoCodesContainer(PartnerCodeEntity codeEntity)
            {
                CodeId = codeEntity.PromoId;
                Name = codeEntity.Name;
                Validity = codeEntity.Validity;
                using (var db = new DataRepository())
                {
                    Activations = db.PlayerPromoCode.LongCount(p => p.CodeId == codeEntity.PromoId);
                }
            }

            public Guid CodeId { get; set; }
            public string Name { get; set; }
            public DateTime Validity { get; set; }
            public long Activations { get; set; }
        }

        [AllowAnonymous, HttpGet]
        public IHttpActionResult ListOfCodes(Guid partnerId)
        {
            using (var db = new DataRepository())
            {
                var partner = db.PartnerAccount.FirstOrDefault(p => p.PartnerId == partnerId);
                if (partner == null)
                {
                    return BadRequest("Partner not found");
                }

                return Json(partner.PromoCodeList.Select(p => new PromoCodesContainer(p)).ToArray());
            }
        }

        public struct ParnerStatisticContainer
        {
            public ParnerStatisticContainer(PartnerAccountEntity partner)
            {
                RegistrationDate = partner.RegistrationDate;
                PromoCodes = partner.PromoCodeList.Select(p => new PromoCodesContainer(p)).ToArray();
                using (var db = new DataRepository())
                {
                    Referrals = db.AccountPlayerEntity.LongCount(p => p.FirstPartnerId == partner.PartnerId);
                }
            }

            public DateTime RegistrationDate { get; set; }
            public long Referrals { get; set; }
            public IEnumerable<PromoCodesContainer> PromoCodes { get; set; }
        }

        [AllowAnonymous, HttpGet]
        public IHttpActionResult ViewPartnerStatistic(Guid partnerId)
        {
            using (var db = new DataRepository())
            {
                var partner = db.PartnerAccount.FirstOrDefault(p => p.PartnerId == partnerId);
                if (partner == null)
                {
                    return BadRequest("Partner not found");
                }

                return Json(new ParnerStatisticContainer(partner));
            }
        }

        public struct PromoCodeStatisticContainer
        {
            public PromoCodeStatisticContainer(PartnerCodeEntity codeEntity)
            {
                Name = codeEntity.Name;
                Validity = codeEntity.Validity;
                using (var db = new DataRepository())
                {
                    Activations = db.PlayerPromoCode.LongCount(p => p.CodeId == codeEntity.PromoId);
                    Dates =
                        db.PlayerPromoCode.Where(p => p.CodeId == codeEntity.PromoId)
                            .AsNoTracking()
                            .Select(p => p.ActivationDate)
                            .ToArray();
                }
            }

            public string Name { get; set; }
            public DateTime Validity { get; set; }
            public long Activations { get; set; }

            public IEnumerable<DateTime> Dates { get; set; }
        }



        [AllowAnonymous, HttpGet]
        public IHttpActionResult ViewCodeStatistic(Guid partnerId, Guid codeId)
        {
            using (var db = new DataRepository())
            {
                var code = db.PartnerCode.FirstOrDefault(c => c.PartnerId == partnerId && c.PromoId == codeId);
                if (code == null)
                {
                    return BadRequest("Code not found");
                }
                return Json(new PromoCodeStatisticContainer(code));
            }
        }

    }
}