﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Http;
//using Loka.Infrastructure;

//using Loka.Common.Store.Item.Kit;
//using Loka.Common.Store.Item.Weapon;

//namespace Loka.Server.Controllers.StoreController
//{
//    public partial class StoreController
//    {
//        public static readonly string[] NickNames = new[]
//    {
//                "Ollene", "Wettri", "Dikyc", "Gorrak",
//                "Tauilu", "Leeyte", "Kie", "Sieso",
//                "Lylin", "Рика","Irefuk","Nalypu",
//                "Valunawe","Ivafyd","Opofit","Irypim","Mizovic",
//                "Bamysig","Beqitezy","Ydyritoz","Opihidix",
//                "Abuwib","Gyxusuho","Mydavu","Tycufihe",
//                "Hisisole","Uhywol","Udubewa","Fubykel",
//                "Ylobiv","Byfovutu","Tefovana","Tysadiky","Uqihype","Ozujiqa",
//                "Diqude","Zehotyx","Owilu","Wesi","Lyne",
//                "Awopiz","Rijur","Ikunut","Arol","Hajewy",
//                "Fuqavu","Uzof","Teqeco","Veju","Cino",
//                "Ejizuh","Ufapa","Tezoj","Umemi",
//                "Hijude","Itemyw","Arozyq", "Victoria_Lecoeur",
//                "Solene_Dumont", "Sara_Henniger", "Jazmin_Murillo",
//                "Mirela_Ilie", "vlucejz", "Gurn", "Kazilkree", "Mazragore",
//                "Kirn", "Nibei"
//            };


//        //[AllowAnonymous, HttpGet]
//        //public IHttpActionResult AddFakePlayers()
//        //{
//        //    using (var db = new DataRepository())
//        //    {
//        //        if (db.AccountPlayerEntity.LongCount() < 100)
//        //        {
//        //            var random = new Random();
//        //            foreach (var name in NickNames)
//        //            {
//        //                if (db.Users.Any(u => u.UserName.ToLower().Equals(name.ToLower()) == false))
//        //                {
//        //                    var id = Guid.NewGuid();
//        //                    var player = PlayerEntity.Factory(id, name);
//        //                    player.InventoryItemList.Add(new PlayerCharacterEntity(id, CharacterModelId.PeopleMan));
//        //                    player.InventoryItemList.Add(new PlayerWeaponEntity(id, WeaponModelId.LOKA_SunGazer));
//        //                    player.Experience.WeekExperience = random.Next(100, 20000);
//        //                    db.Users.Add(new ApplicationUser()
//        //                    {
//        //                        Id = id,
//        //                        UserName = name,
//        //                        PlayerEntity = player,
//        //                        Email = "fakeplayer",
//        //                        PasswordHash = "fakeplayer"
//        //                    });
//        //                }
//        //            }
//        //
//        //            return Ok(db.SaveChanges());
//        //
//        //        }
//        //    }
//        //    return BadRequest("gfgd");
//        //}

//        [AllowAnonymous, HttpGet]
//        public IHttpActionResult AddNotCreatedAchievement()
//        {
//            using (var db = new DataRepository())
//            {
//                foreach (var model in Enum.GetValues(typeof(AchievementTypeId)).Cast<AchievementTypeId>())
//                {
//                    //if (db.StoreItemInstance.ToList().Any(m => m.ModelId == (byte)model) == false)
//                    {
//                        db.Achievements.Add(new AchievementContainer
//                        {
//                            AchievementTypeId = model,
//                            Achievement = new List<AchievementInstance>()
//                            {
//                                new AchievementInstance()
//                                {
//                                    AchievementTypeId = model,
//                                    Amount = 10,
//                                    BonusPackId = 100
//                                },
//                                new AchievementInstance()
//                                {
//                                    AchievementTypeId = model,
//                                    Amount = 20,
//                                    BonusPackId = 250
//                                },
//                                new AchievementInstance()
//                                {
//                                    AchievementTypeId = model,
//                                    Amount = 100,
//                                    BonusPackId = 1200
//                                },
//                                new AchievementInstance()
//                                {
//                                    AchievementTypeId = model,
//                                    Amount = 500,
//                                    BonusPackId = 2000
//                                },
//                            }
//                        });
//                    }
//                }

//                return Ok(db.SaveChanges());
//            }
//        }




//        [
//            AllowAnonymous, HttpGet]
//        public IHttpActionResult AddNotCreatedWeapon()
//        {
//            using (var db = new DataRepository())
//            {
//                foreach (var model in Enum.GetValues(typeof(WeaponModelId)).Cast<WeaponModelId>())
//                {
//                    if (db.StoreItemInstance.ToList().Any(m => m.ModelId == (byte)model) == false)
//                    {
//                        db.StoreItemInstance.Add(new WeaponItemInstance()
//                        {
//                            ModelId = model,
//                            CategoryTypeId = CategoryTypeId.Weapon,
//                            Level = 1,
//                            Cost = new StoreItemCost() { Amount = 1000, IsDonate = false },
//                            FractionId = FractionTypeId.Keepers,
//                            DefaultSkinId = 0,
//                            IsAvalible = false
//                        });
//                    }
//                }
//                return Ok(db.SaveChanges());
//            }
//        }


//        [AllowAnonymous, HttpGet]
//        public IHttpActionResult AddNotCreatedKits()
//        {
//            using (var db = new DataRepository())
//            {
//                //foreach (var model in Enum.GetValues(typeof(KitModelId)).Cast<KitModelId>())
//                //{
//                //    if (db.StoreItemInstance.ToList().Any(m => m.ModelId == (byte)model && m.CategoryTypeId == CategoryTypeId.Kit) == false)
//                //    {
//                //        db.StoreItemInstance.Add(new KitItemInstance()
//                //        {
//                //            ModelId = model,
//                //            CategoryTypeId = CategoryTypeId.Weapon,
//                //            Level = 1,
//                //            Cost = new StoreItemCost() { Amount = 1000, IsDonate = false },
//                //            FractionId = FractionTypeId.Keepers,
//                //            DefaultSkinId = 0,
//                //            IsAvalible = true
//                //        });
//                //    }
//                //}


//                foreach (var model in Enum.GetValues(typeof(GrenadeModelId)).Cast<GrenadeModelId>())
//                {
//                    if (db.StoreItemInstance.ToList().Any(m => m.ModelId == (byte)model && m.CategoryTypeId == CategoryTypeId.Grenade) == false)
//                    {
//                        db.StoreItemInstance.Add(new GrenadeItemInstance()
//                        {
//                            ModelId = model,
//                            CategoryTypeId = CategoryTypeId.Grenade,
//                            Level = 1,
//                            Cost = new StoreItemCost() { Amount = 1000, IsDonate = false },
//                            FractionId = FractionTypeId.Keepers,
//                            DefaultSkinId = 0,
//                            IsAvalible = true
//                        });
//                    }
//                }
//                return Ok(db.SaveChanges());
//            }
//        }

//    }
//}