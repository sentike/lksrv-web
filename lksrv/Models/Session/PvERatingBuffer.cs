﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Antlr.Runtime.Misc;
using Loka.Common.Match.GameMode;
using Loka.Common.Player.Statistic.PvE;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;

namespace Loka.Server.Models.Session
{
    public class PvERatingBuffer : AbstractTemporyBuffer<IReadOnlyCollection<PlayerPvEMatchStatisticView>>
    {
        public static readonly PvERatingBuffer Instance = new PvERatingBuffer(100);

        public PvERatingBuffer(int updateIntevalInSeconds)
            : base(updateIntevalInSeconds)
        {

        }

        const int NumberOfLevels = 10;
        const int NumberOfPlayersForView = 3;
        protected override IReadOnlyCollection<PlayerPvEMatchStatisticView> Update()
        {
            using (var db = new DataRepository())
            {
                List<PlayerPvEMatchStatisticView> result = new List<PlayerPvEMatchStatisticView>(NumberOfLevels * NumberOfPlayersForView);
                var ratingForGameMode = db.PvEMatchStatisticEntities.Where(m => m.GameModeTypeId == GameModeTypeId.PVE_Waves);
                for (int i = 0; i < NumberOfLevels; i++)
                {
                    var i1 = i;
                    var membersPerLevel = ratingForGameMode.Where(m => m.Level == i1).OrderByDescending(m => m.Score).Take(NumberOfPlayersForView);
                    result.AddRange(membersPerLevel.Select(m => new PlayerPvEMatchStatisticView
                    {
                        PlayerId = m.PlayerId,
                        PlayerName = m.PlayerEntity.PlayerName,
                        Score = m.Score,
                        Level = m.Level     
                    }));
                }

                return Object = result;
            }
        }
    }
}