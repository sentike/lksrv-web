﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Player.Models;
using WebGrease.Css.Extensions;

namespace Loka.Server.Models.Session
{
    public class PlayerRatingBuffer : AbstractTemporyBuffer<PlayerRatingTopContainer[]>
    {
        public static readonly PlayerRatingBuffer Instance = new PlayerRatingBuffer(100);

        public PlayerRatingBuffer(int updateIntevalInSeconds)
            : base(updateIntevalInSeconds)
        {

        }

        protected override PlayerRatingTopContainer[] Update()
        {
            using (var db = new DataRepository())
            {
                var onlineTime = DateTime.UtcNow.AddSeconds(-30);
                Object = db.AccountPlayerEntity.Where(p => p.Experience.WeekExperience > 0).AsEnumerable()
                    .Select
                    (
                        p => new PlayerRatingTopContainer()
                        {
                            Id = p.PlayerId,
                            Name = p.PlayerName,
                            Score = p.Experience.WeekExperience,
                            Level = p.Experience.Level,
                            IsOnline = p.IsOnline
                        }
                    ).OrderByDescending(p => p.Score)
                    .ToArray();
                return Object;
            }
        }
    }
}