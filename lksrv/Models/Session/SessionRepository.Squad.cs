﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Queue;
using Loka.Server.Player.Models;
using Ninject;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.Queue;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Player.Infrastructure;

namespace Loka.Server.Session
{
    public partial class SessionRepository 
    {
        public SqaudInforamtion GetSqaudInforamtion(PlayerEntity player)
        {
            //Logger.Error($"QueueEntity is null: {playerAccountEntity.QueueEntity == null}");

            if (player.QueueEntity == null)
            {
                player.QueueEntity = new QueueEntity(player.PlayerId, player.PlayerId, SearchSessionOptions.Default);
                player.QueueEntity.PartyEntity = player.QueueEntity;
                player.QueueEntity.PartyEntity.PlayerEntity = player;
                player.QueueEntity.Ready = QueueState.None;
            }

            if (player.QueueEntity.PartyEntity.PlayerEntity.LastActivityDate.ToElapsedSeconds() >
                TimeSpan.FromMinutes(30).TotalSeconds)
            {
                LeaveFromSquad(player);
            }

            player.QueueEntity.LastActivityDate = DateTime.UtcNow.AddSeconds(20);
            return new SqaudInforamtion(player.QueueEntity);
        }

        public void LeaveFromSquad(PlayerEntity target)
        {
            foreach (var member in target.QueueEntity.Members)
            {
                member.SetDefaultSquad();
            }

            target.QueueEntity.SetDefaultSquad();
        }

        public FReqestOneParam<string> RemoveFromSquad(PlayerEntity leader, Guid? target)
        {
            using (var db = new DataRepository())
            {
                if (target == null || target == Guid.Empty)
                {
                    target = leader.PlayerId;
                }

                var member = db.QueueEntity.FirstOrDefault(m => m.QueueId == target);
                if (member == null)
                {
                    throw new RemoveFromSquadException(RemoveFromSquadExceptionCode.PlayerNotFound);
                }

                if (member.PartyId != leader.QueueEntity.PartyId)
                {
                    throw new RemoveFromSquadException(RemoveFromSquadExceptionCode.PlayerNotSquad);
                }

                if (member.QueueId != leader.PlayerId && leader.QueueEntity.IsLeader == false)
                {
                    throw new RemoveFromSquadException(RemoveFromSquadExceptionCode.YouAreNotLeader);
                }

                member.SetDefaultSquad();
                db.SaveChanges();

                return new FReqestOneParam<string>(member.PlayerEntity.PlayerName);
            }
        }
    }
}