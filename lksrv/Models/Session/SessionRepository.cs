﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using Loka.Common.Cluster;
using Loka.Common.Match.GameMode;
using Loka.Common.Player.Profile.Slot;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Models.Queue;
using Loka.Server.Player.Models;
using Ninject;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.Queue;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Models.Session;
using VRS.Infrastructure;


namespace Loka.Server.Session
{
    public partial class SessionRepository 
    {
        protected static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public void JoinToQueue(PlayerEntity user, SearchSessionOptions options)
        {
            using (var db = new DataRepository())
            {
                try
                {
                    //  if even one set does not have a primary weapon
                    if (user.InventoryProfileList.Any(p => p.Items.Any(i => i.SlotId == PlayerProfileInstanceSlotId.PrimaryWeapon) == false))
                    {
                        throw new SessionSearchBeginException(SessionSearchBeginExceptionCode.NotHavePrimaryWeapon);
                    }

                    //------------------------------------------------------------------------------
                    //  if the player does not have enabled set
                    if (user.InventoryProfileList.Any( /*p => p.IsEnabled*/) == false)
                    {
                        throw new SessionSearchBeginException(SessionSearchBeginExceptionCode.NotHaveEnabledProfiles);
                    }
            
                    var queueEntity = db.QueueEntity.SingleOrDefault(q => q.QueueId == user.PlayerId);
                    if (queueEntity == null)
                    {
                        queueEntity = db.QueueEntity.Add(new QueueEntity(user.PlayerId, user.PlayerId, options));
                        LoggerContainer.SessionLogger.Trace($"[JoinToQueue] {user.PlayerName} added to queue {queueEntity.QueueId}");
                    }
                    else
                    {
                        if (queueEntity.IsLeader && queueEntity.IsMembersReady == false)
                        {
                            queueEntity.Ready = QueueState.WaitingPlayers;
                            var waiting = queueEntity.PartyEntity.Members.Where(p =>
                                p.Ready.HasFlag(QueueState.WaitingPlayers) || p.Ready.HasFlag(QueueState.Searching)).ToArray();

                            LoggerContainer.MatchMakingLogger.Trace($"[JoinToQueue] | waiting: {waiting.Length} | {string.Join(",", waiting.Select(p => p.PlayerEntity.PlayerName).ToArray())}");


                            foreach (var member in waiting)
                            {
                                member.Options = options;
                                member.JoinDate = DateTime.UtcNow;
                            }

                            throw new SessionSearchBeginException(SessionSearchBeginExceptionCode.MembersNotReady);
                        }

	                    if (queueEntity.InSquad && queueEntity.IsLeader == false)
	                    {
	                        queueEntity.JoinDate = DateTime.UtcNow;
                            queueEntity.Ready = QueueState.Searching;
                            queueEntity.Options = queueEntity.PartyEntity.Options;

	                    }
	                    else
	                    {
		                    foreach (var member in queueEntity.PartyEntity.Members)
		                    {
			                    member.Options = options;

			                    member.JoinDate = DateTime.UtcNow;
			                    member.Ready = QueueState.Searching;
		                    }
						}
                    }
                    db.SaveChanges();
                }
                catch (DbUpdateException e)
                {
                    LoggerContainer.SessionLogger.WriteExceptionMessage($"[JoinToQueue][0][{user?.PlayerId}][{user?.PlayerName}][{options}]", e);
                    throw new SessionSearchBeginException(SessionSearchBeginExceptionCode.ErrorOnDataBase);
                }
                catch (DbEntityValidationException e)
                {
                    LoggerContainer.SessionLogger.WriteExceptionMessage($"[JoinToQueue][1][{user?.PlayerId}][{user?.PlayerName}][{options}]", e);
                    throw new SessionSearchBeginException(SessionSearchBeginExceptionCode.ErrorOnDataBase);
                }
                catch (NotSupportedException e)
                {
                    LoggerContainer.SessionLogger.WriteExceptionMessage($"[JoinToQueue][2][{user?.PlayerId}][{user?.PlayerName}][{options}]", e);
                    throw new SessionSearchBeginException(SessionSearchBeginExceptionCode.ErrorOnDataBase);
                }
                catch (ObjectDisposedException e)
                {
                    LoggerContainer.SessionLogger.WriteExceptionMessage($"[JoinToQueue][3][{user?.PlayerId}][{user?.PlayerName}][{options}]", e);
                    throw new SessionSearchBeginException(SessionSearchBeginExceptionCode.ErrorOnDataBase);
                }
                catch (InvalidOperationException e)
                {
                    LoggerContainer.SessionLogger.WriteExceptionMessage($"[JoinToQueue][4][{user?.PlayerId}][{user?.PlayerName}][{options}]", e);
                    throw new SessionSearchBeginException(SessionSearchBeginExceptionCode.ErrorOnDataBase);
                }
            }
        }

        public void LeaveFromQueue(PlayerEntity playerAccountEntity)
        {
            if(playerAccountEntity.QueueEntity == null)
                throw new ArgumentNullException(nameof(playerAccountEntity.QueueEntity));

            //if (playerAccountEntity.QueueEntity?.MatchEntity?.NodeEntity.State == NodeInstanceState.PrepareGame && playerAccountEntity.QueueEntity?.MatchEntity.Created.ToElapsedSeconds() < 10)
            //    return;

            LoggerContainer.SessionLogger.Trace($"{playerAccountEntity.PlayerName} removed queue {playerAccountEntity.QueueEntity?.QueueId}");

            //  The player is not ready to play
            playerAccountEntity.QueueEntity.Ready = QueueState.None;
        }

        public static int PlayerRating(Guid playerId)
        {
            var player = PlayerRatingBuffer.Instance.CkeckUpdate().FirstOrDefault(p => p.Id == playerId);
            return Array.IndexOf(PlayerRatingBuffer.Instance.Object, player);
        }

        public override string ToString()
        {
            using (var db = new DataRepository())
            {
                var activeMatches = db.MatchEntity.Count();
                var membersInQueue = db.QueueEntity.Count(m => m.Ready.HasFlag(QueueState.Searching));
                return $"Active matches: {activeMatches} | in queue: {membersInQueue} players";
            }
        }
    }
}