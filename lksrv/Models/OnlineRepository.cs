﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Store;
using Loka.Server.Player.Models;
using VRS.Infrastructure;

namespace Loka.Server.Models
{
    public class PlayerOnline
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public long Money { get; set; }
        public long Crystals { get; set; }
        public long Bitcoin { get; set; }

        public long Wins { get; set; }
        public DateTime LastActivityDate { get; set; }
    }

    public class OnlineRepository
    {
        private object Sync { get; } = new object();

        public static OnlineRepository Instance { get; } = new OnlineRepository();

        public Dictionary<Guid, PlayerOnline> Players { get; } = new Dictionary<Guid, PlayerOnline>(1000);

        public void AddOrUpdate(PlayerEntity entity)
        {
            try
            {
                lock (Sync)
                {
                    PlayerOnline player;

                    //---------------------------------------------
                    if (Players.TryGetValue(entity.PlayerId, out player) == false)
                    {
                        Players.Add(entity.PlayerId, player = new PlayerOnline());
                    }

                    //---------------------------------------------
                    player.Id = entity.PlayerId;
                    player.Name = entity.PlayerName;
                    player.Bitcoin = entity.TakeCash(GameCurrency.Coin).Amount;
                    player.Money = entity.TakeCash(GameCurrency.Money).Amount;
                    player.Crystals = entity.TakeCash(GameCurrency.Donate).Amount;
                    player.LastActivityDate = DateTime.UtcNow;
                }
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"OnlineRepository:AddOrUpdate:{entity?.PlayerName}", e);
            }

        }
    }
}