﻿using System;
using Loka.Server.Chat;
using Loka.Server.League.Infrastructure;
using Loka.Server.Player;
using Loka.Server.Player.Infrastructure;
using Loka.Server.Session;
using Ninject;
using Ninject.Extensions.NamedScope;
using Ninject.Web.Common;

namespace Loka.Server.Infrastructure.DataRepository
{
    public static class DAL
    {
        public static void RegisterServices(IKernel container)
        {
            container.Bind<PlayerRepository>().ToSelf();
            container.Bind<SessionRepository>().ToSelf();
            container.Bind<LeagueRepository>().ToSelf();
            container.Bind<DataRepository>().ToSelf();

        }
    }
}
