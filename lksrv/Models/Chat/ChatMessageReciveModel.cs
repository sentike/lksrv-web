﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Chat.Chanel;
using Loka.Infrastructure;

namespace Loka.Server.Models.Chat
{
    public class ChatMessageReciveModel
    {
        public ChatMessageReciveModel() { }
        public ChatMessageReciveModel(ChatChanelMessageEntity entity)
        {
            ChanelId = entity.ChanelId;
            MessageId = entity.MessageId;
            Message = entity.Message;

            MemberId = entity.MemberId;
            PlayerName = entity.MemberEntity.PlayerEntity.PlayerName;
            PlayerId = entity.MemberEntity.PlayerId;


            CreatedDate = entity.CreatedDate.ToUnixTime();
        }

        public Guid ChanelId { get; set; }
        public Guid MessageId { get; set; }
        public Guid MemberId { get; set; }
        public Guid PlayerId { get; set; }
        public string PlayerName { get; set; }

        public long CreatedDate { get; set; }
        public string Message { get; set; }
    }
}