﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loka.Server.Models.Chat
{
    public class ChatMessageSendModel
    {
        public Guid ChanelId { get; set; }
        public string Message { get; set; }
    }
}