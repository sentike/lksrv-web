﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loka.Server.Models.Store.Request
{
    public class OrderDeliveryRequest
    {
        public Guid OderId { get; set; }
        public Guid AccountId { get; set; }
        public List<OrderItemDeliveryRequest> Items { get; set; } = new List<OrderItemDeliveryRequest>();
    }
}