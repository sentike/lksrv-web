﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Store;

namespace Loka.Server.Models.Store.Request
{
    public struct InGameProductPair
    {
        public int ModelId { get; set; }
        public long Amount { get; set; }
        public CategoryTypeId ModelCategoryId { get; set; }

    }

    public class OrderItemDeliveryRequest
    {
        public Guid EntityId { get; set; }
        public InGameProductPair Item { get; set; }
    }
}