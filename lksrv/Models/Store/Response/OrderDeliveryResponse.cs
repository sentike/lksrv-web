﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Store.Order;
using Loka.Server.Models.Store.Request;

namespace Loka.Server.Models.Store.Response
{


    public class OrderDeliveryResponse
    {
        public OrderDeliveryResponse(OrderDeliveryRequest request)
        {
            OderId = request.OderId;
            AccountId = request.AccountId;
            Status = OrderDeliveryStatus.DeliveredSuccessfully;
        }

        public Guid OderId { get; set; }
        public Guid AccountId { get; set; }
        public string Message { get; set; }
        public OrderDeliveryStatus Status { get; set; }
        public ICollection<OrderItemDeliveryResponse> Items { get; set; } = new List<OrderItemDeliveryResponse>();

        public OrderItemDeliveryResponse AddItem(Guid entity, int model, OrderItemDeliveryStatus status)
        {
            OrderItemDeliveryResponse r;
            Items.Add(r = new OrderItemDeliveryResponse(entity, model, status));
            return r;
        }
    }
}