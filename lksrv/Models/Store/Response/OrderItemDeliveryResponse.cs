﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Store.Order;

namespace Loka.Server.Models.Store.Response
{


    public class OrderItemDeliveryResponse
    {
        public OrderItemDeliveryResponse(Guid entity, int model, OrderItemDeliveryStatus status)
        {
            EntityId = entity;
            Status = status;
            ModelId = model;
        }

        public Guid EntityId { get; set; }
        public int ModelId { get; set; }
        public Guid? InvoiceId { get; set; }
        public string Message { get; set; }
        public OrderItemDeliveryStatus Status { get; set; }
    }
}