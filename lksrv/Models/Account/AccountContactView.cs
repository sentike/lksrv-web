﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;

namespace Loka.Server.Models.Account
{
    public class AccountContactView
    {
        public string Value { get; set; }
        public bool IsConfirmed { get; set; }
        //public long LastChangeDate { get; set; }

        public AccountContactView()
        {
            
        }

        public AccountContactView(string value, bool confirmed)
        {
            long steamId = 0;
            if (long.TryParse(value, out steamId))
            {
                Value = String.Empty;
                IsConfirmed = false;
            }
            else
            {
                Value = value;
                IsConfirmed = value.IsNullOrWhiteSpace() == false;
            }
            IsConfirmed = IsConfirmed && confirmed;
        }


    }
}