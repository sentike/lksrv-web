﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Threading;
using System.Timers;
using Loka.Common.Player.Statistic;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;

using Loka.Infrastructure;
using Loka.Server.Infrastructure;
using Loka.Server.Infrastructure.Migration;



using Loka.Server.Chat;
using Ninject;

using VRS.Infrastructure;
using VRS.Infrastructure.Environment;


namespace Loka.Server.Player.Infrastructure
{
    public class ClientPrincipal : GenericPrincipal, IDisposable
    {
        public PlayerGameSessionEntity PlayerGameSessionEntity { get; }
        public DataRepository Db { get; }

        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public DateTime LastMoneyGenerate => PlayerGameSessionEntity.LastMoneyGenerate;

        public static ClientPrincipal Factory(Guid sessionId)
        {
            var db = new DataRepository();
            {
                var gameSession = db.GameSessionEntity.SingleOrDefault(s => s.SessionId == sessionId);
                var playerInstance = db.AccountPlayerEntity.FirstOrDefault(p => p.PlayerId == gameSession.PlayerId);
                return new ClientPrincipal(db, gameSession, playerInstance);
            }
        }

        public PlayerEntity PlayerInstance { get; protected set; }
        public Guid SessionId => PlayerGameSessionEntity?.SessionId ?? Guid.Empty;
        public Guid AccountId { get; protected set; }
        public LanguageTypeId Language { get; set; }
        private ClientPrincipal
            (
                DataRepository db, 
                PlayerGameSessionEntity gameSession, 
                PlayerEntity playerEntity
            ) : base(new GenericIdentity(gameSession.PlayerId.ToString()), null)
        {
            Db = db;
            Language = playerEntity?.Language ?? LanguageTypeId.en;
            AccountId = gameSession.PlayerId;
            PlayerGameSessionEntity = gameSession;
            PlayerInstance = playerEntity;
            gameSession.LastActivityDate = DateTime.UtcNow.AddSeconds(20);
            if (playerEntity != null)
            {
                playerEntity.LastActivityDate = DateTime.UtcNow.AddSeconds(20);
            }
        }

        public void Dispose()
        {
            try
            {
                using (Db)
                {
                    Db.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                Logger.Error($"======== Master: {PlayerInstance?.PlayerId} {PlayerInstance?.PlayerName} {e} ==========");
                foreach (var ve in e.EntityValidationErrors)
                {
                    foreach (var error in ve.ValidationErrors)
                    {
                        Logger.Error($"++ {error.PropertyName} = {error.ErrorMessage}");
                    }
                }
                Logger.Error("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                throw;
            }
            catch (Exception e)
            {
                Logger.Error($"Master: {PlayerInstance?.PlayerId} {PlayerInstance?.PlayerName} {e}");
                throw;
            }            
        }
    }

    public class PlayerRepository 
    {
        //public Dictionary<string, PlayerGameSessionEntity> SessionInstanceDictionary { get; } = new Dictionary<string, PlayerGameSessionEntity>(1000 * 1000);
        protected readonly object SyncRoot = new object();

        public Guid SessionBegin(Guid userId)
        {
            using (var db = new DataRepository())
            {
                lock (SyncRoot)
                {
                    var response = db.GameSessionEntity.Add(new PlayerGameSessionEntity(userId)).SessionId;
                    db.SaveChanges();
                    return response;
                }
            }
        }

        public bool SessionComplete(Guid userId)
        {
            using (var db = new DataRepository())
            {
                db.GameSessionEntity.RemoveRange(db.GameSessionEntity.Where(p => p.PlayerId == userId));
                db.SaveChanges();
                return true;
            }
        }

        public bool SessionIsExistToken(Guid sessionToken)
        {
            using (var db = new DataRepository())
            {
                return db.GameSessionEntity.Any(s => s.SessionId == sessionToken);
            }
        }

        public bool SessionIsExistEntity(Guid userId)
        {
            using (var db = new DataRepository())
            {
                return db.GameSessionEntity.Any(s => s.PlayerId == userId);
            }
        }

        public bool SessionIsOnline(Guid userId)
        {
            using (var db = new DataRepository())
            {
                var sessionEntity = db.GameSessionEntity
                    .Where(p => p.PlayerId == userId)
                    .OrderByDescending(p => p.LastActivityDate)
                    .FirstOrDefault();
                return sessionEntity?.LastActivityDate.ToSecondsTime() > 10;
            }
        }

        public void UpdateUserRoles(Guid id, string[] roles, out PlayerRole role )
        {
            role = PlayerRole.None;
            try
            {
                using (var db = new DataRepository())
                {
                    var player = db.AccountPlayerEntity.SingleOrDefault(p => p.PlayerId == id);
                    if (player != null)
                    {
                        UpdateUserRoles(player, roles, out role);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[UpdateUserRoles][2][{id}]", e);
            }
        }

        public void UpdateUserRoles(PlayerEntity player, string[] roles, out PlayerRole role)
        {
            role = PlayerRole.None;

            try
            {
                if (roles?.Any() ?? false)
                {
                    foreach (var r in roles)
                    {
                        PlayerRole tmp = PlayerRole.None;
                        if (Enum.TryParse<PlayerRole>(r, true, out tmp))
                        {
                            role ^= tmp;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[UpdateUserRoles][1][{player?.PlayerName} / {player?.PlayerId}]", e);
            }

            if (player != null)
            {
                player.Role = role;
            }
        }

        public bool CreateGameAccount(string login, Guid accountId, string[] roles, out PlayerRole role)
        {
            role = PlayerRole.None;

            try
            {
                using (var db = new DataRepository())
                {
                    login = login.Replace("\\", "/");
                    login = login.Replace("\"", "\\\"");
                    login = login.Trim();

                    var playerEntity = PlayerEntity.Factory(accountId, login);
                    db.AccountPlayerEntity.Add(playerEntity);

                    //  Unlock characters & profiles
                    {
                        Exception exception = null;
                        if (AccountMigrationTool.Migration.Single(m => m.MigrationId == AccountMigrationAction.AddNotExistProfilesAndCharacters).Migrate(db, playerEntity, out exception) == false)
                        {
                            LoggerContainer.AccountLogger.WriteExceptionMessage($"[CreateGameAccount][{login} / {accountId}][Migration error]", exception);
                        }
                    }

                    UpdateUserRoles(playerEntity, roles, out role);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception exception)
            {
                LoggerContainer.AccountLogger.WriteExceptionMessage($"[CreateGameAccount][{login} / {accountId}]", exception);
                return false;
            }
        }

        public bool IsExistLogin(string login)
        {
            using (var db = new DataRepository())
            {
                lock (SyncRoot)
                { 
                    return db.AccountPlayerEntity.AsNoTracking().Any(p => p.PlayerName.ToLower().Equals(login.ToLower()));
                }
            }
        }

        public bool IsExistIndex(Guid accountId)
        {
            using (var db = new DataRepository())
            {
                lock (SyncRoot)
                {
                    return db.AccountPlayerEntity.AsNoTracking().Any(p => p.PlayerId == accountId);
                }
            }
        }
        
        public string CreateToken(string playerLogin)
        {
            //var Salt1 = "VRS_PRO_LOKA";
            //var Salt2 = playerId.ToString();
            //var Salted = Salt1 + playerLogin + Salt2;
            return CMath.GetHash(Sha256, playerLogin);
        }

        public GenericPrincipal GetClientPrincipal(string index, Guid token)
        {
            if (SessionIsExistToken(token))
            {
                return ClientPrincipal.Factory(token);
            }
            return null;
        }


        private static readonly SHA256 Sha256 = SHA256.Create();
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
    }
}
