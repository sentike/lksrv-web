﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Server.Models.Tournament
{
    public static class TournamentSettings
    {
        public static readonly DateTime TournamentStartDate = DateTime.Parse("2016-11-25T00:00:00+00:00");
        public static readonly DateTime TournamentEndDate = DateTime.Parse("2016-12-25T00:00:00+00:00");
        public static readonly long TournamentAvalibleGameSeconds = Convert.ToInt64(new TimeSpan(60, 0, 0).TotalSeconds);
        public const long BalastTournamentTimeInSeconds = 300;
    }
}
