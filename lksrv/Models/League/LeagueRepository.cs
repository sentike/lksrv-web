﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loka.Server.Infrastructure.DataRepository;
using Loka.Server.League.Models;

namespace Loka.Server.League.Infrastructure
{
    public interface ILeagueRepository : ILeagueRepository<LeagueEntity> { }

    public class LeagueRepository : ILeagueRepository
    {
        public DataRepository DataRepository { get; set; }

        public LeagueRepository(DataRepository repository)
        {
            DataRepository = repository;
            LeagueDictionary = repository.LeagueEntity.ToDictionary(l => l.LeagueId, l => l);
        }

        public IReadOnlyList<LeagueEntity> LeagueList => LeagueDictionary.Values.ToList().AsReadOnly();
        public Dictionary<Guid, LeagueEntity> LeagueDictionary { get; }

        public bool IsValidName(string name, LeagueNameCompare compare)
        {
            throw new NotImplementedException();
        }

        public bool IsExist(string name, LeagueNameCompare compare)
        {
            throw new NotImplementedException();
        }

        public bool IsExist(Guid leagueId)
        {
            throw new NotImplementedException();
        }

        public LeagueEntity FindBy(string name, LeagueNameCompare compare)
        {
            throw new NotImplementedException();
        }

        public LeagueEntity FindBy(Guid leagueId)
        {
            throw new NotImplementedException();
        }

        public LeagueEntity Create(object corporation, Guid playerId)
        {
            throw new NotImplementedException();
        }

        public bool Insert(LeagueEntity leagueInstance)
        {
            throw new NotImplementedException();
        }

        public bool Join(LeagueEntity leagueInstance, Guid playerId)
        {
            throw new NotImplementedException();
        }

        public bool Join(Guid leagueId, Guid playerId)
        {
            throw new NotImplementedException();
        }

        public bool Edit(LeagueEntity leagueInstance, object request)
        {
            throw new NotImplementedException();
        }

        public bool Edit(Guid leagueId, object request)
        {
            throw new NotImplementedException();
        }
    }
}
