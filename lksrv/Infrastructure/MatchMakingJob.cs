using System;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using Loka.Common.Cluster;
using Loka.Common.Match.GameMode;
using Loka.Infrastructure;
using VRS.Infrastructure;

namespace Loka.Server.Infrastructure
{
    public class MatchMakingJob : IJob
    {
        protected static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public static GameModeTypeId LastGameMode { get; set; } = GameModeTypeId.None;

        public Task Execute(IJobExecutionContext context)
        {
            LoggerContainer.MatchMakingLogger.Info("============ OnMatchMakingProcess v4.1 =====================\n");

            //if (Environment.MachineName.Equals("WIN-MasterSrv", StringComparison.OrdinalIgnoreCase) == false)
            //{
            //    Logger.Info($"OnMatchMakingProcess abort, MachineName: {Environment.MachineName} != WIN-MasterSrv\n");
            //    return Task.CompletedTask;
            //}

            try
            {
                using (var db = new DataRepository.DataRepository())
                {
                    var gm = db.GameModeEntity.Shuffle().FirstOrDefault(p => p.GameModeId != LastGameMode);
                    if (gm != null)
                    {
                        try
                        {
                            LastGameMode = gm.GameModeId;
                            gm.Tick();
                        }
                        catch (Exception e)
                        {
                            LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"OnMatchMakingProcess[{gm.GameModeFlag}][0]", e);
                        }

                    }

                    foreach (var mode in db.GameModeEntity.Where(p => p.GameModeId != LastGameMode).Shuffle().ToArray())
                    {
                        try
                        {
                            mode.Tick();
                        }
                        catch (Exception e)
                        {
                            LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"OnMatchMakingProcess[{mode.GameModeFlag}][1]", e);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LoggerContainer.MatchMakingLogger.WriteExceptionMessage($"OnMatchMakingProcess[2]", e);
            }

            return Task.CompletedTask;
        }
    }
}