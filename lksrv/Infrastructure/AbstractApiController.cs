﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Loka.Common.Player.Statistic;
using Loka.Infrastructure;
using Loka.Server.Models;
using Loka.Server.Player.Infrastructure;
using Loka.Server.Player.Models;
using VRS.Infrastructure.Environment;
using VRS.Infrastructure.IpAddress;



namespace Loka.Server.Infrastructure
{
    public abstract class AbstractApiController : ApiController
    {
        public const string ExternalServiceUrl = "http://localhost:80";
        public static Uri ServiceUrl(string service) => new Uri($"{ExternalServiceUrl}/{service}");


        public IHttpActionResult DataResponse<TData>(int code, TData data) 
            where TData : class
        {
            return new NegotiatedContentResult<TData>((HttpStatusCode)code, data, this);
        }

        public IHttpActionResult DataResponse(int code, int data)
        {
            return DataResponse(code, new FReqestOneParam<int>(data));
        }

        public IHttpActionResult DataResponse(int code, long data)
        {
            return DataResponse(code, new FReqestOneParam<long>(data));
        }

        public IHttpActionResult DataResponse(int code, string data)
        {
            return DataResponse(code, new FReqestOneParam<string>(data));
        }

        public IHttpActionResult DataResponse(int code, Guid data)
        {
            return DataResponse(code, new FReqestOneParam<Guid>(data));
        }


        protected override void Dispose(bool disposing)
        {
            User?.Dispose();
            base.Dispose(disposing);
        }

        protected static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        protected LanguageTypeId UserLanguage => User?.Language ?? LanguageTypeId.en;

        protected static string GetIpAddress(HttpContext context)
        {
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ipAddress))
            {
                if (string.IsNullOrEmpty(context.Request.ServerVariables["REMOTE_ADDR"]))
                {
                    return context.Request.UserHostAddress;
                }
            }
            else
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length == 0 || string.IsNullOrEmpty(addresses[0]))
                {
                    if (string.IsNullOrEmpty(context.Request.ServerVariables["REMOTE_ADDR"]))
                    {
                        return context.Request.UserHostAddress;
                    }
                }
                else
                {
                    return addresses[0];

                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        protected CountryTypeId UserCountry
        {
            get
            {
                CountryTypeId countryTypeId = CountryTypeId.None;
                try
                {
                    IPAddress address;
                    if (HttpContext.Current == null)
                    {
                        Logger.Error("IpAddressExtensions: HttpContext was nullptr");
                    }
                    else
                    {
                        if (IPAddress.TryParse(GetIpAddress(HttpContext.Current), out address))
                        {
                            countryTypeId = address.CountryId();
                        }
                        else
                        {
                            Logger.Error($"IpAddressExtensions: UserHostAddress[{GetIpAddress(HttpContext.Current)}] was bad IPAddress");
                        }  
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error($"IpAddressExtensions: {GetIpAddress(HttpContext.Current)} | {exception}");
                }
                return countryTypeId;
            }
        }

        protected static PlayerGameSessionEntity GameSessionProperty => User?.PlayerGameSessionEntity;
        protected new static ClientPrincipal User => HttpContext.Current.User as ClientPrincipal;
        protected static PlayerEntity PlayerAccountEntity => User?.PlayerInstance;
        protected static DataRepository.DataRepository DataRepository => User?.Db;

        public static string UserName => User?.PlayerInstance?.PlayerName ?? String.Empty;
        protected static Guid UserId
        {
            get
            {
                if (User == null)
                    return Guid.Empty;
                return User.AccountId;
            }
        }
    }
}