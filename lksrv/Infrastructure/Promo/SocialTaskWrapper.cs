﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Infrastructure;
using Loka.Server.Models.Promo;

namespace Loka.Server.Infrastructure.Promo
{

    public struct SocialTaskWrapper
    {
        public Guid TaskId { get; set; }
        public string UrlAddress { get; set; }
        public long EndDate { get; set; }
        public SocialGroupId SocialGroupId { get; set; }

        public SocialTaskWrapper(SocialTaskEntity task)
        {
            TaskId = task.TaskId;
            UrlAddress = task.UrlAddress;
            EndDate = task.EndDate.ToUnixTime();
            SocialGroupId = task.SocialGroupId;
        }
    }

}
