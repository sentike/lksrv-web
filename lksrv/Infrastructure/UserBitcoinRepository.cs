﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Loka.Common.Store;
using Loka.Server.Player.Models;
using StackExchange.Redis;
using VRS.Infrastructure;

namespace Loka.Server.Infrastructure
{
	public class UserBitcoinRepository
	{
		public static void Update(PlayerEntity player)
		{
			try
			{
				using (var redis = ConnectionMultiplexer.Connect("localhost, abortConnect=false"))
				{
					var satoshi = player.TakeCash(GameCurrency.Coin).Amount;
					var dsatoshi = Convert.ToDecimal(satoshi);
					var bitcoin = dsatoshi / 100000000;
					redis.GetDatabase(1).StringSet(player.PlayerId.ToString(), bitcoin.ToString(CultureInfo.InvariantCulture));
				}
			}
			catch (Exception e)
			{
				LoggerContainer.StoreLogger.WriteExceptionMessage($"[UserBitcoinRepository][{player?.PlayerName}]", e);
			}
		}
	}
}