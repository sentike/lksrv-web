﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Infrastructure;

namespace Loka.Server.Infrastructure
{
    public abstract class AbstractTemporyBuffer<TObject> 
        where TObject : class
    {
        public int UpdateIntevalInSeconds { get; protected set; }
        public DateTime UpdateDate { get; protected set; }
        public TObject Object { get; protected set; }

        protected AbstractTemporyBuffer(int updateIntevalInSeconds)
        {
            UpdateIntevalInSeconds = updateIntevalInSeconds;
        }


        public virtual TObject CkeckUpdate()
        {
            if (Object == null || UpdateDate.ToElapsedSeconds() > UpdateIntevalInSeconds)
            {
                UpdateDate = DateTime.UtcNow;
                return Update();
            }

            return Object;
        }

        protected abstract TObject Update();
    }
}