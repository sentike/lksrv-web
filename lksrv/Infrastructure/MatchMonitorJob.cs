﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Cluster;
using Loka.Infrastructure;
using Quartz;
using Quartz.Impl;
using VRS.Infrastructure;

namespace Loka.Server.Infrastructure
{
    public class MatchMonitorJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                ClusterInstance.OnTick();
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage("MatchMakingJob", e);
            }
            return Task.CompletedTask;
        }
    }
}
