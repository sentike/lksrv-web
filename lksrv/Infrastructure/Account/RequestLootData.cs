﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loka.Common.Player.Inventory.Modification;
using Loka.Common.Store;
using Loka.Infrastructure;
using Loka.Player;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;
using Loka.Common.Store.Property;


namespace Loka.Server.Controllers.IdentityController.Operation
{
    public abstract class RequestLootData
    {
        public class AddonContainer
        {
            public readonly Guid EntityId;
            public readonly short ModelId;
            public readonly CategoryTypeId CategoryTypeId;

            public AddonContainer(AbstractTargetPlayerItem item)
            {
                EntityId = item.ItemId;
                ModelId = item.ModelId;
                CategoryTypeId = item.CategoryTypeId;
            }
        }

        public class InstalledAddonContainer
        {
            public readonly Guid AddonId;
            public readonly EquipAddonSlot AddonSlot;

            public InstalledAddonContainer(PlayerAddonAttachedEntity item)
            {
                AddonId = item.AddonId;
                AddonSlot = item.AddonSlot;
            }
        }


        public class FInvertoryItemWrapper
        {
            public Guid EntityId { get; set; }
            public short ModelId { get; set; }
            public CategoryTypeId CategoryTypeId { get; set; }
            public IEnumerable<PlayerItemModificationEntity> Modifications { get; set; }
            public IEnumerable<AddonContainer> AvalibleAddons { get; set; }
            public IEnumerable<InstalledAddonContainer> InstalledAddons { get; set; }

            public long Count { get; set; }

            public FInvertoryItemWrapper(PlayerInventoryItemEntity entity)
            {
                EntityId = entity.ItemId;
                ModelId = entity.ModelId;
                CategoryTypeId = entity.CategoryTypeId;
                Modifications = entity.ModificationList.ToArray();
				AvalibleAddons = entity.AvalibleAddonList.Select(s => new AddonContainer(s)).ToArray();
                InstalledAddons = entity.InstalledAddons.Select(s => new InstalledAddonContainer(s)).ToArray();
                Count = entity.Amount;
            }
        }

        public class Response
        {
            public readonly List<FInvertoryItemWrapper> GrenadeList = new List<FInvertoryItemWrapper>(8);
            public readonly List<FInvertoryItemWrapper> KitList = new List<FInvertoryItemWrapper>(8);
            public readonly List<FInvertoryItemWrapper> WeaponList = new List<FInvertoryItemWrapper>(8);
            public readonly List<FInvertoryItemWrapper> ArmourList = new List<FInvertoryItemWrapper>(16);
            public readonly List<FInvertoryItemWrapper> CharacterList = new List<FInvertoryItemWrapper>(2);
            public readonly List<FInvertoryItemWrapper> ServiceList = new List<FInvertoryItemWrapper>(2);
            public readonly List<FInvertoryItemWrapper> BuildingList = new List<FInvertoryItemWrapper>(2);

            public Response(PlayerEntity entity, bool isOnlyUsed = false)
            {
                if (isOnlyUsed == false)
                {
                    WeaponList = entity.InventoryWeaponList.Select(i => new FInvertoryItemWrapper(i)).ToList();
                    ArmourList = entity.InventoryArmourList.Select(i => new FInvertoryItemWrapper(i)).ToList();
                    CharacterList = entity.CharacterList.Select(i => new FInvertoryItemWrapper(i)).ToList();
                    GrenadeList = entity.GrenadeList.Select(i => new FInvertoryItemWrapper(i)).ToList();
                    KitList = entity.KitList.Select(i => new FInvertoryItemWrapper(i)).ToList();
                    ServiceList = entity.ServiceList.Select(i => new FInvertoryItemWrapper(i)).ToList();
                    BuildingList = entity.BuildingList.Select(i => new FInvertoryItemWrapper(i)).ToList();
                }
                else
                {
                    foreach (var p in entity.GetProfileContainers()/*.Where(p => p.IsEnabled)*/)
                    {
                        KitList.AddRange(entity.KitList.Where(a => p.ItemIds.Contains(a.ItemId)).Select(i => new FInvertoryItemWrapper(i)));
                        GrenadeList.AddRange(entity.GrenadeList.Where(a => p.ItemIds.Contains(a.ItemId)).Select(i => new FInvertoryItemWrapper(i)));

                        WeaponList.AddRange(entity.InventoryWeaponList.Where(a => p.ItemIds.Contains(a.ItemId)).Select(i => new FInvertoryItemWrapper(i)));
                        ArmourList.AddRange(entity.InventoryArmourList.Where(a => p.ItemIds.Contains(a.ItemId)).Select(i => new FInvertoryItemWrapper(i)));
                        CharacterList.AddRange(entity.CharacterList.Where(a => a.ItemId == p.CharacterId).Select(i => new FInvertoryItemWrapper(i)));
                    }
                }
            }
        }
    }
}