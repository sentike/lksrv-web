﻿using Loka.Infrastructure;

namespace Loka.Server.Controllers.IdentityController.Operation
{
    public class CreateGameAccount
    {
        public class Request : FReqestOneParam<string>
        {
            public Request(string playerName) : base(playerName)
            {
            }
        }

        public class Response : FReqestOneParam<bool>
        {
            public Response(bool v) : base(v)
            {
            }
        }
    }
}
