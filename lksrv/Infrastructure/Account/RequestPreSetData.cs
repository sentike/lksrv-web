﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loka.Player;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;


namespace Loka.Server.Controllers.IdentityController.Operation
{
    

    public abstract class RequestPreSetData
    {
        public static IEnumerable<PlayerProfileEntityContainer> Response(PlayerEntity entity)
        {
            return entity.GetProfileContainers();
        }
    }
}