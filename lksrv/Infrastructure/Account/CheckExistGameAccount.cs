﻿using Loka.Infrastructure;


namespace Loka.Server.Controllers.IdentityController.Operation
{
    public abstract class CheckExistGameAccount
    {
        public class Response : FReqestOneParam<bool>
        {
            public Response(bool v) : base(v)
            {
            }
        }
    }
}
