﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Loka.Common.Achievements;
using Loka.Common.Player.Achievements;
using Loka.Common.Player.Fraction;
using Loka.Common.Player.Instance;
using Loka.Common.Player.Inventory.Service;
using Loka.Common.Player.Statistic;
using Loka.Common.Player.Statistic.Match;
using Loka.Common.Player.Statistic.PvE;
using Loka.Common.Store;
using Loka.Common.Store.Fraction;
using Loka.Common.Store.Service;
using Loka.Common.Tutorial;
using Loka.Infrastructure;
using Loka.Player;
using Loka.Server.Models.Account;
using Loka.Server.Player.Models;
using Loka.Server.Session;


namespace Loka.Server.Controllers.IdentityController.Operation
{
    public struct PlayerCash
    {
        public GameCurrency Currency { get; set; }
        public long Amount { get; set; }

        public PlayerCash(PlayerCashEntity entity)
        {
            Currency = entity.Currency;
            Amount = entity.Amount;
        }
    }

    public class PlayerTutorialView
    {
        public PlayerTutorialView(PlayerTutorialEntity entity)
        {
            Id = entity.ProgressId;
            TutorialId = entity.TutorialId;
            StepId = entity.StepEntity.StepId;
            TutorialComplete = entity.Complete;
        }

        public Guid Id { get; set; }
        public long TutorialId { get; set; }
        public long StepId { get; set; }
        public bool TutorialComplete { get; set; }
    }

    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public abstract class RequestClientData
    {
        public struct ExperienceContainer
        {
            public ExperienceContainer(PlayerExperience experience)
            {
                Level = experience.Level;
                LastLevel = experience.Level;
                Experience = experience.Experience;
                NextExperience = experience.NextExperience;
                SkillPoints = 0;
            }

            public readonly int Level;
            public readonly int LastLevel;
            public readonly long Experience;
            public readonly long NextExperience;
            public readonly int SkillPoints;     
        }

        public struct PlayeAchievementContainer
        {
            public readonly AchievementTypeId Id;
            public readonly int Amount;

            public PlayeAchievementContainer(PlayerAchievementContainerEntity playerAchievementContainer)
            {
                Id = playerAchievementContainer.AchievementTypeId;
                Amount = playerAchievementContainer.Amount;
            }
        }

        public struct WeekBonusContainer
        {
            public readonly int Stage;
            public readonly bool Notify;
            public WeekBonusContainer(PlayerWeekBonus weekBonus)
            {
                Stage = weekBonus.JoinCount;
                if ((DateTime.UtcNow - weekBonus.NotifyDate) > PlayerWeekBonus.NotifyPeriod)
                {
                    weekBonus.NotifyDate = DateTime.UtcNow;
                    Notify = true;
                }
                else
                {
                    Notify = false;
                }
            }
        }

        public struct RaitingContainer
        {
            public int Position;
            public long Experience;
            public long ConductedTime;
        }


        public struct Response
        {
            public Response(PlayerEntity entity)
            {
                PlayerId = entity.PlayerId;
                Login = entity.PlayerName;
                PlayerRole = entity.Role;

                Experience = new ExperienceContainer(entity.Experience);
                Cash = entity.CashEntities.Select(p => new PlayerCash(p)).ToArray();

                LeagueId = entity.LeagueMemberEntity?.LeagueId;
                FractionTypeId = entity.CurrentFractionEntity?.FractionId ?? FractionTypeId.End;
                Statistic = new PlayerStatisticView(entity.Statistic);
                DateOfNextAvailableProposal = entity.DateOfNextAvailableProposal();

                DefenceExpiryDate = entity.LastAttackedDate.ToUnixTime();
                Email = new AccountContactView("unk@unk", true);
                Phone = new AccountContactView("unk@unk", true);

                IsSquadLeader = entity.IsSquadLeader;

                foreach (var a in entity.AchievementList.Where(a => a.Amount > 0).ToArray())
                {
                    a.AchievementEntity.TryGiveArchivement(a);
                }

                TutorialId = entity.CurrentTutorialId;
                HoverBoardId = entity.CurrentHoverBoardItemId;
                IsAllowVoteImprovement = entity.IsAllowVoteImprovement;
                Tutorials = entity.PlayerTutorialEntities.Select(p => new PlayerTutorialView(p)).ToArray();
                UsingService = entity.UsingServiceEntities.Select(p => new PlayerUsingServiceView(p)).ToList();
                PvEPlayerRating = entity.PvEMatchStatisticEntities.Select(m => new PlayerPvEMatchStatisticView(m));
                Archievements = entity.AchievementList.Select(a => new PlayeAchievementContainer(a)).ToArray();
                FractionExperience = entity.ReputationList.Select(f => new PlayerFractionReputationView(f)).ToArray();
                WeekBonus = new WeekBonusContainer(entity.WeekBonus);
                Raiting = new RaitingContainer
                {
                    Position = SessionRepository.PlayerRating(entity.PlayerId),
                    Experience = entity.Experience.WeekExperience,
                    ConductedTime = entity.Experience.WeekActivityTime,
                };
            }

            public Guid? TutorialId { get; set; }
            public Guid? HoverBoardId { get; set; }
            public PlayerRole PlayerRole { get; set; }

            public readonly AccountContactView Phone;
            public readonly AccountContactView Email;
            public readonly PlayerStatisticView Statistic;
            public readonly RaitingContainer Raiting;
            public readonly WeekBonusContainer WeekBonus;
            public readonly Guid PlayerId;
            public readonly string Login;
            public readonly ExperienceContainer Experience;
            public readonly PlayerCash[] Cash;
            public readonly FractionTypeId FractionTypeId;
            public readonly IEnumerable<PlayerFractionReputationView> FractionExperience;
            public readonly IEnumerable<PlayeAchievementContainer> Archievements;
            public readonly IEnumerable<PlayerPvEMatchStatisticView> PvEPlayerRating;
            public readonly IEnumerable<PlayerTutorialView> Tutorials;
            public readonly List<PlayerUsingServiceView> UsingService;
            public readonly Guid? LeagueId;
            public readonly bool IsSquadLeader;
            public readonly bool IsAllowVoteImprovement;
            public readonly long DateOfNextAvailableProposal;
            public readonly long DefenceExpiryDate;

        }
    }

    public class PlayerUsingServiceView
    {
        public PlayerUsingServiceView(PlayerUsingServiceEntity entity)
        {
            NotifyDate = entity.NotifyDate.ToUnixTime();
            ExpirationDate = entity.ExpirationDate.ToUnixTime();
            ServiceTypeId = entity.ServiceTypeId;

            //======================================================
            if (entity.ExpirationDate < DateTime.UtcNow)
            {
                entity.NotifyDate = DateTime.UtcNow;
            }
        }

        public ServiceTypeId ServiceTypeId { get; set; }
        public long ExpirationDate { get; set; }
        public long NotifyDate { get; set; }
    }

}
