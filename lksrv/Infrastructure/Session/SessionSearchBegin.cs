﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loka.Server.Infrastructure.Session
{
    public enum SessionSearchBeginExceptionCode
    {
        NotHavePrimaryWeapon = 900,
        NotHaveEnabledProfiles,
        GameModeIsNotAvailable,
        MembersNotReady,
        SmallPvEMatchLevel,
        SmallPlayerLevel,
        ErrorOnDataBase
    }

    public class SessionSearchBeginException : AbstractControllerException<SessionSearchBeginExceptionCode>
    {
        public SessionSearchBeginException(SessionSearchBeginExceptionCode responseCode) : base(responseCode)
        {
        }
    }
}
