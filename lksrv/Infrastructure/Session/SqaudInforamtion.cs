﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Common.Match.GameMode;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.Session;
using Loka.Server.Models.Queue;
using VRS.Infrastructure;

namespace Loka.Server.Infrastructure.Queue
{
    public enum ESearchSessionStatus
    {
        None,

        //	Идет поиск матча
        MatchSearch,

        //	Матч найдет, вход в игру
        FoundJoin,

        //	Матч найден, продолжение матча
        FoundContinue,

        //	Матч найден, подготовка ко входу
        FoundPrepare,

        //  Матч найден, но произошла ошибка
        FoundError,

        //	Матч найден, но матч завершен
        Finished,

        //  Недостаточно денег для боя
        NotEnouthMoney,

        //===================================
        //	Только на клиенте

        //	Состояние кнопки, когда игрок начал поиск
        SearchStarting,

        //	Состояние кнопки, когда игрок отменил поиск
        SearchStopping,

        //  Состояние кнопки, когда ожидаются игроки
        SearchPrepare,

        //===================================
        End
    };

    [SuppressMessage("ReSharper", "ConvertIfStatementToConditionalTernaryExpression")]
    public class SqaudInforamtion
    {
        public Guid LeaderId { get; set; }
        public Guid? ServerId { get; set; }

        public string Host { get; set; }
        public Guid Token { get; set; }
        public string Message { get; set; }

        public long ElapsedSeconds { get; set; }
        public ESearchSessionStatus Status { get; set; }
        public SearchSessionOptions Options { get; set; }

        public QueueMemberContainer[] Members { get; set; }

        public bool IsDefence { get; set; }
        public bool IsLeader { get; set; }
        public bool IsReady { get; set; }

        public SqaudInforamtion(QueueEntity queue)
        {
            var party = queue.PartyEntity;
            var playername = queue.PlayerEntity?.PlayerName;

            //===============================================================================
            var member = queue.CurrentMatchMember;

            //===============================================================================
            ElapsedSeconds = party.JoinDate.ToUnixTime();
            Members = party.Members.Select(m => new QueueMemberContainer(m)).ToArray();

            //===============================================================================
            ServerId = member?.MatchId;
            LeaderId = queue.QueueId;
            IsLeader = queue.IsLeader;
            IsReady  = queue.Ready.HasFlag(QueueState.Searching);

            //===============================================================================
            var match = member?.MatchEntity;

            //===============================================================================
            if (match == null)
            {
                Options = party.Options;
            }
            else
            {
                Options = match.Options;
            }

            //===============================================================================
            var bMatch = match != null;
            var bmember = member != null;

            LoggerContainer.SessionLogger.Info($"[SqaudInforamtion][{playername}][0][match: [{bMatch}][member:{bmember}][PartyReady: {party.PartyReady}]");
            if (match == null)
            {
                if (party.Ready == QueueState.NotEnouthMoney)
                {
                    Status = ESearchSessionStatus.NotEnouthMoney;
                }
                else if (party.Ready.HasFlag(QueueState.WaitingPlayers))
                {
                    if (party.PartyReady)
                    {
                        Status = ESearchSessionStatus.MatchSearch;
                    }
                    else
                    {
                        Status = ESearchSessionStatus.SearchPrepare;
                    }
                }
                else if (party.PartyReady)
                {
                    Status = ESearchSessionStatus.MatchSearch;
                }
            }
            else
            {
                //=====================================
                UpdateDefenceStatus(party);
                       
                //=====================================
                if (match.IsMbFinished || member.State == MatchMemberState.Deserted)
                {
                    if (party.Ready.HasFlag(QueueState.Searching))
                    {
                        Status = ESearchSessionStatus.MatchSearch;
                    }
                }
                else if(match.IsMatchInStarting)
                {
                    //	Матч найден, подготовка ко входу
                    Status = ESearchSessionStatus.FoundPrepare;
                }
                //=====================================
                else if (match.State == MatchGameState.Started)
                {
                    if (member.State == MatchMemberState.Created)
                    {
                        //	Матч найден, подготовка ко входу
                        Status = ESearchSessionStatus.FoundPrepare;
                    }
                    else
                    {
                        Token = member.MemberId;
                        Host = match.HostAddress.ToString();

                        if (member.State == MatchMemberState.Started)
                        {
                            //	Матч найден, продолжение матча
                            Status = ESearchSessionStatus.FoundContinue;
                        }
                        else if (member.State == MatchMemberState.Finished)
                        {       
                            //	Матч найден, но матч завершен
                            Status = ESearchSessionStatus.Finished;
                        }
                        else
                        {
                            //	Матч найдет, вход в игру
                            Status = ESearchSessionStatus.FoundJoin;
                        }
                    }
                }
                else if (match.IsFailedStart)
                {
                    Status = ESearchSessionStatus.Finished;
                }
            }
        }

        [SuppressMessage("ReSharper", "ConvertIfStatementToConditionalTernaryExpression")]
        void UpdateDefenceStatus(QueueEntity party)
        {
            try
            {
                MatchMemberEntity[] members;

                var member = party.PartyEntity.CurrentMatchMember;
                var match = party.PartyEntity.CurrentMatchMember?.MatchEntity;

                //==========================================================
                if (match == null || member == null)
                {
                    return;
                }

                //==========================================================
                //  Получаем информацию о нападающем / защитнике из PartyEntity, а не из QueueMember.
                var team = member.TeamId;
                IsDefence = match.Options.UniversalId == member.TeamId;

                //==========================================================
                // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
                if (IsDefence)
                {
                    members = match.MemberList.Where(p => p.TeamId != team).ToArray();
                }
                else
                {
                    members = match.MemberList.Where(p => p.TeamId == team).ToArray();
                }

                //==========================================================
                var names = members.Select(p => p.PlayerEntity.PlayerName);
                Message = string.Join(", ", names);
            }
            catch (Exception e)
            {
                LoggerContainer.SessionLogger.WriteExceptionMessage($"[UpdateDefenceStatus][2][{party.PlayerEntity?.PlayerName}]", e);
            }
        }

        public override string ToString()
        {
            return $"{Options.GameMode} {Status} members: {Members.Length}";
        }
    }
}
