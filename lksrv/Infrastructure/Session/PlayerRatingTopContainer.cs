﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Server.Player.Models;

namespace Loka.Server.Infrastructure.Session
{
    public struct PlayerRatingTopContainer
    {
        public Guid Id;
        public string Name;
        public long Score;
        public int Level;
        public int ArmourLevel;
        public bool IsOnline;

        public PlayerRatingTopContainer(PlayerEntity playerEntity)
        {
            Id = playerEntity.PlayerId;
            Name = playerEntity.PlayerName;
            ArmourLevel = playerEntity.ArmourLevel;
            Score = playerEntity.Experience.WeekExperience;
            Level = playerEntity.Experience.Level;
            IsOnline = playerEntity.IsOnline;
        }
    }
}
