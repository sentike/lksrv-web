﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loka.Server.Infrastructure.Session
{
    public enum RemoveFromSquadExceptionCode
    {
        PlayerNotFound = 700,
        PlayerNotSquad,
        YouAreNotLeader,

        ErrorOnDataBase
    }

    public class RemoveFromSquadException : AbstractControllerException<RemoveFromSquadExceptionCode>
    {
        public RemoveFromSquadException(RemoveFromSquadExceptionCode responseCode) : base(responseCode)
        {
        }
    }
}