﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loka.Infrastructure;

namespace Loka.Server.Infrastructure.Session
{
    public enum SendSquadInviteExceptionCode
    {
        InviteYourself = 700,
        InviteAlreadySent,
        PlayerNotFound,
        PlayerInSquad,
        PlayerNotEnouthMoney,
        PlayerNotOnline,
        YouAreNotLeader,
        YouAreInSquad,
        InviteTimeoutSent,

        YouAreNotEnouthMoney,

        ErrorOnDataBase
    }

    public class SendSquadInviteException : AbstractControllerException<SendSquadInviteExceptionCode>
    {
        public SendSquadInviteException(SendSquadInviteExceptionCode responseCode) : base(responseCode)
        {
        }
    }


    public class SendSquadInviteExceptionWithValue : AbstractControllerException<SendSquadInviteExceptionCode, string>
    {
        public SendSquadInviteExceptionWithValue(SendSquadInviteExceptionCode responseCode, string value) : base(responseCode, value)
        {
        }
    }

}
