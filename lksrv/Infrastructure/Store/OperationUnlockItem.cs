﻿using System;
using Loka.Common.Store;
using Loka.Server.Player.Models;
using Loka.Common.Player.Inventory;
using Loka.Infrastructure;
using Loka.Server.Controllers.IdentityController.Operation;


namespace Loka.Server.Controllers.StoreController.Operation
{
    public class FUnlockItemResponseContainer : RequestLootData.FInvertoryItemWrapper
    {
        public long AmountRequest { get; set; }
        public long AmountBought { get; set; }

        public Guid TargetId { get; set; }
        public string TargetName { get; set; }

        public FUnlockItemResponseContainer(PlayerInventoryItemEntity item, long request, long bought)
            : base(item)
        {
            TargetId = item.PlayerId;
            TargetName = item.PlayerInstance?.PlayerName;

            AmountRequest = request;
            AmountBought = bought;
        }
    }

    public sealed class OperationUnlockItem
    {
        public sealed class Request
        {
            public int ItemId { get; set; }
            public Guid? Target { get; set; }
            public CategoryTypeId ItemType { get; set; }
            public byte Amount { get; set; }

            public override string ToString()
            {
                return $"ItemId: {ItemId} | CategoryTypeId : {ItemType}";
            }
        }
    }
}
