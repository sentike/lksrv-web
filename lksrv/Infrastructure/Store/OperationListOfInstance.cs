﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Loka.Common.Achievements;
using Loka.Common.Achievements.Container;
using Loka.Common.Achievements.Instance;
using Loka.Common.Company.Mission;
using Loka.Common.Store;
using Loka.Common.Store.Abstract;
using Loka.Common.Store.Fraction;
using Loka.Common.Tutorial;
using Loka.Infrastructure;
using Loka.Server.Infrastructure.Store;
using Loka.Server.Models.Tournament;




namespace Loka.Server.Controllers.StoreController.Operation
{
    public sealed class OperationListOfInstance
    {
        public struct AchievementContainer
        {
            public struct AchievementInstance
            {
                public readonly int Amount;
                public readonly StoreItemCost Reward;

                public AchievementInstance(AchievementInstanceEntity instance)
                {
                    Amount = instance.Amount;
                    Reward = new StoreItemCost {Amount = instance.BonusPackId, Currency = GameCurrency.Money};
                }
            }

            public readonly AchievementTypeId Id;
            public readonly IEnumerable<AchievementInstance> Instances;
            public AchievementContainer(AchievementContainerEntity achievementContainer)
            {
                Id = achievementContainer.AchievementTypeId;
                Instances = achievementContainer.Achievement.Select(a => new AchievementInstance(a)).ToArray();
            }
        }

        public struct TournamentContainer
        {
            public long Start;
            public long End;
        }

        public sealed class Response
        {

            public const byte ConstCostOfImprovementVote = 0;
            public const int ConstCostOfSentencePush = 0;

            public readonly byte CostOfImprovementVote = ConstCostOfImprovementVote;
            public readonly int CostOfSentencePush = ConstCostOfSentencePush;
            public IEnumerable<FractionContainer> Fraction { get; set; }
            public TournamentContainer Tournament { get; set; }
            public IEnumerable<ItemBaseContainer> Items { get; set; }
            public IEnumerable<AchievementContainer> Achievements { get; set; }
            public IEnumerable<TutorialShortView> Tutorials { get; set; }

            //public IEnumerable<GameModePropertyContainer> GameModeProperties { get; set; }
            public IEnumerable<ScenarioMissionView> ScenarioMissions { get; set; }
            //public long BuildDateTime { get; set; }
        }
    }
}
