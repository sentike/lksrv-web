﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Store;
using Loka.Server.Player.Models;

namespace Loka.Server.Service.Player
{
    public class UpdateUserNameFromRemoteRequest
    {
        public Guid EntityId { get; set; }
        public string Name { get; set; }
        //public string Secret { get; set; }

        //public const string Key = "SeNTike";
    }

    public class UpdatePlayerStatisticModelRequest
    {
        public Guid EntityId { get; set; }

        public int Level { get; set; }
        public short Reputation { get; set; }

        public long Money { get; set; }
        public long Donate { get; set; }
        public long Bitcoin { get; set; }
    }

    public static class PlayerNotifyService
    {
        public static void UpdatePlayerStatistic(PlayerEntity entity)
        {
            new UpdatePlayerStatisticModelRequest
            {
                EntityId = entity.PlayerId,
                Level = entity.Experience.Level,
                Reputation = entity.ReputationList.Max(p => p.CurrentRank),
                Money = entity.TakeCash(GameCurrency.Money).Amount,
                Donate = entity.TakeCash(GameCurrency.Donate).Amount,
                Bitcoin = entity.TakeCash(GameCurrency.Coin).Amount,
            }.SendNotify("Player/UpdatePlayer");
        }

        public static void UpdateUserName(Guid id, string name)
        {
            new UpdateUserNameFromRemoteRequest
            {
                EntityId = id,
                Name = name,
                //Secret = UpdateUserNameFromRemoteRequest.Key
            }.SendNotify("Account/UpdateUserNameFromRemote");
        }
    }
}