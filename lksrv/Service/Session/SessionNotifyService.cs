﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Player.Statistic;
using Loka.Common.Store;
using Loka.Server.Player.Models;

namespace Loka.Server.Service.Session
{
    public class NewLoginSessionModel
    {
        public Guid EntityId { get; set; }
        public Guid AccountId { get; set; }
        public string ClientIpAddress { get; set; }
    }

    public class UpdateLoginSessionModel
    {
        public Guid EntityId { get; set; }
        public DateTime LastActivityDate { get; set; }
    }



    public class NewMatchMakingSessionModel
    {
        public enum MatchMakingSessionState
        {
            Idle,
            SearchGame,
            PlayGame
        }

        public Guid AccountId { get; set; }
        public Guid SessionId { get; set; }
        public MatchMakingSessionState SessionState { get; set; }

        //==================================
        public long Money { get; set; }
        public long Donate { get; set; }
        public long Bitcoin { get; set; }
        //public short Difficulty { get; set; }
    }

    public static class SessionNotifyService
    {
        public static void AddMatchMakingSession(PlayerEntity entity, NewMatchMakingSessionModel.MatchMakingSessionState state)
        {
            new NewMatchMakingSessionModel
            {
                AccountId = entity.PlayerId,
                SessionId = entity.LastSessionId,
                SessionState = state,
                Donate = entity.TakeCash(GameCurrency.Donate).Amount,
                Bitcoin = entity.TakeCash(GameCurrency.Coin).Amount,
                Money = entity.TakeCash(GameCurrency.Money).Amount,
            }.SendNotify("MatchMaking/AddOrUpdate");
        }

        public static void AddPlayerSession(PlayerGameSessionEntity entity)
        {
            new NewLoginSessionModel
            {
                EntityId = entity.SessionId,
                AccountId = entity.PlayerId,
                ClientIpAddress = "127.0.0.1"
            }.SendNotify("Session/AddNewSession");
        }

        public static void UpdatePlayerSession(PlayerGameSessionEntity entity)
        {
            new UpdateLoginSessionModel
            {
                EntityId = entity.SessionId,
                LastActivityDate = DateTime.UtcNow
            }.SendNotify("Session/UpdateSession");
        }
    }
}