﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Loka.Common.Match.Match;
using Loka.Common.Match.Member;
using Loka.Server.Infrastructure;
using RestSharp;
using VRS.Infrastructure;

namespace Loka.Server.Service.Battle
{
    public class NewGameMatchMemberModel
    {
        public Guid EntityId { get; set; }
        public Guid TeamId { get; set; }
        public Guid PlayerId { get; set; }
        public Guid SessionId { get; set; }
        public MatchMemberState MemberState { get; set; }

        //===============================================
        //  Награда за бой
        public long Money { get; set; }
        public long Donate { get; set; }
        public long Bitcoin { get; set; }

        //===============================================
        //  Игровой прогресс
        public short FractionId { get; set; }
        public long Reputation { get; set; }
        public long Experience { get; set; }

        //===============================================
        //  Статистика
        public long Score { get; set; }
        public long Kills { get; set; }
        public long Deads { get; set; }
        public long Assists { get; set; }
    }

    public class NewGameMatchTeamModel
    {
        public Guid EntityId { get; set; }
        public short Score { get; set; }
        public bool? IsWinnerTeam { get; set; }
    }

    public class NewGameMatchModel 
    {
        public Guid EntityId { get; set; }
        //===============================================
        public Guid BuildVersion { get; set; }
        public int MachineAddress { get; set; }

        //===============================================
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public int RoundTime { get; set; }

        //===============================================
        public int GameModeId { get; set; }
        public int GameMapId { get; set; }

        //===============================================
        public ICollection<NewGameMatchMemberModel> Members { get; set; } = new List<NewGameMatchMemberModel>();
        public ICollection<NewGameMatchTeamModel> Teams { get; set; } = new List<NewGameMatchTeamModel>();
    }

    public static class BattleNotifyService
    {


        public static void PublishBattleProgress(MatchEntity entity)
        {
            try
            {
                //----------------------------------------------------------
                new NewGameMatchModel
                {
                    EntityId = entity.MatchId,
                    GameModeId = (int) entity.Options.GameMode,
                    GameMapId = (int) entity.Options.GameMap,
                    FinishDate = entity.Finished ?? DateTime.UtcNow,
                    StartDate = entity.StartedOfMatch ?? entity.Created,
                    MachineAddress = BitConverter.ToInt32(entity.IpAddress.GetAddressBytes(), 0),
                    RoundTime = entity.Options.RoundTimeInSeconds,
                    Teams = entity.TeamList.Select(p => new NewGameMatchTeamModel
                    {
                        EntityId = p.TeamId,
                        Score = p.Score,
                        IsWinnerTeam = p.TeamId == entity.WinnerTeamId
                    }).ToArray(),

                    Members = entity.MemberList.Select(p => new NewGameMatchMemberModel
                    {
						EntityId = p.MemberId,
						PlayerId = p.PlayerId,
						SessionId = p.PlayerEntity.LastSessionId,
						TeamId = p.TeamId,
						Donate = p.Award.Donate,
						Bitcoin = p.Award.Bitcoin,
						Score = p.Statistic.Score,
						Money = p.Award.Money,
						Experience = p.Award.Experience,
						Assists = p.Statistic.Assists,
						Kills = p.Statistic.Kills,
						Deads = p.Statistic.Deads,
						FractionId = (short)p.Award.FractionId,
						Reputation = p.Award.Reputation,
						MemberState = p.State,
                    }).ToArray()
                }.SendNotify("Statistic/Matches/AddNewMatch");
            }
            catch (Exception e)
            {
                LoggerContainer.ClusterLogger.WriteExceptionMessage($"[CompleteMatch][10-http] [{entity.MatchId}]", e);
            }
        }
    }
}